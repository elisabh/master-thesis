
\chapter{Conclusion and Recommendations for Further Work}
\label{conclusion}

\section{Conclusion}

In this thesis, we have focused on developing a stable and efficient topology optimization method for determining the shape design of unsteady two-dimensional biomedical flows. Using the numerical method presented in this thesis, the optimal shape with respect to an arbitrary objective functional of for instance a bypass can be found, given no information about the initial shape. 

To model the fluid flow, the unsteady incompressible Navier-Stokes equations have been combined with Darcy's equation. The inclusion of the Darcy term makes it possible to determine the impermeability in the domain material, distinguishing solid from fluid. The Navier-Stokes-Darcy (NSD) system has been discretized and solved using a finite element approach with the numerical software FEniCS \cite{fenics} and an incremental pressure correction scheme (IPCS), see Chapter \ref{chapter_discretization}. 

A gradient-based topology optimization algorithm proposed in \cite{sa2016tda} has been successfully implemented. The algorithm is based on a level set method that determines the impermeability in every cell, and is updated using the topological derivative. For the NSD system, the topological derivative associated with the energy dissipation functional has been derived in Section \ref{topological_derivative_dissipation} and used as descent direction in the gradient-based algorithm. Knowledge of the adjoint solution for the NSD system is necessary in order to calculate the topological derivative. The adjoint equations for the NSD system have been derived and solved using a finite element method and a coupled solver. The implementation details can be found in Appendix \ref{adjoint_solver}. 

Deriving the adjoint model and topological derivative can be a very difficult and time consuming task. Using the functionality of dolfin-adjoint \cite{farrell2013ada}, we have been able to investigate other functionals than the energy dissipation functional in a fast and easy way. In few lines of code, we have computed the topolgical gradient numerically, based on the forward solution of the Navier-Stokes-Darcy system. The core of the implementation using dolfin-adjoint is listed in Appendix \ref{dolfin-adjoint}.

The IPCS solver has been verified numerically on Taylor-Green flow and a problem with manufactured solution in Chapter \ref{numerical_verification}, showing convergence of 2nd order in space and 1st order in time. For high values of the kinematic viscosity in problems with Dirichlet boundary conditions, an erroneous boundary layer was discovered. This was diminished by lowering the time step size sufficiently. The gradient computed using dolfin-adjoint has been compared to the topolgical derivative, showing some dissimilarities, but a convergence in space of order 1/2. The optimization algorithm has been applied and compared to examples in other publications on topology optimization for unsteady flow \cite{kreissl2011tou, deng2011tou}, showing similar results. 


The optimization algorithm has been applied to a coronary artery bypass anastomosis in Section \ref{bypass_section} in order to optimize the bypass shape with respect to energy dissipation or vorticity. This has been done for two different models of the bypass, and the optimal shapes are very similar. A differently shaped channel was detected for lower Reynolds numbers, which indicates that a simplified model with respect to length scales should be avoided in order to get reliable results. An additional small channel close to the occlusion is observed in the optimal shapes, but little fluid flow is detected through it. This is due to a weakness in the algorithm. Apart from this erroneousness, in conclusion the topological optimization algorithm seems to handle the biomedical fluid flow problem adequately. 
 



\section{Further Work}
\label{further_work}

The further work of this thesis should include applying the optimization method to the bypass problem with higher Reynolds number, as the highest number in this thesis was $Re = 175$. In the literature \cite{quarteroni2003ocs, rozza2005ocs, agoshkov2006sda, agoshkov2006mad}, a Reynolds number of order $10^3$ is suggested. In addition a longer model for the bypass problem should be tested, in order to find the dependency in optimal shape with respect to the length of the design domain. Also, a more detailed model, with for instance removal of some of the domain above the coronary artery could be implemented, correcting the opening close to the occlusion. Due to time constraints, this was not carried out in this thesis. 

Another interesting topic would be to extend the model to 3D, and investigate techniques for reducing the computational costs. Implementation in three dimensions is generally not a trivial case, as one has to code in parallel and handle the large amount of data. Not all models require three-dimensional simulations to give useful information about the problem. However, for realistic simulations of blood flow, a three-dimensional model is necessary. An extension to three dimensions would also enable the topology optimization method for other problems where a more realistic model is required. As the mathematics in this thesis are dimension-independent, only the implementation has to be extended. 


It would also be interesting to further investigate the difference between the gradient computed using dolfin-adjoint and the topolgical derivative, and what this is caused by, as they differed more than was expected. For instance, one could look at the solution with a very fine grid, as the numerically computed gradient should approach the analytical when the space step size becomes small. One could also apply the numerical gradient to the double-pipe problem in Section \ref{double_pipe_section} and compare the solution to the optimal shape and functional evolvement computed with the topological derivative.


