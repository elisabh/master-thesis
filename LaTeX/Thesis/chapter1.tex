\pagestyle{fancy}
\fancyhf{}
\renewcommand{\chaptermark}[1]{\markboth{\chaptername\ \thechapter.\ #1}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\renewcommand{\headrulewidth}{0.1ex}
\renewcommand{\footrulewidth}{0.1ex}
\fancyfoot[LE,RO]{\thepage}
\fancyhead[LE]{\leftmark}
\fancyhead[RO]{\rightmark}
\fancypagestyle{plain}{\fancyhf{}\fancyfoot[LE,RO]{\thepage}\renewcommand{\headrulewidth}{0ex}}

\pagenumbering{arabic} 				
\setcounter{page}{1}


%===================================== CHAP 1 =================================

\chapter{Introduction}

% Kommentarer fra Jakob:
\/*
- Starte med å si hva oppgaven handler om, motivere det ut ifra litteraturen, så hvordan vi vil gjøre det og hvordan det skal skille seg fra det som har blitt gjort før. 
- Si at jeg vil bruke Topology Optimization, Level Set, Adjoint, FEM til å lage NS. 
- Skriv en oversikt over hvor i oppgaven hvert tema er diskutert. 
*/


\begin{quote}
\emph{"The art of structure is where to put the holes"} \\
Robert Le Ricolais \\
French-American engineer and philosopher\\
(1894-1977)\newline
\end{quote}

%\emph{"Obvious is the most dangerous word in mathematics."}\\ 
%E. T. Bell \newline\\
%\emph{"What about the boundary conditions?!"}\\
%- Unknown
%All models are wrong, but some models are useful 
%--George P. E. Box

% Motivation and example, previous work, my thesis

%\cite{burger2003itd, amstutz2006nat, challis2009lst, amstutz2011als}
\section{Problem Description}


In this thesis, we will apply a topology optimization method to time-dependent fluid flow, using the density method and level set method. This method will be applied to optimize the shape of a coronary artery bypass anastomosis, which is a vessel going around an occluded artery in the heart. The fluid movement is described by the unsteady, incompressible Navier-Stokes equations combined with Darcy's equation. These equations are discretized using a finite element approach in space and a backward Euler method in time. We will consider different objective functionals for the optimization problems. The topological derivative will be calculated based on an adjoint formulation of the Navier-Stokes equations. Using the adjoint solution, we are able to calculate the topological derivative with significantly lower computational costs. The topological derivative will be used as a search direction in a gradient-based algorithm in order to find the (locally) optimal channel shape. 





%\section{Bypass problem}

\section{Medical Background}
\label{medical_background}

% Skriv om, bedre flyt 
The motivation behind the topology optimization method is to design a shape of a coronary artery bypass anastomosis. A bypass surgery is a surgical procedure to restore normal blood flow to an occluded coronary artery. Figure \ref{fig:bypass_model} shows an illustration of a bypass. 
\begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth]{figures/bypass_quarteroni}
    \caption{Simplified model of a bypass, collected from Quarteroni and Rozza \cite{quarteroni2003ocs}. The zone of the incoming branch of the bypass into the coronary, marked with a circle, is known as the toe.}
    \label{fig:bypass_model}
\end{figure}
Bypass surgery can provide relief of angina, which is chest pain related to lack of blood flow, but bypass surgery does not prevent future heart attacks. An understanding of coronaric diseases is very important to reduce surgical and post-surgical failures, as successful grafts typically last 8-15 years. Approximately 8 \% of patients risk bypass failure every year, and 80 \% of bypasses must be replaced within 10 years \cite{quarteroni2003ocs}.  One of the principal causes is anastomotic intimal hyperplasia (Cole et al. \cite{cole2002ith}), which is a physiologic healing response to injury to the blood vessel wall. From clinical observations (Leuprecht et al. \cite{leuprecht2002nsh}), we know that intimal hyperplasia mostly occurs at outflow anastomoses of bypass grafts. Different shapes of coronary artery bypass anastomoses are available, such as Taylor Patch \cite{cole2002nih} and Miller Cuff \cite{cole2002ces}, so different surgery procedures are practiced, see Figure \ref{fig:taylorpatch} for an illustration.
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.35\textwidth}
    \includegraphics[width=\textwidth]{figures/taylor_patch}
    \caption{Taylor patch.}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.35\textwidth}
    \includegraphics[width=\textwidth]{figures/miller_cuff}
    \caption{Miller cuff.}
    \end{subfigure}
    \caption{Illustration of different bypass anastomosis shapes, collected from \cite{taylorpatch}.}
    \label{fig:taylorpatch}
\end{figure}




The left and right coronary arteries are the blood vessels that run on the heart surface, and they regulate the blood flow levels to match the needs of the heart muscle, see Figure \ref{fig:coronary_arteries}. These vessels are relatively narrow and can become blocked, causing angina or heart attack. The coronary arteries are the only source of blood supply to the heart muscle, so blockage of these vessels is critical, generally resulting in death of the heart tissue due to lack of blood supply. 
An anastomosis is an area of vessels that are interconnected, allowing dual blood supply to a region. This normally ensures blood flow even if there is a partial blockage in another branch. Since the anastomoses in the heart are very small, blockage in a coronary artery often results in lack of blood supply and death of the heart cells affected. 
%
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/CoronaryArteries_wiki}
    \caption{Coronary arteries on the heart, collected from \cite{blaus2017arteries}}
    \label{fig:coronary_arteries}
\end{figure}

Shape optimization of a coronary artery bypass anastomosis based on adjoint formulation has been proposed by Quarteroni and Rozza \cite{quarteroni2003ocs} in 2003. In this work, the zone of the incoming branch of the bypass into the coronary, also known as the toe, was optimized, and the cost functionals for vorticity and wall shear stress were minimized. The flow was modeled using steady Stokes flow, and resulted in an optimally shaped cuffed bypass, with a shape that resembled the Taylor arterial patch, described in Cole et al. \cite{cole2002nih}. In 2005 Rozza \cite{rozza2005ocs} extended this work by solving the unsteady Navier-Stokes equations in the original and final configuration of the shape proposed in \cite{quarteroni2003ocs}, and a reduction of 40 \% in vorticity was observed. In 2006 Agoshkov, Rozza and Quarteroni \cite{agoshkov2006sda, agoshkov2006mad} presented a new approach based on small perturbation theory and adjoint formulation in the study of aorto-coronary bypass anastomoses. One approach with steady Stokes flow \cite{agoshkov2006sda}, and another with unsteady Stokes equations \cite{agoshkov2006mad}, both showed reduction in vorticity of about 30 \%. All previous optimization problems were modelled in a two-dimensional geometry and used a chosen velocity profile as inlet velocity. For future development it is suggested to apply fully unsteady incompressible Navier-Stokes equations and extend the geometry to three dimensions, in order to provide a more realistic design. 


We will apply a topology optimization method to a coronary artery bypass anastomosis. As cost functionals, we will consider vorticity, pressure drop and energy dissipation, and the flow will be modeled in two dimensions using the unsteady incompressible Navier-Stokes system combined with Darcy's equation. We will use a flow rate waveform shape for a left anterior descending coronary artery from Thomas et al. \cite{thomas2005vcb} as inlet velocity. 





\section{Topology Optimization in Fluid Flow}

The goal of topology optimization is to modify the shape and connectedness of a domain, such that the desired objective function is minimized. While shape optimization is limited to determining an existing boundary, topology optimization can be used to design features within the domain, allowing new boundaries to be introduced into the design. This makes topology optimization more robust than for instance shape optimization, since less information about the goal shape is required. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/nfig001}
    \caption{An example of three steps of a fluid flow topology optimization procedure, collected from \cite{kreissl2011tou}.}
    \label{fig:topology_optimization}
\end{figure}

The underlying idea of topology optimization is to describe a geometry via its material distribution. By varying the material distribution, subdomains can be created or merged. See Figure \ref{fig:topology_optimization} for an illustration of fluid flow topology optimization, with material distribution of fluid and solid. The density type method and level set method are two methods used in the implementation of topology optimization. The density method was first used for the design of stiffness and compliance mechanics and has been extended to many other physical fields. See Campeao et al. \cite{campeao2014tdp} and its references for examples. 

For several optimization problems, the level set method has been successfully applied to compute optimal geometries without a priori knowledge about the connectedness of the domain. The level set method is a numerical technique for tracking interfaces and shapes in a domain using level sets. One of the advantages of the level set method is that you do not have to parametrize the shapes. This makes it well suited for topology optimization, for instance when a shape splits in two or a hole is developed. See for example Sethian and Wiegmann \cite{sethian2000sbd}.


Topology optimization applied to fluid mechanics was first introduced in 2003 by Borrvall and Petersson \cite{borrvall2003tof} on stationary Stokes flow. Before this, only shape optimization had been applied to fluid mechanics. In \cite{borrvall2003tof} a relaxed Stokes flow formulation was proposed, where the impermeability works as a penalty factor on the flow velocity, inspired by Darcy's equation. Guest and Pr\'evost \cite{guest2005toc} extended this work by applying a stabilized finite element formulation for the Stokes and Darcy equations, treating the solid phase as a porous medium. 

% Level set
Burger, Hackl and Ring \cite{burger2003itd} investigated the use of topological derivatives in combination with the level set method for optimization problems. Amstutz and Andr\"a \cite{amstutz2006nat} proposed a new algorithm for topology optimization using a level set method based only on the topological gradient. The level set method in fluid flow optimization was proposed by Challis and Guest \cite{challis2009lst}. This was suggested as an alternative to density-based approaches using the topological sensitivity of the Stokes flow.

Topology optimization for fluid mechanics was first extended to the Navier-Stokes equations by Gersborg-Hansen et al. \cite{gersborg2005toc} in 2005. They propose an analytical sensitivity analysis using the adjoint method. Topological sensitivity was introduced by Sokolowski and Zochowski in 1999 \cite{sokolowski1999tds}. The adjoint equations for channel flows were proposed by Othmer et al.  \cite{othmer2008caf}, where a continuous adjoint formulation for the incompressible Navier-Stokes equations with Darcy porosity term was derived. S{\'a} et al. \cite{sa2016tda} proposed a steepest descent algorithm based on the topological derivative formulation for the Stokes and Navier-Stokes equations and their adjoint formulations, obtaining an optimal design in few iterations. 

Kreissl et al. \cite{kreissl2011tou} and Deng et al. \cite{deng2011tou, deng2012tos} applied topology optimization to the unsteady incompressible Navier-Stokes for low to moderate Reynolds numbers. In this thesis, our goal is to extend their work by combining the algorithm described in \cite{sa2016tda} with topological derivatives applied to unsteady fluid flow. 



\section{Organization of thesis}

This thesis is organized as follows. In Chapter \ref{chapter:topology_optimization} we will present the tools of topology optimization. The topological derivative will be defined, and some basic examples related to fluid flow topology optimization are presented. In addition, the representation of fluid and solid will be be described through a level set function and impermeability constant. 
Chapter \ref{chapter:fluid_flow_optimization} describes the modelling of fluid flow in topology optimization. The optimization problem with respect to fluid flow problems will be presented, and a solution strategy for the optimization procedure, including algorithms, will be proposed. 
In Chapter \ref{chapter_adjoint}, the adjoint method for both stationary and non-stationary partial differential equations is presented, and the adjoint equations for the steady and unsteady Navier-Stokes-Darcy systems are derived. The adjoint equations are incorporated in the derivation of the topological derivative with respect to the dissipation energy functional at the end of the chapter. The mathematical theory presented in these chapters are dimension-independent. 


In Chapter \ref{chapter_discretization}, the Navier-Stokes-Darcy equations are discretized in space using the finite element method, and in time using a backwards scheme. Two different projection methods, Chorin's scheme and the Incremental Pressure Correction scheme, will be presented, and the time-dependent adjoint equations from Chapter \ref{chapter_adjoint} are discretized in time. At the end of the chapter, numerical software used for discretization is presented.
In Chapter \ref{numerical_verification} we will verify the discretization proposed in Chapter \ref{chapter_discretization} in 2D, using the numerical software presented. In addition, we will investigate the effect of Darcy's equation combined with the Navier-Stokes equations. We will also verify the numerically computed gradient, with respect to the topological derivative proposed in Chapter \ref{chapter_adjoint}.
In Chapter \ref{numerical_experiments}, we will present numerical experiments  based on examples from the literature, also in 2D. In addition, the model and result of the optimal bypass anastomosis will be presented and discussed. 




\cleardoublepage