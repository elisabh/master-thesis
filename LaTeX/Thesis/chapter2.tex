%===================================== CHAP 2 =================================

\chapter{Topology Optimization}
\label{chapter:topology_optimization}


%Here I want a section on what topological optimization is, i.e. a general optimization problem with functional $J(u, m)$.


Introducing the field of topology optimization, we will consider a very general case. Assume we have an open and bounded domain $\Omega \subset \mathbb{R}^d $, with $d = 2$ being the dimension of the space in our case, although the presentation is independent of the dimension. The optimization problem can be stated as
%
\begin{equation}
\label{JF}
\begin{aligned}
\textrm{Minimize } \quad &J,\\
\textrm{subject to } \quad &F= 0 \textrm{ on } \Omega,
\end{aligned}
\end{equation}
where $J$ is the goal functional and $F$ is a set of state equations.


We will apply optimization techniques based on the topological derivative and level sets to find the optimal topology. For a better understanding of the topological derivative, we will first explain the basic concept, before we illustrate it with an example using material distribution. At the end of this chapter, we will give a brief introduction to the level set method in topology optimization. 




\section{The Topological Derivative}

The topological derivative is a measure of the change in a topology with respect to some functional. A topological change in $\Omega$ can be described by the removal of a circular ball $B_{\varepsilon}(\hat{x})$ with radius $\varepsilon$ centered at an arbitrary point $\hat{x}$ in the interior of $\Omega$, $\hat{x} \in \textrm{int}(\Omega)$. The perturbed domain is then given by $\Omega_{\varepsilon}(\hat{x}) = \Omega \backslash \overline{B_{\varepsilon}(\hat{x})}$. See Figure \ref{fig:topder_illustr} for an illustration of the perturbation of a domain.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/Topological_derivative_illustration-crop.pdf}
    \caption{An illustration of a topological perturbation of a domain $\Omega$ by the removal of a circular ball $B_\varepsilon$ with radius $\varepsilon$.}
    \label{fig:topder_illustr}
\end{figure}

Now, let us introduce a functional $J$ associated with the unperturbed domain, $J = J(\Omega)$, and find its topological derivative. The topological derivative is defined by the first order correction of the asymptotic expansion of the functional with respect to a small perturbation in the domain. For the perturbed domain, $\Omega_\varepsilon$, we assume the associated functional $J(\Omega_\varepsilon)$ admitting the following topological asymptotic expansion \cite{novotny2013tds}
%
\begin{equation}
\label{topological_expansion}
J(\Omega_{\varepsilon}(\hat{x})) = J(\Omega) + f(\varepsilon)D_T J(\hat{x}) + o(f(\varepsilon)).
\end{equation}
%
The term $f(\varepsilon) D_T J(\hat{x})$ can be seen as the first order correction of $J(\Omega)$ to approximate $J(\Omega_{\varepsilon}(\hat{x}))$, and the function $f: \mathbb{R^+} \rightarrow \mathbb{R^+}$ must have the property $f(\varepsilon) \rightarrow 0$ when $\varepsilon \rightarrow 0^+$. $D_T J(\hat{x})$ describes the change in $J$ with respect to a perturbation at $\hat{x}$, and is called the topological derivative of $J$. From the expansion of $J$ in equation \eqref{topological_expansion} we obtain the general definition for the topological derivative
%
\begin{equation}
\label{topological_derivative}
D_T J(\hat{x}) := \lim_{\varepsilon \rightarrow 0} \frac{J(\Omega_{\varepsilon}(\hat{x})) - J(\Omega)}{f(\varepsilon)}.
\end{equation}






%\section{A Basic Example of the Topological Derivative}

A basic illustration of the topological derivative is to consider the functional associated with the volume of a domain, 
%
\begin{align}
J(\Omega) &= |\Omega| = \int_\Omega 1.
\end{align}
The corresponding perturbed volume functional, where a ball of radius $\varepsilon$ has been removed, is given as
%
\begin{align}
\label{topological_volume_expansion}
J(\Omega_\varepsilon) = \int_\Omega 1 - \int_{B_\varepsilon} 1 = J(\Omega) - \pi \varepsilon^2.
\end{align}
Comparing the topological expansion in \eqref{topological_volume_expansion} to equation \eqref{topological_expansion}, it is trivial to identify $f(\varepsilon) = \pi \varepsilon^2$ and $D_T J = -1$ as the topological derivative. In this particular example, the order term in \eqref{topological_expansion} is equal to zero, and the topological derivative $D_T J$ is independent of $\hat{x}$. The negative sign appears because we remove a part of the domain.



\section{Topological Derivative with respect to a Fluid Subdomain}

% Maybe better to introduce alpha here? Let dummy functional J = int_\Omega alpha(x)
Restricting ourselves to fluid flow topology optimization, let us investigate the distribution of fluid and solid material in a domain $\Omega$. Denote a fluid subdomain by $\omega$ and the remaining solid subdomain as $\Omega \backslash \bar{\omega}$. Consider now the topological derivative with respect to the fluid domain, i.e. let the functional $J$ be $J = |\omega |$. If $\hat{x}$ is in the fluid subdomain $\omega$, the perturbation would be to remove a ball $B_\varepsilon$ from $\omega$, and we would obtain the same topological derivative as in the basic volume example, namely $D_T J = -1$. If, on the other hand, $\hat{x}$ is stationed in the solid subdomain $\Omega \backslash \bar{\omega}$, the perturbation would be to add a ball $B_\varepsilon$ to the fluid domain, and the sign would be positive. Hence, the topological derivative of $|\omega|$ is described by 
%
\begin{align}
\label{fluid_derivative}
D_T |\omega| = k(\hat{x}),
\end{align}
with 
%
\begin{align}
k(\hat{x}) =
\begin{cases}
    +1 & \quad \text{ if } \hat{x} \in \Omega \backslash \omega\\
    -1 & \quad \text{ if } \hat{x} \in \omega.
\end{cases}
\label{k}
\end{align}
We will make use of this definition of $k$ when we derive the topological derivative with respect to a certain functional in Section \ref{topological_derivative_dissipation}. 








\section{Level Set Representation of Fluid and Solid}

% What is a level set method?
% Why do I want to use a level set method? Advantages, limitations in other approaches?
% How is it combined in topology optimization?
% Example on how to use it.

The level set method is a numerical technique for tracking interfaces and shapes in a domain using level sets \cite{osher1988fpc}. A level set can be expressed as the set where some function $f$ is bounded by a constant $c$, $\{ x  ~| f(x) < c \}$. One of the advantages of the level set model is thatit is not needed to parametrize the shapes since the shape is implicitly defined. This makes it well suited for topology optimization, for instance when a shape is divided into two pieces or a hole is developed \cite{sethian2000sbd}. 


To model the distribution of fluid and solid in the domain $\Omega$, we introduce a fictitious time $t$, such that the evolvement of the domain can be viewed as a family $(\Omega(t))_{t \ge 0}$. This evolution is represented by a level set function $\psi(x,t)$ such that
%
\begin{align}
\label{level_set}
    \begin{cases}
        \psi(t,x) \le 0 \quad &\textrm{ for } x \in \omega(t) \\
        \psi(t,x) > 0 \quad &\textrm{ for } x \in \Omega \backslash \bar{\omega}(t),
    \end{cases}
\end{align}
where $\omega(t)$ is the time-dependent fluid domain. Note that $k(x) = \mathrm{sign} (\psi(t, x))$ from \eqref{k}. Hence, the level set function $\psi$ can be used as a design variable for the fluid-solid distribution. 

A common approach is to model the evolution of the level set function by the Hamilton-Jacobi equation \cite{burger2003itd, challis2009lst}, also known as the level set equation,
%
\begin{align}
\label{hamilton_jacobi}
    \frac{\partial \psi}{\partial t} = -v | \nabla \psi |,
\end{align}
where $v$ is the speed of the moving domain boundary. In \cite{burger2003itd} Burger et al. investigate the use of topological derivatives in combination with the level set function \eqref{hamilton_jacobi} in a new level set method. Another approach, first proposed in \cite{amstutz2006nat}, is to use an evolution equation in combination with the level set function, based only on the concept of the topological derivative $D_T J$,
%
\begin{align}
    \frac{\partial \psi}{\partial t} = P(D_T J),
\end{align}
where $P(\cdot)$ is some function. We will use this formulation and discuss it, in the light of optimality conditions, in Section \ref{level_set_section}. 




\section{Material Distribution}
\label{material_distribution}

We will distribute solid or fluid material at each point $x$ of the domain $\Omega$ by letting an impermeability parameter $\alpha(x)$ vary between two non-negative constants,
%
\begin{align}
\label{alpha}
\alpha(x) = 
\begin{cases}
    \alpha_L    & \quad \text{if } x \in \omega, \\
    \alpha_U    & \quad \text{if } x \in \Omega \backslash \bar{\omega}\\
  \end{cases}
\end{align}
with $\alpha_U \gg \alpha_L \ge 0$. As in the previous section, $\omega$ denotes the fluid subdomain. We omit explicitly writing the time-dependency to simplify notation. 

%
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/alpha_interpolated}
    \caption{Interpolation of $\alpha(\psi)$ for different values of penalty $q$.}
    \label{fig:alpha}
\end{figure}

In some cases, we need the material distribution to be continuously differentiable on the domain, for instance when  computing topological derivatives numerically. The impermeability $\alpha$ can in this case be represented by an interpolation function
%
\begin{align}
\label{alpha_interpolation}
\alpha(\psi) = \alpha_L + (\alpha_U - \alpha_L) \frac{1}{1 + e^{-\psi + q}} 
\end{align}
where $\psi$ is the level set function defined in \eqref{level_set} and $q$ is a penalty parameter that restricts the elements with intermediate impermeability. An illustration of the interpolation can be seen in Figure \ref{fig:alpha}, with $\alpha_L = 2.5 \cdot 10^{-4}$ and $\alpha_U = 2.5 \cdot 10^4$. Letting the penalty parameter $q = 0$ corresponds to the case of no interpolation. 





\section{Optimality Conditions}

In order to carry out topology optimization, we need some basic optimality conditions. A locally necessary optimality condition is on the topological derivative defined in \eqref{topological_derivative}, is
\begin{align}
    \label{necessary}
    D_T J(x) \ge 0 \quad \forall x \in \Omega.
\end{align}
The topological gradient with respect to the optimization problem, hereby denoted $g$, is a function that measures the sensitivity of $J$ with respect to a change in the topology, i.e. the permeability. Hence, if the impermeability $\alpha$ changes from $\alpha_L$ to $\alpha_U$, then the fluid volume $|\omega|$ is increased, and the gradient must be positive, as opposed to when it changes from $\alpha_U$ to $\alpha_L$. Then $|\omega|$ is decreased, and the gradient must be negative, see \cite{amstutz2011als} for more details. The gradient $g$ needs to be a descent direction, so we define it as 

\begin{align}
\label{gradient}
    g(x) = 
    \begin{cases}
        - D_T J(x) \quad &\textrm{ for } x \in \omega \\
        + D_T J(x) \quad &\textrm{ for } x \in \Omega \backslash \bar{\omega}
    \end{cases}
\end{align}
In this way, the local optimality condition \eqref{necessary} can be stated as

\begin{align}
\label{g_optimal}
    \begin{cases}
        g(x) \le 0 \quad &\textrm{ for } x \in \omega \\
        g(x) > 0 \quad &\textrm{ for } x \in \Omega \backslash \bar{\omega}.
    \end{cases}
\end{align}
This optimality condition on the gradient will be used in the optimization algorithm in Chapter \ref{chapter:fluid_flow_optimization}. 


\cleardoublepage