%===================================== CHAP 5 =================================

\chapter{Numerical Verification}
\label{numerical_verification}

The Navier-Stokes-Darcy problems is solved using a finite element method in space and a pressure correction scheme, either Chorin's scheme or IPCS, in time, see Chapter \ref{chapter_discretization}. It is expected to get a convergence of second order in space and first order in time for all cases. In further results, we ended up using IPCS, as it is more stable and gives more accurate solutions. 

To verify the solutions, we consider various test problems, and use the method of manufactured solutions to calculate the source term. The Navier-Stokes-Darcy problem \eqref{NS_Darcy} can in 2D be written in component form as
%
\begin{align}
\label{NSD_component}
        \frac{\partial u}{\partial t} + u \frac{\partial u}{\partial x} + v \frac{\partial u}{\partial y} + \frac{\partial p}{\partial x} - \nu \left( \frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u}{\partial y^2}\right) + \alpha u &= f_x, \\
        \frac{\partial v}{\partial t} + u \frac{\partial v}{\partial x} + v \frac{\partial v}{\partial y} + \frac{\partial p}{\partial y} - \nu \left( \frac{\partial^2 v}{\partial x^2} + \frac{\partial^2 v}{\partial y^2}\right) + \alpha v &= f_y, \\
        \frac{\partial u}{\partial x} + \frac{\partial v}{\partial y} &= 0,
\end{align}
where $u$ and $v$ are the $x$- and $y$-components of $\mathbf{u}$, respectively, and $f_x$ and $f_y$ are the $x$- and $y$-components of the source term $\mathbf{f}$, respectively. The density $\rho$ is set to $1$ in all cases.

Since mainly two values for $\alpha$ will be used, representing fluid and solid material, we will check convergence in both cases. The velocities within solid material and along solid-fluid interface (no-slip condition) need to vanish. Figure 5 in \cite{kreissl2011tou}, section 3.1 suggests that a maximum impermeability of $10^4$ is sufficient to enforce zero velocity. Inspired by the work of Borrvall and Peterson \cite{borrvall2003tof}, the numerical values used for $\alpha$ in all experiments are
%
\begin{align}
    \alpha_L &= \frac{2.5 \nu}{100^2}, \\
    \alpha_U &= \frac{2.5 \nu}{0.01^2}.
\end{align}
In Kreissl et. al \cite{kreissl2011tou}, they investigate the maximum impermeability needed to  enforce zero velocity. They find that for different values of Reynolds number and velocity norms, $\alpha_U > 10^4$ is sufficient. 

The numerical solution $\mathbf{u}$ is computed on a $N \times N$ mesh at end time $T$, and is compared to the analytical solution $\mathbf{u}_e$. The error is calculated by taking the $L^2$-norm of the difference between the solutions, $\| \mathbf{u} - \mathbf{u}_e \|$, using the FEniCS function \texttt{errornorm}. This module computes the error by first interpolating both $\mathbf{u}$ and $\mathbf{u}_e$ to a common space with finer discretization, then subtracting the two fields and then evaluating the integral. A code snippet is included below to illustrate how the error is calculated using Python and the FEniCS environment.
%
\begin{minted}[frame=single, fontsize=\footnotesize]{python}
def errornorm(u, u_e, Ve):
    u_= interpolate(u, Ve)
    u_e_ = interpolate(u_e, Ve)
    e_ = Function(Ve)
    e_.vector()[:] = u_.vector().array() - u_e_.vector().array()
    error = e_**2*dx
    return sqrt(assemble(error))
\end{minted}






\section{Taylor-Green Flow}
First, we will verify the Navier-Stokes solver without the Darcy term, because in this case we have an analytical solution. Taylor-Green flow is an analytical solution to the non-stationary incompressible Navier-Stokes equations on a domain with periodic boundary conditions. The exact solution in 2 dimensions is given by
%
\begin{align}
\label{Taylor-Green}
    u &= - \cos(\pi x) \sin(\pi y) e^{-2 \pi^2 \nu t}, \\
    v &= \cos(\pi y) \sin(\pi x) e^{-2 \pi^2 \nu t}, \\
    p &= \frac{1}{4}\left( \cos(2 \pi x) + \cos(2 \pi y) \right) e^{-4 \pi^2 \nu t},
\end{align}
with the source term $\mathbf{f} = \mathbf{0}$. Because we have an additional Darcy-term in our problem, we have to compute the new source term, which is simply $\mathbf{f} = -\alpha \mathbf{u}$.

The Taylor-Green flow is computed using Chorin's method on a domain $\Omega = [0,2] \times [0,2]$ with periodic boundary conditions in both spatial directions. The kinematic viscosity is $\nu = 0.01$ and the errors are computed at end time $T = 1.0$. The convergence in space is presented in Figure \ref{fig:TG_space}, with mesh size $N = [8, 16, 32, 64]$ and time step $\Delta t = 0.02$. The convergence in time is presented in Figure \ref{fig:TG_time} with time step size $\Delta t = [1.0, 0.5, 0.25, 0.125]$ and grid size $N = 200$. As expected, we have a convergence of second order in space and first order in time.
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/PeriodicBC_space_lowalpha_t10}
        \caption{Low $\alpha$}
        \label{fig:TG_space_la}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/PeriodicBC_space_highalpha_t10}
        \caption{High $\alpha$}
        \label{fig:TG_space_ha}
    \end{subfigure}
    \caption{Convergence rates in space on velocity in $L^2$-norm for Taylor-Green flow at $T = 1.0$.}
    \label{fig:TG_space}
\end{figure}
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/PeriodicBC_time_lowalpha_t10}
        \caption{Low $\alpha$}
        \label{fig:TG_time_la}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/PeriodicBC_time_highalpha_t20}
        \caption{High $\alpha$}
        \label{fig:TG_time_ha}
    \end{subfigure}
    \caption{Convergence rates in time on velocity in $L^2$-norm for Taylor-Green flow at $T = 1.0$.}
    \label{fig:TG_time}
\end{figure}
%
The computed solution at time $T = 1.0$ can be seen in Figure \ref{fig:TG}.
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/TaylorGreen_u}
        \caption{Taylor Green velocity}
        \label{fig:TG_u}
    \end{subfigure}
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{figures/TaylorGreen_p}
        \caption{Taylor Green pressure}
        \label{fig:TG_p}
    \end{subfigure}
    \caption{Computed solution of Taylor-Green flow at $T = 1.0$.}
    \label{fig:TG}
\end{figure}


\section{Convergence of Chorin's Method vs IPCS}

In this section, we will compare Chorin's method to IPCS, in order to see which method is better to proceed with. Because the optimization problems will have Dirichlet boundary conditions, it is reasonable to include the verification of such a model. We choose the analytical solution 
%
\begin{align}
\label{dirichlet_manu}
    u &= - \cos(\pi x) \sin(\pi y) e^{-t}, \\
    v &=  \cos(\pi y) \sin(\pi x) e^{-t}, \\
    p &= \sin(\pi x) \cos(\pi y),
\end{align}
and use the method of manufactured solutions to calculate the source term. Inserting \eqref{dirichlet_manu} into \eqref{NSD_component}, we get the source terms
\begin{align}
    f_x = &-\alpha e^{-t} \sin(\pi y) \cos(\pi x) - 2\nu \pi^2 e^{-t}\sin(\pi y)\cos(\pi x) \nonumber\\
    &+ \pi \cos(\pi x) \cos(\pi y) - \pi e^{-2t} \sin(\pi x) \sin(\pi y)^2 \cos(\pi x) \nonumber\\
    &- \pi e^{-2t}\sin(\pi x) \cos(\pi x) \cos(\pi y)^2 + e^{-t} \sin(\pi y) \cos(\pi x), \\
    f_y = &\alpha e^{-t} \sin(\pi x) \cos(\pi y) + 2\nu \pi^2 e^{-t}\sin(\pi x)\cos(\pi y) \nonumber\\ 
    &- \pi \sin(\pi x) \sin(\pi y) - \pi e^{-2t} \sin(\pi x)^2 \sin(\pi y)\cos(\pi y) \nonumber\\
    & - \pi e^{-2t}\sin(\pi y) \cos(\pi x)^2 \cos(\pi y) - e^{-t} \sin(\pi x) \cos(\pi y),
\end{align}
and zero divergence. The source terms are computed using the python library SymPy \cite{sympy}. The manufactured solution is computed using both Chorin's method and the IPCS method on a domain $\Omega = [0,1] \times [0,1]$, and the analytical solution $(u, v)$ is applied as Dirichlet boundary conditions on the whole boundary. The pressure $p^{k+1}$ is normalized in each iteration, i.e. the mean value of the pressure is subtracted from the pressure solution to keep it in the same range. This is done because $p$ is only defined up to a constant by the Navier-Stokes-Darcy equations \eqref{NS_Darcy}.

As mentioned in Section \ref{projection_methods}, an erroneous boundary layer with width $\approx \sqrt{\nu \Delta t}$ can occur when Dirichlet boundary conditions are applied. This prevents the scheme from being fully second-order in space and first-order in time \cite{guermond2006opm}. By letting the time step $\Delta t$ be very small, and increasing the kinematic viscocity to $\nu = 1.0$ to get a lower Reynold's number, we were able to compute convergence rates at end time $T = 0.1$.

The convergence of Chorin's method in space is presented in Figure \ref{fig:man_space} , with mesh size $N = [4, 8, 16, 32]$ and time step $\Delta t = 0.0000025$. The convergence of Chorin's method in time is presented in Figure \ref{fig:man_time} with time step size $\Delta t$ $= [0.000002$, $0.000001$, $0.0000005$, $0.00000025]$ and grid size $N = 400$. The convergence in space appears to be of third order, which may be an effect of a regular mesh. The convergence in time is of first order, as expected. 
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/DirichleBC_space_lowalpha_t01}
        \caption{Low $\alpha$}
        \label{fig:man_space_la}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/DirichleBC_space_highalpha_t01}
        \caption{High $\alpha$}
        \label{fig:man_space_ha}
    \end{subfigure}
    \caption{Chorin's scheme convergence rates in space on velocity in $L^2$-norm for \eqref{dirichlet_manu} at $T = 0.1$.}
    \label{fig:man_space}
\end{figure}
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/DirichleBC_time_lowalpha_t01}
        \caption{Low $\alpha$}
        \label{fig:man_time_la}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/DirichleBC_time_highalpha_t01}
        \caption{High $\alpha$}
        \label{fig:man_time_ha}
    \end{subfigure}
    \caption{Chorin's scheme convergence rates in time on velocity in $L^2$-norm for \eqref{dirichlet_manu} at $T = 0.1$.}
    \label{fig:man_time}
\end{figure}



Convergence was very hard to achieve for the Chorin scheme, due to the artificial boundary layer. A convergence test was also performed for the IPCS method. With this scheme, we managed to get the same order of convergence using a much bigger time step $\Delta t$, and thus calculated the error at end time $T = 1.0$. It seems that including the pressure in the tentative velocity step, as shown in equation \eqref{tentative_2}, has a huge impact on the erroneous boundary layer when using Dirichlet boundary conditions.

The convergence of the IPCS method in space is presented in Figure \ref{fig:IPCS_space}, with mesh size $N = [4, 8, 16, 32]$ and time step $\Delta t = 0.001$. The convergence of the IPCS method in time is presented in Figure \ref{fig:man_time} with time step size $\Delta t = [0.1, 0.05, 0.025, 0.0125]$ and grid size $N = 60$. The order of convergence is of third-order in space and of first-order in time. In other words, the same result is obtained using much larger time steps $\Delta t$ and smaller grid size $N$. This result indicates that we should proceed with the IPCS method when solving the NSD problem.
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/IPCS_DBC_space_la_t10}
        \caption{Low $\alpha$}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/IPCS_DBC_space_ha_t10}
        \caption{High $\alpha$}
    \end{subfigure}
    \caption{IPCS convergence rates in space on velocity in $L^2$-norm for \eqref{dirichlet_manu} at $T = 1.0$.}
    \label{fig:IPCS_space}
\end{figure}
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/IPCS_DBC_time_la_t10}
        \caption{Low $\alpha$}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{figures/IPCS_DBC_time_ha_t10}
        \caption{High $\alpha$}
    \end{subfigure}
    \caption{IPCS convergence rates in time on velocity in $L^2$-norm for \eqref{dirichlet_manu} at $T = 1.0$.}
    \label{fig:IPCS_time}
\end{figure}



\section{Channel Problem with Darcy Term}

To verify the accuracy of the scheme with respect to material distribution, we compare the results of the flow in a fluid channel using the Darcy term $\alpha$ against the reference solution of a body-fitted mesh with $\alpha = 0$. The boundary conditions are Poiseuille flow with maximum velocity 1.0 at the inflow, open boundary conditions $\mathbf{g}_N = \nu (\nabla \mathbf{u})\mathbf{n}  - p \mathbf{n} = 0$ at the outflow, and no-slip on the remaining boundaries. An illustration of the problems can be seen in Figure \ref{fig:alpha_test}. 
%
\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/Alpha_test}
    \caption{Illustration of the channel for Navier-Stokes-Darcy (left) and Navier-Stokes (right).}
    \label{fig:alpha_test}
\end{figure}

The solution of the channel problem is computed on a domain $\Omega = [0,1] \times [0,1]$ with a mesh containing 4224 cells. The reference solution is computed on a domain $\tilde{\Omega} = [0,1] \times [0.3, 0.7]$, and the mesh is a subset of the initial mesh, thus having only 1696 nodes, as its volume is $4/10$ of the initial domain. 
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.35\textwidth}
        \includegraphics[width=\textwidth]{figures/mesh_u}
    \end{subfigure}
    ~ %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
      %(or a blank line to force the subfigure onto a new line)
    \begin{subfigure}[b]{0.35\textwidth}
        \includegraphics[width=\textwidth]{figures/mesh_ref}
    \end{subfigure}
    \caption{Illustration of the meshes for the channel problem.}
    \label{fig:mesh_u}
\end{figure}

The meshes are created in \texttt{gmsh} \cite{geuzaine2009gmsh}, and a coarse version can be seen in Figure \ref{fig:mesh_u}. It is here easy to observe that the meshes have the exact same structure in the channel area.
Both problems are solved using the IPCS-scheme with time step $\Delta t = 0.001$. The solutions are compared at end time $T = 1.0$, and the $L^2(\tilde{\Omega})$-norm of the error is $0.0018$. 
The velocity solution to the Navier-Stokes-Darcy channel problem is plotted in Figure \ref{fig:channel_velocity} together with the error with respect to the reference solution. The error is most significant in the boundary area, where the impermeability constant $\alpha$ changes its value.
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.4\textwidth}
        \includegraphics[width=\textwidth]{figures/channel_velocity}
        \caption{Velocity for Navier-Stokes-Darcy.}
    \end{subfigure}
    \begin{subfigure}[b]{0.48\textwidth}
        \includegraphics[width=\textwidth]{figures/channel_error}
        \caption{Absolute value of difference in velocities.}
    \end{subfigure}
    \caption{Velocity plots for the channel problem with Darcy term. The $l^2$-norm of the difference in velocities is $0.0018$ }
    \label{fig:channel_velocity}
\end{figure}



\section{Verification of Gradient Using dolfin-adjoint}

Dolfin-adjoint \cite{farrell2013ada} is a software developed for computing adjoints and functional gradients numerically. In order to use the gradient computed in dolfin-adjoint, we will verify it by comparing it to the topological derivative derived in Section \ref{topological_derivative_dissipation}, where the objective is the dissipation energy functional averaged over all time steps, 
%
\begin{equation}
\label{J_dissip(t)}
{J}(\mathbf{u}, \alpha) = \Delta t \sum_{n=1}^{N_t} \left( \int_\Omega \left( \nu |\nabla \mathbf{u}|^2 + \alpha(\psi) |\mathbf{u}^n|^2 \right) + \beta |\omega| \right),
\end{equation}
where $N_t$ is the number of time steps, $|\omega|$ is the volume of the fluid domain and $\beta$ is a penalty parameter. The dolfin-adjoint gradient will be computed using the python commands
%
\begin{minted}[frame=none, fontsize=\footnotesize]{python}
J = Functional((alpha(psi)*inner(u, u) + nu*inner(grad(u), grad(u)))*dx*dt
               + beta * fluid_domain(psi) * dx * dt)
g_a = compute_gradient(J, Control(psi))
\end{minted}
while the topological gradient will be computed using the expression derived in Section \ref{topological_derivative_dissipation}, with the additional fluid volume penalization,
%
\begin{equation}
g_t = \Delta t \sum_{n=1}^{N_t} \left( -(\alpha_U - \alpha_L) \mathbf{u}^n(\mathbf{u}-\mathbf{u}^{a})^n + \beta \right).
\end{equation}


\begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth]{figures/diffuser_domain}
    \caption{Computational domain for the diffuser problem.}
    \label{fig:diffuser_domain_grad}
\end{figure}


Figure \ref{fig:diffuser_domain_grad} shows an illustration of the design domain.
The kinematic viscosity is set to $\nu = 0.01$ and the penalty parameter $\beta = 90.0$. The outlet has traction-free Neumann boundary conditions \eqref{neumann}, $\mathbf{g} = \mathbf{0}$, and the inlet has a time-varying parabolic velocity with value 
%
\begin{equation}
    {u}_\mathrm{in}(t) = \sin \left( \frac{\pi}{2T}t \right)
\end{equation}

The gradients are computed on a $[0,1] \times [0,1]$ domain with $60 \times 60$ triangular elements. The time step size is $\Delta t = 0.0001$ and number of time steps $N_t = 60$. 
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{figures/g_dolfinadjoint}
        \caption{Dolfin-adjoint gradient}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{figures/g_topological}
        \caption{Topological gradient}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.043\textwidth}
        \includegraphics[width=\textwidth]{figures/g_bar_}
        \caption{ }
    \end{subfigure}
    \caption{Comparison of gradients for the diffuser problem.}
    \label{fig:gradient}
\end{figure}
The $L^2$-norm between the two gradients, $\|g_a - g_t\| = 0.1889$, and looking at the plot of the differences in Figure \ref{fig:g_diff}, it is obvious that the biggest difference is in the active areas. The relatively big difference in norm can be explained by the fact that dolfin-adjoint uses the trapezoidal rule to compute the time integral \eqref{J_dissip(t)}, while the topological gradient has been computed at every time step. Figure \ref{fig:gradient_convergence} shows a plot of the $l^2$-norm for different grid sizes, and a convergence of order $1/2$ is observed. 
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.35\textwidth}
        \includegraphics[width=\textwidth]{figures/gradient_diff}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.057\textwidth}
        \includegraphics[width=\textwidth]{figures/gradient_diff_bar}
    \end{subfigure}
    \caption{Difference between gradients for the diffuser problem.}
    \label{fig:g_diff}
\end{figure}


\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/gradient_convergence}
    \caption{L2-norm of the difference in gradients for different mesh sizes $N = [16, 32, 64, 128]$ with time step $\Delta t = 0.01$ and total time $T = 0.15$. A convergence of order $1/2$ is observed.}
    \label{fig:gradient_convergence}
\end{figure}


\cleardoublepage