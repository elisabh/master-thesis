%===================================== CHAP 3,1 =================================

\chapter{Fluid Flow Optimization}
\label{chapter:fluid_flow_optimization}

The optimization problem for the flow of a fluid through a channel is
the same as in the general case \ref{JF},
%
\begin{equation}
    \begin{aligned}
    \label{optimization}
    \textrm{Minimize } \quad &J(u, \alpha) & \\
    \textrm{subject to } \quad &F(u, \alpha) = 0 &\textrm{ on } \Omega,
    \end{aligned}
\end{equation}
where $J(u, \alpha)$ is a functional and $F(u, \alpha)$ is a set of equations. The parameter $u$ describes the movement of the fluid in the channel, and $\alpha$ is a control for fluid-solid distribution. Hence, in order to carry out the optimization, we need to calculate the movement of the fluid. Fluid motion can be described by the Navier-Stokes equations, which is an example of equations on the form $F(u, \alpha) = 0$. 
%Porous media flow is modelled with Darcy, and $\alpha$ is used to couple these equations. 
In order to incorporate the control $\alpha$, we will use the concept of porous media flow, which can be modelled by Darcy's equation. In the following section we will combine these two systems of equations.
At the end of this chapter, we will present a specific optimization problem, and a solution strategy for the problem. 




\section{Combining Navier-Stokes' and Darcy's Equations}

To describe the fluid flow, let us consider the unsteady Navier-Stokes equations for an incompressible fluid. 
%
\begin{align}
\label{NS}
    \frac{\partial \mathbf{u}}{\partial t}-\nu \Delta \mathbf{u} + (\mathbf{u} \cdot \nabla)\mathbf{u} + \frac{1}{\rho}\nabla p &= \mathbf{f} &\textrm{ in } \Omega,\\
    \nabla \cdot \mathbf{u} &= 0 &\textrm{ in } \Omega,
\end{align}
where $t \in (0, T)$ is the time, $\mathbf{u}$ is the velocity vector, $p$ is the pressure, $\nu$ is the kinematic viscosity, $\rho$ is the density, $\mathbf{f}$ is the body force and $\Omega$ is the domain on which the fluid flows. In addition we have suitable initial and boundary conditions, and these are discussed below. Because we want to model a fluid flow on subdomains with different materials, as proposed in Section \ref{material_distribution}, we need a way to describe this. The approach used here is to model certain subdomains as a porous medium using Darcy's equation,
%
\begin{align}
\alpha \mathbf{u} + \nabla p = \mathbf{0},
\end{align}
where $\alpha$ is the impermeability, or the inverse permeability, see \cite{nield2006cpm}. The term $\alpha$ is also called the Brinkman term after Brinkman \cite{ brinkman1947cvf}. The impermeability is a measure of the ability of a porous material to allow fluids to pass through it. Low impermeability gives rapidly moving fluid while high impermeability gives the opposite. In  \cite{borrvall2003tof, gersborg2005toc, deng2012tos} it is proposed to add an artificial friction force into the body force term in \eqref{NS}
 %
\begin{align}
\label{body_force}
    \mathbf{f} = - \alpha \mathbf{u}. 
\end{align}
The coupling of Darcy's law with the unsteady Navier-Stokes equation has been proved to have a unique solution by Cesmelioglu et al.\cite{cesmelioglu2013tdc}. The inclusion of the Darcy term $\alpha \mathbf{u}$ in the Navier-Stokes system allows for punishing counterproductive cells -- the central component of topology optimization  \cite{othmer2008caf}. In this way we can model a domain with fluid and solid subdomains simply by letting the impermeability $\alpha$ vary, as described in Section \ref{material_distribution}.


\section{The Complete Navier-Stokes-Darcy (NSD) system}
Including Dirichlet and Neumann boundary conditions on $\partial \Omega_D$ and $\partial \Omega_N$, respectively, the complete Navier-Stokes-Darcy system on strong form yields: find $(\mathbf{u}, p)$ such that
%
\begin{align}
\label{NS_Darcy}
    \frac{\partial \mathbf{u}}{\partial t}-\nu \Delta \mathbf{u} + (\mathbf{u} \cdot \nabla) \mathbf{u} + \alpha \mathbf{u} + \frac{1}{\rho}\nabla p &= \mathbf{0} &\textrm{ in } \Omega,\\
    \nabla \cdot \mathbf{u} &= 0 &\textrm{ in } \Omega, \\
    \mathbf{u} &= \mathbf{u}_D  &\textrm{ on } \partial \Omega_D, \label{dirichlet}\\
    \nu (\nabla \mathbf{u}) \cdot \mathbf{n}  - \frac{1}{\rho}p \mathbf{n} &= \mathbf{g}_N &\textrm{ on } \partial \Omega_N,
    \label{neumann} \\
    \mathbf{u}(t_0) &= \mathbf{u}_0 &\textrm{ in } \Omega,
    \label{NSD_last}
\end{align}
where $\mathbf{n}$ is the outward unit normal, $\mathbf{g}_N$ is the traction and $\mathbf{u}_0$ is the initial velocity at time $t_0$. We will from here on refer to \eqref{NS_Darcy} - \eqref{neumann} as the Navier-Stokes-Darcy system or the NSD system. 


In fluid flow modelling, the inflow velocity is commonly known, hence a Dirichlet boundary condition is applied to the inflow boundary. On the wall, a no-slip Dirichlet boundary condition with $\mathbf{u}_D = \mathbf{0}$ is applied. For the outflow, we either know the outflow velocity, like for instance in \cite{challis2009lst, borrvall2003tof, sa2016tda}, or if the outflow profile cannot be determined a priori, one can use a traction-free boundary condition, $\mathbf{g}_N = \mathbf{0}$, at the outlets \cite{guest2005toc, othmer2008caf, kreissl2011tou, deng2011tou}. This is  known as a do-nothing boundary condition \cite{quarteroni2014nmd}, as it naturally appears when doing integration by parts. In particular, this open boundary condition will be used on the outlets in the numerical experiments presented in Chapter \ref{numerical_experiments}.   



When solving partial differential equations using a finite element method, as we want here, it is necessary to set up the weak formulation and solve that instead. Then the equations are not required to hold absolutely, but has weak solutions with respect to certain test functions. For the weak form, let
%
\begin{align}
\label{function_spaces}
\mathbf{U} &= \{ \mathbf{u} \in H^1(\Omega;\mathbb{R}) :  \mathbf{u}|_{\partial \Omega_D} = \mathbf{u}_D, ~\mathbf{u}(t_0) = \mathbf{u}_0 \} \\
\mathbf{V} &= \{ \mathbf{u} \in H^1(\Omega;\mathbb{R}) : \mathbf{u}|_{\partial \Omega_D} = \mathbf{0} \} \\
Q &=  L^2(\Omega),
\end{align}
denote the velocity spaces and pressure space, respectively, and let the test functions be defined as $\mathbf{v} \in \mathbf{V}$ and $q \in Q$. The NSD problem in weak form with initial and boundary conditions then yields: find $\mathbf{u} \in \mathbf{U}$ and $p \in Q$ such that
%
\begin{align}
\label{NSD_weak}
    \int_{\Omega} \frac{\partial \mathbf{u}}{\partial t} \cdot \mathbf{v} + \nu \int_{\Omega} \nabla \mathbf{u} \cdot \nabla \mathbf{v}  + \int_{\Omega}(\mathbf{u} \cdot \nabla)\mathbf{u} \cdot \mathbf{v} + \int_{\Omega}\alpha \mathbf{u} \cdot \mathbf{v} \nonumber \\ - \int_\Omega \frac{1}{\rho} (\nabla \cdot \mathbf{v})p + \int_{\partial \Omega_N} p\mathbf{n} \cdot \mathbf{v} -\int_{\partial \Omega_N} \nu \mathbf{n} \cdot \nabla \mathbf{u} \cdot \mathbf{v}
&= 0 &\forall ~ \mathbf{v} \in \mathbf{V},
\\
    \int_\Omega (\nabla \cdot \mathbf{u})q &= 0   &\forall ~ q \in Q.
\end{align}
Note that we have included the Neumann boundary conditions \eqref{neumann} in the integral \eqref{NSD_weak}. We will later on use the weak form in the derivation of the topological derivative and in the numerical discretization. 




\section{The Optimization Problem}
\label{level_set_section}

The optimization problem is to minimize a specific functional $J$ in a fluid flow channel by optimizing the channel shape. Given inflow and outflow conditions on the boundary of a domain $\Omega$, the aim is to distribute fluid and solid material to obtain the optimal shape with respect to $J$. In addition, a constraint is introduced to put a bound on the total volume of fluid.


If $J$ is the cost functional to be minimized, the optimization problem can be stated in the same way as in \eqref{JF}:
%
\begin{equation}
    \begin{aligned}
    \label{minimize}
    \textrm{Minimize } \quad &J(\mathbf{u}, p, \alpha)  \\
    \textrm{subject to } \quad &\mathbf{F}(\mathbf{u}, p, \alpha) = \mathbf{0} &\textrm{ on } \Omega.
    \end{aligned}
\end{equation}
where $\mathbf{u}$ and $p$ stands for velocity and pressure, respectively, and  the impermeability parameter $\alpha$ serves the purpose of the design variable. $\mathbf{F}$ is the system of state equations, more specifically the incompressible Navier-Stokes-Darcy system,
%
\begin{align}
\label{F_NSD}
\mathbf{F}(\mathbf{u}, p, \alpha) = 
\begin{pmatrix}
    \displaystyle\frac{\partial \mathbf{u}}{\partial t}-\nu \Delta \mathbf{u} + (\mathbf{u} \cdot \nabla)\mathbf{u} + \alpha \mathbf{u} + \nabla p\\
    \nabla \cdot \mathbf{u} 
\end{pmatrix}.
\end{align}
We have left out the boundary and initial conditions for readability. These will be treated explicitly in each case. Now that we have formulated the equations needed to model our problem, we can proceed to the solution strategy for the optimization problem.


%\section{Solution Strategy for the Optimization Problem}


In order to solve the optimization problem, we will use the topological derivative of the objective functional $J$ incorporated in a gradient-based algorithm. The topological derivative describes the change in $J$ when the topology changes, and can be used as a steepest descent direction in order to minimize the objective function. The topological derivative is dependent on the movement of the fluid through $(\mathbf{u}, p)$, but also their adjoint states $(\mathbf{u}^a, p^a)$. Thus, we must solve both the Navier-Stokes-Darcy system and its adjoint system in each iteration. This will be described thoroughly in Chapter \ref{chapter_adjoint} and \ref{chapter_discretization}. 





%\section{Level Set Function}


The level set function, $\psi$, defines the state of the material distribution. It will, as defined in \eqref{level_set}, be non-positive in the fluid subdomain and positive in the solid subdomain. Hence, the impermeability parameter $\alpha$, describing the material constant, is directly dependent on $\psi$.  From the optimality condition \eqref{g_optimal}, we see that it is fulfilled if the gradient $g$ and $\psi$ coincide, i.e. $g = \tau \psi$ for some $\tau > 0$. Hence, in the fluid domain $\omega$ we wish to increase $\psi$ where $g > 0$, and in the solid domain $\Omega \backslash \bar{\omega}$ we wish to decrease $\psi$ where $g \le 0$. 

Defining the level-set function $\psi$ as proposed in \cite{amstutz2006nat}, we have the equation
%
\begin{align}
    \label{psi_diff}
    \frac{\partial \psi}{\partial t} = g - \langle g, \psi \rangle \psi
\end{align}
where the right hand side is the complement of the orthogonal projection of $g$ onto $\psi$. Here, and in further calculations, we refer to the  $L^2(\Omega)$ inner product when writing $\langle \cdot, \cdot \rangle$ and let $\| \cdot \|$ denote the corresponding norm. If $\psi$ tends to a stationary point, then \eqref{psi_diff} is zero, and the level-set function $\psi$ and $g$ must coincide. Hence we have a local optimum. 

Assume now that we can apply an Euler forward method in time to \eqref{psi_diff},
%
\begin{align}
\label{psi(P)}
    \psi_{i+1} &= \psi_i + \Delta t P_i,
\end{align}
where $P_i$ is the projection 
%
\begin{align}
    P_i &=  g_i - \langle g_i, \psi_i \rangle \psi_i.
\end{align}
Here we have denoted $\psi(t_{i}) = \psi_i$, and the same applies to $g_i$. We want the new level set function to be normalized, $\| \psi_{i+1} \| = 1$, so we can represent it by a trigonometric relation. Let
%
\begin{align}
    \theta_i = \arccos \left( \frac{\langle g_i, \psi_i \rangle}{\|g_i\|\|\psi_i \|}\right)
\end{align}
be the non-oriented angle between $\psi_i$ and $g_i$. For this value of $\theta_i$ there exists a step-size $\kappa_i \in [0,1]$ such that the new normalized level-set function can be written as
%
\begin{align}
\label{psi(cos)}
    \psi_{i+1} &= \cos (\kappa_i \theta_i) \psi_i + \sin (\kappa_i \theta_i) \frac{P_i}{\|P_i\|}, \\
    \|P_i\| &= \| g_i - \langle g_i, \psi_i \rangle \psi_i \| = \sin\theta_i \|g_i\|,
\end{align}
where $P_i$ is defined in \eqref{psi(P)}. Note here that we get rid of the explicit time dependency. Inserting the expression for $P_i$ into \eqref{psi(cos)} we obtain
%
\begin{align}
    \psi_{i+1} &= \left(\cos(\kappa_i \theta_i) - \frac{\cos\theta_i \sin(\kappa_i \theta_i)}{\sin\theta_i}\right) \psi_i + \frac{\sin(\kappa_i \theta_i)}{\sin\theta_i}\frac{g_i}{\|g_i\|} \\
    \label{new_psi}
    &= \frac{1}{\sin \theta_i} \left(\sin \left((1-\kappa_i)\theta_i\right)\psi_i + \sin(\kappa_i \theta_i)\frac{g_i}{\|g_i\|}\right).
\end{align}
We now have a way of updating the level-set function based on the topological gradient $g_i$, the angle $\theta_i$ between $g_i$ and the previous level-set function $\psi_i$, and a step size $\kappa_i$. 

Since the optimization problem is non-convex \cite{sa2016tda}, the solution is dependent on the initial state, and a global minimum is not guaranteed. When $\theta$ reaches below some numerical tolerance $\varepsilon_\theta$, the solution is considered optimal and the algorithm terminates. 
  

\section{Fluid Volume Penalization}

The solution strategy presented above has no constraints on the size of the fluid volume. In fluid flow channel design optimization, it is reasonable to introduce a linear penalty parameter $\beta$ on the fluid domain volume, such that the new functional to be minimized will be 
%
\begin{align}
    J(\mathbf{u}, p, \alpha) + \beta | \omega |,
\end{align}
with topological derivative
%
\begin{align}
D_T J(\mathbf{u}, p, \alpha) + k(x) \beta,
\end{align}
were the last term is collected from \eqref{fluid_derivative}. The topological derivative of a specific functional will be derived in Chapter \ref{chapter_adjoint}. One drawback with this approach is the introduction of an additional parameter $\beta$ specific for the optimization problem, which must be adjusted in every case. 




\section{Backtracking Line Search}
\label{line_search}

The step size $\kappa_i$ has to be determined for each iteration. A backtracking line search with the Armijo condition \cite{nocedal2006no} has been investigated, with the topological gradient $g$ as search direction. When using the topological derivative, this shows poor results, as the inner product $\langle g, g \rangle$ takes on a very large value, hence it is hard to find a descent direction fulfilling the Armijo condition. Instead we implement a line search where $\kappa_i$ is decreased until the objective function decreases, $J(\mathbf{u}, p, \alpha(\psi_{i+1})) < J(\mathbf{u}, p, \alpha(\psi_{i}))$. If the step size $\kappa_i$ reaches below some numerical tolerance $\varepsilon_\kappa$, the solution at a local minimum, and the algorithm will terminate.

One drawback of a backtracking line search is that it requires us to solve the Navier-Stokes-Darcy system for every update of $\kappa_i$, which can be very costly. Therefore, an approach of letting $\kappa_i = 1.0$ until a threshold $\theta < \theta_\kappa$ is obtained, can be considered, since according to \cite{sa2016tda}, the step size parameter generally starts to diminish only at the end of the iterative process, that is, when $\theta_i$ is small. It will be stated in each experiment in Chapter \ref{numerical_experiments} whether this approach is applied or not. The line search is inspired by \cite{amstutz2011als}, and is performed as follows: 



\begin{algorithm}[ht]
\begin{algorithmic}
\STATE Choose initial step $\kappa_0 = 1.0$
\STATE At iteration $i \ge 1$, update $\kappa_i = \textrm{min} (1.0, ~ 1.5\kappa_{i-1})$
\WHILE {$J(\psi_{i}) > J(\psi_{i-1})$}
    \STATE $\kappa_i \gets \kappa_i/2$
    \IF{$\kappa_i < \varepsilon_\kappa$} \STATE $J(\psi_{i})$ local minimum, terminate \ENDIF
    \STATE update $\psi_{i}$ according to \eqref{new_psi}
    \STATE solve the Navier-Stokes-Darcy system to obtain $\mathbf{u}$, $p$
\ENDWHILE
\end{algorithmic}
\caption{Backtracking Line Search}
\end{algorithm}



\section{Gradient-Based Algorithm}
\label{algorithm}

The optimization algorithm is a gradient-based method proposed in S{\'a} et al.\cite{sa2016tda}. In the numerical algorithm, the gradient $g$ defined by \eqref{gradient} will for the Navier-Stokes-Darcy system be
%
\begin{align}
    g(x) &= \textrm{sign}(\psi) ( D_T J(\mathbf{u}, p, \alpha) + k(x) \beta ) \\
    &= k(x) D_T J(\mathbf{u}, p, \alpha) + \beta,
\end{align}
since $\textrm{sign}(\psi) = k(x)$. $D_T J(\mathbf{u}, p, \alpha)$ will also be dependent on the adjoint states of the velocity and pressure, $\mathbf{u}^a$ and $p^a$, respectively, so both the primal and adjoint system must be solved in each iteration. To solve these systems, knowledge of $\alpha$ which depends on $\psi$ is necessary. The adjoint equations for the Navier-Stokes-Darcy system will be derived in Chapter \ref{chapter_adjoint}. Combining the solution strategy presented above with the fluid volume penalization and backtracking line search, the gradient-based algorithm can be stated as follows:


\begin{algorithm}[ht]
\begin{algorithmic}
\STATE Initialize $\psi_0$ s.t. $\|\psi_0\| = 1.0$, $\kappa_0 = 1.0$
\WHILE {$\theta > \varepsilon_{\theta}$}
    \STATE $\alpha_i \gets \alpha(\psi_i) $
    \STATE solve Navier-Stokes-Darcy system to obtain $\mathbf{u}, p$
    \STATE solve adjoint system to obtain $\mathbf{u}^a, p^a$
    \STATE $g_i \gets k(x) D_T J(\mathbf{u}, p, \mathbf{u}^a, p^a, \alpha) + \beta $
    \STATE $\theta \gets \arccos \left( \frac{\langle g_i, \psi_i \rangle}{\| g_i \| \|\psi \|} \right)$
    \STATE $\kappa_i \gets$ backtracking line search
    \STATE $\psi_{i+1} \gets \frac{1}{\sin \theta_i} \left(\sin \left((1-\kappa_i)\theta_i\right)\psi_i + \sin(\kappa_i \theta_i)\frac{g_i}{\|g_i\|}\right)$
\ENDWHILE
\end{algorithmic}
\caption{Gradient-Based Algorithm}
\end{algorithm}


The implementation of the gradient-based algorithm can be found in Appendix \ref{gradient_based}.


\cleardoublepage