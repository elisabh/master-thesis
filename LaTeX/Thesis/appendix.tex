\section*{\begin{center}{\Huge Appendix}\end{center}}
\addcontentsline{toc}{chapter}{Appendix}
$\\[0.5cm]$

\appendix

\chapter{Numerical Implementation}
\label{numerical_implementation}

\section{Navier-Stokes-Darcy Solver}


\begin{minted}[frame=single, fontsize=\footnotesize]{python}
from dolfin import *

class NSDSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries
        self.T = problem.T
        self.dt = problem.dt

    def solve_IPCS(self, psi):

        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        # Define trial and test functions
        u = TrialFunction(V)
        p = TrialFunction(Q)
        v = TestFunction(V)
        q = TestFunction(Q)

        u0 = Function(V, name='u0')
        u1 = Function(V, name='u1')
        p0 = Function(Q, name='p0')
        p1 = Function(Q, name='p1')

        k = Constant(self.dt)
        
        # Tentative velocity step
        F1 = (1. / k) * inner(u - u0, v) * dx \
            + inner(grad(u0) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + inner(grad(p0), v) * dx \
            + alpha(psi) * inner(u, v) * dx \
            - nu * inner(dot(n, nabla_grad(u)), v) * ds(3) \
            + inner(p0 * n, v) * ds(3)

        a1 = lhs(F1)
        L1 = rhs(F1)

        # Pressure update
        a2 = inner(grad(p), grad(q)) * dx
        L2 = inner(grad(p0), grad(q)) * dx \
            - ((1. / k) + alpha(psi)) * div(u1) * q * dx

        # Velocity update
        a3 = (1. / k + alpha(psi)) * inner(u, v) * dx
        L3 = (1. / k + alpha(psi)) * inner(u1, v) * dx \
            - inner(grad(p1 - p0), v) * dx

        # Assemble system
        A1 = assemble(a1)
        A2 = assemble(a2)
        A3 = assemble(a3)

        b1 = None
        b2 = None
        b3 = None

        t = 0.0
        
        # Save forward solution
        u_list = []

        while t < self.T:
            bcu, bcp = problem.boundary_conditions(V, Q, t)
            t += self.dt

            # Tentative velocity
            b1 = assemble(L1, tensor=b1)
            [bc.apply(A1, b1) for bc in bcu]
            solve(A1, u1.vector(), b1)

            # Pressure correction
            b2 = assemble(L2, tensor=b2)
            [bc.apply(A2, b2) for bc in bcp]
            solve(A2, p1.vector(), b2)

            # Velocity update
            b3 = assemble(L3, tensor=b3)
            [bc.apply(A3, b3) for bc in bcu]
            solve(A3, u1.vector(), b3)

            # Ensure deep copy:
            u_temp = Function(V)
            u_temp.assign(u1)
            u_list.append(u_temp)

            u0.assign(u1)
            p0.assign(p1)

        return u_list
\end{minted}


\section{Adjoint Solver}
\label{adjoint_solver}

\begin{minted}[frame=single, fontsize=\footnotesize]{python}

from dolfin import *

class AdjointSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.boundaries = problem.adjoint_boundaries
        self.T = problem.T
        self.dt = problem.dt

    def solve_coupled(self, psi, u_list):
    
        # psi is the level set function
        # u_list contains forward solution

        # Adjoint coupled:
        P2 = VectorElement('P', 'triangle', 2)
        P1 = FiniteElement('P', 'triangle', 1)
        TH = P2 * P1
        W = FunctionSpace(self.mesh, TH)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        (u, p) = TrialFunctions(W)
        (v, q) = TestFunctions(W)
        w = Function(W)
        (ua, pa) = split(w)

        # in order to assign w.sub(0) to u_a
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        assigner = FunctionAssigner(V, W.sub(0)) 
        
        # Adjoint system with Neumann boundary conditions
        F_a = (1. / k) * inner(u - ua, v) * dx \
                  - inner(dot(grad(u), u0), v) * dx \
                  - inner(dot(u0, grad(u)), v) * dx \
                  + nu * inner(grad(u), grad(v)) * dx \
                  + alpha(psi) * inner(u, v) * dx \
                  - div(u) * q * dx - div(v) * p * dx \
                  - 2 * nu * inner(grad(u0), grad(v)) * dx \
                  - 2 * alpha(psi) * inner(u0, v) * dx \
                  - inner(p * n, v) * ds(3) \
                  + nu * inner(dot(n, grad(u)), v) * ds(3) \
                  + inner(dot(u0, u) * n, v) * ds(3) \
                  + inner(dot(u0, n) * u, v) * ds(3)
            a = lhs(F_a)
            L = rhs(F_a)
        
        # save adjoint solution
        ua_list = []

        # Constant Dirichlet boundary conditions
        bcu = problem.adjoint_boundary_conditions(V)

        k = Constant(self.dt)
        t = self.T

        for u0 in reversed(u_list):
            t -= self.dt

            problem = LinearVariationalProblem(a, L, w, bcu)
            solver = LinearVariationalSolver(problem)
            solver.solve()
            (ua, pa) = split(w)

            # ensure deep copy
            u_a = Function(V)  
            assigner.assign(u_a, w.sub(0))
            ua_list.append(u_a)

        # return reversed ua_list such that it has the same ordering
        # as u_list
        return ua_list[::-1]
\end{minted}


\section{Gradient-Based Method}
\label{gradient_based}

\begin{minted}[frame=single, fontsize=\footnotesize]{python}

from dolfin import *

class GradientMethod:

    def __init__(self, problem):
        self.problem = problem
        self.V0 = FunctionSpace(problem.mesh, "CG", 1)
        self.PSI = "-1"
        self.n_max = 20
        self.tol_theta = 0.01
        self.tol_kappa = 0.001

    def functional(self, psi, u_list):
        J = 0.0
        N = len(u_list)
        for u in u_list:
            J += problem.dt * assemble(alpha(psi) * inner(u, u) * dx
                                      + nu * inner(grad(u), grad(u)) * dx)
        J += assemble(self.beta * fluid_domain(psi) * dx)
        return J

    def gradient(self, g, u_, ua_):
        N = len(u_)
        g.vector().zero()
        for i in range(N):
            u = u_[i]
            ua = ua_[i]
            direction = - (ALPHA_U - ALPHA_L) * inner(u, u - ua)
            g_temp = project(direction + self.beta, self.V0)
            g.vector().axpy(1./N, g_temp.vector())

    def angle(self, g, psi):
        dot_prod = assemble(dot(g, psi) * dx)
        nrm_g = sqrt(assemble(dot(g, g) * dx))
        nrm_psi = sqrt(assemble(dot(psi, psi) * dx))
        return acos(dot_prod / (nrm_g * nrm_psi))

    def new_psi(self, psi, kappa, theta, psi_ref, g):
        nrm_g = sqrt(assemble(dot(g, g) * dx))
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
        k2 = sin(kappa * theta) / (sin(theta) * nrm_g)
        psi.vector().zero()
        psi.vector().axpy(k1, psi_ref.vector())
        psi.vector().axpy(k2, g.vector())

    def run_gradient_method(self):

        kappa = 1.0

        # Design parameter
        psi_ref = project(Expression(self.PSI, degree=1), self.V0)
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        psi = project(Expression(self.PSI) / nrm_psi, self.V0)
        g = Function(self.V0)

        NSD = NSDSolver(self.problem)
        adjoint = AdjointSolver(self.problem)

        u_list = NSD.solve_IPCS(psi)
        ua_list = adjoint.solve_coupled(psi, u_list)

        J = self.functional(psi, u_list)
        
        # Iterate until convergence
        for n in range(self.n_max):
        
            print "Iteration #: " + str(n+1)

            self.gradient(g, u_list, ua_list)
            theta = self.angle(g, psi)
            if theta < self.tol_theta:
                print "Theta smaller that tolerance!"
                break

            kappa = min(1.0, kappa*1.5)

            # Store old psi and update new psi
            psi_ref.assign(psi)
            self.new_psi(psi, kappa, theta, psi_ref, g)

            # Solve forward and adjoint system
            u_list = NSD.solve_IPCS(psi)
            ua_list = adjoint.solve_coupled(psi, u_list)

            # calculate new functional
            J_new = self.functional(psi, u_list)


            # Line search
            while J_new > J and kappa > self.tol_kappa:
                # Did not find smaller J, decreasing kappa."
                kappa = kappa*0.5
                
                # Update psi and calculate new J
                self.new_psi(psi, kappa, theta, psi_ref, g)
                u_list = NSD.solve_IPCS(psi)
                J_new = self.functional(psi, u_list)
                
            if J_new < J:
                J = J_new
            else:
                # Did not find smaller J, terminating
                return psi, J
                
        return psi, J
        
\end{minted}



\section{Implementation with dolfin-adjoint}
\label{dolfin-adjoint}

\begin{minted}[frame=single, fontsize=\footnotesize]{python}
from dolfin import *
from dolfin_adjoint import *

class NSDsolver:

    def __init__(self, problem):
        self.problem = problem
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries

    def solve(self, psi):

        adj_reset()
        timestep = problem.dt

        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        # Initial velocity and pressure
        u0, p0 = problem.initial_values()

        # Declare velocity functions
        u = Function(V, name="u")
        v = TestFunction(V)

        # Weak form for tentative velocity
        F1 = (1./timestep) * inner(u - u0, v) * dx \
            + inner(grad(u) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + alpha(psi) * inner(u, v) * dx \
            + inner(grad(p0), v) * dx \
            - nu * inner(dot(n, nabla_grad(u)), v) * ds(2) \
            + inner(p0 * n, v) * ds(2)


        # Declare pressure functions 
        p = Function(Q, name="p")
        q = TestFunction(Q)

        # Pressure correction weak form
        F2 = inner(grad(p - p0), grad(q)) * dx \
             + (1. / timestep) * div(u0) * q * dx


        # Velocity update weak form
        F3 = (1./timestep)*inner(u - u0, v) * dx \
             + inner(grad(p - p0), v) * dx

        t = 0.0
        psi_tmp = psi.copy(deepcopy=True)

        adj_time = True
        annotate = True
        adj_start_timestep()

        while t < problem.T:

            # u and p are trial functions
            # u0 and p0 are newest value

            # Boundary conditions, Pousille flow at inlet
            bcu, bcp = problem.boundary_conditions(V, Q, t)

            solve(F1 == 0, u, bcu)
            u0.assign(u, annotate=annotate)
            solve(F2 == 0, p, bcp)
            solve(F3 == 0, u, bcu)

            psi.assign(psi_tmp, annotate=annotate)
            u0.assign(u, annotate=annotate)
            p0.assign(p, annotate=annotate)

            # plot(u0, interactive=True)

            t += float(timestep)
            if adj_time:
                adj_inc_timestep(t, t > problem.T)

        # dolfin-adjoint only needs the last solution of u and p
        return u0, p0
        
        
        
class GradientMethod:
    def __init__(self, problem):
        self.problem = problem
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries
        self.V0 = FunctionSpace(self.mesh, "CG", 1)
        self.PSI = "-1"
        self.n_max = 20
        self.kappa_tol = 0.001

    def new_psi(self, kappa, psi, g):

        dot_prod = assemble(dot(g, psi) * dx)
        nrm_g = sqrt(assemble(dot(g, g) * dx))
        nrm_psi = sqrt(assemble(dot(psi, psi) * dx))

        theta = acos(dot_prod / (nrm_g * nrm_psi))
        k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
        k2 = sin(kappa * theta) / (sin(theta) * nrm_g)

        psi = project(k1 * psi - k2 * g, self.V0, annotate=False)
        return psi

    def run_gradient_method(self):

        ds = Measure("ds")(subdomain_data=self.boundaries)
        kappa = 0.5

        # Design parameter
        psi_ref = project(Expression(self.PSI, degree=1), self.V0)
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        psi = project(Expression(self.PSI, degree=2) / nrm_psi, 
                      self.V0, annotate=True)

        # Initialize problem and Navier-Stokes-Darcy solver
        nsd = NSDsolver(self.problem)
        u, p = nsd.solve(psi)

        # Dolfin-adjoint functional
        J = Functional((p * ds(1) - 3. * p * ds(2)) * dt
                       + beta * fluid_domain(psi) * dx * dt)

        for i in range(self.n_max):
            print "\nIteration #" + str(i+1)

            g = compute_gradient(J, Control(psi), forget=False)

            # Store old values of J and psi:
            Jm = ReducedFunctional(J, Control(psi))
            J_old = Jm(psi)
            psi_ref.assign(psi)

            # Update psi and calculate new J
            kappa = min(0.5, kappa * 1.5)
            psi = self.new_psi(kappa, psi_ref, g)
            u, p = nsd.solve(psi)

            J = Functional((p * ds(1) - 3. * p * ds(2)) * dt
                           + beta * fluid_domain(psi) * dx * dt)
            Jm = ReducedFunctional(J, Control(psi))
                
        return psi, J

\end{minted}