%===================================== CHAP 5 =================================


\chapter{Numerical Experiments}
\label{numerical_experiments}

In this chapter, numerical experiments are presented to demonstrate the features and utility of the proposed topological optimization algorithm for unsteady Navier-Stokes-Darcy flow. In particular, two examples from Deng et al.\cite{deng2011tou} and Kreissl et al.\cite{kreissl2011tou} are implemented to verify the topological optimization method for unsteady flow. At the end of this chapter, the optimization method is applied to a biomedical flow problem to optimize the topology of a coronary artery bypass anastomosis. The exact topological derivative for dissipation, see Section \ref{topological_derivative_dissipation}, and the topological gradient computed using dolfin-adjoint \cite{farrell2013ada}, will be applied in the experiments. It will be specified in each case which one is used. Listings of the code can be found in Appendix \ref{numerical_implementation}.

The values of the impermeability constants are set to $\alpha_L = 2.5 \nu/100^2$ and $\alpha_U = 2.5 \nu/0.01^2$, as discussed in Section \ref{numerical_verification}.  The stopping criterion on the angle $\theta$, presented in Section \ref{algorithm}, is $\varepsilon_\theta = 0.01$. For the line search in Section \ref{line_search}, the step size tolerance is set to $\varepsilon_\kappa = 0.0001$.

At the end of every experiment, a discussion of the results is included, and a summary of the discussions is presented in Chapter \ref{conclusion}.
%
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/doublechannel01}
    \caption{Design domain for the double pipe problem}
    \label{fig:doublepipe_design}
\end{figure}

\section{Double Pipe with Analytical Topological Derivative}
\label{double_pipe_section}

%
The double pipe optimization problem with varying inflow condition is proposed in \cite{deng2011tou}, and will be reconstructed here in order to compare the resulting topology. The design domain is shown in Figure \ref{fig:doublepipe_design}, and the mesh consists of $43 \times 43$ triangular elements, see Figure \ref{fig:doublepipe_mesh}. The kinematic viscosity is $\nu = 1$, the density $\rho = 1$ and the velocities imposed at the two inlets are time-varying, given by
%
\begin{align}
    \mathbf{u}_1 = -144 (y - 4/6)(y - 5/6) \cos (t) \mathbf{n}, \\
    \mathbf{u}_2 = -144 (y - 1/6)(y - 2/6) \sin (t) \mathbf{n},
\end{align}
with $t \in [0, 2\pi]$. At the outlets, traction free Neumann boundary conditions are imposed, $\mathbf{g} = \mathbf{0}$, see \eqref{neumann}. The time step used is $\Delta t = 0.00002$. 
%
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.3\textwidth]{figures/doublepipe_mesh_02}
    \caption{Mesh for the double pipe problem.}
    \label{fig:doublepipe_mesh}
\end{figure}


The objective functional used in this case is the same as in equation \eqref{J_dissip}, with an extra term for fluid volume penalization
%
\begin{equation}
\label{J_dissipation(t)}
{J}(\mathbf{u}, \alpha) = \int_0^T \int_\Omega \left( \nu |\nabla \mathbf{u}|^2 + \alpha |\mathbf{u}|^2 \right) \mathrm{d}t + \beta |\omega |,
\end{equation}
where $|\omega|$ denotes the fluid volume. The penalty parameter $\beta$ is set to $200$, and the gradient is the topological derivative in equation \eqref{topological_derivative_dissipation}. In order to increase the accuracy of the adjoint solution, a coupled solver is used, see Appendix \ref{adjoint_solver} for the implementation. The impermeability parameter $\alpha$ is not interpolated, corresponding to a penalty value of $q = 0$ in equation \eqref{alpha_interpolation}, since the exact topological derivative does not require it to be differentiable. In other words, the material distribution is discontinuous, which can be observed in the resulting shape. 

The stopping criterion for the optimization procedure, is that the angle $\theta$ reaches below the tolerance $\varepsilon_\theta = 0.01$. The optimization algorithm terminated after $20$ iterations, and some snapshots of the optimization procedure are presented in Figure \ref{fig:psi_doublepipe}. The evolvement of the objective function $J$ and angle $\theta$ are shown in Figure \ref{fig:doublepipe_objective}.
%

\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figures/t01}
        \caption{Iteration 1}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figures/t05}
        \caption{Iteration 5}
    \end{subfigure}
    ~ 
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figures/t07}
        \caption{Iteration 7}
    \end{subfigure}
    ~
    
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figures/t09}
        \caption{Iteration 9}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figures/t13}
        \caption{Iteration 13}
    \end{subfigure}
    ~ 
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figures/t20}
        \caption{Iteration 20}
    \end{subfigure}
    \caption{Optimization procedure for double pipe problem.}
    \label{fig:psi_doublepipe}
\end{figure}
%
\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/objective_theta}
    \caption{Evolvement of objective function $J$ and angle $\theta$ in the optimization procedure for the double pipe problem.}
    \label{fig:doublepipe_objective}
\end{figure}


The optimal design is presented together with the optimal design proposed in \cite{deng2011tou}, in Figure \ref{fig:doublepipe_compare}, and the values for the objective functional $J$ and fluid fraction $\gamma$ of these are presented in Table \ref{tab:doublepipe}. The values of $J$ come from eq. \eqref{J_dissipation(t)} and Table 2 in \cite{deng2011tou}. 
%
\begin{table}[H]
    \caption{Functional value $J$ and fluid fraction $\gamma$ for the optimal design and design in \cite{deng2011tou} for the double pipe problem.}
    \centering
    \begin{tabular}{l c c}
    \hline
        & $J$  & $\gamma$ \\ \hline 
        Fig. \ref{fig:dp_our}& 111.3  & 0.3797 \\
        Fig. \ref{fig:dp_deng}  & 113.8  & 0.3333 \\ \hline
    \end{tabular}
    \label{tab:doublepipe}
\end{table}
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{figures/t20}
        \caption{Optimal design.}
        \label{fig:dp_our}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{figures/deng_optimal}
        \caption{Optimal design in \cite{deng2011tou}.}
        \label{fig:dp_deng}
    \end{subfigure}
    
    \caption{Optimal designs for the double pipe problem.}
    \label{fig:doublepipe_compare}
\end{figure}


The two optimal shapes presented in Figure \ref{fig:doublepipe_compare} have been found using different optimization algorithms, and the most obvious difference is the smoothness, or lack of, in the material distribution. Nevertheless, the shapes are quite similar, and the objective values in Table \ref{tab:doublepipe} likewise, so it is reasonable to assume that the gradient-based algorithm with the exact topological derivative as gradient is capable of optimizing a shape for unsteady fluid flow. 



\section{Diffuser with Approximate Topological Derivative}


The diffuser optimization problem, originally from Borrvall and Peterson \cite{borrvall2003tof}, is proposed for unsteady flow in Kreissl et al.\cite{kreissl2011tou}, and will be reconstructed here in order to compare the optimization algorithms. The design domain is a unit square, $[0, 1] \times [0, 1]$, shown in Figure \ref{fig:diffuser_domain}, and the mesh consists of $42 \times 42$ triangular elements. The kinematic viscosity is $\nu = 0.01$, the density $\rho = 1.0$ and the velocity at the inlet is time dependent and parabolic shaped, given by
%
\begin{align}
    \mathbf{u}_{1}(t) = \sin \left( \frac{\pi}{2T}t \right)^2(1 - (2y-L)^2) \mathbf{n} 
\end{align}
where the inlet length $L=1.0$ in our case. At the outlet, traction-free Neumann boundary conditions are imposed, $\mathbf{g} = \mathbf{0}$, see \eqref{neumann}. The time step used is $\Delta t = 0.0001$, and total time $T = 15 \Delta t$, as suggested in \cite{kreissl2011tou}. 
%
\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/diffuser_domain}
    \caption{Computational domain for the diffuser problem.}
    \label{fig:diffuser_domain}
\end{figure}

The objective functional is the pressure difference between the inlet and outlet, and the fluid volume penalization term,
%
\begin{align}
    {J}(\mathbf{u}, \alpha) = \int_0^T p_{in}(t) - p_{out}(t)  \mathrm{d}t + \beta |\omega(\psi)|,
\end{align}
where $p_{in}(t)$ is the pressure averaged spatially over the inlet and correspondingly for the outlet in $p_{out}$. The gradient is computed using the dolfin-adjoint python call
\begin{minted}[frame=none, fontsize=\footnotesize]{python}
J = Functional(( p * ds(inlet) - p * ds(outlet) / (1./3)) * dt
               + beta * fluid_domain(psi) * dx * dt)
g = compute_gradient(J, Control(psi))
\end{minted}
where $\mathtt{ds(inlet)}$ and $\mathtt{ds(outlet)}$ are the inlet and outlet boundary measures, respectively. The factor $1/3$ comes from the averaging over the outlet length, see Figure \ref{fig:diffuser_domain}. The penalty parameter $\beta$ is set to $95.0$. Because we use dolfin-adjoint for the gradient computation, we need the impermeability on the domain to be differentiable. Hence, we apply an interpolation of $\alpha$ described in \eqref{alpha_interpolation}, with penalty parameter $q = 0.1$. Another observation, is that the optimization algorithm is unstable if the step size $\kappa = 1.0$ is used, so the backtracking line search algortihm, see Section \ref{line_search}, is applied in every iteration to find a decrease in the functional $J$. 
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figures/diff_01}
        \caption{Iteration 1}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figures/diff_02}
        \caption{Iteration 2}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.25\textwidth}
        \includegraphics[width=\textwidth]{figures/diff_15}
        \caption{Iteration 15}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.05\textwidth}
        \includegraphics[width=\textwidth]{figures/diff_bar}
        \caption{ }
    \end{subfigure}
    \caption{Optimization procedure for diffusion problem.}
    \label{fig:alpha_diff}
\end{figure}

The impermeability distribution in the two first and last iterations are presented in Figure \ref{fig:alpha_diff}. The evolvement of the objective function $J$ and angle $\theta$ is shown in Figure \ref{fig:diff_J}. As $\theta$ did not converge to a tolerance $\varepsilon_\theta$ when applying the numerically computed gradient, insufficient change in the functional $\|J_{i+1} - J_i \|_{L^2} < \varepsilon_J = 0.01$ was used as stopping criterion. The optimization algorithm terminated after $15$ iterations. 
%
\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/diffuser_Z_theta}
    \caption{Evolvement of objective function $J$ and angle $\theta$ in the optimization procedure for the diffuser problem.}
    \label{fig:diff_J}
\end{figure}
%
The optimal design is presented together with the optimal design from \cite{kreissl2011tou} in Figure \ref{fig:diffuser_compare}, and the values for the objective functional $\tilde{J}$ and fluid fraction $\gamma$ of these are presented in Table \ref{tab:diffuser}. The values of $\tilde{J}$ are computed by the formula 
%
\begin{align}
\label{kreissl_functional}
    \tilde{J} = \frac{1}{N_t} \sum_{n=1}^{N_t} (p^{\textrm{tot}}_{\textrm{in}}(t^n) - p^{\textrm{tot}}_{\textrm{out}}(t^n)),
\end{align}
where $N_t = 15$ is the number of time steps, and the inlet and outlet pressures, $p^{\textrm{tot}}_{\textrm{in}}$ and $p^{\textrm{tot}}_{\textrm{out}}$ are averaged spatially over the inlet and outlet ports. \eqref{kreissl_functional} is collected from equation (35), and the value of the objective function is obtained from Table III, both in \cite{kreissl2011tou}.

The two optimal shapes presented in Figure \ref{fig:diffuser_compare} are computed with the same interpolation penalty for the impermeability $\alpha$, but in Figure \ref{fig:diff_kreissl} a fluid layer has been added in order to resolve the unsteady flow close to the inlet. This was not done in our case, and in addition our mesh has twice as many elements, due to the fact that FEniCS does not support quadrilateral elements, only simplices. The objective values presented in Table \ref{tab:diffuser} are highly different, and must be a result of different ways of averaging the pressure. The author has so far not assimilated how this has been calculated in \cite{kreissl2011tou}. The gradient-based algorithm with a numerically computed gradient (using dolfin-adjoint), does not behave in the same way as with the analytical topological gradient, as it is unstable for step size $\kappa = 1.0$. A backtracking line search is applied in order to ensure sufficient decrease in the objective $J$, but the algorithm performes better with a constant step size $\kappa < 1$. Combining a default step size of $\kappa = 0.5$ with a backtracking line search show better results in few iterations, and is applied to the algorithm when using the numerically computed gradient. In addition, the optimality criterion $\theta$ did not behave as expected when using the numerically computed gradient, see Figure \ref{fig:diff_J}. This is another indication that the numerical gradient is not fully a descent direction, and caution should be made when applying this. Further work should investigate how the gradient behaves during mesh refinement. The advantage of using the numerically computed gradient, is that it is not necessary to derive the topological derivative when applying different objective functionals.

%
\begin{figure}[H]
    \centering
        \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{figures/diff_15}
        \caption{Optimized diffuser design.}
        \label{fig:diff_our}
    \end{subfigure}
    \begin{subfigure}[b]{0.335\textwidth}
        \includegraphics[width=\textwidth]{figures/kreissl_diffuser_channel}
        \caption{Diffuser design from \cite{kreissl2011tou}.}
        \label{fig:diff_kreissl}
    \end{subfigure}
    \caption{Optimal designs for the diffuser problem.}
    \label{fig:diffuser_compare}
\end{figure}
%
\begin{table}[H]
\caption{Functional value $J$ and fluid fraction $\gamma$ for the optimal design and design in \cite{kreissl2011tou} for the diffuser problem.}
    \centering
    \begin{tabular}{l c c}
    \hline
        & $\tilde{J}$  & $\gamma$ \\ \hline 
        Fig. \ref{fig:diff_our}  & 576.36 & 0.527 \\
        Fig. \ref{fig:diff_kreissl}  & 36 400  & 0.500  \\ \hline
    \end{tabular}
    \label{tab:diffuser}
\end{table}
%

%IF TIME: Run diffuser with constant step size $\kappa=0.5$ or $\kappa=0.3$. Did not work!

\section{Coronary Artery Bypass Anastomosis Models}
\label{bypass_section}

The gradient-based optimization algorithm is applied to a coronary artery bypass anastomosis with the purpose of finding an optimal shape. We refer the reader to Section \ref{medical_background} for background information and terminology on coronary artery bypass anastomoses.

In the literature, Quarteroni, Rozza and Agoshov \cite{quarteroni2003ocs, rozza2005ocs, agoshkov2006sda, agoshkov2006mad} have investigated the reduction in vorticity when optimizing a known shape of the toe of the bypass. Since we are dealing with topology optimization, we have no prior information about the shape of the bypass. This opens up for the possibility to optimize the whole domain of the bypass, starting with just the obstructed artery. Instead of looking at the relative reduction of vorticity, as we have no reference shape, we will investigate the optimal shapes with respect to minimizing different objective functionals, including vorticity, for different domains.

The inlet velocity used in the bypass experiments is based on data from \cite{thomas2005vcb}, and is plotted in Figure \ref{fig:blood_flow}, with a time period of $T = 0.8$ seconds, which corresponds to a pulse of 75 beats per minute. Notice that the velocity is negative at around $t = 0.1$ $s$. 
%
\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/blood_flow}
    \caption{Velocity profile for the left anterior descending coronary artery. Data taken from \cite{thomas2005vcb}.}
    \label{fig:blood_flow}
\end{figure}

The kinematic viscosity is $\nu = 0.04$ $\mathrm{cm^2/s}$, the blood density $\rho = 1.0$ $\mathrm{g/cm^{3}}$, and the arterial diameter is $D = 0.35$ $\mathrm{cm}$ \cite{cole2002ith, agoshkov2006sda}. The mean velocity is set to $\bar{u} = 20.0$ $\mathrm{cm/s}$, which corresponds to a Reynolds number $Re = \bar{u} \cdot D / \nu$ of $175$. This is reasonable according to \cite{leuprecht2002nsh}.

In order to model a bypass, we have made some assumptions. The design domain for the bypass is set to $L = 6.0$ $\mathrm{cm}$, and the inlet velocity has a Poiseuille profile. We assume that the artery is fully occluded. The fluid penalization parameter $\beta$ is affected by the size of the domain, velocity magnitude and kinematic viscosity, and will be adjusted in every case such that the bypass channel is sufficiently narrow. For the material distribution $\alpha$, we have chosen a penalty factor $q = 0.1$. 

We will consider the energy dissipation functional
\begin{equation}
\label{J_dissipation_bypass}
{J}_1(\mathbf{u}, \alpha) = \int_0^T \int_\Omega \left( \nu |\nabla \mathbf{u}|^2 + \alpha |\mathbf{u}|^2 \right) \mathrm{d}t + \beta |\omega |,
\end{equation}
and the vorticity functional
\begin{equation}
\label{J_vorticity_bypass}
{J}_2(\mathbf{u}, \alpha) = \int_0^T \int_\Omega \left( \nu |\nabla \times \mathbf{u}|^2 + \alpha |\mathbf{u}|^2 \right) \mathrm{d}t + \beta |\omega |,
\end{equation}
where $| \omega |$ is the volume of the fluid in the design domain. For efficient switching between functionals, all gradients are computed numerically using dolfin-adjoint \cite{farrell2013ada}.


\subsection{Simple Bypass Model}

A simple model, illustrated in Figure \ref{fig:simple_bypass}, is implemented for the bypass problem. The area under the dashed line represents the coronary artery, and the area above is the design domain. A rectangle part of width $0.2~ \mathrm{cm}$ and height $0.35~ \mathrm{cm}$ is removed where the occlusion is. The time-dependent inlet velocity presented in Figure \ref{fig:blood_flow} is applied with a Poiseuille profile at the inlet, and a traction-free boundary condition is imposed at the outlet. For every iteration, the level set function in the coronary artery sub-domain is enforced to have negative value, ensuring fluid representation in this area. The mesh for this model is created using \texttt{gmsh} \cite{geuzaine2009gmsh}, with a spatial step size of 0.05 cm around the occlusion, and 0.1 cm elsewhere, in total 4534 cells. Figure \ref{fig:artery_custom} shows an illustration of the mesh. 
%
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{figures/bypass_simple}
    \caption{Illustration of the simple bypass model.  All spatial measures are in $\mathrm{cm}$.}
    \label{fig:simple_bypass}
\end{figure}
%
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figures/artery_02_custom}
    \caption{Grid for the simple bypass model.}
    \label{fig:artery_custom}
\end{figure}

A preliminary experiment for the energy dissipation functional \eqref{J_dissipation_bypass} with $10 \%$ of the inlet velocity magnitude was implemented, corresponding to a Reynolds number of $Re = 17.5$. Figure \ref{fig:bypass_10percent} shows the optimal shape. A small opening around the occlusion is observed, although little fluid movement is detected in the area. The opening is suspected to come from the update of the level set function in the coronary artery domain at every iteration, thus affecting the topological gradient. Experiments without the manual level-set update were tested without satisfactory results, see Section \ref{further_work} for a suggestion on further investigation. 
%
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/alpha_bypass_long_14_14}
    \caption{Optimal shape for the bypass problem with respect to the energy dissipation functional, with 10 \% of the velocity magnitude and $\beta = 10.0$.}
    \label{fig:bypass_10percent}
\end{figure}

The simple model for the bypass was implemented for the energy dissipation functional with mean velocity $\bar{u} = 20.0$. The penalty parameter used was $\beta = 500.0$ and the time step size $\Delta t = 0.002$. The optimal shape was found after 20 iterations, and is presented in Figure  \ref{fig:alpha_bypass_08}, together with an illustration of the fluid flow at end time $T = 0.8$ s and the optimal shape with the mesh. It is easily observed that the optimal shape in Figure \ref{bypass_08} differs from the one in Figure \ref{fig:bypass_10percent} with lower Reynolds number. This observation enhances the importance of implementing a model that represents the actual physical properties of the system with the correct scaling of variables. 
%
\begin{figure}[H]
    \centering    
    \begin{subfigure}[b]{0.7\textwidth}
        \includegraphics[width=\textwidth]{figures/bypass_plain_08}
        \caption{Optimal shape iterations.}
        \label{bypass_08}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.73\textwidth}
        \includegraphics[width=\textwidth]{figures/bypass_arrows_08}
        \caption{Fluid flow in optimal shape at time $T = 0.8$ s.}
    \end{subfigure}
    \begin{subfigure}[b]{0.7\textwidth}
        \includegraphics[width=\textwidth]{figures/bypass_grid_08}
        \caption{Optimal shape with mesh.}
    \end{subfigure}
    \caption{Optimial shape for the simple bypass model with respect to the energy dissipation functional \eqref{J_dissipation_bypass}.}
    \label{fig:alpha_bypass_08}
\end{figure}


%\subsection{Simple Bypass Model with Vorticity Functional}


The simple model for the bypass was also implemented for the vorticity functional \eqref{J_vorticity_bypass} with mean velocity $\bar{u} = 20.0$. The penalty parameter used was $\beta = 500.0$ and the time step size $\Delta t = 0.002$. The optimal shape was found after 20 iterations, and is presented in Figure \ref{fig:alpha_bypass_10} with the fluid flow at end time $T = 0.8$ s.

%
\begin{figure}[ht]
    \centering    
        \includegraphics[width=0.75\textwidth]{figures/bypass_arrows_11}
        \caption{Fluid flow at time $T = 0.8$ s in optimal shape for the simple bypass model with respect to the vorticity functional \eqref{J_vorticity_bypass}.}
    \label{fig:alpha_bypass_10}
\end{figure}

The optimal shapes for the two functionals are very similar. 
Figure \ref{fig:bypass_objective_simple} shows the functional values for the energy dissipation functional $J_1$ and the vorticity functional $J_2$ at each iteration. It seems that the functional values at early iterations are strongly dependent on the fluid penalization term, $\beta |\omega |$, as the functional values follow the same pattern. 

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/objective_simple}
    \caption{Evolvement of the objective functionals \eqref{J_dissipation_bypass} and \eqref{J_vorticity_bypass} for the simple model.}
    \label{fig:bypass_objective_simple}
\end{figure}


\subsection{Extended Bypass Model}

An extended model, illustrated in Figure \ref{fig:extended_bypass}, is also implemented for the bypass problem, where 1.0 cm is added to the coronary artery on each side, in order to resolve the unsteady flow close to the inlet and outlet. The mesh for the extended model is created using \texttt{gmsh} \cite{geuzaine2009gmsh}, with a spatial step size of 0.05 cm along the upper part of the coronary artery, and 0.1 cm elsewhere, in total 6626 cells. Figure \ref{fig:extended_mesh} shows an illustration of the mesh. 
%
\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{figures/bypass_extended}
    \caption{Illustration of the extended bypass model.}
    \label{fig:extended_bypass}
\end{figure}
%
\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/extended_mesh}
    \caption{Mesh for the extended bypass model.}
    \label{fig:extended_mesh}
\end{figure}

The extended model for the bypass was used with the energy dissipation functional and vorticity functional, with mean velocity $\bar{u} = 20.0$. The penalty parameter is set to $\beta = 500.0$ and the time step size $\Delta t = 0.002$. The optimal shape for the energy dissipation functional $J_1$ was found after 19 iterations, and the optimal shape for the vorticity functional $J_2$ was found after 17 iterations. The optimal shapes are presented with fluid flow at end time $T = 0.8$. in Figure \ref{fig:alpha_extended}. The optimal shapes for the two functionals are in this case also very similar. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/objective_extended}
    \caption{Evolvement of the objective functionals \eqref{J_dissipation_bypass} and \eqref{J_vorticity_bypass} for the extended model.}
    \label{fig:bypass_objective_extended}
\end{figure}


Figure \ref{fig:bypass_objective_extended} shows the functional values for the energy dissipation functional $J_1$ and the vorticity functional $J_2$ at each iteration. Also for the extended model, the functional values follow the same pattern at early iterations, which means that also in this model the fluid volume penalization term $\beta |\omega |$ dominates the functional value. 

%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{\textwidth}
    \includegraphics[width=\textwidth]{figures/extended_arrows_01}
    \caption{Energy dissipation functional}
    \end{subfigure}
    \begin{subfigure}[b]{\textwidth}
    \includegraphics[width=\textwidth]{figures/extended_arrows_03}
    \caption{Vorticity functional}
    \end{subfigure}
    \caption{Fluid flow at time $T = 0.8$ s in optimal shape for the extended bypass model for the different functionals.}
    \label{fig:alpha_extended}
\end{figure}




\subsection{Comparison of the Two Models}


The evolvement of the objective function during each optimization procedure is presented in Figure \ref{fig:bypass_objective}. The objective values seem to follow a similar pattern, even for the two different models. The peak at iteration 7 is in all four cases caused by intermediate impermeability values inside of the channel. As this is not beneficial, the impermeability in the channel is restored by the algorithm at iteration 8. 
%
\begin{table}[H]
    \caption{Numerical results of the different bypass models with respect to the energy dissipation functional $J_1$ and vorticity functional $J_2$.}
    \centering
    \begin{tabular}{l c c c}
    \hline
        & Objective value & $\beta$  & Number of iterations  \\ \hline 
        Simple $J_1$ & 2123 & 500.0 & 20 \\
        Simple $J_2$  & 2076 & 500.0  & 20 \\ 
        Extended $J_1$  & 2680 & 500.0 & 19 \\ 
        Extended $J_2$  & 2711 & 500.0 & 17  \\ \hline
    \end{tabular}
    \label{tab:bypass}
\end{table}



\begin{figure}[H]
    \centering
    \includegraphics[width=0.75\textwidth]{figures/objective_all}
    \caption{Evolvement of the objective functionals $J_1$ \eqref{J_dissipation_bypass} and $J_2$ \eqref{J_vorticity_bypass} for the simple and extended model.}
    \label{fig:bypass_objective}
\end{figure}

The objective functional values for the four different cases are presented in Table \ref{tab:bypass} together with penalty value $\beta$ and number of iterations. 
%
\begin{figure}[H]
    \centering
    \begin{subfigure}[b]{\textwidth}
    \includegraphics[width=\textwidth]{figures/compare_dissipation_bypass}
    \caption{Energy dissipation functional}
    \end{subfigure}
    \begin{subfigure}[b]{\textwidth}
    \includegraphics[width=\textwidth]{figures/compare_vorticity_bypass}
    \caption{Vorticity functional}
    \end{subfigure}
    \caption{Comparing of the optimal shapes for the simple and extended bypass model. The channel for the simple model is colored red.}
    \label{fig:compare_bypass}
\end{figure}
%

The optimal shapes for the simple and extended model are compared for both functionals in Figure \ref{fig:compare_bypass}. Small differences, especially at the inlet and outlet of the design domain, are observed. The optimal shapes for the simple model have a wider outlet than for the extended model, which favors the extended model. As the dissipation energy and vorticity was calculated on the whole domain, the functional values are not comparable in the two models. This can easily be computed by calculating the energy dissipation and vorticity in the common domains. See Section \ref{further_work} for further discussion on improvement of these models. 






\cleardoublepage