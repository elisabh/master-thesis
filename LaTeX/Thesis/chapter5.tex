%===================================== CHAP 5 =================================

\chapter{Numerical Discretization}
\label{chapter_discretization}

% Re-use from project thesis, add time discretization
To solve the problems numerically, a finite element approximation of the velocity and pressure functions is applied. Although the mathematical presentation is dimension-independent, the numerical experiments assume a triangular grid in 2D. We assume that the domain $\Omega$ is discretized into finite elements in 2D with the parameter $h$ denoting the characteristic size of the largest element.


\section{Discretization in Space of the NSD Equations}
 Let the spaces $\mathbf{V} = [H^1(\Omega)]^d$ and $Q = L^2(\Omega)$ denote the velocity and pressure spaces, respectively. For the discretized velocities and pressures, $\mathbf{u}_h$ and $p_h$,  we need spaces $\mathbf{V}_h \subset \mathbf{V}$ and $Q_h \subset Q$, which are families of finite-dimensional spaces that depend on a discretization parameter $h$, see \cite{brenner2008mtf} for more details. The Navier-Stokes-Darcy system discretized in space can be written as
 %
 \begin{align}
    \label{NSD_discretized}
    \frac{\partial \mathbf{u}_h}{\partial t} - \nu \Delta \mathbf{u}_h + (\mathbf{u}_h \cdot \nabla)\mathbf{u}_h  + \alpha \mathbf{u}_h + \nabla p_h &= \mathbf{0}, \\
    \label{div_discretized}
    \nabla \cdot \mathbf{u}_h &= 0.
\end{align}

 To find the weak form, let us now define test functions for the velocity and pressure, $\mathbf{v}_h$ and $p_h$, respectively. The weak form is derived by multiplying the test functions with \eqref{NS_Darcy} and integrate over the domain $\Omega$. The Galerkin approximation in space of the NSD problem then yields: find $\mathbf{u}_h \in \mathbf{U}_h$ and $p_h \in Q_h$ such that
%
\begin{align}
\label{Galerkin}
    \int_{\Omega} \frac{\partial \mathbf{u}_h}{\partial t} \cdot \mathbf{v}_h + \nu \nabla \mathbf{u}_h \cdot \nabla \mathbf{v}_h + (\mathbf{u}_h \cdot \nabla)\mathbf{u}_h \cdot \mathbf{v}_h + \alpha \mathbf{u}_h \cdot \mathbf{v}_h + \nabla p_h \cdot \mathbf{v}_h \qquad \quad
   \nonumber \\
    -\int_{\partial \Omega} \nu \mathbf{n}  \nabla \mathbf{u}_h \mathbf{v}_h
= 0 \quad \forall ~ \mathbf{v}_h \in \mathbf{V}_h,
\\
    \int_\Omega (\nabla \cdot \mathbf{u}_h)q_h = 0 \quad \forall ~ q_h \in Q_h.
\end{align}
Time is here kept continuous, and will be discretized below. 

\section{Finite Elements and Inf-Sup Stability}

As the Navier-Stokes-Darcy problem \eqref{NS_Darcy} is of order 1 in $p$ and of order 2 in $\mathbf{u}$, it makes sense to use piecewise polynomials of degree
%
\begin{align}
    k \quad &\textrm{ for } \mathbf{V}_h, \\
    k - 1 \quad &\textrm{ for } Q_h ,
\end{align}
for the basis functions. This is known as Taylor-Hood elements, in general written $\mathbb{P}_k - \mathbb{P}_{k-1}$, $k \ge 2$. One popular choice is quadratic piecewise polynomials for the velocity components and linear piecewise polynomials for the pressure \cite{langtangen2002nmi}, in other words $k = 2$. See Figure \ref{fig:Elements} for an illustration of $\mathbb{P}_2 - \mathbb{P}_{1}$ Taylor-Hood elements.
%
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/PElements.pdf}
    \caption{$\mathbb{P}_1$ and $\mathbb{P}_2$ Taylor-Hood elements.}
    \label{fig:Elements}
\end{figure}
%
The Ladyzenskaja-Babuska-Brezzi (LBB) condition, also known as the inf-sup condition, states that there exists a constant $\chi$ independent of the grid size $h$ such that
\begin{align}
\label{LBB}
    \inf_{q_h \in Q_h} \sup_{\mathbf{v}_h \in \mathbf{V}_h} \frac{\langle \nabla \cdot \mathbf{v}_h, q_h \rangle}{\|\mathbf{v}_h\|_{\mathbf{V}_h} \|q_h\|_{Q_h}} \ge \chi > 0,
\end{align}
see see 16.3 in \cite{quarteroni2014nmd}. This condition guarantees solvability and stability of the Navier-Stokes problem. Hence we must choose the element spaces $\mathbf{V}_h, Q_h$ in such a way that the LBB condition \eqref{LBB} is satisfied. Elements that do not satisfy the LBB condition may not converge to the solution, and if they do, they may not converge as fast as the order of the elements would suggest. The Taylor-Hood elements satisfy the LBB condition, and are a stable choice for the Navier-Stokes problem, according to \cite{fenicsBook}. 

The level set function $\psi$, defined in \eqref{level_set}, will be discretized in space by $\mathbb{P}_{1}$ elements. As a consequence, one can not expect convergence of more than 1st order in space when including the Darcy term $\alpha$, which is dependent on the level set function, see Section \ref{material_distribution}.



\section{Discretization in Time of the NSD Equations}
\label{DiscretizationInTime}

The incompressible Navier-Stokes-Darcy equations can be solved simultaneously with a coupled solver, using for instance a Newton solver. Coupled solvers are known to be robust, stable and accurate. The downside is that they demand a lot of memory, and may not be efficient. Since we have a time-dependent optimization problem, we want to use a method that is faster, but be can allow less accuracy. The most popular numerical solution strategies for the incompressible Navier-Stokes equations are based on operator splitting, also known as projection methods \cite{langtangen2002nmi}. 

Consider the first part of the discretized Navier-Stokes-Darcy system \eqref{NSD_discretized}. The equation must be discretized in time, so we introduce the first-order backwards differencing operator at time step $k+1$,
%
\begin{align}
    D_t \mathbf{u} = \frac{1}{\Delta t} (\mathbf{u}^{k+1} - \mathbf{u}^{k}),
\end{align}
where $\Delta t$ is the time step size. We omit writing the $h$ in $\mathbf{u}_h$ for better readability. The equation \eqref{NSD_discretized} with a backward difference scheme in time now becomes
%
 \begin{align}
    \frac{1}{\Delta t} (\mathbf{u}^{k+1} - \mathbf{u}^{k}) + [(\mathbf{u} \cdot \nabla)\mathbf{u}]^{k+1} - \nu \Delta \mathbf{u}^{k+1}   + \alpha \mathbf{u}^{k+1} + \nabla p^{k+1} &= 0.
\end{align}
The weak form of this equation will be presented later in Section \ref{projection_methods}.

\/*
\begin{align}
\int_{\Omega} \frac{1}{\Delta t} (\mathbf{u}^{k+1} - \mathbf{u}^{k}) \cdot \mathbf{v} + \nu \nabla \mathbf{u}^{k+1} \cdot \nabla \mathbf{v} + [(\mathbf{u} \cdot \nabla)\mathbf{u}]^{k+1} \cdot \mathbf{v} + \alpha \mathbf{u}^{k+1} \cdot \mathbf{v} + \nabla p^{k+1} \cdot \mathbf{v} = 0
\end{align}
*/
We need a way to discretize the second, non-linear term $[(\mathbf{u} \cdot \nabla)\mathbf{u}]^{k+1}$. One option that is suggested in the literature \cite{langtangen2002nmi, deville2002hom, guermond2006opm} is a simple linearization $(\mathbf{u}^k \cdot \nabla) \mathbf{u}^{k+1}$, which is a first order accurate form. Another faster, but not so accurate form is to just use the old value $(\mathbf{u}^k \cdot \nabla) \mathbf{u}^{k}$. If the latter option is used, the matrix on the left hand side of the equation is independent of time, and can be computed once. This saves a lot of computational time, and is a good choice in our case. 


\section{Time Discretization and Stability}
\label{time_stability}

Numerical experiments for the Navier–Stokes-Darcy equations \eqref{NS_Darcy} indicate that the first order discretization in time is stable under the Courant–Friedrichs–Lewy (CFL) conditions proposed in \cite{johnston2004ase}, where a diffusive and convective stability constraint are proposed. The diffusive stability constraint states 
%
\begin{align}
\label{CFL_diffusive}
    \nu \frac{\Delta t}{\Delta x^2} \le (\frac{1}{2})^d
\end{align}
where the dimension $d = 2$ in our case. The convective stability constraint, which is the usual CFL type,
\begin{align}
\label{CFL_convective}
    \| u \|_{L^{\infty}} \frac{\Delta t}{\Delta x} \le 1.
\end{align}
The stability is highly affected by the Reynolds number \cite{white2006vff}, $Re = UL/\nu$ where $U$ is a characteristic velocity, $L$ is a characteristic length and $\nu$ is the kinematic viscosity. If $\nu$ is small (large Reynolds number), implicit treatment of the viscous term does nothing to stabilize the convection term, so \eqref{CFL_convective} is critical. 
For large $\nu$ (small Reynolds number), \eqref{CFL_diffusive} dominates \eqref{CFL_convective}. 

When $\nu$ is large, the time step has to be very small in order to keep the scheme stable. As an example, if the spatial discretization $\Delta x = 1/40$, $U=1$ and the viscosity $\nu = 1.0$, then the diffusive stability constraint \eqref{CFL_diffusive} requires a time step $\Delta t \le 0.00015$ for the system to be stable while the convective stability constraint \eqref{CFL_convective} requires only $\Delta t \le 0.025$. 




\section{Projection Methods}
\label{projection_methods}

Projection methods are based on the idea of splitting the system \eqref{NS_Darcy} into a series of simpler, familiar equations. The first step is neglecting the incompressibility condition and computing a tentative velocity. In the next step, the velocity is corrected by performing a projection onto the divergence free vector fields. The tentative velocity $\mathbf{u}^*$ is computed by solving the equation
%
 \begin{align}
 \label{tentative}
    \frac{1}{\Delta t} (\mathbf{u}^{*} - \mathbf{u}^{k}) + (\mathbf{u}^k \cdot \nabla)\mathbf{u}^{k} - \nu \Delta \mathbf{u}^{*}   + \alpha \mathbf{u}^{*} + \xi\nabla  p^{k} &= 0,
\end{align}
\/*
\begin{align}
\label{tentative}
\int_{\Omega} \frac{1}{\Delta t} (\mathbf{u}^{*} - \mathbf{u}^{k}) \cdot \mathbf{v} + \nu \nabla \mathbf{u}^{*} \cdot \nabla \mathbf{v} + (\mathbf{u}^k \cdot \nabla)\mathbf{u}^{k} \cdot \mathbf{v} + \alpha \mathbf{u}^{*} \cdot \mathbf{v} + \beta \nabla p^{k} \cdot \mathbf{v} = 0,
\end{align}
*/
where $\xi$ is an adjustable factor for flexible control of the pressure information. Note here that it is actually the weak form of \eqref{tentative} that will be solved, but for readability, we use the strong form for the explanation of the projection methods. At the end of this section, we will present the weak form of the equations to be solved. If $\xi$ is set to zero, we get the non-incremental pressure correction scheme, also known as Chorin's method \cite{langtangen2002nmi}. Taking $\xi = 1$, we get the incremental pressure correction scheme, also known as IPCS \cite{guermond2006opm}. The boundary conditions for $\mathbf{u}^*$ are the same as for the original problem. Since $\mathbf{u}^*$ does not fulfill the incompressibility equation \eqref{div_discretized}, we need to correct it. This is done by evaluating the pressure and velocity in certain terms at time step $k+1$
%
 \begin{align}
 \label{tentative_2}
    \frac{1}{\Delta t} (\mathbf{u}^{k+1} - \mathbf{u}^{k}) + (\mathbf{u}^k \cdot \nabla)\mathbf{u}^{k} - \nu \Delta \mathbf{u}^{*}   + \alpha \mathbf{u}^{k+1} + \nabla  p^{k+1} &= 0.
\end{align}
\/*
\begin{align}
 \label{tentative_2}
\int_{\Omega} \frac{1}{\Delta t} (\mathbf{u}^{k+1} - \mathbf{u}^{k}) \cdot \mathbf{v} + \nu \nabla \mathbf{u}^{k+1} \cdot \nabla \mathbf{v} + (\mathbf{u}^k \cdot \nabla)\mathbf{u}^{k} \cdot \mathbf{v} + \alpha \mathbf{u}^{k+1} \cdot \mathbf{v} + \nabla p^{k+1} \cdot \mathbf{v} = 0.
\end{align}
*/
Subtracting \eqref{tentative} from \eqref{tentative_2}, we end up with
%
 \begin{align}
 \label{subtraction}
    \frac{1}{\Delta t} (\mathbf{u}^{k+1} - \mathbf{u}^{*})  + \alpha (\mathbf{u}^{k+1} - \mathbf{u}^{*}) + \nabla (p^{k+1} - \xi p^k) &= 0.
\end{align}

\/*
 \begin{align}
 \label{subtraction}
    \int_\Omega \frac{1}{\Delta t} (\mathbf{u}^{k+1} - \mathbf{u}^{*}) \cdot \mathbf{v}  + \alpha (\mathbf{u}^{k+1} - \mathbf{u}^{*}) \cdot \mathbf{v} + \nabla (p^{k+1} - \beta p^k) \cdot \mathbf{v}&= 0.
\end{align}
*/
Now we take the divergence of the equation \eqref{subtraction} to get rid of the unknown velocity, using that $\nabla \cdot \mathbf{u}^{k+1} = 0$, and get a Poisson equation for the pressure $p^{k+1}$
%
 \begin{align}
 \label{poisson_p}
    \delta (p^{k+1} - \xi p^k) = (\frac{1}{\Delta t} + \alpha)  \nabla \cdot \mathbf{u}^{*}.
\end{align}

\/*
\begin{align}
 \label{poisson_p}
\int_\Omega - \nabla (p^{k+1} - \beta p^k) \cdot \nabla q = \int_\Omega (\frac{1}{\Delta t} + \alpha) ( \nabla \cdot \mathbf{u}^{*}) q.
\end{align}
*/
We observe from \eqref{subtraction} that  $\nabla (p^{k+1} - \xi p^k) \cdot \mathbf{n} |_{\partial \Omega_D} = 0$ since $\mathbf{u}^{k+1}$ and $\mathbf{u}^*$ have the same value on $\partial \Omega_D$. This implies that 

\begin{align}
    \nabla p^{k+1} \cdot \mathbf{n} |_{\partial \Omega_D} = \xi \nabla p^{k} \cdot \mathbf{n} |_{\partial \Omega_D} = \hdots = \xi^{k+1}\nabla p^{0} \cdot \mathbf{n} |_{\partial \Omega_D}.
\end{align}
These non-physical Neumann boundary conditions will introduce a numerical boundary layer \cite{guermond2006opm}. This may result in large errors near the boundary, and often an error of $\mathcal{O}(1)$ is experienced in a boundary layer with width $\approx \sqrt{\nu\Delta t}$, according to \cite{langtangen2002nmi}. 

Solving for the pressure $p^{k+1}$, there remains only one unknown in equation \eqref{subtraction}, and the velocity $\mathbf{u}^{k+1}$ can be computed.  The updated velocity will be divergence free, but not satisfy the boundary conditions exactly. It is possible to enforce boundary conditions on the velocity update.

The pressure correction scheme in weak form can be summed up in three equations to be solved:
\/*
\begin{enumerate}
    \item[i)] $\begin{aligned}
        \left( \frac{1}{\Delta t} - \nu \Delta + \alpha \right) \mathbf{u}^{*}  &= \frac{1}{\Delta t} \mathbf{u}^{k} - (\mathbf{u}^k \cdot \nabla)\mathbf{u}^{k} - \beta\nabla  p^{k}
    \end{aligned}$
    \item[ii)] $\begin{aligned}
        \delta p^{k+1} = (\frac{1}{\Delta t} + \alpha) ( \nabla \cdot \mathbf{u}^{*}) + \beta\Delta p^k
    \end{aligned}$
    \item[iii)] $\begin{aligned}
        \left(\frac{1}{\Delta t} + \alpha \right) \mathbf{u}^{k+1} &= \left(\frac{1}{\Delta t} + \alpha \right) \mathbf{u}^{*} - \nabla ( p^{k+1} - \beta p^k)
    \end{aligned}$
\end{enumerate}
weak form:
*/
%
\begin{enumerate}
    \item[i)] $\begin{aligned}
        \int_\Omega \left( \frac{1}{\Delta t} + \alpha \right) \mathbf{u}^* \cdot \mathbf{v} + \nu \nabla \mathbf{u}^* \cdot \nabla \mathbf{v} &= \int_\Omega \frac{1}{\Delta t} \mathbf{u}^{k} \cdot \mathbf{v} - (\mathbf{u}^k \cdot \nabla)\mathbf{u}^{k} \cdot \mathbf{v} - \xi\nabla  p^{k} \cdot \mathbf{v},
    \end{aligned}$
    \item[ii)] $\begin{aligned}
        \int_\Omega \nabla p^{k+1} \cdot \nabla q = \int_\Omega \xi\nabla p^k \cdot \nabla q -(\frac{1}{\Delta t} + \alpha) ( \nabla \cdot \mathbf{u}^{*}) \cdot q ,
    \end{aligned}$
    \item[iii)] $\begin{aligned}
        \int_\Omega \left(\frac{1}{\Delta t} + \alpha \right) \mathbf{u}^{k+1} \cdot \mathbf{v} &= \int_\Omega \left(\frac{1}{\Delta t} + \alpha \right) \mathbf{u}^{*} \cdot \mathbf{v} - \nabla ( p^{k+1} - \xi p^k) \cdot \mathbf{v}.
    \end{aligned}$
\end{enumerate}
These three steps can be described as i) tentative velocity, ii) pressure correction and iii) velocity update. It now becomes clear that the matrices on the left hand are independent of the solution at time $k$, and only needs to be assembled in the first iteration. 




\section{Discrete Time-Dependent Adjoint Equations}


For the discrete time-dependent adjoint equations, let us divide the time interval $[0, T]$ into $N_t$ equidistant time steps, such that we have $N_t + 1$ residual systems of equations. In this way, we can express the objective function as a sum
%
\begin{align}
\tilde{J}(\mathbf{u}, p, \alpha) = \sum_{0}^{N_t} J(\mathbf{u}^{(i)}, p^{(i)}, \alpha) =: \sum_{0}^{N_t} J^{(i)},
\end{align}
and each system of residuals can be expressed as a function of the state history,
%
\begin{align}
\mathbf{R}^{(i)} := \mathbf{R}(\mathbf{u}^{(i)}, \mathbf{u}^{(i-1)}, \ldots \mathbf{u}^{(0)}, p^{(i)}, p^{(i-1)}, \ldots, p^{(0)}, \alpha),
\end{align}
where the superscript denotes the time step number. Based on this, the Lagrangian can be written as
%
\begin{align}
\label{Lagrangian(Dt)}
\mathcal{L}(\mathbf{u}, p, \alpha) = \sum_{i=0}^{N_t} \left( J^{(i)} - \int_\Omega  (\mathbf{u}^a, p^a)^{(i)^T} \mathbf{R}^{(i)} \mathrm{d}\Omega \right).
\end{align}


Now, we want to differentiate \eqref{Lagrangian(Dt)} with respect to $\alpha$, and calculate the sensitivities. In order to calculate the gradient of $J(\mathbf{u}, p, \alpha)$ with respect to the control parameters $\alpha$, just like the stationary method, we differentiate with respect to each variable. The difference now, is that the derivatives depend on state variables from different time steps, so the expression becomes quite comprehensive.
%
\begin{align}
\label{dL(Dt)}
 \frac{\mathrm{d} \mathcal{L}}{\mathrm{d}\alpha}(\mathbf{u}, p, \alpha) &= \sum_{i=0}^{N_t} \left(\frac{\partial{J}^{(i)}}{\partial{\alpha}} + \sum_{k=0}^{i} \frac{\partial{J}^{(i)}}{\partial{\mathbf{u}^{(k)}}} \frac{\mathrm{d} \mathbf{u}^{(k)}}{\mathrm{d}\alpha} + \sum_{k=0}^{i} \frac{\partial{J}^{(i)}}{\partial{p^{(k)}}} \frac{\mathrm{d} p^{(k)}}{\mathrm{d}\alpha} \right) \nonumber \\
 &- \sum_{i=0}^{N_t} \left( \int_\Omega (\mathbf{u}^a, p^a)^{(i)^T} \left(\frac{\partial\mathbf{R}^{(i)}}{\partial{\alpha}} + \sum_{k=0}^{i} \frac{\partial\mathbf{R}^{(i)}}{\partial{\mathbf{u}^{(k)}}} \frac{\mathrm{d} \mathbf{u}^{(k)}}{\mathrm{d}\alpha} \right.\right.
 \nonumber\\
& \qquad \qquad \qquad \qquad \qquad \qquad \quad \left.\left. + \sum_{k=0}^{i} \frac{\partial\mathbf{R}^{(i)}}{\partial{p^{(k)}}} \frac{\mathrm{d} p^{(k)}}{\mathrm{d}\alpha} \right) \mathrm{d}\Omega \right).
\end{align}
Rearranging \eqref{dL(Dt)} with respect to implicit derivatives, we get
%
\begin{align}
\label{dLda(t)}
 \frac{\mathrm{d} \mathcal{L}}{\mathrm{d}\alpha}(\mathbf{u}, p, \alpha) = ~ &\sum_{i=0}^{N_t} \frac{\partial{J}^{(i)}}{\partial{\alpha}} - \sum_{i=0}^{N_t} \left( \int_\Omega (\mathbf{u}^a, p^a)^{(i)^T} \frac{\partial\mathbf{R}^{(i)}}{\partial{\alpha}} \right)\nonumber \\
 &+ \left( \frac{\partial{J}^{(N_t)}}{\partial{\mathbf{u}^{(N_t)}}} - \int_\Omega (\mathbf{u}^a, p^a)^{(N_t)^T} \frac{\partial\mathbf{R}^{(N_t)}}{\partial{\mathbf{u}^{(N_t)}}} \right) \frac{\mathrm{d} \mathbf{u}^{(N_t)}}{\mathrm{d}\alpha} \nonumber \\
 &+ \sum_{i=0}^{N_t-1} \left(\sum_{k=i}^{N_t} \left(\frac{\partial{J}^{(k)}}{\partial{\mathbf{u}^{(i)}}} -  \int_\Omega (\mathbf{u}^a, p^a)^{(k)^T} \frac{\partial\mathbf{R}^{(k)}}{\partial{\mathbf{u}^{(i)}}} \right) \right) \frac{\mathrm{d} \mathbf{u}^{(i)}}{\mathrm{d}\alpha} \nonumber\\
 &+ \left( \frac{\partial{J}^{(N_t)}}{\partial{p^{(N_t)}}} - \int_\Omega (\mathbf{u}^a, p^a)^{(N_t)^T} \frac{\partial\mathbf{R}^{(N_t)}}{\partial{p^{(N_t)}}} \right) \frac{\mathrm{d} p^{(N_t)}}{\mathrm{d}\alpha} \nonumber\\
 &+ \sum_{i=0}^{N_t-1} \left(\sum_{k=i}^{N_t} \left(\frac{\partial{J}^{(k)}}{\partial{p^{(i)}}} -  \int_\Omega (\mathbf{u}^a, p^a)^{(k)^T} \frac{\partial\mathbf{R}^{(k)}}{\partial{p^{(i)}}} \right) \right) \frac{\mathrm{d} p^{(i)}}{\mathrm{d}\alpha}.
\end{align}
We seek the values of $\mathbf{u}^{a}, p^{a}$ such that the implicit terms vanish, which means we want the four last lines of \eqref{dLda(t)} to disappear. This gives rise to the adjoint equations for the unstationary Navier-Stokes-Darcy system.



For the four last terms in \eqref{dLda(t)} to vanish, it is obvious that the solution of $\{ (\mathbf{u}^a, p^a)^{(i)}\}$ is given by
%
\begin{equation}
\begin{aligned}
 \int_\Omega (\mathbf{u}^a, p^a)^{(N_t)^T} \frac{\partial\mathbf{R}^{(N_t)}}{\partial{\mathbf{u}^{(N_t)}}} &= \frac{\partial{J}^{(N_t)}}{\partial{\mathbf{u}^{(N_t)}}}, \\
  \int_\Omega (\mathbf{u}^a, p^a)^{(N_t)^T} \frac{\partial\mathbf{R}^{(N_t)}}{\partial{p^{(N_t)}}} &= \frac{\partial{J}^{(N_t)}}{\partial{p^{(N_t)}}}, \\
 \int_\Omega (\mathbf{u}^a, p^a)^{(i)^T} \frac{\partial\mathbf{R}^{(i)}}{\partial{\mathbf{u}^{(i)}}} &= \frac{\partial{J}^{(i)}}{\partial{\mathbf{u}^{(i)}}} + \sum_{k=i+1}^{N_t} \left( \frac{\partial{J}^{(k)}}{\partial{\mathbf{u}^{(i)}}} +  \int_\Omega (\mathbf{u}^a, p^a)^{(k)^T}\frac{\partial\mathbf{R}^{(k)}}{\partial{\mathbf{u}^{(i)}}} \right), \\
  \int_\Omega (\mathbf{u}^a, p^a)^{(i)^T} \frac{\partial\mathbf{R}^{(i)}}{\partial{p^{(i)}}} &= \frac{\partial{J}^{(i)}}{\partial{p^{(i)}}} + \sum_{k=i+1}^{N_t} \left( \frac{\partial{J}^{(k)}}{\partial{p^{(i)}}} +  \int_\Omega (\mathbf{u}^a, p^a)^{(k)^T}\frac{\partial\mathbf{R}^{(k)}}{\partial{p^{(i)}}} \right).
\end{aligned}
\end{equation}

 To compute the adjoints, we need access to the whole primal velocity and pressure $\{ \mathbf{u}^{(i)}, p^{(i)} \}$. This can be done by storing the velocity and pressure at each time step in memory. If the solution is too big, a possibility is to use a checkpointing scheme \cite{farrell2013ada}, where the solution is stored at chosen intervals. The missing solutions can be reconstructed from the nearest available checkpoint when it is needed. Once the missing solution is available, the corresponding adjoint can be computed. For instance, a checkpoint interval can be of the size corresponding to the required memory capacity, in order to minimize the number of checkpoints.
 
 
% The accuracy requirements of the adjoint may be lower than that of the primal, as the gradient is only  an estimate of the optimal solution. Hence, the adjoint may be computed with 
 




\section{Numerical Software}
The solution $\mathbf{u}_h$ and $p_h$ of the Navier-Stokes-Darcy system is obtained by using the FEniCS environment \cite{fenics}. FEniCS is a collection of open source software components made to enable automated solution of differential equations. The differential equations can be specified in near-mathematical notation, such as finite element variational problems, and hence it is very useful tool for computational mathematics. The language used to express weak forms is called UFL (Unified Form Language). FEniCS can be programmed in both C++ and Python, using a library called DOLFIN which functions as the main user interface of FEniCS. A just-in-time compiler called FFC (FEniCS Form Compiler) generates efficient low-level C++ code from the high-level mathematical description (UFL) of the variational problem. 

To demonstrate, a small part of a simple Python program that solves the  Navier-Stokes-Darcy problem with $\mathbb{P}_2 - \mathbb{P}_1$ elements and Chorin's method is presented under. 

\begin{minted}[frame=single, fontsize=\footnotesize]{python}
# Function spaces for velocity and pressure:
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)

# Functions
u = TrialFunction(V)
p = TrialFunction(Q)
v = TestFunction(V)
q = TestFunction(Q)

u0 = Function(V)
u1 = Function(V)
p1 = Function(Q)

# Boundary conditions
bcu = DirichletBC(V, (0, 0), "on_boundary")

# Tentative velocity
F1 = (1./dt)*inner(u - u0, v)*dx + inner(u0*grad(u0), v)*dx \
    + nu*inner(grad(u), grad(v))*dx + alpha*inner(u, v)*dx
a1 = lhs(F1)
L1 = rhs(F1)

# Pressure update
a2 = inner(grad(p), grad(q))*dx
L2 = -(1./dt)*div(u1)*q*dx - alpha*div(u1)*q*dx

# Velocity update
a3 = (1./dt + alpha)*inner(u, v)*dx
L3 = (1./dt + alpha)*inner(u1, v)*dx - inner(grad(p1), v)*dx

while t < T:
    t += dt
    solve(a1 == L1, u1, bcu)
    solve(a2 == L2, p1)
    solve(a3 == L3, u1, bcu)
    u0.assign(u1)
\end{minted}



dolfin-adjoint \cite{farrell2013ada} exploits the high-level structure of the finite element method and builds on the FEniCS Project. As the forward model is executed through FEniCS, dolfin-adjoint records and analyzes the dependencies and structure of the equations. The resulting execution graph stores a symbolic mathematical representation of these equations and captures the entire information flow through the simulation. By reasoning about this graph, dolfin-adjoint can linearize the equations to derive a symbolic representation of the discrete tangent linear equations, and reverse the propagation of information through the graph to derive the corresponding adjoint equations. By invoking the FEniCS automatic code generator on these equations, dolfin-adjoint obtains solutions of the adjoint models.

In our case, dolfin-adjoint will be used to compute the topological derivative for different objective functionals, and used as gradients in the gradient-based optimization algorithm. The advantage of computing the gradient numerically is that one omits deriving the adjoint model and topological derivative, which can be quite comprehensive and time consuming. The disadvantage is that the gradient is not exact.

\cleardoublepage