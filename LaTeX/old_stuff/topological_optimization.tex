\section{Topology optimization}

%Here I want a section on what topological optimization is, i.e. a general optimization problem with functional $J(u, m)$.




In topology optimization, the goal is to modify the shape and connectedness of a domain, such that the desired objective function is minimized. While shape optimization is limited to determining an existing boundary, topology optimization can be used to design features within the domain, allowing new boundaries to be introduced into the design. This makes topology optimization more robust, since less information about the goal shape is required. 


Let us as an introduction to the field, look at the general case of topology optimization. 


Let us consider an open and bounded domain $\Omega \subset \mathbb{R}^d $, with $d = 2$ being the dimension of the space in our case. Assume the optimization problem is to minimize the functional

\begin{equation}
\label{JF}
\begin{aligned}
\textrm{Minimize } \quad &J(u, \alpha) \\
\textrm{subject to } \quad &F(u, \alpha) = 0 \textrm{ on } \Omega
\end{aligned}
\end{equation}
being some state equations.


In this thesis, we will apply optimization techniques based on the topological derivative and level sets to achieve the optimal topology. For a better understanding of the topological derivative, we will first explain the basic concept, before we illustrate it with an example including material distribution. 




\subsection{The topological derivative}

The topological derivative is a measure of the change in a topology with respect to some functional. It is defined in the first order correction of the asymptotic expansion of the shape functional with respect to a small perturbation in the domain, such as a hole or inclusion. A topological change in $\Omega$ can be described by the removal of a circular ball $B_{\varepsilon}(\hat{x})$ with radius $\varepsilon$ centered at an arbitrary point $\hat{x} \in \textrm{int}(\Omega)$. The perturbed domain is then given by $\Omega_{\varepsilon}(\hat{x}) = \Omega \backslash \overline{B_{\varepsilon}(\hat{x})}$.  

Now, let us introduce a functional $J(\Omega)$ and derive its topological derivative. For the perturbed domain, $\Omega_\varepsilon$, we assume
the associated functional $J(\Omega_\varepsilon)$ admits the following topological asymptotic expansion \cite{novotny2013tds}

\begin{equation}
\label{topological_expansion}
J(\Omega_{\varepsilon}(\hat{x})) = J(\Omega) + f(\varepsilon)D_T J(\hat{x}) + o(f(\varepsilon)).
\end{equation}
The term $f(\varepsilon) D_T(\hat{x})$ can be seen as the first order correction of $J$ to approximate $J(\Omega_{\varepsilon}(\hat{x}))$, and the function $f: \mathbb{R^+} \rightarrow \mathbb{R^+}$ must have the property that $f(\varepsilon) \rightarrow 0$ when $\varepsilon \rightarrow 0$. The remaining $D_T J(\hat{x})$ describes the change in $J$ with respect to a perturbation at $\hat{x}$, and is called the topological derivative of $J$. From the expansion in \eqref{topological_expansion} we obtain the general definition for the topological derivative

\begin{equation}
\label{topological_derivative}
D_T J(\hat{x}) := \lim_{\varepsilon \rightarrow 0} \frac{J(\Omega_{\varepsilon}(\hat{x})) - J(\Omega)}{f(\varepsilon)}.
\end{equation}



\subsection{A basic example of the topological derivative}

The perhaps easiest illustration of the topological derivative in our case, is to consider the functional associated with the volume of the domain, 

\begin{align}
J(\Omega) &= |\Omega| = \int_\Omega 1.
\end{align}
The corresponding perturbed volume functional, where a ball of radius $\varepsilon$ has been removed, is given as

\begin{align}
\label{topological_volume_expansion}
J(\Omega_\varepsilon) = \int_\Omega 1 - \int_{B_\varepsilon} 1 = J(\Omega) - \pi \varepsilon^2.
\end{align}
Comparing the topological expansion in \eqref{topological_volume_expansion} to \eqref{topological_expansion}, it is trivial to identify $f(\varepsilon) = \pi \varepsilon^2$ and  

\begin{align}
D_T J = -1
\end{align}
as the topological derivative. In this particular example, the order term in \eqref{topological_expansion} is equal to zero, and the topological derivative $D_T J$ is independent of $\hat{x}$. The negative sign appears because we remove a part of the domain, 



\subsection{Topological derivative of a fluid domain}

% Maybe better to introduce alpha here? Let dummy functional J = int_\Omega alpha(x)

In this thesis we will investigate the optimal distribution of fluid and solid material on a domain $\Omega$. Let us define the fluid sub-domain as $\omega$ and the remaining solid sub-domain as $\Omega \backslash \bar{\omega}$. Let us now investigate the topological derivative with respect to the fluid domain, i.e. the functional $J = |\omega |$. 

If $\hat{x}$ is stationed in the fluid sub-domain $\omega$, the perturbation would be removing a ball $B_\varepsilon$ from the fluid sub-domain, and we would obtain the same topological derivative as in the basic volume example, $D_T J = -1$. If, on the other hand, $\hat{x}$ is stationed in the solid sub-domain $\Omega \backslash \bar{\omega}$, the perturbation would be adding a ball $B_\varepsilon$ to the fluid domain, and the sign would be positive. Hence, the topological derivative of $|\omega|$ is described by 

\begin{align}
D_T |\omega| = k(x)
\end{align}
with 
\begin{align}
k(x) =
\begin{cases}
    +1 & \quad \text{ if } \hat{x} \in \Omega \backslash \omega\\
    -1 & \quad \text{ if } \hat{x} \in \omega.
\end{cases}
\label{eq:k}
\end{align}
We will make use of this expression when we derive the topological derivative for our optimization problem later. 








\subsection{Level set method in topology optimization}

% What is a level set method?
% Why do I want to use a level set method? Advantages, limitations in other approaches?
% How is it combined in topology optimization?
% Example on how to use it.

The level set method is a numerical technique for tracking interfaces and shapes in a domain using level sets. One of the advantages of the level set model is that you don't have to parametrize the shapes. This makes it well suited for topology optimization, for instance when a shape splits in two or a hole is developed. 


To model the distribution of fluid and solid in the domain $\Omega$, we introduce a fictitious time $t$, such that the evolvement of the domain can be viewed as a family $(\Omega(t))_{t \ge 0}$. This evolvement is represented by a level set function $\psi(x,t)$ such that

\begin{align}
    \begin{cases}
        \psi(t,x) < 0 \quad &\textrm{ for } x \in \omega(t) \\
        \psi(t,x) > 0 \quad &\textrm{ for } x \in \Omega \backslash \bar{\omega}(t).
    \end{cases}
\end{align}
where $\omega$ is the time-dependent fluid domain. Hence, the level set function $\psi$ can be used as a design variable for the fluid-solid distribution. 

A usual approach is to model the level set function by the Hamilton-Jacobi equation \cite{challis2009lst, burger2003itd},

\begin{align}
\label{hamilton_jacobi}
    \frac{\partial \psi}{\partial t} = -v | \nabla \psi |
\end{align}

where $v$ is the speed of the moving domain boundary, but in 2D this makes it difficult to create new holes in the domain. In \cite{burger2003itd} they investigate the use of topological derivatives in combination with the level set method, but still holding on to \eqref{hamilton_jacobi}. Another approach, first proposed in \cite{amstutz2006nat}, is an evolution equation for the level set function based only on the concept of the topological gradient $\tilde{g}$,
\begin{align}
    \frac{\partial \psi}{\partial t} = P(\tilde{g})
\end{align}
where $P(\cdot)$ is some function. We will discuss this function later in the light of optimality conditions, but first let us introduce the optimization problem. 








