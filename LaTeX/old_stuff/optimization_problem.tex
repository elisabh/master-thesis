\section{Fluid flow channel optimization}

----------------------------------------------------- \newline
Motivation for this section. Generalization of fluid flow channel optimization problems with applications to bypass design optimization. 


The time-dependent Navier-Stokes has been coupled with Darcy flows in \cite{cesmelioglu2013tdc}. Darcy flow and Brinkman's equation is thoroughly described in \cite{nield2006cpm}. Navier-Stokes equations with a term that accounts for out-of-plane viscous forces, presented in \cite{gersborg2005toc}.

As the body-force contribution we include a Darcy term representing friction. 
\newline----------------------------------------------------- 


\subsection{Navier-Stokes combined with Darcy's equation}


In this thesis we will investigate the optimal distribution of fluid and solid material on a domain $\Omega$. To describe the fluid flow, let us consider the unsteady Navier-Stokes equations for an incompressible fluid. 


\begin{equation}
\begin{aligned}
\label{NS}
    \frac{\partial \mathbf{u}}{\partial t}-\mu \Delta \mathbf{u} + (\nabla \mathbf{u})\mathbf{u} + \nabla p &= \mathbf{f} &\textrm{ in } \Omega\\
    \nabla \cdot \mathbf{u} &= 0 &\textrm{ in } \Omega\\
\end{aligned}
\end{equation}
where $t \in (0, T)$ is the time, $\mathbf{u}$ is the velocity vector in two dimensions, $p$ is the pressure, $\mu$ is the kinematic viscosity and $\Omega$ is the domain on which the fluid flows. Because we want to model a fluid flow on sub-domains with different material, we need a way to describe this. Darcy's law describes the flow of a fluid through a porous medium,

\begin{align}
\alpha \mathbf{u} + \nabla p = 0,
\end{align}
where $\alpha$ is the inverse permeability, see \cite{nield2006cpm, brinkman1947cvf} for more details. The permeability is a measure of the ability of a porous material to allow fluids to pass through it. High permeability gives rapidly moving fluid while low permeability gives the opposite. In  \cite{borrvall2002tof, gersborg2005toc, deng2012tos} they propose adding an artificial friction force into the body force term in \eqref{NS}
\begin{align}
\label{body_force}
    \mathbf{f} = - \alpha \mathbf{u}. 
\end{align}
The coupling of Darcy's law with the unsteady Navier-Stokes equation has been proved to have a unique solution by \cite{cesmelioglu2013tdc}. The inclusion of the Darcy term $\alpha u$ in the Navier-Stokes system allows for punishing counterproductive cells - the central component of topology optimization  \cite{othmer2008caf}. In this way we can model a domain with fluid and solid sub-domains simply by letting the inverse permeability $\alpha$ vary. The resulting system yields: find $\mathbf{u}, p$ such that


\begin{equation}
\begin{aligned}
\label{NS_Darcy}
    \frac{\partial \mathbf{u}}{\partial t}-\mu \Delta \mathbf{u} + (\nabla \mathbf{u})\mathbf{u} + \alpha \mathbf{u} + \nabla p &= 0 &\textrm{ in } \Omega\\
    \nabla \cdot \mathbf{u} &= 0 &\textrm{ in } \Omega\\
\end{aligned}
\end{equation}
We will from here on refer to \eqref{NS_Darcy} as the Navier-Stokes-Darcy system. 

..\newline
....\newline
..



\subsection{Boundary conditions}

The commonly used boundary conditions for the incompressible Navier-Stokes system are Dirichlet boundary conditions
\begin{align}
    \mathbf{u} &= \mathbf{u}_D  &\textrm{ on } \partial \Omega_D
\end{align}
with $\mathbf{u_D}$ being the velocity on the boundary $\partial \Omega_D$, and Neumann boundary conditions
\begin{align}
\label{neumann}
     \mu (\nabla \mathbf{u})\mathbf{n}  - p \mathbf{n} &= \mathbf{g}_N  &\textrm{ on } \partial \Omega_N
\end{align}
where $\mathbf{n}$ is the outward unit normal and $\mathbf{g}_N$ is the traction on the boundary $\partial \Omega_N$. In fluid flow channel optimization, it is common practice that the inflow velocity is known, hence a Dirichlet boundary condition is applied to the inflow boundary. For the outflow, we either know the outflow velocity, like for instance in \cite{borrvall2002tof, sa2016tda, challis2009lst}, or if the outflow profile cannot be determined a priori, one can use a Neumann boundary condition at the outflow \cite{guest2005toc, othmer2008caf, kreissl2011tou, deng2011tou}. In particular, an open boundary condition at the ouflow can be expressed as \eqref{neumann} with $\mathbf{g}_N = \mathbf{0}$, and will be used in the numerical examples presented in this thesis. On the wall, a no-slip Dirichlet boundary condition with $\mathbf{u}_D = \mathbf{0}$ is applied.  





\subsection{Material distribution}

We will distribute solid or fluid material at each point $x$ of the domain $\Omega$ by letting the inverse permeability parameter $\alpha(x)$ vary between two positive constants,

\begin{align}
\label{alpha}
\alpha(x) = 
\begin{cases}
    \alpha_L    & \quad \text{if } x \in \Omega \backslash \bar{\omega}\\
    \alpha_U    & \quad \text{if } x \in \omega \\
  \end{cases}
\end{align}
with $\alpha_U \gg \alpha_L > 0$. Like in the chapter on topology optimization, $\omega$ denotes the fluid sub-domain.






\subsection{Optimization problem}

Our optimization problem is to minimize some functional $J$ in a fluid flow channel by optimizing the channel shape. Given inflow and outflow conditions on the boundary of a domain $\Omega$, the aim is to distribute fluid and solid material to obtain the optimal shape. In addition, a constraint is introduced to put a bound on the volume the fluid is allowed to flow on.


If $J$ is the cost function to be minimized, the optimization problem can be stated as follows:

\begin{equation}
    \begin{aligned}
    \label{minimize}
    \textrm{Minimize } \quad &J(\mathbf{u}, p, \alpha)  \\
    \textrm{subject to } \quad &F(\mathbf{u}, p, \alpha) = 0 \textrm{ on } \Omega.
    \end{aligned}
\end{equation}
where $\mathbf{u}$ and $p$ stands for velocity and pressure, respectively. The inverse permeability parameter $\alpha$ serves the purpose of design variable, and $F$ is the system of state equations, more specifically the incompressible Navier-Stokes-Darcy system \eqref{NS_Darcy}.
