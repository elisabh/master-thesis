\section{Topological derivatives applied to Navier-Stokes in 2D}

When I first started working with adjoints, I found the concept very hard to understand. I was told the adjoint equations run backwards in time, but what does that really mean? This motivated me to introduce the reader, hopefully in a trivial way, to the concept of adjoints and why they are so useful. This section is highly inspired by the work of Funke and Farrel \cite{funke2013fap}.

\subsection{The concept of adjoints}




Let us consider the problem \eqref{JF} where $F(u, \alpha) = 0$ is a system of partial differential equations and $J(u, \alpha)$ is the functional of interest. A general optimization problem can be stated as: find the $\alpha$ that minimizes $J(u, \alpha)$ such that $F(u, \alpha) = 0$ is satisfied. Since $\alpha$ is our control parameters, we want to evaluate the change in  $J(u, \alpha)$ with respect to $\alpha$ by calculating the gradient

\begin{equation}
\label{dJ}
\frac{\mathrm{d}J}{\mathrm{d}\alpha} = \frac{\partial J}{\partial u} \frac{\mathrm{d}u}{\mathrm{d}\alpha} + \frac{\partial J}{\partial \alpha}.
\end{equation}

In this thesis we will apply a gradient-based optimization algorithm to find an optimal solution. The tangent linear approach is a good way to calculate $\mathrm{d} J / \mathrm{d} \alpha$ when the number of parameters $\alpha$ is small. The tangent linear equation can be derived from the PDE constraint,

\begin{align}
\label{dF}
    \frac{\mathrm{d}F}{\mathrm{d}\alpha} = \frac{\partial F}{\partial u} \frac{\mathrm{d}u}{\mathrm{d}\alpha} + \frac{\partial F}{\partial \alpha} = 0.
\end{align}
The approach is to solve \eqref{dF} for $\mathrm{d}u/\mathrm{d}\alpha$ and insert into \eqref{dJ}. However, the term $\mathrm{d}u/\mathrm{d}\alpha$ is often rather difficult to compute. In addition, the number of parameters in fluid flow constraint optimization is generally quite large. This motivates the use of adjoints.



If we assume that \eqref{dF} is solvable, we can insert the expression for $\mathrm{d}u/\mathrm{d}\alpha$ into \eqref{dJ},

\begin{align}
    \frac{\mathrm{d}J}{\mathrm{d}\alpha} = -\frac{\partial J}{\partial u}\left(\frac{\partial F}{\partial u}\right)^{-1}\frac{\partial F}{\partial \alpha}  + \frac{\partial J}{\partial \alpha}.
\end{align}
Let us now assign the variables differentiated with respect to $u$ to a new variable, 

\begin{align}
    \frac{\mathrm{d}J}{\mathrm{d}\alpha} &= -\lambda^T \frac{\partial F}{\partial \alpha}  + \frac{\partial J}{\partial \alpha}.
\end{align}
The new variable $\lambda$ is called the adjoint variable or Lagrangian multiplier. It is a lot easier to compute, and is the solution to the adjoint equation
\begin{align}
\label{adjoint}
    \frac{\partial F}{\partial u}^{T}\lambda &= \frac{\partial J}{\partial u}^T.
\end{align}
By taking the transpose of $\partial F / \partial u$, we reverse the flow of information in the system. One can think of it as when a lower triangular matrix is transposed to an upper triangular matrix. Then the system has to be solved with a backwards method, so the information propagates the opposite way. $(\partial J / \partial u )^T$ is the source term of the adjoint equation, and makes the adjoint solution specific to a particular functional, as opposed to the tangent linear approach. This makes it possible to easily compute the gradient of $J$ with respect to any parameter $\alpha$, which makes the adjoint approach extremely efficient when the number of parameters $\alpha$ is large.

The steps of the adjoint approach reads:

\begin{itemize}
    \item Choose initial $\alpha$.
    \item Solve $F(u, \alpha) = 0$ and \eqref{adjoint} for $u$ and $\lambda$, respectively.
    \item Calculate $\mathrm{d} J / \mathrm{d} \alpha$ and use in optimization algorithm, which gives a new value of $\alpha$. \\
    If $\mathrm{d} J / \mathrm{d} \alpha$ is close to zero, terminate.
\end{itemize}

What we have done in this section, is basically to derive the Lagrangian equations. The first-order necessary optimality conditions for \eqref{JF}, also known as the KKT conditions \cite{nocedal2006no}, include the Lagrangian

\begin{align}
\label{Lagrangian}
    \mathcal{L}(u, \alpha, \lambda) = J(u, \alpha) - \lambda^T F(u, \alpha).
\end{align}
where $\lambda$ is the Lagrangian multiplier, also known as the adjoint variable. The KKT conditions state that for all local minima $(\bar{u}, \bar{\alpha})$, there exists a $\bar{\lambda}$ such that

\begin{align}
    \frac{\partial \mathcal{L}}{\partial{u}} (\bar{u}, \bar{\alpha}) &= \frac{\partial{J}}{\partial{u}} (\bar{u}, \bar{\alpha}) - \bar{\lambda}^T \frac{\partial F}{\partial{u}} (\bar{u}, \bar{\alpha}) = 0 \\
    \frac{\partial \mathcal{L}}{\partial{\alpha}} (\bar{u}, \bar{\alpha}) &= \frac{\partial{J}}{\partial{\alpha}} (\bar{u}, \bar{\alpha}) - \bar{\lambda}^T \frac{\partial F}{\partial{\alpha}} (\bar{u}, \bar{\alpha}) = 0 \\
    \frac{\partial \mathcal{L}}{\partial{\lambda}} (\bar{u}, \bar{\alpha}) &=  F (\bar{u}, \bar{\alpha}) = 0.
\end{align}





\subsection{Adjoint of the Navier-Stokes system}
\label{Adjoint_NS}

In this section we will derive the adjoint equations for the incompressible unsteady Navier-Stokes system with respect to a general functional $J$, using the Lagrangian equation \eqref{Lagrangian}. 

Let the Lagrangian multiplier be written as $\mathbf{\lambda} = (\mathbf{u}^a, p^a)$ where $\mathbf{u}^a$ is the adjoint velocity and $p^a$ is the adjoint pressure. Using the Navier-Stokes-Darcy equations \eqref{NS_Darcy} as residuals,

\begin{align}
    \mathbf{R}(\mathbf{u}, p, \alpha) = 
    \begin{cases}
        \frac{\partial \mathbf{u}}{\partial t}-\mu \Delta \mathbf{u} + (\nabla \mathbf{u})\mathbf{u} + \alpha \mathbf{u} + \nabla p \\
        \nabla \cdot \mathbf{u} 
    \end{cases}
    = \mathbf{0}
\end{align}
we obtain the Lagrangian function 

\begin{align}
\mathcal{L} &= J - \int_\Omega (\mathbf{u}^a, p^a)^T \mathbf{R}(\mathbf{u}, p, \alpha) \\
    &= J - \int_\Omega \mathbf{u}^a \cdot ( - \mu \nabla^2 \mathbf{u} + (\nabla \mathbf{u})\mathbf{u} + \alpha \mathbf{u} + \nabla p) - \int_\Omega p^a ( -\nabla \cdot \mathbf{u})
\end{align}

To calculate the gradient of $J$ with respect to the control parameters $\alpha$, 

\begin{equation}
\begin{aligned}
 \frac{\mathrm{d} \mathcal{L}}{\mathrm{d}\alpha} =  &\frac{\partial{J}}{\partial{\mathbf{u}}} \frac{\mathrm{d} \mathbf{u}}{\mathrm{d}\alpha} + \frac{\partial{J}}{\partial{p}} \frac{\mathrm{d} p}{\mathrm{d}\alpha} + \frac{\partial{J}}{\partial{\alpha}} + \int_\Omega \frac{\mathrm{d}}{\mathrm{d}\alpha}  (\mathbf{u}^a, p^a)^T \underbrace{\mathbf{R}(\mathbf{u}, p, \alpha) }_\text{= 0}\\
    &+ \int_\Omega (\mathbf{u}^a, p^a)^T \left( \frac{\partial \mathbf{R}}{\partial \mathbf{u}} \frac{\mathrm{d}\mathbf{u}}{\mathrm{d}\alpha}  +  \frac{\partial \mathbf{R}}{\partial p} \frac{\mathrm{d}p}{\mathrm{d}\alpha} + \frac{\partial \mathbf{R}}{\partial \alpha} \right) 
\end{aligned}
\end{equation}
Introducing the short terms $\partial_{\alpha}$ for $\partial / \partial \alpha$ and $\mathrm{d}_{\alpha}$ for $\mathrm{d}/\mathrm{d}\alpha$, and arranging everything a bit, we get

\begin{align}
     \mathrm{d}_{\alpha} \mathcal{L} = \partial_{\alpha}J + \int_\Omega \mathbf{u}^a \partial_{\alpha} \mathbf{R} + \left( \partial_{\mathbf{u}}J + \int_\Omega (\mathbf{u}^a, p^a)^T \partial_{\mathbf{u}} \mathbf{R} \right) \mathrm{d}_{\alpha} \mathbf{u} + \left( \int_\Omega (\mathbf{u}^a, p^a)^T \partial_p \mathbf{R} \right) \mathrm{d}_{\alpha} p
\end{align}


\begin{align}
     \frac{\mathrm{d} \mathcal{L}}{\mathrm{d}\alpha} =\frac{\partial{J}}{\partial{\alpha}} + \int_\Omega \mathbf{u}^a \frac{\partial \mathbf{R}}{\partial \alpha} + \left( \frac{\partial{J}}{\partial{\mathbf{u}}} + \int_\Omega (\mathbf{u}^a, p^a)^T \frac{\partial \mathbf{R}}{\partial \mathbf{u}} \right) \frac{\mathrm{d} \mathbf{u}}{\mathrm{d}\alpha} + \int_\Omega (\mathbf{u}^a, p^a)^T \frac{\partial \mathbf{R}}{\partial p}  \frac{\mathrm{d} p}{\mathrm{d}\alpha}
\end{align}











\subsection{---}


Using the adjoint velocity $\mathbf{u^a}$ and adjoint pressure $p^a$ as Lagrangian multipliers, $\mathbf{\lambda} = (\mathbf{u^a}, p^a)$ with \eqref{NS} as constraints, we obtain the Lagrangian function

\begin{align}
\mathcal{L} = \mathcal{J_\omega} - \int_\Omega u^a \cdot ( - \mu \nabla^2 u + (\nabla u)u + \alpha u + \nabla p) - \int_\Omega p^a ( -\nabla \cdot u),
\end{align}
We can choose $u^a$ and $p^a$ in such a way that the variation with respect to the primal homogenized velocity and pressure is zero, 

\begin{align}
\label{del_L}
\delta_{{u}} \mathcal{L} + \delta_p \mathcal{L} &= 0 
\end{align}
Deriving the two terms separately, we get
\begin{align}
\delta_{{u}} \mathcal{L} &= \delta_{{u}} \mathcal{J}_\omega - \int_{\Omega} u^a \cdot  \left((\delta {u} \cdot \nabla){u} + ({u} \cdot \nabla)\delta {u} - \mu \nabla^2 \delta {{u}} + \alpha \delta {{u}} \right) - \int_\Omega p^a (-\nabla \cdot \delta {u}) \\
\delta_p \mathcal{L} &= \delta_{p} \mathcal{J}_\omega - \int_{\Omega}(-\nabla \cdot u^a) \delta p .
\end{align}
We decompose the objective function $\mathcal{J}_\omega$ into contributions from the interior and boundary, $\Omega$ and $\partial \Omega$, respectively such that

\begin{align}
\label{J_decompose}
\mathcal{J}_\omega &= \int_\Omega J_\Omega + \int_{\partial \Omega} J_{\partial \Omega} 
\end{align}
where $J_\Omega$ and $J_{\partial \Omega}$ have the values
\begin{align}
J = \mu \|\nabla u\|^2 + \alpha \|u\|^2
\end{align}
for the interior and boundary values of $u$, respectively. 
After integration by parts we can reformulate \eqref{del_L} as

\begin{equation}
\begin{aligned}
\label{adjoint_integral}
&\int_\Omega \left( - \nabla u^a \cdot {u} - ({u} \cdot \nabla) u^a - \mu \nabla^2 u^a + \alpha u^a + \nabla p^a - \frac{\partial J_\Omega}{\partial {u}} \right) \delta {u} \\
+ &\int_{\partial \Omega} \left( n(u^a \cdot {u}) + u^a({u} \cdot n) + \mu n \cdot \nabla u^a - p^a n - \frac{\partial J_{\partial \Omega}}{\partial {u}} \right) \delta {u} - \int_{\partial \Omega} \mu (n \cdot \nabla) \delta {u}\cdot u^a \\
+ &\int_{\Omega} \left( -\nabla \cdot u^a - \frac{\partial J_\Omega}{\partial p} \right) \delta p + \int_{\partial \Omega} \left( u^a \cdot n - \frac{\partial J_{\partial \Omega}}{\partial p} \right) \delta p &= 0
\end{aligned}
\end{equation}
This equation must be fulfilled for any $\delta {u}, \delta p$ that satisfy the Navier-Stokes system, which can only be accomplished if the integrals vanish individually. Hence, the integrands must be zero, and from the interior integrals we get out the adjoint equations

\begin{align}
    - \mu \nabla^2 u^a -\nabla u^a \cdot {u} + (\nabla {u})^\textrm{T} u^a  + \alpha u^a + \nabla p^a &=   \frac{\partial J_\Omega}{\partial {u}} \\
    - \nabla \cdot u^a &= \frac{\partial J_\Omega}{\partial p}
\end{align}
The boundary terms are exactly the same as in the Stokes case, so $u^a|_{\partial \Omega} = 0$ and the resulting adjoint system yields: Find $u^a \in V, p^a \in Q$ such that
\begin{equation}
\begin{aligned}
    - \mu \nabla^2 u^a -\nabla u^a \cdot {u} + (\nabla {u})^\textrm{T} u^a  + \alpha u^a + \nabla p^a &= -2 \mu \nabla^2 {u} + 2 \alpha {u} &\textrm{ in } \Omega\\
    - \nabla \cdot u^a &= 0 &\textrm{ in } \Omega \\
    u^a &= 0 &\textrm{ on } \partial \Omega
\end{aligned}
\end{equation}





\subsection{Topological derivative of the Navier-Stokes equations}