from dolfin import *

class AdjointSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.boundaries = problem.adjoint_boundaries
        self.T = problem.T
        self.dt = problem.dt

    def solve_coupled(self, psi, u_list):

        # psi is the level set function
        # u_list contains forward solution

        # Adjoint coupled:
        P2 = VectorElement('P', 'triangle', 2)
        P1 = FiniteElement('P', 'triangle', 1)
        TH = P2 * P1
        W = FunctionSpace(self.mesh, TH)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        (u, p) = TrialFunctions(W)
        (v, q) = TestFunctions(W)
        w = Function(W)
        (ua, pa) = split(w)

        # in order to assign w.sub(0) to u_a
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        assigner = FunctionAssigner(V, W.sub(0))

        # Adjoint system with Neumann boundary conditions
        F_a = (1. / k) * inner(u - ua, v) * dx \
                  - inner(dot(grad(u), u0), v) * dx \
                  - inner(dot(u0, grad(u)), v) * dx \
                  + nu * inner(grad(u), grad(v)) * dx \
                  + alpha(psi) * inner(u, v) * dx \
                  - div(u) * q * dx - div(v) * p * dx \
                  - 2 * nu * inner(grad(u0), grad(v)) * dx \
                  - 2 * alpha(psi) * inner(u0, v) * dx \
                  - inner(p * n, v) * ds(3) \
                  + nu * inner(dot(n, grad(u)), v) * ds(3) \
                  + inner(dot(u0, u) * n, v) * ds(3) \
                  + inner(dot(u0, n) * u, v) * ds(3)
            a = lhs(F_a)
            L = rhs(F_a)

        # save adjoint solution
        ua_list = []

        # Constant Dirichlet boundary conditions
        bcu = problem.adjoint_boundary_conditions(V)

        k = Constant(self.dt)
        t = self.T

        for u0 in reversed(u_list):
            t -= self.dt

            problem = LinearVariationalProblem(a, L, w, bcu)
            solver = LinearVariationalSolver(problem)
            solver.solve()
            (ua, pa) = split(w)

            # ensure deep copy
            u_a = Function(V)
            assigner.assign(u_a, w.sub(0))
            ua_list.append(u_a)

        # return reversed ua_list such that it has the same ordering
        # as u_list
        return ua_list[::-1]
