from dolfin import *
from dolfin_adjoint import *

class NSDsolver:

    def __init__(self, problem):
        self.problem = problem
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries

    def solve(self, psi):

        adj_reset()
        timestep = problem.dt

        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        # Initial velocity and pressure
        u0, p0 = problem.initial_values()

        # Declare velocity functions
        u = Function(V, name="u")
        v = TestFunction(V)

        # Weak form for tentative velocity
        F1 = (1./timestep) * inner(u - u0, v) * dx \
            + inner(grad(u) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + alpha(psi) * inner(u, v) * dx \
            + inner(grad(p0), v) * dx \
            - nu * inner(dot(n, nabla_grad(u)), v) * ds(2) \
            + inner(p0 * n, v) * ds(2)


        # Declare pressure functions
        p = Function(Q, name="p")
        q = TestFunction(Q)

        # Pressure correction weak form
        F2 = inner(grad(p - p0), grad(q)) * dx \
             + (1. / timestep) * div(u0) * q * dx


        # Velocity update weak form
        F3 = (1./timestep)*inner(u - u0, v) * dx \
             + inner(grad(p - p0), v) * dx

        t = 0.0
        psi_tmp = psi.copy(deepcopy=True)

        adj_time = True
        annotate = True
        adj_start_timestep()

        while t < problem.T:

            # u and p are trial functions
            # u0 and p0 are newest value

            # Boundary conditions, Pousille flow at inlet
            bcu, bcp = problem.boundary_conditions(V, Q, t)

            solve(F1 == 0, u, bcu)
            u0.assign(u, annotate=annotate)
            solve(F2 == 0, p, bcp)
            solve(F3 == 0, u, bcu)

            psi.assign(psi_tmp, annotate=annotate)
            u0.assign(u, annotate=annotate)
            p0.assign(p, annotate=annotate)

            # plot(u0, interactive=True)

            t += float(timestep)
            if adj_time:
                adj_inc_timestep(t, t > problem.T)

        # dolfin-adjoint only needs the last solution of u and p
        return u0, p0



class GradientMethod:
    def __init__(self, problem):
        self.problem = problem
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries
        self.V0 = FunctionSpace(self.mesh, "CG", 1)
        self.PSI = "-1"
        self.n_max = 20
        self.kappa_tol = 0.001

    def new_psi(self, kappa, psi, g):

        dot_prod = assemble(dot(g, psi) * dx)
        nrm_g = sqrt(assemble(dot(g, g) * dx))
        nrm_psi = sqrt(assemble(dot(psi, psi) * dx))

        theta = acos(dot_prod / (nrm_g * nrm_psi))
        k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
        k2 = sin(kappa * theta) / (sin(theta) * nrm_g)

        psi = project(k1 * psi - k2 * g, self.V0, annotate=False)
        return psi

    def run_gradient_method(self):

        ds = Measure("ds")(subdomain_data=self.boundaries)
        kappa = 0.5

        # Design parameter
        psi_ref = project(Expression(self.PSI, degree=1), self.V0)
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        psi = project(Expression(self.PSI, degree=2) / nrm_psi,
                      self.V0, annotate=True)

        # Initialize problem and Navier-Stokes-Darcy solver
        nsd = NSDsolver(self.problem)
        u, p = nsd.solve(psi)

        # Dolfin-adjoint functional
        J = Functional((p * ds(1) - 3. * p * ds(2)) * dt
                       + beta * fluid_domain(psi) * dx * dt)

        for i in range(self.n_max):
            print "\nIteration #" + str(i+1)

            g = compute_gradient(J, Control(psi), forget=False)

            # Store old values of J and psi:
            Jm = ReducedFunctional(J, Control(psi))
            J_old = Jm(psi)
            psi_ref.assign(psi)

            # Update psi and calculate new J
            kappa = min(0.5, kappa * 1.5)
            psi = self.new_psi(kappa, psi_ref, g)
            u, p = nsd.solve(psi)

            J = Functional((p * ds(1) - 3. * p * ds(2)) * dt
                           + beta * fluid_domain(psi) * dx * dt)
            Jm = ReducedFunctional(J, Control(psi))

        return psi, J
