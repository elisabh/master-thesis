from dolfin import *

class GradientMethod:

    def __init__(self, problem):
        self.problem = problem
        self.V0 = FunctionSpace(problem.mesh, "CG", 1)
        self.PSI = "-1"
        self.n_max = 20
        self.tol_theta = 0.01
        self.tol_kappa = 0.001

    def functional(self, psi, u_list):
        J = 0.0
        N = len(u_list)
        for u in u_list:
            J += problem.dt * assemble(alpha(psi) * inner(u, u) * dx
                                      + nu * inner(grad(u), grad(u)) * dx)
        J += assemble(self.beta * fluid_domain(psi) * dx)
        return J

    def gradient(self, g, u_, ua_):
        N = len(u_)
        g.vector().zero()
        for i in range(N):
            u = u_[i]
            ua = ua_[i]
            direction = - (ALPHA_U - ALPHA_L) * inner(u, u - ua)
            g_temp = project(direction + self.beta, self.V0)
            g.vector().axpy(1./N, g_temp.vector())

    def angle(self, g, psi):
        dot_prod = assemble(dot(g, psi) * dx)
        nrm_g = sqrt(assemble(dot(g, g) * dx))
        nrm_psi = sqrt(assemble(dot(psi, psi) * dx))
        return acos(dot_prod / (nrm_g * nrm_psi))

    def new_psi(self, psi, kappa, theta, psi_ref, g):
        nrm_g = sqrt(assemble(dot(g, g) * dx))
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
        k2 = sin(kappa * theta) / (sin(theta) * nrm_g)
        psi.vector().zero()
        psi.vector().axpy(k1, psi_ref.vector())
        psi.vector().axpy(k2, g.vector())

    def run_gradient_method(self):

        kappa = 1.0

        # Design parameter
        psi_ref = project(Expression(self.PSI, degree=1), self.V0)
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        psi = project(Expression(self.PSI) / nrm_psi, self.V0)
        g = Function(self.V0)

        NSD = NSDSolver(self.problem)
        adjoint = AdjointSolver(self.problem)

        u_list = NSD.solve_IPCS(psi)
        ua_list = adjoint.solve_coupled(psi, u_list)

        J = self.functional(psi, u_list)

        # Iterate until convergence
        for n in range(self.n_max):

            print "Iteration #: " + str(n+1)

            self.gradient(g, u_list, ua_list)
            theta = self.angle(g, psi)
            if theta < self.tol_theta:
                print "Theta smaller that tolerance!"
                break

            kappa = min(1.0, kappa*1.5)

            # Store old psi and update new psi
            psi_ref.assign(psi)
            self.new_psi(psi, kappa, theta, psi_ref, g)

            # Solve forward and adjoint system
            u_list = NSD.solve_IPCS(psi)
            ua_list = adjoint.solve_coupled(psi, u_list)

            # calculate new functional
            J_new = self.functional(psi, u_list)


            # Line search
            while J_new > J and kappa > self.tol_kappa:
                # Did not find smaller J, decreasing kappa."
                kappa = kappa*0.5

                # Update psi and calculate new J
                self.new_psi(psi, kappa, theta, psi_ref, g)
                u_list = NSD.solve_IPCS(psi)
                J_new = self.functional(psi, u_list)

            if J_new < J:
                J = J_new
            else:
                # Did not find smaller J, terminating
                return psi, J

        return psi, J
