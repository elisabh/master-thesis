// Gmsh project created on Tue Sep 13 15:48:24 2016
Point(1) = {0, 0, 0, 1.0};
Delete {
  Point{1};
}
Point(1) = {0, 0, 0, 0.1};
Point(2) = {1.5, 0, 0, 0.1};
Point(3) = {1.5, 1, 0, 0.1};
Point(4) = {0, 1, 0, 0.1};
Point(5) = {0.74, 0, 0, 0.5};
Point(6) = {0.76, 0, 0, 0.5};
Point(7) = {0.76, 0.21, 0, 1};
Point(8) = {0.74, 0.21, 0, 1};
Line(1) = {1, 5};
Line(2) = {5, 8};
Line(3) = {8, 7};
Line(4) = {7, 6};
Line(5) = {6, 2};
Line(6) = {2, 3};
Line(7) = {3, 4};
Line(8) = {4, 1};
Line Loop(9) = {7, 8, 1, 2, 3, 4, 5, 6};
Plane Surface(10) = {9};
Physical Line(11) = {7, 6, 5, 4, 3, 2, 1, 8};
Physical Surface(12) = {10};
