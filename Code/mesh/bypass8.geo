// Gmsh project created on Mon Jan 16 11:16:08 2017
Point(1) = {0, 0, 0, 0.03};
Point(2) = {2.0, 0, 0, 0.03};
Point(3) = {2.0, 0.1, 0, 0.03};
Point(4) = {0, 0.1, 0, 0.03};
Point(5) = {0, 0.5, 0, 0.03};
Point(6) = {2.0, 0.5, 0, 0.03};
Point(8) = {1.2, 0, 0, 0.03};
Point(9) = {1.2, 0.1, 0, 0.03};
Point(10) = {0.8, 0.1, 0, 0.03};
Point(11) = {0.8, 0, 0, 0.03};

Line(1) = {1, 11};
Line(2) = {11, 10};
Line(3) = {10, 4};
Line(4) = {4, 1};
Line(5) = {4, 5};
Line(6) = {5, 6};
Line(7) = {6, 3};
Line(8) = {3, 2};
Line(9) = {2, 8};
Line(10) = {8, 9};
Line(11) = {9, 3};
Line(12) = {9, 10};

Line Loop(13) = {5, 6, 7, -11, 12, 3};
Plane Surface(14) = {13};
Line Loop(15) = {4, 1, 2, 3};
Plane Surface(16) = {15};
Line Loop(17) = {11, 8, 9, 10};
Plane Surface(18) = {17};
