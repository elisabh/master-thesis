# Diffuser problem, try to make smooth gradient with object oriented coding

from dolfin import *
from dolfin_adjoint import *


class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.0) and between(x[1], (0.3333, 0.6667))


def alpha(psi, name="alpha", q=0.1):
    nrm_psi = sqrt(assemble(dot(psi, psi) * dx))
    if nrm_psi == 0:
        q = 1.0
        print "q = " + str(q)
    return ALPHA_L + (ALPHA_U - ALPHA_L) * (1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q)))


def fluid_domain(psi, q=0.1):
    nrm_psi = sqrt(assemble(dot(psi, psi) * dx))
    if nrm_psi == 0:
        q = 1.0
    return 1.0 - 1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q))


set_log_level(40)

nu = Constant(0.01, name="nu")
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)
beta = 95.0

# Mesh

class Problem:
    def __init__(self):
        self.mesh = None
        self.boundaries = None

        self.make_mesh()
        self.mark_boundaries()
        self.T = 0.0015
        self.timestep = Constant(0.0001)

    def make_mesh(self):
        self.mesh = UnitSquareMesh(42, 42)

    def mark_boundaries(self):
        noslip = Noslip()
        inlet = Inlet()
        outlet = Outlet()

        self.boundaries = MeshFunction("size_t", self.mesh, 1)
        self.boundaries.set_all(4)
        noslip.mark(self.boundaries, 0)
        inlet.mark(self.boundaries, 1)
        outlet.mark(self.boundaries, 2)

    def boundary_conditions(self, V, Q, t):
        U = sin(3.1415 / (2 * self.T) * t)
        u_1 = Expression(("U * (1 - (2*(x[1]-0.5)) * (2*(x[1]-0.5)) )", "0"),
                         U=U, degree=3)

        bcu = [DirichletBC(V, (0.0, 0.0), self.boundaries, 0),
               DirichletBC(V, u_1, self.boundaries, 1)]
        bcp = [DirichletBC(Q, 0.0, self.boundaries, 2)]

        return bcu, bcp

    def initial_values(self):
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        u_ic = project(Expression(("0.0", "0.0"), degree=3),  V, name="u_ic")
        p_ic = project(Expression("0.0", degree=1), Q, name="p_ic")
        u0 = u_ic.copy(deepcopy=True)
        p0 = p_ic.copy(deepcopy=True)
        return u0, p0


class NSDsolver:

    def __init__(self, problem):
        self.problem = problem
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries

    def solve(self, psi):

        #print "Solving Navier-Stokes-Darcy"

        adj_reset()
        timestep = problem.timestep

        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        # Initial velocity and pressure
        u0, p0 = problem.initial_values()

        # Declare velocity functions
        u = Function(V, name="u", annotate=True)
        v = TestFunction(V)

        # Weak form for tentative velocity
        F1 = (1./timestep) * inner(u - u0, v) * dx \
            + inner(grad(u) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + alpha(psi) * inner(u, v) * dx \
            + inner(grad(p0), v) * dx \
            - nu * inner(dot(n, nabla_grad(u)), v) * ds(2) \
            + inner(p0 * n, v) * ds(2)


        # Declare pressure functions (after tentative velocity weak form!!)
        p = Function(Q, name="p", annotate=True)
        q = TestFunction(Q)

        # Pressure correction weak form
        F2 = inner(grad(p - p0), grad(q)) * dx \
             + (1. / timestep) * div(u0) * q * dx


        # Velocity update weak form
        F3 = (1./timestep)*inner(u - u0, v) * dx \
             + inner(grad(p - p0), v) * dx

        t = 0.0
        psi_tmp = psi.copy(deepcopy=True)

        Z = 0.0 # calculate pressure difference like in Kreissl

        adj_time = True
        annotate = True
        adj_start_timestep()

        while t < problem.T:

            adj_html("forward.html", "forward")
            adj_html("adjoint.html", "adjoint")

            # u and p work as trial functions
            # u0 and p0 work as newest value

            # Boundary conditions, Pousille flow at inlet
            bcu, bcp = problem.boundary_conditions(V, Q, t)

            solve(F1 == 0, u, bcu)
            u0.assign(u, annotate=annotate)
            solve(F2 == 0, p, bcp)
            solve(F3 == 0, u, bcu)

            psi.assign(psi_tmp, annotate=annotate)
            u0.assign(u, annotate=annotate)
            p0.assign(p, annotate=annotate)

            # plot(u0, interactive=True)
            Z += 1./15 * assemble(p0 * ds(1) - 3. * p0 * ds(2))

            t += float(timestep)
            if adj_time:
                adj_inc_timestep(t, t > problem.T)

        print "Kreissl functional Z = " + str(Z)
        return u0, p0, Z


class GradientMethod:
    def __init__(self, problem):
        self.problem = problem
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries
        self.V0 = FunctionSpace(self.mesh, "CG", 1)
        self.PSI = "-1"
        self.n_max = 20
        self.kappa_tol = 0.001
        self.J_list = []
        self.theta_list = []
        self.kappa_list = []
        self.fluid_list = []

    def new_psi(self, kappa, psi, g):

        dot_prod = assemble(dot(g, psi) * dx)
        nrm_g = sqrt(assemble(dot(g, g) * dx))
        nrm_psi = sqrt(assemble(dot(psi, psi) * dx))

        if nrm_g * nrm_psi == 0:
            print "nrm_g * nrm_psi = 0!"
            nrm_g = 1.0
            nrm_psi = 1.0
        theta = acos(dot_prod / (nrm_g * nrm_psi))
        print "Kappa = " + str(kappa)
        print "Theta = " + str(theta)
        self.theta_list.append(theta)
        self.kappa_list.append(kappa)

        k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
        k2 = sin(kappa * theta) / (sin(theta) * nrm_g)

        psi = project(k1 * psi - k2 * g, self.V0, annotate=False)
        return psi

    def run_gradient_method(self):

        ds = Measure("ds")(subdomain_data=self.boundaries)
<<<<<<< HEAD
        kappa = 0.5
=======
        kappa = 1.0
>>>>>>> 97a7a9fd51c9cbd1d9c8756c9ce5ce1741f0e85d
        Z_list = []

        # Design parameter
        psi_ref = project(Expression(self.PSI, degree=1), self.V0)
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        psi = project(Expression(self.PSI, degree=2) / nrm_psi, self.V0, annotate=True)
        a = project(alpha(psi), self.V0, annotate=False)

        if write_:
<<<<<<< HEAD
            alpha_file = File('./ParaView/Diffuser_markov/alpha_diffuser_functional.pvd')
=======
            alpha_file = File('alpha_diffuser_basic.pvd')
>>>>>>> 97a7a9fd51c9cbd1d9c8756c9ce5ce1741f0e85d
            alpha_file << a

        # Initialize problem and Navier-Stokes-Darcy solver
        nsd = NSDsolver(self.problem)
        u, p, Z = nsd.solve(psi)
        

        # Dolfin-adjoint functional
        J = Functional((p * ds(1) - 3. * p * ds(2)) * dt
                       + beta * fluid_domain(psi) * dx * dt)
       

        for i in range(self.n_max):
            print "\nIteration #" + str(i+1)

            # Dolfin-adjoint gradient, check that it gets an expected value
            #print "Computing gradient"
            g = compute_gradient(J, Control(psi), forget=False)

            # Store old values of J and psi:
            Jm = ReducedFunctional(J, Control(psi))
            J_old = Jm(psi)
            self.J_list.append(J_old)
            Z_list.append(Z)
            psi_ref.assign(psi)

            # Update psi and calculate new J
<<<<<<< HEAD
            kappa = min(0.5, kappa * 1.5)
=======
            kappa = min(1.0, kappa * 1.5)
>>>>>>> 97a7a9fd51c9cbd1d9c8756c9ce5ce1741f0e85d
            psi = self.new_psi(kappa, psi_ref, g)
            u, p, Z = nsd.solve(psi)

            J = Functional((p * ds(1) - 3. * p * ds(2)) * dt
                           + beta * fluid_domain(psi) * dx * dt)

            Jm = ReducedFunctional(J, Control(psi))

            print "old J = " + str(J_old)
            print "new J = " + str(Jm(psi))

            # Check if J decreases, if not backtracking line search
<<<<<<< HEAD
            if i >= 9:
		while Jm(psi) > J_old:
                	kappa = kappa*0.5

                	if kappa < self.kappa_tol:
                   		"Solution at local minimum"
                    		print "Theta_list:"
                    		print self.theta_list
                    		return psi_ref, self.J_list

                	print "Not decrease in J, backtracking line search for new psi"
                	psi = self.new_psi(kappa, psi_ref, g)
                	u, p, Z = nsd.solve(psi)
                	J = Functional((p * ds(1) - 3. * p * ds(2)) * dt
                               		+ beta * fluid_domain(psi) * dx * dt)
                	Jm = ReducedFunctional(J, Control(psi))
                	print "new J = " + str(Jm(psi))
=======
            while Jm(psi) > J_old:
                kappa = kappa*0.5

                if kappa < self.kappa_tol:
                    "Solution at local minimum"
                    print "Theta_list:"
                    print self.theta_list
                    return psi_ref, self.J_list

                print "Not decrease in J, backtracking line search for new psi"
                psi = self.new_psi(kappa, psi_ref, g)
                u, p = nsd.solve(psi)
                J = Functional((p * ds(1) - 3. * p * ds(2)) * dt
                               + beta * fluid_domain(psi) * dx * dt)
                Jm = ReducedFunctional(J, Control(psi))
                print "new J = " + str(Jm(psi))
>>>>>>> 97a7a9fd51c9cbd1d9c8756c9ce5ce1741f0e85d

            if plot_:
                plot(psi, title="psi", key="psi")
                plot(alpha(psi), title="alpha(psi)", key="a")
            fluid = assemble(conditional(psi > 0, 0, 1) * dx)
            print "Fluid = " + str(fluid)
            self.fluid_list.append(fluid)

            a = project(alpha(psi), self.V0, annotate=False)
            if write_:
                alpha_file << a

        self.J_list.append(Jm(psi))
        print "Theta_list:"
        print self.theta_list
        print "Z_list = "
        print Z_list
        return psi, self.J_list

        


<<<<<<< HEAD
write_ = True
=======
write_ = False
>>>>>>> 97a7a9fd51c9cbd1d9c8756c9ce5ce1741f0e85d
plot_ = False

problem = Problem()
method = GradientMethod(problem)
psi, J_list = method.run_gradient_method()
print "J_list:"
print J_list


interactive()
