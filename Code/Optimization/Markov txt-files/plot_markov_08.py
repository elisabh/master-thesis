import matplotlib.pyplot as plt

J_list = [4817.1167181590035, 4871.387726070062, 4745.860472321524, 3624.8556009776557, 3560.121208136086, 2663.9113599519796, 3133.7433862871567, 2478.690889328053, 2548.915867302446, 2383.9024231169064, 2267.425488167558, 2254.029874820896, 2140.056741785508, 2237.269039247945, 2156.03746786745, 2131.878378437693, 2174.2485143885337, 2195.0384471204984, 2156.4873501369066, 2122.9121628646244]
fluid_list = [9.89999999999999, 9.79871321395296, 8.190949949909113, 6.8928126074268805, 5.2061356463286526, 4.684051010923852, 3.9669061288275054, 3.9487055080366864, 3.6057579422444785, 3.520631144224808, 3.2545285819710807, 3.158241675749389, 2.9917145890517842, 2.9124314776433335, 2.9130949691515404, 2.8044415634053808, 2.769504751658264, 2.7761929510621917, 2.765469236026785, 2.715128416536463]

x = [i+1 for i in range(len(J_list))]
y = [i+1 for i in range(len(fluid_list))]

fluid = [i/10.0 for i in fluid_list]


plt.plot(x, J_list, "-o", color='red', lw="2", label="Objective")
# plt.plot(y, fluid_list, "-s", label="Fluid")
plt.legend()
plt.ylabel('Objective function')
plt.xlabel("Iteration")
plt.show()


# fig, ax1 = plt.subplots()
# line1 = ax1.plot(x, J_list, 'b-o', label="Objective")
# ax2 = ax1.twinx()
# line2 = ax2.plot(y, fluid, 'r-s', label="Fluid fraction")
#
# lines = line1+line2
# labls = [l.get_label() for l in lines]
# ax1.legend(lines, labls)
#
# ax1.set_ylabel('Objective')
# ax2.set_ylabel('Fluid fraction')
# ax1.set_xlabel('Iteration')
#
# plt.show()