import matplotlib.pyplot as plt

J_list = [2428.609067208781, 2593.7224245616144, 2699.368397780821, 2061.9285687444503, 2084.4484310761327, 1579.3150435380492, 1907.1536996432826, 1468.6293249357623, 1533.4434088962566, 1461.2016909531499, 1499.2932464733537, 1460.5732649021463, 1407.801732255198, 1413.1968175097852, 1392.7201980663144, 1402.3643596396782, 1362.2745092774953]

fluid_list = [9.89999999999999, 9.8168727985921, 8.701386763291952, 7.8312653988076555, 5.831114567649803, 5.30748980798957, 4.490997143074892, 4.473743881537715, 4.192002472638941, 4.152187713252186, 3.982574011272592, 3.9077476480088955, 3.7784859104082114, 3.7142928461547196, 3.6356957283981957, 3.580744878000601, 3.5781125155139315]

x = [i+1 for i in range(len(J_list))]
y = [i+1 for i in range(len(fluid_list))]

fluid = [i/10.0 for i in fluid_list]


plt.plot(x, J_list, "-o", color='red', lw="2", label="Vorticity functional")
# plt.plot(y, fluid_list, "-s", label="Fluid")
plt.legend()
plt.ylabel('Objective function')
plt.xlabel("Iteration")
plt.show()


# fig, ax1 = plt.subplots()
# line1 = ax1.plot(x, J_list, 'b-o', label="Objective")
# ax2 = ax1.twinx()
# line2 = ax2.plot(y, fluid, 'r-s', label="Fluid fraction")
#
# lines = line1+line2
# labls = [l.get_label() for l in lines]
# ax1.legend(lines, labls)
#
# ax1.set_ylabel('Objective')
# ax2.set_ylabel('Fluid fraction')
# ax1.set_xlabel('Iteration')
#
# plt.show()
