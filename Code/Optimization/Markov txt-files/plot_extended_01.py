import matplotlib.pyplot as plt

J_list = [5510.7037726855815, 5447.02875448627, 5357.958098465301, 4256.38038521518, 4328.219104842335, 3299.4700176905267, 4354.77431399854, 3165.3959990341104, 3058.829178001417, 3022.228380537937, 2915.2503357640812, 2803.84075609369, 2768.3463697263255, 2880.2121071550455, 2749.4356129691205, 2723.873585931489, 2782.595233605142, 2789.98520823696, 2679.799143494542]

fluid_list = [9.899999999999975, 9.817093891998537, 8.149757480372866, 7.045915177610802, 5.365754980491232, 4.853016290879693, 4.132550060054648, 4.119999701377538, 3.729227355365545, 3.5488457173318, 3.338172576922882, 3.185604667059656, 3.019977352871738, 2.960571975040031, 2.897296558027624, 2.7990372223890048, 2.7780733114814735, 2.7848436898335507, 2.726957463379422]

x = [i+1 for i in range(len(J_list))]
y = [i+1 for i in range(len(fluid_list))]

fluid = [i/10.0 for i in fluid_list]


plt.plot(x, J_list, "-o", color='red', lw="2", label="Energy dissipation functional")
# plt.plot(y, fluid_list, "-s", label="Fluid")
plt.legend()
plt.ylabel('Objective function')
plt.xlabel("Iteration")
plt.show()


# fig, ax1 = plt.subplots()
# line1 = ax1.plot(x, J_list, 'b-o', label="Objective")
# ax2 = ax1.twinx()
# line2 = ax2.plot(y, fluid, 'r-s', label="Fluid fraction")
#
# lines = line1+line2
# labls = [l.get_label() for l in lines]
# ax1.legend(lines, labls)
#
# ax1.set_ylabel('Objective')
# ax2.set_ylabel('Fluid fraction')
# ax1.set_xlabel('Iteration')
#
# plt.show()
