# Bypass problem, try to make smooth gradient with object oriented coding

from dolfin import *
from dolfin_adjoint import *
import numpy as np
from matplotlib import pyplot as plt



class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


# Boundaries for Bypass
class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0, 0.35)) and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0, 0.35)) and near(x[0], 6.0)


class Vein(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[1], (0.0, 0.35))


def alpha(psi, name="alpha", q=0.1):
    nrm_psi = sqrt(assemble(dot(psi, psi) * dx))
    if nrm_psi == 0:
        q = 1.0
        print "q = " + str(q)
    return ALPHA_L + (ALPHA_U - ALPHA_L) * (1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q)))


def fluid_domain(psi, q=0.1):
    nrm_psi = sqrt(assemble(dot(psi, psi) * dx))
    if nrm_psi == 0:
        q = 1.0
    return 1.0 - 1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q))


set_log_level(40)

# nu = Constant(0.0035, name="nu") # From Cole 2002
nu = Constant(0.04) # From Rozza 2005, Agoshkov 2006
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)
beta = Constant(200.0)
# beta=0.0001 works better than 0.001 for vorticity. Try 0.0005 next
mesh_name = "../mesh/artery_02_custom.xml"
# Mesh

class Problem:
    def __init__(self):
        self.mesh = None
        self.boundaries = None
        self.domains = None

        self.make_mesh()
        self.mark_boundaries()
        self.mark_domains()
        self.T = 0.8
        self.timestep = Constant(0.002)

    def make_mesh(self):
        self.mesh = Mesh(mesh_name)

    def mark_boundaries(self):
        noslip = Noslip()
        inlet = Inlet()
        outlet = Outlet()

        self.boundaries = MeshFunction("size_t", self.mesh, 1)
        self.boundaries.set_all(4)
        noslip.mark(self.boundaries, 0)
        inlet.mark(self.boundaries, 1)
        outlet.mark(self.boundaries, 2)

    def boundary_conditions(self, V, Q, t):

        # Left anterior descending coronary artery
        # waveform source: Ku DN and Zhu C. In: Sumpio BE, Hemodynamic Forces and Vascular Cell Biology. RG Landes Company, Austin, pp. 1.23, 1993.
        # Fourier coefficients derived by DA Steinman, and placed in the public domain.

        A = [1, -0.459305, 0.27711, -0.0632653, 0.213156, 0.0850977, -0.0446033, 0.00836924,
             -0.0300109, -0.0433541, 0.00988043, -0.0112574, -0.0048216, 0.0166013]
        B = [0, -0.678336, -0.128518, -0.0820616, -0.110139, 0.152294, 0.0456851, 0.0261691,
             0.0417271, -0.0198078, -0.0115691, 0.00690928, -0.0209537, 0.000802334]

        N = 13
        U = 0.0
        T = 0.8
        for k in range(N + 1):
            Ak = A[k]
            Bk = B[k]
            U += Ak * np.cos(2 * np.pi * k * t / T) + Bk * np.sin(2 * np.pi * k * t / T)

        u_1 = Expression(("U * (1 - (2*(x[1]-0.175)/0.35) * (2*(x[1]-0.175)/0.35) )", "0"),
                         U=20.0*U, degree=3)

        bcu = [DirichletBC(V, (0.0, 0.0), self.boundaries, 0),
               DirichletBC(V, u_1, self.boundaries, 1)]
        bcp = [DirichletBC(Q, 0.0, self.boundaries, 2)]

        return bcu, bcp

    def initial_values(self):
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        u_ic = project(Expression(("0.0", "0.0"), degree=3),  V, name="u_ic")
        p_ic = project(Expression("0.0", degree=1), Q, name="p_ic")
        u0 = u_ic.copy(deepcopy=True)
        p0 = p_ic.copy(deepcopy=True)
        return u0, p0

    def mark_domains(self):
        self.domains = CellFunction("size_t", self.mesh)
        self.domains.set_all(1)

        vein = Vein()
        vein.mark(self.domains, 0)


class NSDsolver:

    def __init__(self, problem):
        self.problem = problem
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries

    def solve(self, psi):

        print "Solving Navier-Stokes-Darcy"

        adj_reset()
        timestep = problem.timestep

        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        # Initial velocity and pressure
        u0, p0 = problem.initial_values()

        # Declare velocity functions
        u = Function(V, name="u", annotate=True)
        v = TestFunction(V)

        # Weak form for tentative velocity
        F1 = (1./timestep) * inner(u - u0, v) * dx \
            + inner(grad(u) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + alpha(psi) * inner(u, v) * dx \
            + inner(grad(p0), v) * dx \
            - nu * inner(dot(n, nabla_grad(u)), v) * ds(2) \
            + inner(p0 * n, v) * ds(2)


        # Declare pressure functions (after tentative velocity weak form!!)
        p = Function(Q, name="p", annotate=True)
        q = TestFunction(Q)

        # Pressure correction weak form
        F2 = inner(grad(p - p0), grad(q)) * dx \
             + (1. / timestep) * div(u0) * q * dx


        # Velocity update weak form
        F3 = (1./timestep)*inner(u - u0, v) * dx \
             + inner(grad(p - p0), v) * dx

        t = 0.0
        psi_tmp = psi.copy(deepcopy=True)

        adj_time = True
        annotate = True
        adj_start_timestep()

        while t < problem.T:

            # u and p work as trial functions
            # u0 and p0 work as newest value

            # Boundary conditions, Pousille flow at inlet
            bcu, bcp = problem.boundary_conditions(V, Q, t)

            solve(F1 == 0, u, bcu)
            u0.assign(u, annotate=annotate)
            solve(F2 == 0, p, bcp)
            solve(F3 == 0, u, bcu)

            psi.assign(psi_tmp, annotate=annotate)
            u0.assign(u, annotate=annotate)
            p0.assign(p, annotate=annotate)

            t += float(timestep)
            if adj_time:
                adj_inc_timestep(t, t > problem.T)

        return u0, p0


class GradientMethod:
    def __init__(self, problem):
        self.problem = problem
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries
        self.domains = problem.domains
        self.V0 = FunctionSpace(self.mesh, "CG", 1)
        self.PSI = "-1"
        self.n_max = 20
        self.kappa_tol = 0.001
        self.J_list = []
        self.theta_list = []
        self.fluid_list = []

    def new_psi(self, kappa, psi, g):

        dx = Measure("dx")(subdomain_data=self.domains)
        dot_prod = assemble(dot(g, psi) * dx(1))
        nrm_g = sqrt(assemble(dot(g, g) * dx(1)))
        nrm_psi = sqrt(assemble(dot(psi, psi) * dx(1)))

        if nrm_g * nrm_psi == 0:
            print "nrm_g * nrm_psi = 0!"
            nrm_g = 1.0
            nrm_psi = 1.0
        theta = acos(dot_prod / (nrm_g * nrm_psi))
        print "Kappa = " + str(kappa)
        print "Theta = " + str(theta)

        k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
        k2 = sin(kappa * theta) / (sin(theta) * nrm_g)

        psi = project(k1 * psi - k2 * g, self.V0, annotate=False)
        psi = project(Expression("psi*(x[1]>=0.35) - nrm*(x[1]<0.35)",
                                 psi=psi, nrm=nrm_psi, degree=2), self.V0, annotate=False)
        return psi, theta

    def run_gradient_method(self):

        ds = Measure("ds")(subdomain_data=self.boundaries)
        dx = Measure("dx")(subdomain_data=self.domains)
        kappa = 0.5

        # Design parameter
        psi_ref = Function(self.V0)
        psi = project(Expression(self.PSI, degree=2), self.V0, annotate=True)


        # Initialize problem and Navier-Stokes-Darcy solver
        nsd = NSDsolver(self.problem)
        u, p = nsd.solve(psi)

        if write_:
            a = project(alpha(psi), self.V0, annotate=False)
            alpha_file = File('./ParaView/Bypass_markov/alpha_bypass_markov_10.pvd')
            alpha_file << a
            u_file = File('./ParaView/Bypass_markov/u_bypass_markov_10.pvd')
            u_file << u

        # Dolfin-adjoint functional
        # J = Functional(nu*inner(grad(u), grad(u)) * dx * dt
        #                + alpha(psi) * inner(u, u) * dx * dt
        #                + beta * fluid_domain(psi) * dx(1) * dt)
        J = Functional(nu*(inner(curl(u), curl(u))) * dx * dt
                       + alpha(psi) * inner(u, u) * dx * dt
                       + beta * fluid_domain(psi) * dx(1) * dt)

        for i in range(self.n_max):
            print "\nIteration #" + str(i+1)

            # Dolfin-adjoint gradient, check that it gets an expected value
            print "Computing gradient"
            g = compute_gradient(J, Control(psi), forget=False)

            # Store old values of J and psi:
            Jm = ReducedFunctional(J, Control(psi))
            J_old = Jm(psi)
            self.J_list.append(J_old)
            psi_ref.assign(psi)

            # Update psi and calculate new J
            kappa = min(0.5, kappa * 1.5)
            psi, theta = self.new_psi(kappa, psi_ref, g)
            u, p = nsd.solve(psi)

            J = Functional(nu*(inner(curl(u), curl(u))) * dx * dt
                           + alpha(psi) * inner(u, u) * dx * dt
                           + beta * fluid_domain(psi) * dx(1) * dt)

            Jm = ReducedFunctional(J, Control(psi))

            print "old J = " + str(J_old)
            print "new J = " + str(Jm(psi))

            fluid = assemble(conditional(psi > 0, 0, 1) * dx(1))
            print "Fluid = " + str(fluid)
            self.theta_list.append(theta)
            self.fluid_list.append(fluid)

            if write_:
                a = project(alpha(psi), self.V0, annotate=False)
                alpha_file << a
                u_file << u

        self.theta_list.append(theta)
        self.J_list.append(Jm(psi))

        print "beta = " + str(float(beta))
        print "theta_list:"
        print self.theta_list
        print "J_list:"
        print self.J_list
        print "fluid_list:"
        print self.fluid_list




write_ = True
plot_ = False

problem = Problem()
method = GradientMethod(problem)
method.run_gradient_method()



interactive()
