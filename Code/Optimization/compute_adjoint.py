from dolfin import *
from dolfin_adjoint import *
import copy

class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0) and \
               (between(x[1], (0.6667, 0.8333)) or between(x[1], (0.1667, 0.3333)))


class Left(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0)


class Inlet1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0) and between(x[1], (0.6667, 0.8333))


class Inlet2(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0) and between(x[1], (0.1667, 0.3333))


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.5) and \
               (between(x[1], (0.6667, 0.8333)) or between(x[1], (0.1667, 0.3333)))


def alpha(psi):
    # return ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi, 0), 0, 1)
    # return ALPHA_L + (ALPHA_U - ALPHA_L) * psi * (1 + q)/(psi + q)
    return ALPHA_L + (ALPHA_U - ALPHA_L) * q * (1 - psi)/(psi + q)


def fluid_domain(psi):
    return conditional(psi > 0, 0, 1)

nu = Constant(1.0, name="nu")
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)
q = Constant(0.01)


class Problem:

    def __init__(self):
        self.N = 30
        self.T = 0.1
        # self.N_t = 10 # Time steps
        self.dt = 0.01 #self.T/self.N_t
        self.mesh = None
        self.boundaries = None
        self.adjoint_boundaries = None

        self.make_mesh()
        self.mark_boundaries()
        self.mark_adjoint_boundaries()

    def make_mesh(self):
        self.mesh = RectangleMesh(Point(0, 0), Point(1.5, 1), self.N, self.N, "right")

    def mark_boundaries(self):

        # Mark boundaries
        noslip = Noslip()
        inlet1 = Inlet1()
        inlet2 = Inlet2()
        outlet = Outlet()

        self.boundaries = MeshFunction("size_t", self.mesh, 1)
        self.boundaries.set_all(4)
        noslip.mark(self.boundaries, 0)
        inlet1.mark(self.boundaries, 1)
        inlet2.mark(self.boundaries, 2)
        outlet.mark(self.boundaries, 3)



    def mark_adjoint_boundaries(self):

        noslip = Noslip()
        outlet = Outlet()

        self.adjoint_boundaries = MeshFunction("size_t", self.mesh, 1)
        self.adjoint_boundaries.set_all(4)
        noslip.mark(self.adjoint_boundaries, 0)
        outlet.mark(self.adjoint_boundaries, 3)


class NSDSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries
        self.T = problem.T
        self.dt = problem.dt

    def set_bcs(self, t):
        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        # Initial velocity
        # u_0 = Expression(("U * (1 - (2*(x[1]-0.25)/0.1667)*(2*(x[1]-0.25)/0.1667)) * (x[1]>0.1667)*(x[1]<0.3333) + "
        #                   "U * (1 - (2*(x[1]-0.75)/0.1667)*(2*(x[1]-0.75)/0.1667)) * (x[1]>0.6667)*(x[1]<0.8333)",
        #                   "0"), U=1.0, degree=3)
        u_1 = Expression(("-U * (x[1]-4./6.)*(x[1]-5./6.) * (x[1]>0.6667)*(x[1]<0.8333) * cos(8*t)",
                          "0"), U=144.0, t=t, degree=3)
        u_2 = Expression(("-U * (x[1]-1./6.)*(x[1]-2./6.) * (x[1]>0.1667)*(x[1]<0.3333) * sin(8*t)",
                          "0"), U=144.0, t=t, degree=3)

        bcu = [DirichletBC(V, (0.0, 0.0), self.boundaries, 0),
               DirichletBC(V, u_1, self.boundaries, 1),
               DirichletBC(V, u_2, self.boundaries, 2)]

        bcp = [DirichletBC(Q, 0.0, self.boundaries, 3)]
        return bcu, bcp

    def solve_IPCS(self, psi):

        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        # Define trial and test functions
        u = TrialFunction(V)
        p = TrialFunction(Q)
        v = TestFunction(V)
        q = TestFunction(Q)

        u0 = Function(V, name='u0')
        u1 = Function(V, name='u1')
        p0 = Function(Q, name='p0')
        p1 = Function(Q, name='p1')


        k = Constant(self.dt)
        # Tentative velocity step
        F1 = (1./k) * inner(u - u0, v) * dx \
            + inner(grad(u0) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + inner(grad(p0), v) * dx \
            + alpha(psi) * inner(u, v) * dx \
            - nu * inner(dot(n, nabla_grad(u)), v) * ds(3) \
            + inner(p0 * n, v) * ds(3)
        a1 = lhs(F1)
        L1 = rhs(F1)

        # Pressure update
        a2 = inner(grad(p), grad(q)) * dx
        L2 = inner(grad(p0), grad(q)) * dx \
            - ((1./k) + alpha(psi)) * div(u1) * q * dx

        # Velocity update
        a3 = (1. / k + alpha(psi)) * inner(u, v) * dx
        L3 = (1. / k + alpha(psi)) * inner(u1, v) * dx \
            - inner(grad(p1 - p0), v) * dx

        # Assemble system
        A1 = assemble(a1)
        A2 = assemble(a2)
        A3 = assemble(a3)

        b1 = None
        b2 = None
        b3 = None

        t = 0

        u_list = []
        p_list = []

        while t < self.T:
            bcu, bcp = self.set_bcs(t)
            t += self.dt

            # Tentative velocity
            b1 = assemble(L1, tensor=b1)
            [bc.apply(A1, b1) for bc in bcu]
            solve(A1, u1.vector(), b1)

            # Pressure correction
            b2 = assemble(L2, tensor=b2)
            [bc.apply(A2, b2) for bc in bcp]
            solve(A2, p1.vector(), b2)

            # Velocity update
            b3 = assemble(L3, tensor=b3)
            [bc.apply(A3, b3) for bc in bcu]
            solve(A3, u1.vector(), b3)

            u_temp = Function(V)
            u_temp.assign(u1)
            u_list.append(u_temp)

            p_temp = Function(Q)
            p_temp.assign(p1)
            p_list.append(p_temp)


            u0.assign(u1)
            p0.assign(p1)

        return u_list, p_list


class AdjointSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.boundaries = problem.adjoint_boundaries
        self.T = problem.T
        self.dt = problem.dt

    def solve_coupled(self, psi, u_list):

        # Adjoint coupled:
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)
        W = V*Q

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        (u, p) = TrialFunctions(W)
        (v, q) = TestFunctions(W)
        w = Function(W)
        (ua, pa) = split(w)


        u_a = Function(V) # adjoints to be stored
        assigner = FunctionAssigner(V, W.sub(0)) # to assign w.sub(0) to u_a
        ua_list = [] # store adjoints u_a

        bcs = [DirichletBC(W.sub(0), (0.0, 0.0), self.boundaries, 0)]


        k = Constant(self.dt)
        t = self.T

        for u0 in reversed(u_list):
            t -= self.dt

            # Not sure if this explicitly is necessary
            F_a = (1. / k) * inner(u - ua, v) * dx \
                - inner(dot(grad(u), u0), v) * dx \
                - inner(dot(u0, grad(u)), v) * dx \
                + nu * inner(grad(u), grad(v)) * dx \
                + alpha(psi) * inner(u, v) * dx \
                - div(u) * q * dx - div(v) * p * dx \
                - 2 * nu * inner(grad(u0), grad(v)) * dx \
                - 2 * alpha(psi) * inner(u0, v) * dx \
                - inner(p * n, v) * ds(3) \
                + nu * inner(dot(n, grad(u)), v) * ds(3) \
                + inner(dot(u0, u) * n, v) * ds(3) \
                + inner(dot(u0, n) * u, v) * ds(3)

        # - 2 * nu * inner(grad(u0) * n, v) * ds(3) \
        # + 2 * alpha(psi) * inner(u0, v) * ds(3)


            a = lhs(F_a)
            L = rhs(F_a)

            problem = LinearVariationalProblem(a, L, w, bcs)
            solver = LinearVariationalSolver(problem)
            solver.solve()
            (ua, pa) = split(w)

            plot(u0, title="Velocity", key="u")
            plot(ua, title="Adjoint", key="ua")


            assigner.assign(u_a, w.sub(0))
            ua_list.append(u_a)

        return ua_list[::-1]


problem = Problem()
V0 = FunctionSpace(problem.mesh, "DG", 0)
# psi = project(Expression("1 - 2*(x[1]>0.16)*(x[1]<0.33)", degree=1), V0)
psi = project(Expression("-1", degree=1), V0)





# plot(sign(psi))

NSD = NSDSolver(problem)
adjoint = AdjointSolver(problem)

u_list, p_list = NSD.solve_IPCS(psi)
# ua_list = adjoint.solve_coupled(psi, u_list)
#
# left = Left()
# left.mark(problem.boundaries, 5)

# J = 0.0
# N = len(p_list)
# ds = Measure("ds")(subdomain_data=problem.boundaries)
# n = FacetNormal(problem.mesh)
# for p in p_list:
#     p_inlet = assemble(p*ds(5))
#     p_outlet = assemble(sqrt(inner(p*n, p*n))*ds(5))
#     print "\np_outlet, p_inlet:"
#     print p_inlet
#     print (p_outlet)
#     print p_outlet - p_inlet

beta = 100.0
u = u_list[-1]
J = Functional(alpha(psi)*inner(u, u) * dx
               + nu * inner(grad(u), grad(u)) * dx
               + (beta * fluid_domain(psi)) * dx)


m = Control(psi)
dJ = compute_gradient(J, m)

plot(dJ)

# N = len(u_list)
# # print N
# for i in range(N):
#     u1 = u_list[i]
#     ua = ua_list[i]
#     plot(u1, range_min=0., range_max=1., title="Velocity", key="2")
#     plot(ua, title="Adjoint", key="a")


# plot(u_list[0], title="u_start", key="u1")
# plot(ua_list[-1], title="ua_start", key="ua1")

# plot(u_list[-1], title="u_end", key="u2")
# plot(ua_list[0], title="ua_end", key="ua2")

interactive()
# vtkfile = File('u2_start.pvd')
# vtkfile << u_list[0]
#
# vtkfile2 = File('u2_end.pvd')
# vtkfile2 << u_list[-1]