from dolfin import *

class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.3, 0.7)) and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.3, 0.7)) and near(x[0], 1.0)

# options:
# "mesh": takes in mesh string or grid size N
# "Darcy": solves ordinary NS if False and NSD if True


class Problem:
    def __init__(self, options):
        # Parameters must be defined by subclass
        self.f = None
        self.bcu = []
        self.bcp = []
        self.nu = Constant(0.01)
        self.U = 1.0
        self.N = None
        self.y1 = 0.3
        self.y2 = 0.7
        self.boundaries = None
        self.dt = 0.001
        self.T = 1.0

        self.ALPHA_U = 2.5 * self.nu / (0.01 * 0.01)
        self.ALPHA_L = 2.5 * self.nu / (100 * 100)

        # Save options:
        self.options = options

        if ".xml" in options["mesh"]:
            self.mesh = Mesh(options["mesh"])
        else:
            self.N = options["mesh"]
            self.mesh = UnitSquareMesh(self.N, self.N)


    def refine_mesh(self):
        self.mesh = refine(self.mesh)
        self.mark_boundaries()

    def get_mesh(self):
        self.mark_boundaries()
        return self.mesh

    def initial_conditions(self):
        y1 = self.y1
        y2 = self.y2
        u_in = Expression(("U * (1 - (2*(x[1]-dy)/(y2-y1))*(2*(x[1]-dy)/(y2-y1))) * (x[1]>y1)*(x[1]<y2)",
                           "0"), U=self.U, dy=0.5, y1=y1, y2=y2, degree=3)
        return u_in

    def mark_boundaries(self):
        noslip = Noslip()
        inlet = Inlet()
        outlet = Outlet()

        boundaries = MeshFunction("size_t", self.mesh, 1)
        boundaries.set_all(3)
        noslip.mark(boundaries, 0)
        inlet.mark(boundaries, 1)
        outlet.mark(boundaries, 2)

        self.boundaries = boundaries

    def boundary_conditions(self, u_in, V, Q):

        bcu = [DirichletBC(V, (0.0, 0.0), self.boundaries, 0),
               DirichletBC(V, u_in, self.boundaries, 1)]
        bcp = [DirichletBC(Q, 0, self.boundaries, 2)]

        return bcu, bcp

    def level_set(self):
        PSI = "1 - 2*(x[1] > y1)*(x[1] < y2)"
        V0 = FunctionSpace(self.mesh, "DG", 0)
        psi = project(Expression(PSI, y1=self.y1, y2=self.y2, degree=1), V0)
        return psi