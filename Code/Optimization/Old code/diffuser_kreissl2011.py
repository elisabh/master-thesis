# -- Diffuser from Kreissl 2011 --

from dolfin import *
from dolfin_adjoint import *
import time


class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 47./42) and between(x[1], (0.3333, 0.6667))

class Design(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (5./42, 1.0))


def alpha(psi):
    return ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi, 0), 0, 1)


def fluid_domain(psi):
    return conditional(psi > 0, 0, 1)


class Problem:

    def __init__(self):
        self.nx = 47
        self.ny = 42
        self.dt = 0.00002 #self.T/self.N_t
        self.N_t = 15
        self.T = self.N_t*self.dt

        self.mesh = None
        self.boundaries = None
        self.adjoint_boundaries = None
        self.domains = None

        self.make_mesh()
        self.mark_boundaries()
        self.mark_adjoint_boundaries()
        self.mark_subdomain()

    def make_mesh(self):
        self.mesh = RectangleMesh(Point(0, 0), Point(47./42, 1), self.nx, self.ny, "right/left")

    def mark_boundaries(self):

        # Mark boundaries
        noslip = Noslip()
        inlet = Inlet()
        outlet = Outlet()

        self.boundaries = MeshFunction("size_t", self.mesh, 1)
        self.boundaries.set_all(4)
        noslip.mark(self.boundaries, 0)
        inlet.mark(self.boundaries, 1)
        outlet.mark(self.boundaries, 3)


    def boundary_conditions(self, V, Q, t):
        U = sin(3.1415/(2*self.T)*t)
        u_1 = Expression(("U * (1 - (2*(x[1]-0.5)) * (2*(x[1]-0.5)) )", "0"),
                         U=U, degree=3)

        bcu = [DirichletBC(V, (0.0, 0.0), self.boundaries, 0),
               DirichletBC(V, u_1, self.boundaries, 1)]
        bcp = [DirichletBC(Q, 0.0, self.boundaries, 3)]

        return bcu, bcp

    def mark_adjoint_boundaries(self):

        noslip = Noslip()
        outlet = Outlet()

        self.adjoint_boundaries = MeshFunction("size_t", self.mesh, 1)
        self.adjoint_boundaries.set_all(4)
        noslip.mark(self.adjoint_boundaries, 0)
        outlet.mark(self.adjoint_boundaries, 3)

    def adjoint_boundary_conditions(self, V):
        return [DirichletBC(V, (0.0, 0.0), self.adjoint_boundaries, 0)]

    def mark_subdomain(self):

        design = Design()

        self.domains = CellFunction("size_t", self.mesh)
        self.domains.set_all(0)
        design.mark(self.domains, 1)


class NSDSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries
        self.domains = problem.domains
        self.T = problem.T
        self.dt = problem.dt

    def solve_IPCS(self, psi):

        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        dx = Measure("dx")(subdomain_data=self.domains)
        n = FacetNormal(self.mesh)

        # Define trial and test functions
        u = TrialFunction(V)
        p = TrialFunction(Q)
        v = TestFunction(V)
        q = TestFunction(Q)

        u0 = Function(V, name='u0')
        u1 = Function(V, name='u1')
        p0 = Function(Q, name='p0')
        p1 = Function(Q, name='p1')

        k = Constant(self.dt)
        # Tentative velocity step
        F1 = (1./k) * inner(u - u0, v) * dx \
            + inner(grad(u0) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + inner(grad(p0), v) * dx \
            + alpha(psi) * inner(u, v) * dx \
            - nu * inner(dot(n, nabla_grad(u)), v) * ds(3) \
            + inner(p0 * n, v) * ds(3)

        a1 = lhs(F1)
        L1 = rhs(F1)

        # Pressure update
        a2 = inner(grad(p), grad(q)) * dx
        L2 = inner(grad(p0), grad(q)) * dx \
            - ((1./k) + alpha(psi)) * div(u1) * q * dx

        # Velocity update
        a3 = (1. / k + alpha(psi)) * inner(u, v) * dx
        L3 = (1. / k + alpha(psi)) * inner(u1, v) * dx \
            - inner(grad(p1 - p0), v) * dx

        # Assemble system
        A1 = assemble(a1)
        A2 = assemble(a2)
        A3 = assemble(a3)

        b1 = None
        b2 = None
        b3 = None

        t = 0.0
        u_list = []

        while t < self.T:
            bcu, bcp = problem.boundary_conditions(V, Q, t)
            t += self.dt

            # Tentative velocity
            b1 = assemble(L1, tensor=b1)
            [bc.apply(A1, b1) for bc in bcu]
            solve(A1, u1.vector(), b1)

            # Pressure correction
            b2 = assemble(L2, tensor=b2)
            [bc.apply(A2, b2) for bc in bcp]
            solve(A2, p1.vector(), b2)

            # Velocity update
            b3 = assemble(L3, tensor=b3)
            [bc.apply(A3, b3) for bc in bcu]
            solve(A3, u1.vector(), b3)

            if plot_u:
                plot(u1, title="Velocity", key="u1")

            u_temp = Function(V)
            u_temp.assign(u1)
            u_list.append(u_temp)

            u0.assign(u1)
            p0.assign(p1)

        return u_list


class AdjointSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries
        self.T = problem.T
        self.dt = problem.dt

    def solve_coupled(self, psi, u_list):

        # Adjoint coupled:
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)
        W = V*Q

        # P2 = VectorElement('P', 'triangle', 2)
        # P1 = FiniteElement('P', 'triangle', 1)
        # TH = P2 * P1
        # W = FunctionSpace(mesh, TH)
        # (V, Q) = split(W)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        (u, p) = TrialFunctions(W)
        (v, q) = TestFunctions(W)
        w = Function(W)
        (ua, pa) = split(w)

        assigner = FunctionAssigner(V, W.sub(0)) # to assign w.sub(0) to u_a
        ua_list = []

        bcu = problem.adjoint_boundary_conditions(V)

        k = Constant(self.dt)
        t = self.T

        for u0 in reversed(u_list):
            t -= self.dt

            # Not sure if this explicitly is necessary
            F_a = (1. / k) * inner(u - ua, v) * dx \
                  - inner(dot(grad(u), u0), v) * dx \
                  - inner(dot(u0, grad(u)), v) * dx \
                  + nu * inner(grad(u), grad(v)) * dx \
                  + alpha(psi) * inner(u, v) * dx \
                  - div(u) * q * dx - div(v) * p * dx \
                  - 2 * nu * inner(grad(u0), grad(v)) * dx \
                  - 2 * alpha(psi) * inner(u0, v) * dx \
                  - inner(p * n, v) * ds(3) \
                  + nu * inner(dot(n, grad(u)), v) * ds(3) \
                  + inner(dot(u0, u) * n, v) * ds(3) \
                  + inner(dot(u0, n) * u, v) * ds(3)
            a = lhs(F_a)
            L = rhs(F_a)

            adj_problem = LinearVariationalProblem(a, L, w, bcu)
            solver = LinearVariationalSolver(adj_problem)
            solver.solve()
            (ua, pa) = split(w)
            # plot(ua, title="Adjoint", key="a")

            u_a = Function(V)  # to solve the adjoints
            assigner.assign(u_a, w.sub(0))
            ua_list.append(u_a)

        # return reversed ua_list, such that it has the same ordering as u_list
        return ua_list[::-1]


class GradientMethod:

    def __init__(self, problem):
        self.problem = problem
        self.domains = problem.domains
        self.boundaries = problem.boundaries
        self.V0 = FunctionSpace(problem.mesh, "DG", 0)
        self.PSI = "-1"
        self.beta = 1600.0
        self.n_max = 20
        self.tol_theta = 0.01
        self.tol_kappa = 0.1

    def fluid(self, psi):
        dx = Measure("dx")(subdomain_data=self.domains)
        return assemble(fluid_domain(psi) * dx(1))

    def functional(self, psi, u_list):
        J = 0.0
        N = len(u_list)
        dx = Measure("dx")(subdomain_data=self.domains)
        for u in u_list:
            J += 1./N * assemble(alpha(psi)*inner(u, u)*dx()
                                 + nu*inner(grad(u), grad(u))*dx())
        J += assemble(self.beta*fluid_domain(psi)*dx(1))
        return J

    def pressure_functional(self, psi, p_list):
        J = 0.0
        N = len(p_list)
        ds = Measure("ds")(subdomain_data=self.boundaries)
        dx = Measure("dx")(subdomain_data=self.domains)
        n = FacetNormal(problem.mesh)
        for p in p_list:
            p_inlet = assemble(p*ds(1))
            p_outlet = assemble(p*ds(3))
            J += 1./N * (p_inlet/1.0 - p_outlet/1.0)
        J += assemble(self.beta * fluid_domain(psi) * dx(1))
        return J

    def gradient(self, g, u_, ua_):
        N = len(u_)
        g.vector().zero()
        for i in range(N):
            u = u_[i]
            ua = ua_[i]
            direction = - (ALPHA_U - ALPHA_L) * inner(u, u - ua)
            g_temp = project(direction + self.beta, self.V0)
            g.vector().axpy(1./N, g_temp.vector())

    def angle(self, g, psi):
        dx = Measure("dx")(subdomain_data=self.domains)
        dot_prod = assemble(dot(g, psi) * dx(1))
        nrm_g = sqrt(assemble(dot(g, g) * dx(1)))
        nrm_psi = sqrt(assemble(dot(psi, psi) * dx(1)))
        return acos(dot_prod / (nrm_g * nrm_psi))

    def new_psi(self, psi, kappa, theta, psi_ref, g):

        dx = Measure("dx")(subdomain_data=self.domains)
        nrm_g = sqrt(assemble(dot(g, g) * dx(1)))
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx(1)))

        k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
        k2 = sin(kappa * theta) / (sin(theta) * nrm_g)

        psi.vector().zero()
        psi.vector().axpy(k1, psi_ref.vector())
        psi.vector().axpy(k2, g.vector())


    def run_gradient_method(self):

        kappa = 1.0

        # Design parameter
        psi_ref = project(Expression(self.PSI, degree=1), self.V0)
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        psi = project(Expression(self.PSI, degree=1) / nrm_psi, self.V0)
        g = Function(self.V0)

        NSD = NSDSolver(self.problem)
        adjoint = AdjointSolver(self.problem)

        if write_psi:
            psi_file = File('psi_diffuser.pvd')
            psi_file << psi

        u_list = NSD.solve_IPCS(psi)
        ua_list = adjoint.solve_coupled(psi, u_list)

        theta_list = []
        J_list = []

        J = self.functional(psi, u_list)
        J_list.append(J)

        for n in range(self.n_max):
            print " "
            print "Iteration #: " + str(n+1)

            self.gradient(g, u_list, ua_list)

            theta = self.angle(g, psi)
            theta_list.append(theta)
            print "Theta = " + str(theta)
            if theta < self.tol_theta:
                print "Theta smaller that tolerance!"
                break

            kappa = min(1.0, kappa*1.5)

            psi_ref.assign(psi)
            self.new_psi(psi, kappa, theta, psi_ref, g)

            # Update fluid inlet part of psi
            psi = project(Expression("psi*(x[0]>=5./42) - 1*(x[0] < 5./42)", psi=psi, degree=1), self.V0)

            print "Fluid domain = " + str(self.fluid(psi))

            if plot_psi:
                plot(sign(psi), range_min=-2., range_max=2., key="psi", title="Sign(psi)")

            u_list = NSD.solve_IPCS(psi)
            ua_list = adjoint.solve_coupled(psi, u_list)

            if write_psi:
                psi_file << psi

            J_new = self.functional(psi, u_list)

            print "J     = " + str(J)
            print "J_new = " + str(J_new)

            if J_new - J < 0:
                print "Found smaller J"
                J = J_new
            else:
                print "Did not find smaller J! Decreasing kappa."
                kappa = kappa*0.5
                self.new_psi(psi, kappa, theta, psi_ref, g)
                u_list = NSD.solve_IPCS(psi)
                J = self.functional(psi, u_list)
                print "J_new = " + str(J)
            J_list.append(J)


        if write_u:
            u_file = File("u_diffuser.pvd")
            for u in u_list:
                u_file << u

        print "J_list = "
        print J_list

        print " "
        print "theta_list = "
        print theta_list
        print " "
        print "fluid_domain = "
        print self.fluid(psi)


nu = Constant(1.0, name="nu")
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)

plot_u = False
plot_psi = False
write_u = False
write_psi = True

set_log_level(40)
# make problem
problem = Problem()
method = GradientMethod(problem)
# run gradient_method
method.run_gradient_method()


if plot_u or plot_psi:
    interactive()
