from dolfin import *
from dolfin_adjoint import *

class IPCSsolver:
    def __init__(self):
        self.something = None


    def solve(self, problem, psi):

        T = problem.T
        dt = problem.dt
        nu = problem.nu

        # if problem.options["Darcy"] is False:
        #     alpha = lambda psi: Constant(0.0)
        # else:
        alpha = lambda psi: problem.alpha(psi)

        mesh = problem.get_mesh()
        n = FacetNormal(mesh)
        ds = Measure("ds")(subdomain_data=problem.boundaries)

        # Define function spaces (P2-P1)
        V = VectorFunctionSpace(mesh, "CG", 2)
        Q = FunctionSpace(mesh, "CG", 1)

        # Define trial and test functions
        u = TrialFunction(V)
        p = TrialFunction(Q)
        v = TestFunction(V)
        q = TestFunction(Q)

        u_0 = problem.initial_conditions()
        bcu, bcp = problem.boundary_conditions(u_0, V, Q)

        u0 = Function(V)
        u1 = Function(V)
        p0 = Function(Q)
        p1 = Function(Q)

        # psi = problem.level_set()

        k = Constant(dt)
        # Tentative velocity step
        F1 = (1./k) * inner(u - u0, v) * dx \
            + inner(grad(u0) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + inner(grad(p0), v) * dx \
            + alpha(psi) * inner(u, v) * dx
            # - nu * inner(dot(n, nabla_grad(u)), v) * ds(2) \
            # + inner(p0 * n, v) * ds(2)
        a1 = lhs(F1)
        L1 = rhs(F1)

        # Pressure update
        a2 = inner(grad(p), grad(q)) * dx
        L2 = inner(grad(p0), grad(q)) * dx \
            - ((1./k) + alpha(psi)) * div(u1) * q * dx

        # Velocity update
        a3 = (1. / k + alpha(psi)) * inner(u, v) * dx
        L3 = (1. / k + alpha(psi)) * inner(u1, v) * dx \
            - inner(grad(p1 - p0), v) * dx

        # Assemble system
        A1 = assemble(a1)
        A2 = assemble(a2)
        A3 = assemble(a3)

        b1 = None
        b2 = None
        b3 = None

        t = dt

        while t < T:

            # Tentative velocity
            b1 = assemble(L1, tensor=b1)
            [bc.apply(A1, b1) for bc in bcu]
            solve(A1, u1.vector(), b1)

            # Pressure correction
            b2 = assemble(L2, tensor=b2)
            [bc.apply(A2, b2) for bc in bcp]
            solve(A2, p1.vector(), b2)

            # Velocity update
            b3 = assemble(L3, tensor=b3)
            [bc.apply(A3, b3) for bc in bcu]
            solve(A3, u1.vector(), b3)

            u0.assign(u1)
            p0.assign(p1)

            t += dt

        t -= dt

        return u0, p0
