
from dolfin import *
from dolfin_adjoint import *


class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0) and \
               (between(x[1], (0.6667, 0.8333)) or between(x[1], (0.1667, 0.3333)))


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.5) and \
               (between(x[1], (0.6667, 0.8333)) or between(x[1], (0.1667, 0.3333)))


def alpha(psi):
    return ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi, 0), 0, 1)
    # return ALPHA_U - (ALPHA_U - ALPHA_L) * psi * (1 + q)/(psi + q) # Does NOT work!!

def fluid_domain(psi):
    return conditional(psi > 0, 0, 1)

nu = Constant(0.01, name="nu")
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)
# q = Constant(0.1)



class Problem:

    def __init__(self):
        self.N = 40
        self.T = 0.1

        self.dt = 0.001 #self.T/self.N_t
        self.N_t = self.T/self.dt # Time steps
        self.mesh = None

        self.make_mesh()

    def make_mesh(self):
        self.mesh = RectangleMesh(Point(0, 0), Point(1.5, 1), self.N, self.N, "right")


class NSDSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.T = problem.T
        self.dt = problem.dt

    def set_bcs(self):
        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)

        # Initial velocity
        u_0 = Expression(("U * (1 - (2*(x[1]-0.25)/0.1667)*(2*(x[1]-0.25)/0.1667)) * (x[1]>0.1667)*(x[1]<0.3333) + "
                          "U * (1 - (2*(x[1]-0.75)/0.1667)*(2*(x[1]-0.75)/0.1667)) * (x[1]>0.6667)*(x[1]<0.8333)",
                          "0"), U=1.0, degree=3)

        # Mark boundaries
        noslip = Noslip()
        inlet = Inlet()
        outlet = Outlet()

        boundaries = MeshFunction("size_t", self.mesh, 1)
        boundaries.set_all(3)
        noslip.mark(boundaries, 0)
        inlet.mark(boundaries, 1)
        outlet.mark(boundaries, 1)

        bcu = [DirichletBC(V, (0.0, 0.0), boundaries, 0),
               DirichletBC(V, u_0, boundaries, 1)]

        return bcu

    def solve_IPCS(self, psi):

        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        # Define trial and test functions
        u = TrialFunction(V)
        p = TrialFunction(Q)
        v = TestFunction(V)
        q = TestFunction(Q)

        u0 = Function(V, name='u0')
        u1 = Function(V, name='u1')
        p0 = Function(Q, name='p0')
        p1 = Function(Q, name='p1')


        k = Constant(self.dt)
        # Tentative velocity step
        F1 = (1./k) * inner(u - u0, v) * dx \
            + inner(grad(u0) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + inner(grad(p0), v) * dx \
            + alpha(psi) * inner(u, v) * dx
        a1 = lhs(F1)
        L1 = rhs(F1)

        # Pressure update
        a2 = inner(grad(p), grad(q)) * dx
        L2 = inner(grad(p0), grad(q)) * dx \
            - ((1./k) + alpha(psi)) * div(u1) * q * dx

        # Velocity update
        a3 = (1. / k + alpha(psi)) * inner(u, v) * dx
        L3 = (1. / k + alpha(psi)) * inner(u1, v) * dx \
            - inner(grad(p1 - p0), v) * dx

        # Assemble system
        A1 = assemble(a1)
        A2 = assemble(a2)
        A3 = assemble(a3)

        b1 = None
        b2 = None
        b3 = None

        t = 0
        bcu = self.set_bcs()
        u_list = []

        while t < self.T:
            t += self.dt

            # Tentative velocity
            b1 = assemble(L1, tensor=b1)
            [bc.apply(A1, b1) for bc in bcu]
            solve(A1, u1.vector(), b1)

            # Pressure correction
            b2 = assemble(L2, tensor=b2)
            solve(A2, p1.vector(), b2)

            # Velocity update
            b3 = assemble(L3, tensor=b3)
            [bc.apply(A3, b3) for bc in bcu]
            solve(A3, u1.vector(), b3)

            u_temp = Function(V)
            u_temp.assign(u1)
            u_list.append(u_temp)

            u0.assign(u1)
            p0.assign(p1)

        return u_list


class AdjointSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.T = problem.T
        self.dt = problem.dt

    def solve_coupled(self, psi, u_list):

        # Adjoint coupled:
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)
        W = V*Q

        (u, p) = TrialFunctions(W)
        (v, q) = TestFunctions(W)
        w = Function(W)
        (ua, pa) = split(w)

        assigner = FunctionAssigner(V, W.sub(0)) # to assign w.sub(0) to u_a
        ua_list = []

        bcs = DirichletBC(W.sub(0), (0, 0), "on_boundary")

        k = Constant(self.dt)
        t = self.T

        for u0 in reversed(u_list):
            t -= self.dt

            # Not sure if this explicitly is necessary
            F_a = (1. / k) * inner(u - ua, v) * dx \
                - inner(dot(grad(u), u0), v) * dx \
                - inner(dot(u0, grad(u)), v) * dx \
                + nu * inner(grad(u), grad(v)) * dx \
                + alpha(psi) * inner(u, v) * dx \
                - div(u) * q * dx - div(v) * p * dx \
                - 2 * nu * inner(grad(u0), grad(v)) * dx \
                - 2 * alpha(psi) * inner(u0, v) * dx

            a = lhs(F_a)
            L = rhs(F_a)

            problem = LinearVariationalProblem(a, L, w, bcs)
            solver = LinearVariationalSolver(problem)
            solver.solve()
            (ua, pa) = split(w)
            plot(ua, title="Adjoint", key="a")

            u_a = Function(V)  # to solve the adjoints
            assigner.assign(u_a, w.sub(0))
            ua_list.append(u_a)

        # return reversed ua_list, such that it has the same ordering as u_list
        return ua_list[::-1]


class GradientMethod:

    def __init__(self, problem):
        self.problem = problem
        self.V0 = FunctionSpace(problem.mesh, "DG", 0)
        self.PSI = "-1"
        self.beta = 13.0
        self.n_max = 15
        self.tol_theta = 0.1
        self.tol_kappa = 0.1

    def functional(self, psi, u_list):
        J = 0.0
        for u in u_list:
            J += assemble(alpha(psi)*inner(u, u)*dx
                          + nu*inner(grad(u), grad(u))*dx
                          + self.beta*fluid_domain(psi)*dx)
        return 1./self.problem.N_t * J

    def gradient(self, u_, ua_):
        N = len(u_)
        g = Function(self.V0)
        for i in range(N):
            u = u_[i]
            ua = ua_[i]
            direction = - (ALPHA_U - ALPHA_L) * inner(u, u - ua)
            g_temp = project(direction + self.beta, self.V0)
            g.vector().axpy(1./N, g_temp.vector())

        return g

    def angle(self, g, psi):
        dot_prod = assemble(dot(g, psi) * dx)
        nrm_g = sqrt(assemble(dot(g, g) * dx))
        nrm_psi = sqrt(assemble(dot(psi, psi) * dx))
        return acos(dot_prod / (nrm_g * nrm_psi))

    def new_psi(self, psi, kappa, theta, psi_ref, g):

        nrm_g = sqrt(assemble(dot(g, g) * dx))
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
        k2 = sin(kappa * theta) / (sin(theta) * nrm_g)
        psi.vector().zero()
        psi.vector().axpy(k1, psi_ref.vector())
        psi.vector().axpy(k2, g.vector())


    def run_gradient_method(self):

        kappa = 1.0

        # Design parameter
        psi_ref = project(Expression(self.PSI, degree=1), self.V0)
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))

        psi = project(Expression(self.PSI) / nrm_psi, self.V0, name="psi")
        g = Function(self.V0)

        NSD = NSDSolver(self.problem)
        adjoint = AdjointSolver(self.problem)

        for n in range(self.n_max):
            print "Iteration #: " + str(n+1)

            # solve NSD
            u_list = NSD.solve_IPCS(psi)
            plot(sign(psi), key="alpha", title="alpha")
            plot(u_list[-1], key="u", title="End velocity")

            # Select which part of u_list to calculate adjoints from
            # solve adjoint
            u_list_short = u_list #[-10:]
            ua_list = adjoint.solve_coupled(psi, u_list_short)

            J = self.functional(psi, u_list)

            # u_ = [u_list[-1]]
            # ua_ = [ua_list[-1]]
            g = self.gradient(u_list_short, ua_list)

            theta = self.angle(g, psi)

            print "Theta = " + str(theta)
            if theta < self.tol_theta:
                print "Theta smaller that tolerance!"
                break

            kappa = min(1.0, kappa * 1.5)

            psi_ref.assign(psi)
            self.new_psi(psi, kappa, theta, psi_ref, g)

            J_new = self.functional(psi, u_list)

            if J_new - J < 0:
                print "Found smaller J"
            else:
                print "Did not find smaller J!"
            print "J_new - J = " + str(J_new - J)

        plot(sign(psi), key="alpha", title="alpha")


# make problem
problem = Problem()
method = GradientMethod(problem)

method.run_gradient_method()
# run gradient_method


interactive()
