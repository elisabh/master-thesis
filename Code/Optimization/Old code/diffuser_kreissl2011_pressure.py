# -- Diffuser from Kreissl 2011 --

from dolfin import *
from dolfin_adjoint import *
import time


class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        # return on_boundary and near(x[0], 52./42) and between(x[1], (0.3333, 0.6667))
        return on_boundary and near(x[0], 1.0) and between(x[1], (0.3333, 0.6667))


class Design(SubDomain):
    def inside(self, x, on_boundary):
        # return between(x[0], (5./42, 47./42))
        return between(x[0], (0, 1.0))


class PressureInlet(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (4./42, 5./42))


class PressureOutlet(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (47./42, 48./42)) and between(x[1], (0.3333, 0.6667))


def alpha(psi, q=1.0):
    return ALPHA_L + (ALPHA_U - ALPHA_L)*(1.0/(1.0 + 2.718281828459045**((-psi)/q)))


def fluid_domain(psi, q=0.07):
    return 1.0 - 1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q))


class Problem:

    def __init__(self):
        self.nx = 40
        self.ny = 40
        self.dt = 0.00002 #self.T/self.N_t
        self.N_t = 15
        self.T = self.N_t*self.dt

        self.mesh = None
        self.boundaries = None
        self.adjoint_boundaries = None
        self.domains = None

        self.make_mesh()
        self.mark_boundaries()
        # self.mark_adjoint_boundaries()
        self.mark_subdomain()

    def make_mesh(self):
        # self.mesh = RectangleMesh(Point(0, 0), Point(52./42, 1), self.nx, self.ny, "right/left")
        self.mesh = RectangleMesh(Point(0, 0), Point(1, 1), self.nx, self.ny, "right/left")

    def mark_boundaries(self):

        # Mark boundaries
        noslip = Noslip()
        inlet = Inlet()
        outlet = Outlet()

        self.boundaries = MeshFunction("size_t", self.mesh, 1)
        self.boundaries.set_all(4)
        noslip.mark(self.boundaries, 0)
        inlet.mark(self.boundaries, 1)
        outlet.mark(self.boundaries, 3)

    def boundary_conditions(self, V, Q, t):
        U = sin(3.1415/(2*self.T)*t)
        u_1 = Expression(("U * (1 - (2*(x[1]-0.5)) * (2*(x[1]-0.5)) )", "0"),
                         U=U, degree=3)

        bcu = [DirichletBC(V, (0.0, 0.0), self.boundaries, 0),
               DirichletBC(V, u_1, self.boundaries, 1)]
        bcp = [DirichletBC(Q, 0.0, self.boundaries, 3)]

        return bcu, bcp

    def mark_adjoint_boundaries(self):

        noslip = Noslip()
        outlet = Outlet()

        self.adjoint_boundaries = MeshFunction("size_t", self.mesh, 1)
        self.adjoint_boundaries.set_all(4)
        noslip.mark(self.adjoint_boundaries, 0)
        outlet.mark(self.adjoint_boundaries, 3)

    def adjoint_boundary_conditions(self, V):
        return [DirichletBC(V, (0.0, 0.0), self.adjoint_boundaries, 0)]

    def mark_subdomain(self):

        design = Design()
        # p_inlet = PressureInlet()
        # p_outlet = PressureOutlet()

        self.domains = CellFunction("size_t", self.mesh)
        self.domains.set_all(0)
        design.mark(self.domains, 1)
        # p_inlet.mark(self.domains, 2)
        # p_outlet.mark(self.domains, 3)



class NSDSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries
        self.domains = problem.domains
        self.T = problem.T
        self.dt = problem.dt

    def solve_IPCS(self, psi):

        # Reset dolfin-adjoint to re-run time scheme
        adj_reset()
        annotate = False

        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        # dx = Measure("dx")(subdomain_data=self.domains)
        n = FacetNormal(self.mesh)

        # Initial velocity and pressure
        u_ic = project(Expression(("0.0", "0.0"), degree=2), V, name="u_ic")
        p_ic = project(Expression("0.0", degree=1), Q, name="p_ic")
        u0 = u_ic.copy(deepcopy=True)
        p0 = p_ic.copy(deepcopy=True)

        # Declare velocity functions
        u = Function(V, name="u", annotate=annotate)
        v = TestFunction(V)

        timestep = Constant(self.dt)
        # Weak form for tentative velocity
        F1 = (1. / timestep) * inner(u - u0, v) * dx \
             + inner(grad(u) * u0, v) * dx \
             + nu * inner(grad(u), grad(v)) * dx \
             + alpha(psi) * inner(u, v) * dx \
             + inner(grad(p0), v) * dx \
             - nu * inner(dot(n, nabla_grad(u)), v) * ds(3) \
             + inner(p0 * n, v) * ds(3)

        # Declare pressure functions (after tentative velocity weak form!!)
        p = Function(Q, name="p", annotate=annotate)
        q = TestFunction(Q)

        # Pressure correction weak form
        F2 = inner(grad(p - p0), grad(q)) * dx \
             + (1. / timestep) * div(u0) * q * dx

        # Velocity update weak form
        F3 = (1. / timestep) * inner(u - u0, v) * dx \
             + inner(grad(p - p0), v) * dx


        annotate = True
        t = 0.0

        psi_tmp = psi.copy(deepcopy=True)
        adj_time = True
        adj_start_timestep()


        while t < self.T:

            # one initial run without annotation for initial condition
            # if t > float(timestep) and not adj_time:
            #     adj_time = True
            #     annotate = True
            #     adj_start_timestep()

            bcu, bcp = problem.boundary_conditions(V, Q, t)

            # u and p work as trial functions
            # u0 and p0 work as newest value

            solve(F1 == 0, u, bcu)
            u0.assign(u, annotate=annotate)
            solve(F2 == 0, p, bcp)
            solve(F3 == 0, u, bcu)

            psi.assign(psi_tmp, annotate=annotate)
            u0.assign(u, annotate=annotate)
            p0.assign(p, annotate=annotate)

            if plot_u:
                plot(u0, title="Velocity", key="u1")

            t += float(timestep)
            if adj_time:
                adj_inc_timestep(t, t > self.T)

        return u0, p0


class GradientMethod:

    def __init__(self, problem):
        self.problem = problem
        self.domains = problem.domains
        self.boundaries = problem.boundaries
        self.V0 = FunctionSpace(problem.mesh, "CG", 1)
        self.PSI = "-1" # + 2*(x[0]>1.0 + 5./42)*((x[1]>0.6667) + (x[1]<0.3333))"
        self.beta = 100000.0
        self.n_max = 7
        self.tol_theta = 0.01
        self.tol_kappa = 0.1

    def fluid(self, psi):
        # dx = Measure("dx")(subdomain_data=self.domains)
        return assemble(conditional(psi > 0, 0, 1)*dx(1))

    def angle(self, g, psi):
        dx = Measure("dx")(subdomain_data=self.domains)
        dot_prod = assemble(dot(g, psi) * dx(1))
        nrm_g = sqrt(assemble(dot(g, g) * dx(1)))
        nrm_psi = sqrt(assemble(dot(psi, psi) * dx(1)))
        if nrm_g*nrm_psi == 0:
            return 1.0
        return acos(dot_prod / (nrm_g * nrm_psi))

    def new_psi(self, kappa, theta, psi_ref, g):

        dx = Measure("dx")(subdomain_data=self.domains)
        nrm_g = sqrt(assemble(dot(g, g) * dx(1)))
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx(1)))

        k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
        k2 = sin(kappa * theta) / (sin(theta) * nrm_g)

        psi = project(k1 * psi_ref + k2 * g, self.V0, annotate=True)
        return psi


    def run_gradient_method(self):

        ds = Measure("ds")(subdomain_data=self.boundaries)
        dx = Measure("dx")(subdomain_data=self.domains)

        # Design parameter
        psi_ref = project(Expression(self.PSI, degree=1), self.V0)
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        psi = project(Expression(self.PSI, degree=2) / nrm_psi, self.V0)
        g = Function(self.V0)
        kappa = 1.0


        NSD = NSDSolver(self.problem)

        for n in range(self.n_max):
            print "Iteration #: " + str(n + 1)

            # Forward model:
            print "Solving Navier-Stokes-Darcy system"
            u, p = NSD.solve_IPCS(psi)

            plot(u, title="u", key="u")
            plot(p, title="p", key="p")

            # J = Functional((p*dx(2) - 3.*p*dx(3))*dt[FINISH_TIME])
                           # + self.beta*fluid_domain(psi)*dx(1)*dt[FINISH_TIME])
            J = Functional((alpha(psi) * inner(u, u) + nu * inner(grad(u), grad(u))) * dx * dt[FINISH_TIME])

            print "Computing gradient"
            g_ascent = compute_gradient(J, Control(psi))
            nrm_g = sqrt(assemble(dot(g_ascent, g_ascent) * dx))
            g.assign(-1.0*g_ascent/nrm_g)

            fluid_volume = self.fluid(g)
            print "Fluid = " + str(fluid_volume)
            theta = self.angle(g, psi)
            print "Theta = " + str(theta)

            if theta < self.tol_theta:
                print "Theta smaller that tolerance!"
                break

            # plot(sign(g), title="Sign(g)", key="g")

            # Update step length
            # kappa = min(1.0, kappa * 1.5)

            # Store old psi and calculate new
            psi_ref.assign(psi)
            psi = self.new_psi(kappa, theta, psi_ref, g)

            # psi = project(Expression(
            #     "psi*(x[0]>=5./42)*(x[0]<47./42) - 1*(x[0] < 5./42) + 1*(x[0] > 47./42) - 2*(x[0] > 47./42)*(x[1]<0.67)*(x[1]>0.33)",
            #     psi=g, degree=1), self.V0)
            # nrm_psi = sqrt(assemble(dot(psi, psi) * dx(1)))
            # print "nrm_psi = " + str(nrm_psi)
            # psi.assign(psi/nrm_psi)

            plot(g, title="gradient", key="g")
            plot(psi, title="psi", key="ps")
            # plot(sign(psi), title="Sign(psi)", key="psi")
            plot(alpha(psi), title="alpha", key="a")


set_log_level(40)

nu = Constant(1.0, name="nu")
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)

plot_u = False
plot_psi = False
write_u = False
write_psi = True

# make problem
problem = Problem()
method = GradientMethod(problem)
# run gradient_method
method.run_gradient_method()


interactive()
