from IPCS_solver import *
from dolfin_adjoint import *


class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0) and \
               (between(x[1], (0.6667, 0.8333)) or between(x[1], (0.1667, 0.3333)))


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.5) and \
               (between(x[1], (0.6667, 0.8333)) or between(x[1], (0.1667, 0.3333)))


# options:
# "mesh": takes in mesh string or grid size N
# "Darcy": solves ordinary NS if False and NSD if True


class Problem:
    def __init__(self, options):
        # Parameters must be defined by subclass
        self.f = None
        self.bcu = []
        self.bcp = []
        self.nu = Constant(0.01)
        self.U = 1.0
        self.N = None
        # self.y1 = 1./6
        # self.y2 = 2./6
        self.boundaries = None
        self.dt = 0.001
        self.T = 0.0011

        self.ALPHA_U = 2.5 * self.nu / (0.01 * 0.01)
        self.ALPHA_L = 2.5 * self.nu / (100 * 100)

        # Save options:
        self.options = options

        if ".xml" in options["mesh"]:
            self.mesh = Mesh(options["mesh"])
        else:
            self.N = 40
            self.mesh = RectangleMesh(Point(0, 0), Point(1.5, 1), self.N, self.N, "right")

    def alpha(self, psi):
        return self.ALPHA_U - (self.ALPHA_U - self.ALPHA_L) * conditional(gt(psi, 0), 0, 1)

    def refine_mesh(self):
        self.mesh = refine(self.mesh)
        self.mark_boundaries()

    def get_mesh(self):
        self.mark_boundaries()
        return self.mesh

    def initial_conditions(self):

        u_0 = Expression(("U * (1 - (2*(x[1]-0.25)/0.1667)*(2*(x[1]-0.25)/0.1667)) * (x[1]>0.1667)*(x[1]<0.3333) + "
                          "U * (1 - (2*(x[1]-0.75)/0.1667)*(2*(x[1]-0.75)/0.1667)) * (x[1]>0.6667)*(x[1]<0.8333)",
                          "0"), U=self.U, degree=3)
        return u_0

    def mark_boundaries(self):
        noslip = Noslip()
        inlet = Inlet()
        outlet = Outlet()

        boundaries = MeshFunction("size_t", self.mesh, 1)
        boundaries.set_all(3)
        noslip.mark(boundaries, 0)
        inlet.mark(boundaries, 1)
        outlet.mark(boundaries, 1)

        self.boundaries = boundaries

    def boundary_conditions(self, u_0, V, Q):

        bcu = [DirichletBC(V, (0.0, 0.0), self.boundaries, 0),
               DirichletBC(V, u_0, self.boundaries, 1)]
        # bcp = [DirichletBC(Q, 0, self.boundaries, 2)]
        bcp = []

        return bcu, bcp


#
# class Adjoint_solver:
#     def __init__(self):
#         self.something = None
#
#     def solve(self, problem, psi):
#
#         T = problem.T
#         dt = problem.dt
#         nu = problem.nu
#
#         alpha = lambda psi: problem.alpha(psi)
#
#         mesh = problem.get_mesh()
#         n = FacetNormal(mesh)
#         ds = Measure("ds")(subdomain_data=problem.boundaries)
#
#         # Define function spaces (P2-P1)
#         V = VectorFunctionSpace(mesh, "CG", 2)
#         Q = FunctionSpace(mesh, "CG", 1)
#
#         # Define trial and test functions
#         u = TrialFunction(V)
#         p = TrialFunction(Q)
#         v = TestFunction(V)
#         q = TestFunction(Q)
#
#         bcu = [DirichletBC(V, (0.0, 0.0), "on_boundary")]
#
#         u_a = Function(V)
#         p_a = Function(Q)
#
#         a = nu*inner(grad(u), grad(v))*dx + alpha(psi)*inner(u, v)*dx \
#             +
#
#
#         # psi = problem.level_set()
#
#         k = Constant(dt)
#         # Tentative velocity step
#         F1 = (1./k) * inner(u - u0, v) * dx \
#             + inner(grad(u0) * u0, v) * dx \
#             + nu * inner(grad(u), grad(v)) * dx \
#             + inner(grad(p0), v) * dx \
#             + alpha(psi) * inner(u, v) * dx
#             # - nu * inner(dot(n, nabla_grad(u)), v) * ds(2) \
#             # + inner(p0 * n, v) * ds(2)
#         a1 = lhs(F1)
#         L1 = rhs(F1)
#
#         # Pressure update
#         a2 = inner(grad(p), grad(q)) * dx
#         L2 = inner(grad(p0), grad(q)) * dx \
#             - ((1./k) + alpha(psi)) * div(u1) * q * dx
#
#         # Velocity update
#         a3 = (1. / k + alpha(psi)) * inner(u, v) * dx
#         L3 = (1. / k + alpha(psi)) * inner(u1, v) * dx \
#             - inner(grad(p1 - p0), v) * dx
#
#         # Assemble system
#         A1 = assemble(a1)
#         A2 = assemble(a2)
#         A3 = assemble(a3)
#
#         b1 = None
#         b2 = None
#         b3 = None
#
#         t = dt
#
#         while t < T:
#
#             # Tentative velocity
#             b1 = assemble(L1, tensor=b1)
#             [bc.apply(A1, b1) for bc in bcu]
#             solve(A1, u1.vector(), b1)
#
#             # Pressure correction
#             b2 = assemble(L2, tensor=b2)
#             [bc.apply(A2, b2) for bc in bcp]
#             solve(A2, p1.vector(), b2)
#
#             # Velocity update
#             b3 = assemble(L3, tensor=b3)
#             [bc.apply(A3, b3) for bc in bcu]
#             solve(A3, u1.vector(), b3)
#
#             u0.assign(u1)
#             p0.assign(p1)
#
#             t += dt
#
#         t -= dt
#
#         return u0, p0


# Gradient based algorithm

class GradientBasedMethod(Problem):
    def __init__(self, options):
        Problem.__init__(self, options)
        self.tol_theta = 0.01
        self.tol_kappa = 0.0001
        self.tol_F = 0.00001
        self.PSI = "-1"
        self.beta = 100
        self.max_iter = 40

    def fluid_domain(self, psi):
        return conditional(psi > 0, 0, 1)

    def gradient_k(self, u, u_a):
        return -(self.ALPHA_U - self.ALPHA_L)*inner(u, u - u_a) + self.beta

    def J_expr(self, u, psi):
        return self.nu*inner(grad(u), grad(u))*dx + self.alpha(psi)*inner(u, u)*dx

    def F_expr(self, u, psi):
        return self.J_expr(u, psi) + self.beta*self.fluid_domain(psi)

    def optimize(self):

        # Initial values:
        kappa = 1.0
        theta = 90
        n = 0
        NSDsolver = IPCSsolver()

        V0 = FunctionSpace(self.mesh, "DG", 0)
        psi = project(Expression(self.PSI, degree=1), V0)
        m = Control(psi)

        # Solve Navier-Stokes-Darcy system
        u, p = NSDsolver.solve(self, psi)

        # Solve adjoint system
        # J = Functional(self.J_expr(u, psi))
        J = Functional(inner(self.alpha(psi) * u, u) * dx('everywhere')*dt[FINISH_TIME]
                       + self.nu * inner(grad(u), grad(u)) * dx('everywhere')*dt[FINISH_TIME])


        adj_html("forward.html", "forward")


        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)
        # v = TestFunction(V)
        # q = TrialFunction(Q)
        u_a = Function(V)
        p_a = Function(Q)


        for (adjoint, var) in compute_adjoint(J, forget=False):
            plot(adjoint)
            interactive()

        # dJda = compute_gradient(J, Control(psi))
        adj_html("adjoint.html", "adjoint")
        plot(u)
        interactive()

options = {"mesh": "40",
           "Darcy": True}
obj = GradientBasedMethod(options)
obj.optimize()

#
#
# options = {"mesh": "40",
#            "Darcy": True}
#
# double_channel = Problem(options)
# solver = IPCSsolver()


# u = solver.solve(double_channel, psi)
# plot(u, title="Velocity")

# interactive()