from IPCS_solver import *
from dolfin import *
from dolfin_adjoint import *


class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0) and \
               (between(x[1], (0.6667, 0.8333)) or between(x[1], (0.1667, 0.3333)))


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.5) and \
               (between(x[1], (0.6667, 0.8333)) or between(x[1], (0.1667, 0.3333)))


def alpha(psi):
    return ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi, 0), 0, 1)
    # return ALPHA_U - (ALPHA_U - ALPHA_L) * psi * (1 + q)/(psi + q)

nu = Constant(0.01, name="nu")
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)
q = Constant(0.1)
N = 20
dt = 0.001
T = 0.005
PSI = "-1"


mesh = RectangleMesh(Point(0, 0), Point(1.5, 1), N, N, "right")

# Design parameter
V0 = FunctionSpace(mesh, "DG", 0)
psi = project(Expression(PSI, degree=1), V0, name="psi", annotate=False)

# Function spaces
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)

# Initial velocity
u_0 = Expression(("U * (1 - (2*(x[1]-0.25)/0.1667)*(2*(x[1]-0.25)/0.1667)) * (x[1]>0.1667)*(x[1]<0.3333) + "
                          "U * (1 - (2*(x[1]-0.75)/0.1667)*(2*(x[1]-0.75)/0.1667)) * (x[1]>0.6667)*(x[1]<0.8333)",
                          "0"), U=1.0, degree=3)

# Mark boundaries
noslip = Noslip()
inlet = Inlet()
outlet = Outlet()

boundaries = MeshFunction("size_t", mesh, 1)
boundaries.set_all(3)
noslip.mark(boundaries, 0)
inlet.mark(boundaries, 1)
outlet.mark(boundaries, 1)

bcu = [DirichletBC(V, (0.0, 0.0), boundaries, 0),
       DirichletBC(V, u_0, boundaries, 1)]

# Define trial and test functions
u = TrialFunction(V)
p = TrialFunction(Q)
v = TestFunction(V)
q = TestFunction(Q)

u0 = Function(V, name='u0')
ut = Function(V, name='u_tentative')
u1 = Function(V, name='u1')
p0 = Function(Q, name='p0')
p1 = Function(Q, name='p1')


k = Constant(dt)
# Tentative velocity step
F1 = (1./k) * inner(u - u0, v) * dx \
    + inner(grad(u0) * u0, v) * dx \
    + nu * inner(grad(u), grad(v)) * dx \
    + inner(grad(p0), v) * dx \
    + alpha(psi) * inner(u, v) * dx
a1 = lhs(F1)
L1 = rhs(F1)

# Pressure update
a2 = inner(grad(p), grad(q)) * dx
L2 = inner(grad(p0), grad(q)) * dx \
    - ((1./k) + alpha(psi)) * div(ut) * q * dx

# Velocity update
a3 = (1. / k + alpha(psi)) * inner(u, v) * dx
L3 = (1. / k + alpha(psi)) * inner(ut, v) * dx \
    - inner(grad(p1 - p0), v) * dx

# Assemble system
A1 = assemble(a1)
A2 = assemble(a2)
A3 = assemble(a3)

b1 = None
b2 = None
b3 = None

t = 0
u1_list = []


while t < T:
    t += dt

    # Tentative velocity
    b1 = assemble(L1, tensor=b1)
    [bc.apply(A1, b1) for bc in bcu]
    solve(A1, ut.vector(), b1)

    # Pressure correction
    b2 = assemble(L2, tensor=b2)
    solve(A2, p1.vector(), b2)

    # Velocity update
    b3 = assemble(L3, tensor=b3)
    [bc.apply(A3, b3) for bc in bcu]
    solve(A3, u1.vector(), b3)

    u1_list.append(u1)
    u0.assign(u1)
    p0.assign(p1)




# Adjoint coupled:

W = V*Q
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)
w = Function(W)
(ua, pa) = split(w)
u_a = Function(V) # to solve the adjoints
assigner = FunctionAssigner(V, W.sub(0)) # to assign w.sub(0) to u_a
ua_list = []

bcs_a = DirichletBC(W.sub(0), (0, 0), "on_boundary")




while t > 0:
    t -= dt
    u0 = u1_list.pop()

    F_a = - (1. / k) * inner(u - ua, v) * dx \
          - inner(dot(grad(u), u0), v) * dx \
          - inner(dot(u0, grad(u)), v) * dx \
          + nu * inner(grad(u), grad(v)) * dx \
          + alpha(psi) * inner(u, v) * dx \
          - div(u) * q * dx - div(v) * p * dx \
          - 2 * nu * inner(grad(u0), grad(v)) * dx \
          - 2 * alpha(psi) * inner(u0, v) * dx

    a = lhs(F_a)
    L = rhs(F_a)

    # solve(a, L, w, bcs_a, annotate=False)
    problem = LinearVariationalProblem(a, L, w, bcs_a)
    solver = LinearVariationalSolver(problem)

    solver.solve()
    (ua, pa) = split(w)
    assigner.assign(u_a, w.sub(0))
    ua_list.append(u_a)




plot(ua)
interactive()

#u0 is last solution at time T:
# bcua = [DirichletBC(V, (0.0, 0.0), "on_boundary")]
#
# ua = TrialFunction(V)
# pa = TrialFunction(Q)
#
# ua_0 = Function(V, name='u0')
# ua_t = Function(V, name='u_tentative')
# ua_1 = Function(V, name='u1')
# pa_0 = Function(Q, name='p0')
# pa_1 = Function(Q, name='p1')
#
# F1 = - (1./k) * inner(ua - ua_0, v) * dx \
#     - inner(grad(ua) * u0, v) * dx \
#     - inner(dot(u0, grad(ua)), v) * dx \
#     + nu * inner(grad(ua), grad(v)) * dx \
#     + inner(grad(pa_0), v) * dx \
#     + alpha(psi) * inner(ua, v) * dx
# a1 = lhs(F1)
# L1 = rhs(F1) + 2*nu*inner(grad(u0), grad(v))*dx + 2*alpha(psi)*inner(u0, v)*dx
#
# # Pressure update
# a2 = inner(grad(pa), grad(q)) * dx
# L2 = inner(grad(pa_0), grad(q)) * dx \
#     - ((1./k) + alpha(psi)) * div(ua_t) * q * dx
#
# # Velocity update
# a3 = (1. / k + alpha(psi)) * inner(ua, v) * dx
# L3 = (1. / k + alpha(psi)) * inner(ua_t, v) * dx \
#     - inner(grad(pa_1 - pa_0), v) * dx
#
# ua_list = []
#
# while t > 0:
#     t -= dt
#
#     # Tentative velocity
#     b1 = assemble(L1, tensor=b1)
#     [bc.apply(A1, b1) for bc in bcua]
#     solve(A1, ua_t.vector(), b1)
#
#     # Pressure correction
#     b2 = assemble(L2, tensor=b2)
#     solve(A2, pa_1.vector(), b2)
#
#     # Velocity update
#     b3 = assemble(L3, tensor=b3)
#     [bc.apply(A3, b3) for bc in bcua]
#     solve(A3, ua_1.vector(), b3)
#
#     ua_list.append(ua_1)
#     ua_0.assign(ua_1)
#     pa_0.assign(pa_1)


# Adjoint solution
m = Control(psi)
J = Functional(alpha(psi)*inner(u0, u0) * dx() + nu*inner(grad(u0), grad(u0)) * dx())

adj_html("forward.html", "forward")
adj_html("adjoint.html", "adjoint")
count = 0

# for (adjoint, var) in compute_adjoint(J, forget=False):
#
#     # if "u0" in str(var.name):
#     #     ua_list.append(adjoint)
#     # plot(adjoint, title="Adjoint for " + str(var.name))
#     count += 1
#     print count

# print "Lists:"
# for u in u1_list:
#     plot(u)
# for ua in ua_list:
#     plot(ua)
#
# print len(u1_list)
# print len(ua_list)



# print len(u1_list)
# print len(ua_list)



# Problem01_gradient
# Handeling Domains with Different Materials

