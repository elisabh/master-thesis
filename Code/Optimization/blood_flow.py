import numpy as np
from matplotlib import pyplot as plt

A = [1, -0.459305, 0.27711, -0.0632653, 0.213156, 0.0850977, -0.0446033, 0.00836924,
     -0.0300109, -0.0433541, 0.00988043, -0.0112574, -0.0048216, 0.0166013]

B = [0, -0.678336, -0.128518, -0.0820616, -0.110139, 0.152294, 0.0456851, 0.0261691,
     0.0417271, -0.0198078, -0.0115691, 0.00690928, -0.0209537, 0.000802334]

N = 13
U_list = []
T = 0.08
t = 0.0
dt = 0.0001

while t < T:
    U = 0.0
    for k in range(N+1):
        Ak = A[k]
        Bk = B[k]
        U += Ak*np.cos(2*np.pi*k*t/T) + Bk*np.sin(2*np.pi*k*t/T)
    U_list.append(U)
    t += dt
    print U/100

plt.plot(U_list, '*')
plt.show()

# waveform source: Ku DN and Zhu C. In: Sumpio BE, Hemodynamic Forces and Vascular Cell Biology. RG Landes Company, Austin, pp. 1.23, 1993.
# Fourier coefficients derived by DA Steinman, and placed in the public domain.


