# Stokes universal solver for all problems

from dolfin import *
import numpy
import time


# All boundary for noslip, overloaded by inlet and outlet
class AllBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


# Boundaries for Double Pipe
class DoublePipeBoundary_u0(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.1667, 0.3333))

class DoublePipeBoundary_u1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.6667, 0.8333)) 

class DoublePipeBoundary_p(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0) and between(x[1], (0.1667, 0.3333))


# Boundaries for Diffuser
class Diffuser_inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0)

class Diffuser_outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1) and ( between(x[1], (0.3333, 0.6667) ) )


# Boundaries for Obstacle
class Obstacle_inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0) and between(x[1], (0.65, 0.85))

class Obstacle_outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], 0) and between(x[0], (0.65, 0.85))

# Boundaries for Pipe Bend
class PipeBend_inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0) and between(x[1], (0.7, 0.9))

class PipeBend_outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], 0) and between(x[0], (0.7, 0.9))


#Boundaries for Rugby Ball
class RugbyBall_inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and (x[1] < 1)

class RugbyBall_outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[1], 1)

# Boundaries Force Term
class ForceTerm_inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.5833, 0.75)) and near(x[0], 0)

class ForceTerm_outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.5833, 0.75)) and near(x[0], 1)

class ForceTerm_circle(SubDomain):
    def inside(self, x, on_boundary):
        dx = x[0] - 0.5
        dy = x[1] - 0.3333 
        return (sqrt(dx*dx + dy*dy) <= 0.084)



# Velocity for inlet and outlet
class Velocity(Expression):
    def __init__(self, l, dt, u_x, u_y):
        self.l = l
        self.dt = dt
        self.u_x = u_x
        self.u_y = u_y
    def eval(self, value, x):
        t_y = x[1] - self.dt
        t_x = x[0] - self.dt
        value[0] = self.u_x*(1-(2*t_y/self.l)*(2*t_y/self.l))
        value[1] = self.u_y*(1-(2*t_x/self.l)*(2*t_x/self.l))
    def value_shape(self):
        return (2,)

class Force(Expression):
    def eval(self, value, x):
        dx = x[0] - 0.5
        dy = x[1] - 0.3333 
        value[0] = 500.0*(sqrt(dx*dx + dy*dy) <= 0.084)
        value[1] = 0
    def value_shape(self):
        return (2,)

# Expressions:
def diffusion(u,v,mu):
    return mu * inner(grad(u), grad(v))

def friction(u,v,alpha):
    return alpha * inner(u, v)

def incompressibility(u,q):
    return - div(u) * q

def a_stokes(u,p,v,q,mu,alpha):
    return diffusion(u, v, mu) + friction(u, v, alpha) + \
        + incompressibility(u, q) + incompressibility(v, p)

def conv_comb(psi,psi_ref,g,k1,k2):
    psi.vector().zero()
    psi.vector().axpy(k1, psi_ref.vector())
    psi.vector().axpy(k2, g.vector())



#mesh_name = "DoublePipeLong"
#mesh_name = "DoublePipeShort"
#mesh_name = "Diffuser"
#mesh_name = "Obstacle.xml"
#mesh_name = "PipeBend"
mesh_name = "RugbyBall"
#mesh_name = "ForceTerm.xml"

#filename = "AL_g08_" + mesh_name + ".pvd"
plt = False
write = True


# Global variables (constants)
f = Constant((0.0, 0.0))
MU = Constant(1.0)
ALPHA_U = 2.5*MU/(0.01*0.01)
ALPHA_L = 2.5*MU/(100*100)
#BETA = 1000
#FRACTION = 0.9
#PSI = "-1"
PSI = "-1 + 2*(x[0]>=0.2)*(x[0]<=0.8)*(x[1]>=0.2)*(x[1]<=0.8)"
tol_theta = 0.01
tol_kappa = 0.0001
tol_F = 0.00001


class FlowModel(object):
    def __init__(self, mesh_name):
        self.mesh_name = mesh_name
        self.mesh = self.get_mesh()
        self.init_functions()
        self.set_boundaries()
        self.set_levelset()
        self.set_AL()
        self.make_variational_form()
        self.make_gradient_method_tools()

    
    def get_mesh(self):
        # Get mesh from .xml file or by built in Mesh generator
        nx = 100
        ny = 100
        if (".xml" in self.mesh_name):
            mesh = Mesh(self.mesh_name)
            mesh = refine(mesh)
            #mesh = refine(mesh)
        elif (self.mesh_name is "DoublePipeLong"):
            mesh = RectangleMesh(Point(0,0), Point(1.5,1), nx, ny, "right")
        else: 
            mesh = UnitSquareMesh(nx, ny, "crossed")

        return mesh

    def plot_mesh(self):
        plot(self.mesh, interactive=True)

    def set_boundaries(self):
        print "mesh_name = " + self.mesh_name + "\n"
        noslip = Constant((0.0, 0.0))
        # imposes Dirichlet boundaries on base
        # initializes inflow and outflow on active boundary
        self.boundaries = MeshFunction("size_t", self.mesh, 1)
        self.boundaries.set_all(4)
        
        noslipBoundary = AllBoundary()
        noslipBoundary.mark(self.boundaries, 0)

        if ("DoublePipe" in self.mesh_name):
            l = 0.1667; dt_0 = 0.25; dt_1 = 0.75; U = 1.0
            u0 = Velocity(l, dt_0, U, 0)
            u1 = Velocity(l, dt_1, U, 0)
            inletBoundary = DoublePipeBoundary_u0()
            outletBoundary = DoublePipeBoundary_u1()
            pressureBoundary = DoublePipeBoundary_p()

        elif ("Diffuser" in self.mesh_name):
            U0 = 1.0; U1 = 3.0; dt = 0.5; l_0 = 1.0; l_1 = 0.3333
            u0 = Velocity(l_0, dt, U0, 0)
            u1 = Velocity(l_1, dt, U1, 0)
            inletBoundary = Diffuser_inlet()
            outletBoundary = Diffuser_outlet()
            pressureBoundary = Diffuser_outlet()

        elif ("Obstacle" in self.mesh_name):
            l = 0.2; dt = 0.75; U = 1.0
            u0 = Velocity(l, dt, U, 0)
            u1 = Velocity(l, dt, 0, -U)
            inletBoundary = Obstacle_inlet()
            outletBoundary = Obstacle_outlet()
            pressureBoundary = Obstacle_outlet()

        elif("PipeBend" in self.mesh_name):
            l = 0.2; dt = 0.8; U = 1.0
            u0 = Velocity(l, dt, U, 0)
            u1 = Velocity(l, dt, 0, -U)
            inletBoundary = PipeBend_inlet()
            outletBoundary = PipeBend_outlet()
            pressureBoundary = PipeBend_outlet()

        elif ("RugbyBall" in self.mesh_name):
            # Uniform velocity:
            u0 = Constant((0.0, 1.0))
            u1 = Constant((0.0, 1.0))
            inletBoundary = RugbyBall_inlet()
            outletBoundary = RugbyBall_outlet()
            pressureBoundary = RugbyBall_outlet()

        elif ("ForceTerm" in self.mesh_name):
            l = 0.1667; dt = 0.6667; U = 1.0
            u0 = Velocity(l, dt, U, 0)
            u1 = Velocity(l, dt, U, 0)
            inletBoundary = ForceTerm_inlet()
            outletBoundary = ForceTerm_outlet()
            pressureBoundary = ForceTerm_outlet()


        inletBoundary.mark(self.boundaries, 1)
        outletBoundary.mark(self.boundaries, 2)
        #pressureBoundary.mark(self.boundaries, 3)
        #forceDomain = ForceTerm_circle()
        #forceDomain.mark(self.boundaries, 5)
        p0 = Constant(0.0)

        self.bcs = [DirichletBC(self.W.sub(0), noslip, self.boundaries, 0), 
                    DirichletBC(self.W.sub(0), u0, self.boundaries, 1),
                    DirichletBC(self.W.sub(0), u1, self.boundaries, 2),
                    DirichletBC(self.W.sub(1), p0, pressureBoundary)]
        #plot(self.boundaries)

    def init_functions(self):
        # Function to initialize functionspaces, test/trial functions
        self.V = VectorFunctionSpace(self.mesh, "CG", 2)
        self.Q = FunctionSpace(self.mesh, "CG", 1)
        self.W = self.V * self.Q
        self.V0 = FunctionSpace(self.mesh, "DG", 0)

        (self.u,self.p) = TrialFunctions(self.W)
        (self.u_a,self.p_a) = TrialFunctions(self.W)
        (self.v,self.q) = TestFunctions(self.W)

        self.w = Function(self.W)
        (self.u_,self.p_) = self.w.split()

        self.psi = Function(self.V0)
        self.psi_ref = Function(self.V0)


    def set_levelset(self):
        # Level set function 
        # Make sure norm(psi) = 1
        psi_temp = project(Expression(PSI), self.V0)
        nrm_psi = sqrt(assemble(dot(psi_temp,psi_temp)*dx))
        self.psi = project(Expression(PSI)/nrm_psi, self.V0)

        # fluid_domain: 1 in fluid elements, 0 in solid elements
        self.fluid_domain = conditional(self.psi > 0, 0, 1)

        # alpha: ALPHA_L in fluid elements, ALPHA_U in solid elements
        self.alpha = ALPHA_U - (ALPHA_U - ALPHA_L)*self.fluid_domain

    def set_AL(self):
        expected_fluid_domain = FRACTION*project(Expression("1"), self.V0)
        # Value of expected fluid domain:
        self.gamma = assemble(expected_fluid_domain*dx)


    def make_variational_form(self):
        self.a = a_stokes(self.u, self.p, self.v, self.q, MU, self.alpha)*dx
        self.L = inner(f, self.v)*dx
        self.problem = LinearVariationalProblem(self.a, self.L, self.w, self.bcs)


    def solve_stokes(self):
        solver = LinearVariationalSolver(self.problem)
        solver.parameters["linear_solver"] = "mumps"
        solver.solve()


    def make_gradient_method_tools(self):
        self.G_expr = 0.5*diffusion(self.u_, self.u_, MU)*dx
        self.J_expr = 0.5*(diffusion(self.u_,self.u_,MU) + friction(self.u_,self.u_,self.alpha))*dx
        # Gradient g and constant k
        self.k = friction(self.u_, self.u_, -(ALPHA_U - ALPHA_L))
        self.g = Function(self.V0)

        self.theta_vector = []
        self.kappa_vector = []
        self.c_vector = []


    def get_F(self):
        return assemble(self.F_expr)

    def get_FluidDomain(self):
        return assemble(self.fluid_domain*dx)

    def get_theta(self, beta, mu_pen, c):

        self.g = project(self.k + beta + mu_pen*c, self.V0)
        
        dot_prod = assemble(dot(self.g, self.psi)*dx)
        self.nrm_g = sqrt(assemble(dot(self.g,self.g)*dx))
        self.nrm_psi = sqrt(assemble(dot(self.psi,self.psi)*dx))
        
        theta_inv = dot_prod/self.nrm_g
        if theta_inv >= 0:
            theta_inv = min(1.0, theta_inv)
        else:
            theta_inv = max(-1.0, theta_inv)

        theta = acos(theta_inv)
        self.theta_vector.append(theta)
        #print "Theta = " + str(theta)
        return theta

    def get_constraint(self):
        self.omega = self.get_FluidDomain()
        c = self.omega - self.gamma
        self.c_vector.append(c)
        return c

    def update_psi(self, kappa, theta):
        if theta > 0:
            k1 = sin((1-kappa)*theta)/(sin(theta))
            k2 = sin(kappa*theta)/(sin(theta)*self.nrm_g)
            conv_comb(self.psi, self.psi_ref, self.g, k1, k2)
        else:
            self.psi = self.g/self.nrm_g

    def store_old_psi(self):
        self.psi_ref.assign(self.psi)





def gradient_method(model, beta, mu_pen, k):
    kappa = 1.0
    n_max = 40
    theta = 10.0
    n = 0
    F_vec = []

    model.solve_stokes()
    c = model.get_constraint()
    F = assemble(model.J_expr) + beta*c + 0.5*mu_pen*c*c
    F_vec.append(F)

    while theta > tol_theta and n < n_max:

        n += 1
        

        theta = model.get_theta(beta, mu_pen, c)
        if theta < tol_theta: # -0.01*k (dont know why this is here)
            return c
        #plot(sign(model.psi))

        kappa = min(1.0, kappa*1.5)
        model.store_old_psi()
        model.update_psi(kappa, theta)
        model.solve_stokes()

        c = model.get_constraint()
        F_new = assemble(model.J_expr) + beta*c + 0.5*mu_pen*c*c

        if F_new >= F:
            print "Entering linesearch"
            kappa = line_search(model, F, F_new, kappa, theta, beta, mu_pen)
            if kappa < tol_kappa:
                print "Line search did not find descent direction"
                print "Theta = " + str(theta)
                c = model.get_constraint()
                return c
            c = model.get_constraint()
            F = assemble(model.J_expr) + beta*c + 0.5*mu_pen*c*c
        else:
            F = F_new
        

        model.kappa_vector.append(kappa)
        F_vec.append(F)


        #print "F = " + str(F)
        if n > 2 and abs(F_vec[-1]-F_vec[-3]) < tol_F:
            print "Not sufficient decrease in F last two iterations"
            print "Theta = " + str(theta)
            return c
    print "\ngradient method iterations = " + str(n)

    return c


def line_search(model, F, F_new, kappa, theta, beta, mu_pen):
    it = 0
    while F_new >= F and kappa > tol_kappa:
        it += 1
        kappa = kappa*0.5        
        #print "Linesearch iteration #" + str(it) + ", Kappa = " + str(kappa)
        
        model.update_psi(kappa, theta)
        model.solve_stokes()

        c = model.get_constraint()
        F_new = assemble(model.J_expr) + beta*c + 0.5*mu_pen*c*c
    return kappa





def main():
    # Importing mesh and initializing class FlowModel
    optimality = False
    c_tol = 0.01
    beta = BETA
    mu_pen = 50.0 # Hypothetically a good initial value! 
    n_max = 40
    model = FlowModel(mesh_name)
    omega_vec = []
    beta_vec = []
    mu_pen_vec = []

    # Initial constraint with no penalty parameter
    c_ref = gradient_method(model, beta, 0, 0)
    omega = model.get_FluidDomain()
    omega_vec.append(omega)
    beta_vec.append(beta)
    mu_pen_vec.append(mu_pen)

    for k in range(n_max):
        
        # Start from neutral level set function
        # model.set_levelset() (not according to alg 17.3 Nocedal)
        c = gradient_method(model, beta, mu_pen, k)
        omega = model.get_FluidDomain()
        omega_vec.append(omega)
        if plt is True:
            plot(sign(model.psi), key='psi')

        print "\nAugmented Lagrangian iteration #" + str(k+1)
        print "Constraint   = " + str(c)
        print "Omega        = " + str(omega)
        print "For beta     = " + str(beta)
        print "and mu_pen   = " + str(mu_pen)

        if abs(c) < c_tol:
            optimality = True
            break

        # Must avoid beta < 0!
        beta = beta + mu_pen*c
        print "New beta     = " + str(beta)

        # If no change in constraint: increase penalty factor
        if abs(c-c_ref) < 0.01:
            mu_pen = mu_pen*1.5
        c_ref = c
        beta_vec.append(beta)
        mu_pen_vec.append(mu_pen)

    if optimality:
        print "Found optimal beta = " + str(beta) + " at iteration #" + str(k+1)
    else:
        print "Attempt failed. Sad face."

    print "Omega ="
    print omega_vec
    print "Beta ="
    print beta_vec
    print "Mu_pen ="
    print mu_pen_vec
    

    G = assemble(model.G_expr)
    J = assemble(model.J_expr)
    F = J + beta*c + 0.5*mu_pen*c*c
    print "\nF  = " + str(F)
    print "J    = " + str(J)
    print "G    = " + str(G)
    print "gamma        = " + str(model.gamma)
    print "omega        = " + str(omega)
    print "initial beta = " + str(BETA)
    print "final beta   = " + str(beta)
    print "mu_pen       = " + str(mu_pen)
    print "psi          = " + PSI
    print "Filename: " + filename


    if plt is True:
        plot(sign(model.psi), key='psi')
        interactive()

    # Dump solution to file in VTK format
    if write is True:
        file = File(filename)
        file << model.psi


BETA = 200
FRACTION = 0.9
filename = "AL_LB2_" + mesh_name + ".pvd"

start = time.time()
main()
end = time.time()

print "Elapsed time = " + str(end-start)





