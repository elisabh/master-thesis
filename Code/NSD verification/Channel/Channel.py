from dolfin import *
import numpy
import matplotlib.pyplot as plt
import fenicstools as ft

# Channel verification of Navier-Stokes-Darcy
# Comparing the alpha distribution to exact channel

# Boundary for whole domain
class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


# Boundaries for Bypass
class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.3, 0.7)) and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.3, 0.7)) and near(x[0], 1.0)


# Constants
dt = 0.01
T = 0.1
nu = Constant(0.01)
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)
U = 0.1

def alpha(psi):
    return ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi, 0), 0, 1)


mesh = Mesh("channel_alpha.xml")

# mesh = UnitSquareMesh(64, 64)
n = FacetNormal(mesh)

# Define function spaces (P2-P1)
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)

# Define trial and test functions
u = TrialFunction(V)
p = TrialFunction(Q)
v = TestFunction(V)
q = TestFunction(Q)

# Define boundary domains
noslip = Noslip()
inlet = Inlet()
outlet = Outlet()

boundaries = MeshFunction("size_t", mesh, 1)
boundaries.set_all(3)
noslip.mark(boundaries, 0)
inlet.mark(boundaries, 1)
outlet.mark(boundaries, 2)
ds = Measure("ds")(subdomain_data=boundaries)

# Poiseuille inflow
u_in = Expression(("(x[1]<0.7)*(x[1]>0.3)* U * (1 - (2*(x[1]-dy)/l)*(2*(x[1]-dy)/l))",
                   "0"), U=U, dy=0.5, l=0.4, degree=3)

# Dirichlet boundary conditions
bcu = [DirichletBC(V, (0.0, 0.0), boundaries, 0),
       DirichletBC(V, u_in, boundaries, 1)]
bcp = [DirichletBC(Q, 0, boundaries, 2)]

# Level set function
PSI = "1 - 2*(x[1]>0.3)*(x[1]<0.7)"
V0 = FunctionSpace(mesh, "DG", 0)
psi = project(Expression(PSI, degree=1), V0)

# Functions
u0 = interpolate(u_in, V)
u1 = Function(V)
p0 = Function(Q)
p1 = Function(Q)
k = Constant(dt)


# IPCS
# Tentative velocity step01
U = 0.5 * (u0 + u)
F1 = (1./k)*inner(u - u0, v)*dx + inner(grad(u0)*u0, v)*dx + \
    nu*inner(grad(u), grad(v))*dx + inner(grad(p0), v)*dx + alpha(psi)*inner(u, v)*dx - \
    nu*inner(dot(n, nabla_grad(u)), v)*ds(2) + inner(p0*n, v)*ds(2)
a1 = lhs(F1)
L1 = rhs(F1)

# Pressure update
a2 = inner(grad(p), grad(q))*dx
L2 = inner(grad(p0), grad(q))*dx - ((1./k) + alpha(psi))*div(u1)*q*dx

# Velocity update
a3 = (1./k + alpha(psi))*inner(u, v)*dx
L3 = (1./k + alpha(psi))*inner(u1, v)*dx - inner(grad(p1-p0), v)*dx

# Assemble system
A1 = assemble(a1)
A2 = assemble(a2)
A3 = assemble(a3)

b1 = None
b2 = None
b3 = None

t = dt

while t < T:

    # Tentative velocity
    b1 = assemble(L1, tensor=b1)
    [bc.apply(A1, b1) for bc in bcu]
    solve(A1, u1.vector(), b1)
    # plot(u1, title="Tentative", key="u1")

    # Pressure correction
    b2 = assemble(L2, tensor=b2)
    [bc.apply(A2, b2) for bc in bcp]
    solve(A2, p1.vector(), b2)

    # Velocity update
    b3 = assemble(L3, tensor=b3)
    [bc.apply(A3, b3) for bc in bcu]
    solve(A3, u1.vector(), b3)
    # plot(u1, title="Corrected", key="u2")

    u0.assign(u1)
    p0.assign(p1)

    t += dt

t = 0



# Reference solution ----------------------------------
########################################################

class Channel_domain(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[1], (0.3, 0.7))

channel = Channel_domain()

# Initialize mesh function for interior domains
subdomains = CellFunction('size_t', mesh)
subdomains.set_all(0)
channel.mark(subdomains, 1)

# submesh = SubMesh(mesh, subdomains, 1)


smesh = Mesh("channel_ref.xml")
n = FacetNormal(smesh)

# Define function spaces (P2-P1)
V_ = VectorFunctionSpace(smesh, "CG", 2)
Q_ = FunctionSpace(smesh, "CG", 1)

# Define trial and test functions
u = TrialFunction(V_)
p = TrialFunction(Q_)
v = TestFunction(V_)
q = TestFunction(Q_)

# Define boundary domains
noslip = Noslip()
inlet = Inlet()
outlet = Outlet()

boundaries = MeshFunction("size_t", smesh, 1)
boundaries.set_all(3)
noslip.mark(boundaries, 0)
inlet.mark(boundaries, 1)
outlet.mark(boundaries, 2)
ds = Measure("ds")(subdomain_data=boundaries)

# Dirichlet boundary conditions
bcu = [DirichletBC(V_, (0.0, 0.0), boundaries, 0),
       DirichletBC(V_, u_in, boundaries, 1)]
bcp = [DirichletBC(Q_, 0, boundaries, 2)]


# Functions
u0_ = interpolate(u_in, V_)
u1_ = Function(V_)
p0_ = Function(Q_)
p1_ = Function(Q_)
k = Constant(dt)


# IPCS
# Tentative velocity step
U = 0.5 * (u0_ + u)
F1 = (1./k)*inner(u - u0_, v)*dx + inner(grad(u0_)*u0_, v)*dx + \
    nu*inner(grad(u), grad(v))*dx + inner(grad(p0_), v)*dx \
    - nu*inner(dot(n, nabla_grad(u)), v)*ds(2) + inner(p0_*n, v)*ds(2)
a1 = lhs(F1)
L1 = rhs(F1)

# Pressure update
a2 = inner(grad(p), grad(q))*dx
L2 = inner(grad(p0_), grad(q))*dx - (1./k)*div(u1_)*q*dx

# Velocity update
a3 = (1./k)*inner(u, v)*dx
L3 = (1./k)*inner(u1_, v)*dx - inner(grad(p1_-p0_), v)*dx


# Assemble system
A1 = assemble(a1)
A2 = assemble(a2)
A3 = assemble(a3)

b1 = None
b2 = None
b3 = None

t = dt

while t < T:

    # Tentative velocity
    b1 = assemble(L1, tensor=b1)
    [bc.apply(A1, b1) for bc in bcu]
    solve(A1, u1_.vector(), b1)
    # plot(u1, title="Tentative", key="u1")

    # Pressure correction
    b2 = assemble(L2, tensor=b2)
    [bc.apply(A2, b2) for bc in bcp]
    solve(A2, p1_.vector(), b2)

    # Velocity update
    b3 = assemble(L3, tensor=b3)
    [bc.apply(A3, b3) for bc in bcu]
    solve(A3, u1_.vector(), b3)
    # plot(u1, title="Corrected", key="u2")

    u0_.assign(u1_)
    p0_.assign(p1_)

    t += dt

t -= dt

# Calculate difference by projecting both u0 and u0_ to the same space

plot(u0, title="Velocity alpha")
plot(u0_, title="Velocity ref")

# u_ref = project(u0_, V)
u_err = Function(V)
# interpolate(u_err, u0_)
# plot(u_err)
u2 = ft.interpolate_nonmatching_mesh(u0_, V)
err = errornorm(u2, u0, degree_rise=2)
u_err = Function(V)
u_err.vector()[:] = u2.vector().array() - u0.vector().array()
plot(u_err)

print err
# plot(u_err)

interactive()







