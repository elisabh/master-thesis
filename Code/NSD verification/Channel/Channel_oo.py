from dolfin import *
import fenicstools as ft


class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.3, 0.7)) and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.3, 0.7)) and near(x[0], 1.0)

# options:
# "mesh": takes in mesh string or grid size N
# "Darcy": solves ordinary NS if False and NSD if True


class Problem:
    def __init__(self, options):
        # Parameters must be defined by subclass
        self.f = None
        self.bcu = []
        self.bcp = []
        self.nu = Constant(0.01)
        self.U = 1.0
        self.N = None
        self.y1 = 0.3
        self.y2 = 0.7
        self.boundaries = None
        self.dt = 0.001
        self.T = 1.0

        self.ALPHA_U = 2.5 * self.nu / (0.01 * 0.01)
        self.ALPHA_L = 2.5 * self.nu / (100 * 100)

        # Save options:
        self.options = options

        if ".xml" in options["mesh"]:
            self.mesh = Mesh(options["mesh"])
        else:
            self.N = options["mesh"]
            self.mesh = UnitSquareMesh(self.N, self.N)


    def refine_mesh(self):
        self.mesh = refine(self.mesh)
        self.mark_boundaries()

    def get_mesh(self):
        self.mark_boundaries()
        return self.mesh

    def initial_conditions(self):
        y1 = self.y1
        y2 = self.y2
        u_in = Expression(("U * (1 - (2*(x[1]-dy)/(y2-y1))*(2*(x[1]-dy)/(y2-y1))) * (x[1]>y1)*(x[1]<y2)",
                           "0"), U=self.U, dy=0.5, y1=y1, y2=y2, degree=3)
        return u_in

    def mark_boundaries(self):
        noslip = Noslip()
        inlet = Inlet()
        outlet = Outlet()

        boundaries = MeshFunction("size_t", self.mesh, 1)
        boundaries.set_all(3)
        noslip.mark(boundaries, 0)
        inlet.mark(boundaries, 1)
        outlet.mark(boundaries, 2)


        self.boundaries = boundaries

    def boundary_conditions(self, u_in, V, Q):

        bcu = [DirichletBC(V, (0.0, 0.0), self.boundaries, 0),
               DirichletBC(V, u_in, self.boundaries, 1)]
        bcp = [DirichletBC(Q, 0, self.boundaries, 2)]

        return bcu, bcp

    def level_set(self):
        PSI = "1 - 2*(x[1] > y1)*(x[1] < y2)"
        V0 = FunctionSpace(self.mesh, "DG", 0)
        psi = project(Expression(PSI, y1=self.y1, y2=self.y2, degree=1), V0)
        return psi


class IPCSsolver:
    def __init__(self):
        self.something = None

    def solve(self, problem):

        T = problem.T
        dt = problem.dt
        nu = problem.nu

        if problem.options["Darcy"]:
            alpha = lambda psi: problem.ALPHA_U - (problem.ALPHA_U - problem.ALPHA_L) * conditional(gt(psi, 0), 0, 1)
        else:
            alpha = lambda psi: Constant(0.0)

        mesh = problem.get_mesh()
        n = FacetNormal(mesh)
        ds = Measure("ds")(subdomain_data=problem.boundaries)

        # Define function spaces (P2-P1)
        V = VectorFunctionSpace(mesh, "CG", 2)
        Q = FunctionSpace(mesh, "CG", 1)

        # Define trial and test functions
        u = TrialFunction(V)
        p = TrialFunction(Q)
        v = TestFunction(V)
        q = TestFunction(Q)

        u_in = problem.initial_conditions()
        bcu, bcp = problem.boundary_conditions(u_in, V, Q)

        u0 = interpolate(u_in, V)
        u1 = Function(V)
        p0 = Function(Q)
        p1 = Function(Q)

        psi = problem.level_set()

        k = Constant(dt)
        # Tentative velocity step
        F1 = (1./k) * inner(u - u0, v) * dx \
            + inner(grad(u0) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + inner(grad(p0), v) * dx \
            + alpha(psi) * inner(u, v) * dx \
            - nu * inner(dot(n, nabla_grad(u)), v) * ds(2) \
            + inner(p0 * n, v) * ds(2)
        a1 = lhs(F1)
        L1 = rhs(F1)

        # Pressure update
        a2 = inner(grad(p), grad(q)) * dx
        L2 = inner(grad(p0), grad(q)) * dx \
            - ((1./k) + alpha(psi)) * div(u1) * q * dx

        # Velocity update
        a3 = (1. / k + alpha(psi)) * inner(u, v) * dx
        L3 = (1. / k + alpha(psi)) * inner(u1, v) * dx \
            - inner(grad(p1 - p0), v) * dx

        # Assemble system
        A1 = assemble(a1)
        A2 = assemble(a2)
        A3 = assemble(a3)

        b1 = None
        b2 = None
        b3 = None

        t = dt

        while t < T:

            # Tentative velocity
            b1 = assemble(L1, tensor=b1)
            [bc.apply(A1, b1) for bc in bcu]
            solve(A1, u1.vector(), b1)

            # Pressure correction
            b2 = assemble(L2, tensor=b2)
            [bc.apply(A2, b2) for bc in bcp]
            solve(A2, p1.vector(), b2)

            # Velocity update
            b3 = assemble(L3, tensor=b3)
            [bc.apply(A3, b3) for bc in bcu]
            solve(A3, u1.vector(), b3)

            u0.assign(u1)
            p0.assign(p1)

            t += dt

        t -= dt

        return u0

def compare_solutions(u, u_ref):

    error_mesh = UnitSquareMesh(50, 50)
    print "Error mesh cells: "
    print error_mesh.num_cells()
    V_e = VectorFunctionSpace(error_mesh, "CG", 2)

    u1 = ft.interpolate_nonmatching_mesh(u, V_e)
    u2 = ft.interpolate_nonmatching_mesh(u_ref, V_e)
    u_err = Function(V_e)
    u_err.vector()[:] = u2.vector().array() - u1.vector().array()

    plot(u_err, title="Error in velocity")

    return sqrt(assemble(u_err**2*dx))





options_alpha = {"mesh": "channel_alpha.xml",
                 "Darcy": True}
options_ref = {"mesh":"channel_ref.xml",
               "Darcy": False}

channel = Problem(options_alpha)
channel_ref = Problem(options_ref)


err_values = []
solver = IPCSsolver()

for i in range(1):

    u = solver.solve(channel)
    u_ref = solver.solve(channel_ref)

    plot(u, title="Velocity")
    print "Mesh cells: "
    print channel.mesh.num_cells()

    err = compare_solutions(u, u_ref)
    err_values.append(err)

    channel.refine_mesh()
    channel_ref.refine_mesh()

print err_values


interactive()

# options_ref = {"mesh":"channel_ref.xml",
#                "Darcy": False}
# channel_ref = Problem(options_ref)
# u_ref = solver.solve(channel_ref)
#
#
# plot(u_ref, title="Velocity ref")
#
# # Compare solutions


# ------------------------------------------------------
# Alternative to creating mesh, does not run in parallel
# ------------------------------------------------------
# class Channel_domain(SubDomain):
#     def inside(self, x, on_boundary):
#         return between(x[1], (0.3, 0.7))
#
# channel = Channel_domain()
#
# # Initialize mesh function for interior domains
# subdomains = CellFunction('size_t', mesh)
# subdomains.set_all(0)
# channel.mark(subdomains, 1)
#
# submesh = SubMesh(mesh, subdomains, 1)
