from dolfin import *
import numpy as np
import math
import matplotlib.pyplot as plt

MU = 1.0
PI = 3.14159265
U = 1.0
Re = U*1.0/MU
lam = Re/2.0 - sqrt(Re*Re/4.0 + 4*PI*PI)

class KovasznayFlow(Expression):
    def eval(self, value, x):
        value[0] = 1 - exp(lam*x[0])*cos(2*PI*x[1])
        value[1] = lam/(2.0*PI)*exp(lam*x[0])*sin(2*PI*x[1])
    def value_shape(self):
        return (2,)


class AllBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

class PressureBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0.5)


def diffusion(u,v,mu):
    return mu * inner(grad(u), grad(v))

def incompressibility(u,q):
    return - div(u) * q

def a_stokes(u,p,v,q,mu):
    return diffusion(u, v, mu) + incompressibility(u, q) + incompressibility(v, p)

def errornorm_u(u_e_Ve, u, Ve):
    u_Ve = interpolate(u, Ve)
    e_Ve = Function(Ve)
    # Subtract degrees of freedom for the error field
    e_Ve.vector()[:] = u_e_Ve.vector().array() - \
                       u_Ve.vector().array()
    
    error = e_Ve**2*dx
    return sqrt(assemble(error))


def stokes():

    n_e  = 64
    mesh_e = RectangleMesh(Point(-0.5, -0.5), Point(0.5, 0.5), n_e, n_e, 'crossed')
    V_e = VectorFunctionSpace(mesh_e, "CG", 2)
    u_e = KovasznayFlow()
    u_e_Ve = interpolate(u_e, V_e)
    p_0 = Constant(0.0)
    
    f = Constant((0.0, 0.0))
    allBoundary = AllBoundary()
    pressureBoundary = PressureBoundary()
    e_vec = []

    for n in [4, 8, 16, 32, 64]:
        mesh = RectangleMesh(Point(-0.5, -0.5), Point(0.5, 0.5), n, n, 'crossed')

        #pbc = PeriodicBoundary()

        V = VectorFunctionSpace(mesh, "CG", 2)
        Q = FunctionSpace(mesh, "CG", 1)
        W = V * Q


        bc_u = DirichletBC(W.sub(0), u_e, allBoundary)
        bc_p = DirichletBC(W.sub(1), p_0, pressureBoundary)
        bcs = [bc_u, bc_p]


        (u, p) = TrialFunctions(W)
        (v, q) = TestFunctions(W)
        w = Function(W) 
        u_,p_ = w.split()

        a = a_stokes(u, p, v, q, MU)*dx
        L = inner(f, v)*dx

        problem = LinearVariationalProblem(a, L, w, bcs)
        solver = LinearVariationalSolver(problem)
        #solver.parameters["linear_solver"] = "gmres"
        solver.solve()
        

        w_k = Function(W)

        a2 = a_stokes(u, p, v, q, MU)*dx + inner(dot(grad(u), u_),v)*dx
        L2 = inner(f,v)*dx
        iter = 0
        while iter < 1:
            iter += 1
            solve(a2 == L2, w_k, bcs)
            diff = w.vector().array() - w_k.vector().array()
            eps = np.linalg.norm(diff)
            print 'iter=%d: norm=%g' % (iter, eps) 
            w.vector()[:] = w_k.vector().array()
            
        # Newton
        J = a_stokes(u, p, v, q, MU)*dx + inner(dot(grad(u), u_),v)*dx + inner(dot(grad(u_),u),v)*dx
        F = a_stokes(u_, p_, v, q, MU)*dx + inner(dot(grad(u_),u_),v)*dx 
        problem_N = NonlinearVariationalProblem(F, w, bcs, J)
        solver_N = NonlinearVariationalSolver(problem_N)
        prm = solver_N.parameters
        prm['newton_solver']['absolute_tolerance'] = 1E-10
        prm['newton_solver']['maximum_iterations'] = 25
        prm['newton_solver']['relaxation_parameter'] = 1.0
        solver_N.solve()

        E = errornorm_u(u_e_Ve, u_, V_e)
        print "\nn = " + str(n) 
        print str(E)
        e_vec.append(E)

    print e_vec

stokes()
interactive()
