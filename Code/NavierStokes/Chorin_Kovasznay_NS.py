from dolfin import *
import numpy

MU = 1.0
PI = 3.14159265
U = 1.0
Re = U*1.0/MU
lam = Re/2.0 - sqrt(Re*Re/4.0 + 4*PI*PI)


class KovasznayFlow(Expression):
    def eval(self, value, x):
        value[0] = 1 - exp(lam*x[0])*cos(2*PI*x[1])
        value[1] = lam/(2.0*PI)*exp(lam*x[0])*sin(2*PI*x[1])
    def value_shape(self):
        return (2,)


class AllBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

class PressureBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0.5) and between(x[1], 0, 0.1)




n = 16
mesh = RectangleMesh(Point(-0.5, -0.5), Point(0.5, 0.5), n, n, 'crossed')

# Define function spaces (P2-P1)
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)

# Define trial and test functions
u = TrialFunction(V)
p = TrialFunction(Q)
v = TestFunction(V)
q = TestFunction(Q)

dt = 0.00000001
T = 3
nu = 0.01

u_e = KovasznayFlow()
p_0 = Constant(0.0)
allBoundary = AllBoundary()
pressureBoundary = PressureBoundary()

bcu = DirichletBC(V, u_e, allBoundary)
bcp = DirichletBC(Q, p_0, pressureBoundary)

f = Constant((0.0, 0.0))
u0 = Function(V, name='u0', annotate=False)
u1 = Function(V, name='u1', annotate=False)
u_ = Function(V, name='u_')
p0 = Function(Q, name='p0')
p1 = Function(Q, name='p1')


k = Constant(dt)
for i in range(0):
    # Tentative velocity
    F1 = inner(grad(u)*u0, v)*dx + \
         nu*inner(grad(u), grad(v))*dx - inner(f, v)*dx
    a1 = lhs(F1)
    L1 = rhs(F1)
    solve(a1 == L1, u1, bcu)


    # Pressure update
    a2 = inner(grad(p), grad(q))*dx
    L2 = -(1/k)*div(u1)*q*dx
    solve(a2 == L2, p1)

    # Velocity update
    a3 = inner(u, v)*dx
    L3 = inner(u1, v)*dx - k*inner(grad(p1), v)*dx
    solve(a3 == L3, u_, bcu)

    # u0 = Function(V, name='u0', annotate=False)
    u0.assign(u1)

u_ee = interpolate(u_e, V)
u__ = Function(V)
u__.vector()[:] = u_ee.vector().array() - u_.vector().array()
plot(u__, title="error")
maxdiff = numpy.abs(u_ee.vector().array() - u_.vector().array()).max()
print maxdiff

plot(u_, key="u")
interactive()




