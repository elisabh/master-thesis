from dolfin import *
import numpy
import math


class TaylorGreenFlow(Expression):
    def __init__(self, t):
        self.t = t
    def eval(self, value, x):
        value[0] = -sin(pi*x[1])*cos(pi*x[0])*exp(-2.*pi*pi*nu*self.t)
        value[1] = sin(pi*x[0])*cos(pi*x[1])*exp(-2.*pi*pi*nu*self.t)
    def value_shape(self):
        return (2,)

def TaylorGreenPressure(tt):
    return Expression("-(cos(2*pi*x[0])+cos(2*pi*x[1]))*exp(-4.*pi*pi*nu*t)/4.", t=tt, nu=nu)

class AllBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class PeriodicDomain(SubDomain):
    def inside(self, x, on_boundary):
        # return True if on left or bottom boundary AND NOT on one of the two corners (0, 2) and (2, 0)
        return bool((near(x[0], 0) or near(x[1], 0)) and
                    (not ((near(x[0], 0) and near(x[1], 2)) or
                          (near(x[0], 2) and near(x[1], 0)))) and on_boundary)

    def map(self, x, y):
        if near(x[0], 2) and near(x[1], 2):
            y[0] = x[0] - 2.0
            y[1] = x[1] - 2.0
        elif near(x[0], 2):
            y[0] = x[0] - 2.0
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1] - 2.0


constrained_domain = PeriodicDomain()


Nx = 20
Ny = 20
mesh = RectangleMesh(Point(0, 0), Point(2, 2), Nx, Ny)

# Define function spaces (P2-P1)
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)

# Define trial and test functions
u = TrialFunction(V)
p = TrialFunction(Q)
v = TestFunction(V)
q = TestFunction(Q)

allBoundary = AllBoundary()

dt = 0.01
T = 1.0
nu = 0.01
t = 0.0     # dt/2. if p

# Initialize u0, u1, u2, p0, p1

u0 = Function(V, name='u0', annotate=False)
u1 = Function(V)
u_ = Function(V, name='u_')
# p0 = Function(Q, name='p0')
p1 = Function(Q, name='p1')

# Exact solutions :
u_0 = interpolate(TaylorGreenFlow(t), V)
# u_1 = interpolate(TaylorGreenFlow(t), V)

# p_0 = interpolate(TaylorGreenPressure(t + dt/2.), Q)
# p_1 = interpolate(TaylorGreenPressure(t + dt/2.), Q)

u_e = TaylorGreenFlow(t)
p_e = TaylorGreenPressure(t)
u_00 = Expression(("-sin(pi*x[1])*cos(pi*x[0])", "sin(pi*x[0])*cos(pi*x[1])"), degree=2)

f = Constant((0.0, 0.0))
k = Constant(dt)

while t <= T:

    bcu = [DirichletBC(V, TaylorGreenFlow(t), "on_boundary")]
    bcp = DirichletBC(Q, TaylorGreenPressure(t), allBoundary)

    # Tentative velocity
    F1 = 1/k * inner(u - u_0, v)*dx + \
        inner(grad(u)*u_0, v)*dx + \
        nu*inner(grad(u), grad(v))*dx
    a1 = lhs(F1)
    L1 = rhs(F1)
    solve(a1 == L1, u1, bcu)

    # Pressure update
    a2 = inner(grad(p), grad(q))*dx
    L2 = -(1/k)*div(u1)*q*dx
    solve(a2 == L2, p1, bcp)

    # Velocity update
    a3 = inner(u, v)*dx
    L3 = inner(u1, v)*dx - k*inner(grad(p1), v)*dx
    solve(a3 == L3, u_, bcu)
    plot(u_, key="u_", title="u_")

    u0.assign(u_) # u0 for t = -dt
    t += dt


u_exact = interpolate(TaylorGreenFlow(T), V)

u_err = Function(V)
u_err.vector()[:] = u_.vector().array() - u_exact.vector().array()
plot(u_err, title="error")
maxdiff = numpy.abs(u_.vector().array() - u_exact.vector().array()).max()
print maxdiff

plot(u_exact, title="u_exact")
interactive()




