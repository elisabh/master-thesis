# IPCS solver for the coupled Navier-Stokes-Darcy equations


from dolfin import *
import math


# Boundary for whole domain
class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

# Boundaries for Bypass
class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.4, 0.6)) and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0.4, 0.6)) and near(x[0], 1.0)




# # Velocity profile (Poiseuille flow) for only one axis
# # length of the profile
# # midpoint relative to (0,0)
# class Poiseuille(Expression):
#     def __init__(self, length, midpoint, u, direction):
#         self.l = length
#         self.mid = midpoint
#         self.u = u
#         self.dir = direction
#
#     def eval(self, value, x):
#         if self.dir is "x":
#             # Horizontal velocity profile
#             y_ = x[1] - self.mid
#             value[0] = self.u_x * (1 - (2 * y_ / self.l) * (2 * y_ / self.l))
#             value[1] = 0
#         elif self.dir is "y":
#             # Vertical velocity profile
#             x_ = x[0] - self.mid
#             value[0] = 0
#             value[1] = self.u_y * (1 - (2 * x_ / self.l) * (2 * x_ / self.l))

# Velocity for inlet and outlet (Poiseuille flow)
class Poiseuille(Expression):
    def __init__(self, l, center, u_x, u_y):
        self.l = l
        self.dt = center
        self.u_x = u_x
        self.u_y = u_y

    def eval(self, value, x):
        t_y = x[1] - self.dt
        t_x = x[0] - self.dt
        value[0] = self.u_x * (1 - (2 * t_y / self.l) * (2 * t_y / self.l))
        value[1] = self.u_y * (1 - (2 * t_x / self.l) * (2 * t_x / self.l))

    def value_shape(self):
        return (2,)


# Permeability distribution:
def alpha(psi):
    return ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi, 0), 0, 1)

def fluid(psi):
    return conditional(gt(psi, 0), 0, 1)

MU = Constant(0.01)
ALPHA_U = 2.5 * MU / (0.01 * 0.01)
ALPHA_L = 2.5 * MU / (100 * 100)
T = 0.5
N = 10
dt = T / N

# Mesh
nx = 100
mesh = UnitSquareMesh(nx, nx)

# Function spaces
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)

# Solid-fluid distribution:
PSI = "1 - 2*(x[1]>0.4)*(x[1]<0.6)"
# + 2*(x[1]>0.45)*(x[1]<0.55)*(x[0]>0.25)*(x[0]<0.35)"
# PSI = "1"
V0 = FunctionSpace(mesh, "DG", 0) # should it be of degree 1?
psi = project(Expression(PSI, degree=1), V0)




# Boundaries
boundaries = MeshFunction("size_t", mesh, 1)
boundaries.set_all(3)

noslip = Noslip()
noslip.mark(boundaries, 0)
inlet = Inlet()
inlet.mark(boundaries, 1)
outlet = Outlet()
outlet.mark(boundaries, 2)

u_noslip = Constant((0.0, 0.0))
l = 0.2
center = 0.5
U_in = 10.0

u_in = Poiseuille(l, center, U_in, 0)

bcu = [DirichletBC(V, u_noslip, boundaries, 0),
       DirichletBC(V, u_in, boundaries, 1)]
bcp = [DirichletBC(Q, 0, boundaries, 2)]
# Include ( np - n * grad(u) )*ds(2) = 0 in weak formulation

# Neumann boundary conditions - where to put them?
# + inner(p * n, v) * ds(2)
# - MU * inner(grad(u) * n, v) * ds(2)

ds = Measure("ds")(subdomain_data=boundaries)
n = FacetNormal(mesh)

u = TrialFunction(V)
p = TrialFunction(Q)
v = TestFunction(V)
q = TestFunction(Q)

u0 = Function(V, name='u0', annotate=False)
u1 = Function(V, name='u1', annotate=False)
u_ = Function(V, name='u_')
p0 = Function(Q, name='p0')
p1 = Function(Q, name='p1')
p_ = Function(Q, name='p_')



# Initial values of u0, u1 and p1:

# Tentative velocity
F = 1./dt*inner(u - u0, v)*dx \
    + inner(dot(u0, nabla_grad(u)), v)*dx\
    + MU*inner(grad(u), grad(v))*dx \
    + inner(grad(p0), v)*dx \
    + alpha(psi) * inner(u, v) * dx \
    - MU*inner(dot(n, nabla_grad(u)), v)*ds(2) \
    + inner(p0*n, v)*ds(2)

a = lhs(F)
L = rhs(F)
solve(a == L, u_, bcu)

# Pressure correction
Fp = inner(grad(p), grad(q))*dx \
    - inner(grad(p0), grad(q))*dx \
    + (1./dt)*div(u_)*q*dx \
    + alpha(psi)*div(u_)*q*dx # changed from - to + here

ap = lhs(Fp)
Lp = rhs(Fp)
solve(ap == Lp, p1, bcp)

# Velocity update
Fu = 1./dt*inner(u, v)*dx \
    - 1./dt*inner(u_, v)*dx \
    + inner(grad(p1), v)*dx \
    - inner(grad(p0), v)*dx \
    + alpha(psi)*inner(u, v)*dx \
    - alpha(psi)*inner(u_, v)*dx

au = lhs(Fu)
Lu = rhs(Fu)
solve(au == Lu, u1, bcu)

# plot(u1)
# plot(p1)


t = 0
while t < T:
    print "t = " + str(t)
    T = t
    # U_in = 1.0 + sin(2*t)
    #
    # u_in = Poiseuille(l, center, U_in, 0)
    #
    # bcu = [DirichletBC(V, u_noslip, boundaries, 0),
    #        DirichletBC(V, u_in, boundaries, 1)]

    # Tentative velocity step
    F = 1./(2*dt)*inner((3*u - 4*u1 + u0), v)*dx \
        + inner(dot(2*u1-u0, nabla_grad(u)), v)*dx \
        + MU*inner(grad(u), grad(v))*dx \
        + inner(grad(p1), v)*dx \
        + alpha(psi) * inner(u, v) * dx \
        - MU*inner(dot(n, nabla_grad(u)), v)*ds(2) \
        + inner(p1*n, v)*ds(2)

    a = lhs(F)
    L = rhs(F)
    solve(a == L, u_, bcu)
    plot(u_, key="u*")

    # Pressure correction
    Fp = inner(grad(p), grad(q))*dx \
        - inner(grad(p1), grad(q))*dx \
        + (3./(2*dt))*div(u_)*q*dx \
        - alpha(psi)*div(u_)*q*dx

    ap = lhs(Fp)
    Lp = rhs(Fp)
    solve(ap == Lp, p_, bcp)
    plot(p_, key="p")


    # Velocity update
    Fu = (3./(2*dt))*inner(u, v)*dx \
        - (3./(2*dt))*inner(u_, v)*dx \
        + inner(grad(p_), v)*dx \
        - inner(grad(p1), v)*dx \
        + alpha(psi)*inner(u, v)*dx \
        - alpha(psi)*inner(u_, v)*dx

    au = lhs(Fu)
    Lu = rhs(Fu)
    solve(au == Lu, u_, bcu)
    plot(u_, key="u")

    t += dt

interactive()