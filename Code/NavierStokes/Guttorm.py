# Tentative Velocity
F1 = inner(rho * 1. / dt * (u - u0), v) * dx + dot(rho * dot(u0, nabla_grad(u0)), v) * dx \
     + inner(sigma(0.5 * (u + u0), p0), eps(v)) * dx \
     + dot(p0 * n, v) * ds(1)  # - mu*dot(dot(grad(0.5*(u + u0)), n), v)*ds(1) - inner(f,v)*dx

# Pressure Correction
a2 = dot(dt * grad(p), grad(q)) * dx
L2 = dot(dt * grad(p0), grad(q)) * dx - rho * div(u1) * q * dx

# Velocity Correction
a3 = dot(rho * u, v) * dx
L3 = dot(rho * u1, v) * dx + dot(dt * grad(p0 - p1), v) * dx

# Assembling
a1 = lhs(F1);
L1 = rhs(F1)
A1 = assemble(a1)
A2 = assemble(a2)
A3 = assemble(a3)
b1 = None;
b2 = None;
b3 = None

print_dkdt_info = 10
start = timer()
t = dt_val

while t <= T:
    start = timer()
    b1 = assemble(L1, tensor=b1)
    pc = PETScPreconditioner("sor")
    sol = PETScKrylovSolver("minres", pc)
    sol.solve(A1, u1.vector(), b1)

    # end = timer()
    # print "Tent %.3f" % (end-start)

    # start = timer()
    b2 = assemble(L2, tensor=b2)
    pc2 = PETScPreconditioner("hypre_amg")
    sol2 = PETScKrylovSolver("gmres", pc2)
    sol2.solve(A2, p1.vector(), b2)

    # end = timer()
    # print "Press %.3f" % (end-start)

    # start = timer()
    b3 = assemble(L3, tensor=b3)
    pc3 = PETScPreconditioner("sor")
    sol3 = PETScKrylovSolver("minres", pc2)
    sol3.solve(A3, u1.vector(), b3)