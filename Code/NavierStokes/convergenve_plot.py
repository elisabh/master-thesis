import matplotlib.pyplot as plt
import numpy as np

N = [20, 40, 80, 160, 320]
h = [1./n for n in N]

Maxdiff = [0.0723866656795, 0.00884192441324, 0.00555255884326, 0.00109735118043, 0.00306345783553]
Error = [0.0746875148782, 0.00766588119443, 0.00413396652468, 0.000731198430612, 0.0030570404262]

plt.plot(h, Maxdiff)
plt.ylabel('error')
plt.yscale('log')
plt.xscale('log')
plt.show()

