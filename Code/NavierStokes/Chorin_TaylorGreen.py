from dolfin import *
import numpy


class PeriodicDomain(SubDomain):
    def inside(self, x, on_boundary):
        # return True if on left or bottom boundary AND NOT on one of the two corners (0, 2) and (2, 0)
        return bool((near(x[0], 0) or near(x[1], 0)) and
                    (not ((near(x[0], 0) and near(x[1], 2)) or
                          (near(x[0], 2) and near(x[1], 0)))) and on_boundary)

    def map(self, x, y):
        if near(x[0], 2) and near(x[1], 2):
            y[0] = x[0] - 2.0
            y[1] = x[1] - 2.0
        elif near(x[0], 2):
            y[0] = x[0] - 2.0
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1] - 2.0

# Constants
dt = 0.001
T = 0.2
nu = 0.01
err_values = []
h = []

for N in [20, 40, 80, 160, 320]:
    print N
    h.append(1./N)

    mesh = RectangleMesh(Point(0, 0), Point(2, 2), N, N)
    n = FacetNormal(mesh)

    # Define function spaces (P2-P1)
    V = VectorFunctionSpace(mesh, "CG", 2, constrained_domain=PeriodicDomain())
    Q = FunctionSpace(mesh, "CG", 1)

    # Define trial and test functions
    u = TrialFunction(V)
    p = TrialFunction(Q)
    v = TestFunction(V)
    q = TestFunction(Q)

    # Initial velocity and pressure
    u_initial = Expression(("-sin(pi*x[1])*cos(pi*x[0])", "sin(pi*x[0])*cos(pi*x[1])"), degree=2)
    p_initial = Expression("-(cos(2*pi*x[0])+cos(2*pi*x[1]))/4.", degree=1)
    u0 = interpolate(u_initial, V)
    p0 = interpolate(p_initial, Q)

    # Functions
    u1 = Function(V)
    p1 = Function(Q)

    k = Constant(dt)
    # Tentative velocity step


    F1 = (1./k)*inner(- u0, v)*dx + inner(grad(u)*u0, v)*dx
         #- nu*inner(dot(grad(u0), n), v)*ds
    a1 = lhs(F1)
    L1 = rhs(F1)

    # Pressure update
    a2 = inner(grad(p), grad(q))*dx
    L2 = -(1./k)*div(u1)*q*dx

    # Velocity update
    a3 = inner(u, v)*dx
    L3 = inner(u1, v)*dx - k*inner(grad(p1), v)*dx


    # Assemble system
    A1 = None
    A1_1 = assemble(nu*inner(grad(u), grad(v))*dx)
    A1_2 = assemble((1./k)*inner(u, v)*dx)
    A2 = assemble(a2)
    A3 = assemble(a3)
    b1 = None; b2 = None; b3 = None

    t = dt

    while t < T:

        # Tentative velocity
        A1 = assemble(a1) + A1_1 + A1_2
        b1 = assemble(L1)
        solve(A1, u1.vector(), b1, "minres", "sor")

        # Pressure correction
        b2 = assemble(L2)
        solve(A2, p1.vector(), b2, "gmres", "hypre_amg")

        # Velocity update
        b3 = assemble(L3)
        solve(A3, u1.vector(), b3, "minres", "sor")
        # plot(u1, title="u1")

        u0.assign(u1)
        p0.assign(p1)
        t += dt




    # Exact solutions :
    u_expr = Expression(("-sin(pi*x[1])*cos(pi*x[0])*exp(-2.*pi*pi*nu*t)",
                          "sin(pi*x[0])*cos(pi*x[1])*exp(-2.*pi*pi*nu*t)"), nu=nu, t=t, degree=2)
    u_exact = interpolate(u_expr, V)




    u_err = Function(V)
    u_err.vector()[:] = u0.vector().array() - u_exact.vector().array()
    err = sqrt(assemble(inner(u_err, u_err)*dx))
    err_values.append(err)
    # plot(uu, title="error")
    # plot(u1, title="u1")
    # plot(u_err, title="error")
    # plot(u_exact, title="u_exact")
    # maxdiff = numpy.abs(u0.vector().array() - u_exact.vector().array()).max()
    # maxdiff = numpy.abs(u_err.vector().array().max())
    # print "Maxdiff = " + str(maxdiff)
    # print "Error   = " + str(err)
print err_values

# interactive()




