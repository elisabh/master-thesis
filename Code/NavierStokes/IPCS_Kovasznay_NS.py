# IPCS solver for the coupled Navier-Stokes-Darcy equations

from dolfin import *
import math
import numpy

MU = 1.0
PI = 3.14159265
U = 1.0
Re = U*1.0/MU
lam = Re/2.0 - sqrt(Re*Re/4.0 + 4*PI*PI)

class KovasznayFlow(Expression):
    def eval(self, value, x):
        value[0] = 1 - exp(lam*x[0])*cos(2*PI*x[1])
        value[1] = lam/(2.0*PI)*exp(lam*x[0])*sin(2*PI*x[1])
    def value_shape(self):
        return (2,)


class AllBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

class PressureBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0.5)


def errornorm_u(u_e_Ve, u, Ve):
    u_Ve = interpolate(u, Ve)
    e_Ve = Function(Ve)
    # Subtract degrees of freedom for the error field
    e_Ve.vector()[:] = u_e_Ve.vector().array() - \
                       u_Ve.vector().array()
    error = e_Ve ** 2 * dx

    plot(e_Ve, key='u_e')

    return sqrt(assemble(error))


T = 0.005
N = 100
dt = T / N


def navier_stokes():

    # Reference solution
    #n_e = 64
    #mesh_e = RectangleMesh(Point(-0.5, -0.5), Point(0.5, 0.5), n_e, n_e, 'crossed')
    #V_e = VectorFunctionSpace(mesh_e, "CG", 2)
    u_e = KovasznayFlow()
    #u_e_Ve = interpolate(u_e, V_e)
    p_0 = Constant(0.0)

    allBoundary = AllBoundary()
    pressureBoundary = PressureBoundary()
    e_vec = []

    for n in [4, 8, 16, 32, 64]:

        mesh = RectangleMesh(Point(-0.5, -0.5), Point(0.5, 0.5), n, n, 'crossed')

        # Function spaces
        V = VectorFunctionSpace(mesh, "CG", 2)
        Q = FunctionSpace(mesh, "CG", 1)

        bcu = DirichletBC(V, u_e, allBoundary)
        bcp = DirichletBC(Q, p_0, pressureBoundary)

        u = TrialFunction(V)
        p = TrialFunction(Q)
        v = TestFunction(V)
        q = TestFunction(Q)

        u0 = Function(V, name='u0', annotate=False)
        u1 = Function(V, name='u1', annotate=False)
        u_ = Function(V, name='u_')
        p0 = Function(Q, name='p0')
        p1 = Function(Q, name='p1')

        # Initial values of u0, u1 and p1: ------------------------

        # Tentative velocity
        F = (1./dt)*inner(u - u0, v)*dx \
            + inner(dot(u0, grad(u0)), v)*dx\
            + MU*inner(grad(u), grad(v))*dx

        a = lhs(F)
        L = rhs(F)
        solve(a == L, u1, bcu)

        # Pressure correction
        ap = inner(grad(p), grad(q))*dx
        Lp = -(1./dt)*div(u1)*q*dx
        solve(ap == Lp, p0, bcp)

        # Velocity update
        au = inner(u, v)*dx
        Lu = inner(u1, v)*dx - dt*inner(grad(p0), v)*dx
        solve(au == Lu, u_, bcu)

        u0.assign(u1)
        u1.assign(u_)
        # ----------------------------------------------

        # # Second order ---------------------------------
        # Tentative velocity step
        F = 1./(2*dt)*inner((3*u - 4*u1 + u0), v)*dx \
            + inner(dot(2*u1-u0, grad(u)), v)*dx \
            + MU*inner(grad(u), grad(v))*dx \
            + inner(grad(p0), v)*dx

        a = lhs(F)
        L = rhs(F)
        solve(a == L, u_, bcu)
        plot(u_, key="u*")

        # Pressure correction
        Fp = inner(grad(p), grad(q))*dx \
            - inner(grad(p0), grad(q))*dx \
            + (3./(2*dt))*div(u_)*q*dx

        ap = lhs(Fp)
        Lp = rhs(Fp)
        solve(ap == Lp, p1, bcp)
        plot(p1, key="p")


        # Velocity update
        Fu = (3./(2*dt))*inner(u, v)*dx \
            - (3./(2*dt))*inner(u_, v)*dx \
            + inner(grad(p1), v)*dx \
            - inner(grad(p0), v)*dx

        au = lhs(Fu)
        Lu = rhs(Fu)
        solve(au == Lu, u_, bcu)
        plot(u_, key="u")
        u_ee = interpolate(u_e, V)
        maxdiff = numpy.abs(u_ee.vector().array() - u_.vector().array()).max()

        # E = errornorm_u(u_e_Ve, u_, V_e)
        print "\nn = " + str(n)
        # print str(E)
        e_vec.append(maxdiff)



    print e_vec


navier_stokes()
interactive()