from dolfin import *
import numpy
import matplotlib.pyplot as plt

# Verification of solution for Navier-Stokes-Darcy equation in 2D

# Constants
dt = 0.0000025
T = 0.00025
nu = Constant(1.0)
alpha = Constant(10.0)
t = 0.0

class Poiseuille(Expression):
    def __init__(self, l, dt, u_x, u_y):
        self.l = l
        self.dt = dt
        self.u_x = u_x
        self.u_y = u_y

    def eval(self, value, x):
        t_y = x[1] - self.dt
        t_x = x[0] - self.dt
        value[0] = self.u_x * (1 - (2 * t_y / self.l) * (2 * t_y / self.l))
        value[1] = self.u_y * (1 - (2 * t_x / self.l) * (2 * t_x / self.l))

    def value_shape(self):
        return (2,)

def right(x, on_boundary): return x[0] > (1.0 - DOLFIN_EPS)

def left(x, on_boundary): return x[0] < DOLFIN_EPS

def top_bottom(x, on_boundary):
    return x[1] > 1.0 - DOLFIN_EPS or x[1] < DOLFIN_EPS

def top_sides(x, on_boundary):
    return x[1] > 0.0 + DOLFIN_EPS and on_boundary

def bottom(x, on_boundary):
    return x[0] < DOLFIN_EPS and on_boundary


# u_expr = Expression(("x[0]", "-x[1]"), t=0.0, degree=3)
# p_expr = Expression("0.0", degree=3)
# f = Expression(("x[0]*(1 + alpha)",
#                 "x[1]*(1 - alpha)"),
#                alpha=alpha, t=0.0, degree=3)

u_expr = Expression(("-cos(pi*x[0])*sin(pi*x[1])*exp(-t)",
                     "sin(pi*x[0])*cos(pi*x[1])*exp(-t)"),
                    pi=DOLFIN_PI, t=0.0, degree=3)

p_expr = Expression("sin(pi*x[0])*cos(pi*x[1])", pi=DOLFIN_PI, degree=3)

f = Expression(("-alpha*exp(-t)*sin(pi*x[1])*cos(pi*x[0]) - 2*nu*pi*pi*exp(-t)*sin(pi*x[1])*cos(pi*x[0]) + \
                pi*cos(pi*x[0])*cos(pi*x[1]) - pi*exp(-2*t)*sin(pi*x[0])*sin(pi*x[1])*sin(pi*x[1])*cos(pi*x[0]) \
                - pi*exp(-2*t)*sin(pi*x[0])*cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[1]) + exp(-t)*sin(pi*x[1])*cos(pi*x[0])",
                "alpha*exp(-t)*sin(pi*x[0])*cos(pi*x[1]) + 2*nu*pi*pi*exp(-t)*sin(pi*x[0])*cos(pi*x[1]) \
                - pi*sin(pi*x[0])*sin(pi*x[1]) - pi*exp(-2*t)*sin(pi*x[0])*sin(pi*x[0])*sin(pi*x[1])*cos(pi*x[1])  \
                - pi*exp(-2*t)*sin(pi*x[1])*cos(pi*x[0])*cos(pi*x[0])*cos(pi*x[1]) - exp(-t)*sin(pi*x[0])*cos(pi*x[1])"),
               alpha=alpha, nu=nu, pi=DOLFIN_PI, t=0.0, degree=3)

# f = Expression(("0", "0"), t=0.0, degree=2)


err_values = []
err_J = []
h = []
h2 = []
h3 = []


for N in [4, 8, 16, 32]:

    t = 0.0

    print N
    h.append(1./N)
    h2.append(1./(N*N))
    h3.append(1./(N**3))

    mesh = RectangleMesh(Point(0, 0), Point(1, 1), N, N)
    n = FacetNormal(mesh)

    # Define function spaces (P2-P1)
    V = VectorFunctionSpace(mesh, "CG", 2)
    Q = FunctionSpace(mesh, "CG", 1)

    # Define trial and test functions
    u = TrialFunction(V)
    p = TrialFunction(Q)
    v = TestFunction(V)
    q = TestFunction(Q)
    u_err = Function(V)
    p_exact = interpolate(p_expr, Q)
    # Initial velocity and pressure
    # u_in = Poiseuille(1.0, 0.5, 1.0, 0.0)
    # f = Constant((0, 0))
    u_expr.t = 0.0
    f.t = 0.0
    u0 = project(u_expr, V)
    p0 = project(p_expr, Q)
    # Boundary conditions
    # bc_in = DirichletBC(V, u_in, left)
    # noslip = DirichletBC(V, Constant((0, 0)), top_bottom)
    # bcu = [bc_in, noslip]
    # bc_out = DirichletBC(Q, Constant(0.0), right)
    # bcp = [bc_out]
    bcu = [DirichletBC(V, u_expr, "on_boundary")]
    bcp = [DirichletBC(Q, p_expr, "on_boundary")]

    # bcp_point = DirichletBC(Q, Constant(0), "on_boundary && near(x[0], 0) && near(x[1], 1)", method="pointwise")
    # bcp_point = DirichletBC(Q, Constant(0), "on_boundary && near(x[0], 0)")

    # Functions
    # u0 = Function(V)
    # p0 = Function(Q)
    u1 = Function(V)
    p1 = Function(Q)
    k = Constant(dt)

    # IPCS
    # # Tentative velocity step01
    # U = 0.5 * (u0 + u)
    # F1 = (1./k)*inner(u - u0, v)*dx + inner(grad(u0)*u0, v)*dx + \
    #     nu*inner(grad(u), grad(v))*dx + inner(grad(p0), v)*dx + \
    #     alpha*inner(u, v)*dx - inner(f, v)*dx - nu * inner(grad(U).T * n, v) * ds
    # a1 = lhs(F1)
    # L1 = rhs(F1)
    #
    # # Pressure update
    # a2 = inner(grad(p), grad(q))*dx
    # L2 = inner(grad(p0), grad(q))*dx - ((1./k) + alpha)*div(u1)*q*dx
    #
    # # Velocity update
    # a3 = (1./k + alpha)*inner(u, v)*dx
    # L3 = (1./k + alpha)*inner(u1, v)*dx - inner(grad(p1-p0), v)*dx


    # # CHORIN
    # Tentative velocity
    F1 = (1./k)*inner(u - u0, v)*dx + inner(dot(u0, nabla_grad(u0)), v)*dx \
        + nu*inner(grad(u), grad(v))*dx + alpha*inner(u, v)*dx \
        - inner(f, v)*dx
    a1 = lhs(F1)
    L1 = rhs(F1)

    # Pressure update

    a2 = inner(grad(p), grad(q))*dx #- inner(grad(p), n)*q*ds
    L2 = -(1./k)*div(u1)*q*dx - alpha*div(u1)*q*dx

    # Velocity update
    a3 = (1./k)*inner(u, v)*dx + alpha*inner(u, v)*dx
    L3 = (1./k)*inner(u1, v)*dx - inner(grad(p1), v)*dx + alpha*inner(u1, v)*dx

    # Assemble system
    A1 = assemble(a1)
    A2 = assemble(a2)
    A3 = assemble(a3)

    b1 = None
    b2 = None
    b3 = None

    t = dt

    while t < T:

        # Tentative velocity
        f.t = t
        b1 = assemble(L1, tensor=b1)
        u_expr.t = t
        # bcu = [DirichletBC(V, u_expr, "on_boundary")]
        [bc.apply(A1, b1) for bc in bcu]
        solve(A1, u1.vector(), b1)#, "minres", "sor")
        # plot(u1, title="u1")
        # plot(div(u1), title="divu")

        # Pressure correction
        b2 = assemble(L2, tensor=b2)
        # [bc.apply(A2, b2) for bc in bcp]
        # bcp_point.apply(A2, b2)
        # [bc.apply(p1.vector()) for bc in bcp]
        normalize(b2)
        # set_log_level(DEBUG)
        solve(A2, p1.vector(), b2)#, "gmres", "hypre_amg")
        normalize(p1.vector())
        # plot(p1, interactive=True, title="p1")

        # Velocity update
        b3 = assemble(L3, tensor=b3)
        [bc.apply(A3, b3) for bc in bcu]
        solve(A3, u1.vector(), b3)#, "minres", "sor")

        u0.assign(u1)
        p0.assign(p1)

        # u_exact = interpolate(u_expr, V)
        # plot(u_exact)
        # plot(u_exact, title="u_exact", key='u_ex')
        # p_exact = interpolate(p_expr, Q)
        # u_err.vector()[:] = u0.vector().array() - u_exact.vector().array()
        # plot(u0, title="u0", key="u0")
        # plot(u_exact, title="u_exact", key="ue")



        # print errornorm(u0, u_exact, degree_rise=2)
        # print sqrt(assemble(inner(u_err, u_err) * dx))
        t += dt

    t -= dt
    # plot(p0, title="Pressure", key="p")
    # plot(u0, title="Velocity", key="u")
    # # Exact solutions :
    u_expr.t = t
    u_exact = interpolate(u_expr, V)

    # plot(u_exact, title="Exact u", key="u_e")
    # plot(p_exact, title="Exact p")
    # #
    # # u_err = Function(V)
    u_err.vector()[:] = u0.vector().array() - u_exact.vector().array()
    # plot(u_err, title="Error u", key="eu")
    #
    # # #
    p_err = Function(Q)
    p_err.vector()[:] = p0.vector().array() - p_exact.vector().array()
    # plot(p_err, title="Error p", key="ep")
    # # #
    # # # err = sqrt(assemble(inner(u_err, u_err) * dx))
    err = errornorm(u0, u_exact, degree_rise=2)
    err_values.append(err)
    # maxdiff = numpy.abs(u_err.vector().array().max())
    # print maxdiff



print err_values
plt.loglog(h, err_values, 'r.-', h, h3, 'b', h, h2, 'g')
plt.title("L2-norm u")
plt.show()
interactive()




