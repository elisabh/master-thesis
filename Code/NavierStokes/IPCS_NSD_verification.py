from dolfin import *
import numpy
import matplotlib.pyplot as plt

# Verification of solution for Navier-Stokes-Darcy equation in 2D

# Constants
dt = 0.001
T = 0.1
nu = Constant(0.01)
alpha = Constant(0.0)
t = 0.0

# class f_expression(Expression):
#     def eval(self, value, x, t):
#         t = t
#         x = x[0]
#         y = x[1]
#         value[0] = sin(t + x)*sin(t + y)**2*cos(t + x) + (2*nu + alpha)*sin(t + x)*sin(t + y) + \
#                    sin(t + x)*cos(t + x)**2*cos(t + y) + sin(t + x)*cos(t + y) + \
#                    sin(t + y)*cos(t + x) + cos(t + x - y)
#         value[1] = -2*sin(t + x)**2*sin(t + y)*cos(t + x) - 2*nu*sin(t + x)**2 - \
#                    2*sin(t + x)*cos(t + x) + (2*nu + alpha)*cos(t + x)**2 - cos(t + x - y)
#     def value_shape(self):
#         return (2,)

# u_expr = Expression(("sin(x[0]+t)*sin(x[1]+t)", "cos(x[0]+t)*cos(x[1]+t)"), t=0.0, degree=3)
# p_expr = Expression("sin(x[0]-x[1]+t)", t=0.0, degree=3)

# f_expr = Expression(("sin(t + x[0])*sin(t + x[1])*sin(t + x[1])*cos(t + x[0]) + "
#                      "(2*nu + alpha)*sin(t + x[0])*sin(t + x[1]) + "
#                      "sin(t + x[0])*cos(t + x[0])*cos(t + x[0])*cos(t + x[1]) + "
#                      "sin(t + x[0])*cos(t + x[1]) + sin(t + x[1])*cos(t + x[0]) + cos(t + x[0] - x[1])",
#                      "-2*sin(t + x[0])*sin(t + x[0])*sin(t + x[1])*cos(t + x[0]) - "
#                      "2*nu*sin(t + x[0])*sin(t + x[0]) - 2*sin(t + x[0])*cos(t + x[0]) + "
#                      "(2*nu + alpha)*cos(t + x[0])*cos(t + x[0]) - cos(t + x[0] - x[1])"),
#                     nu=nu, alpha=alpha, t=0.0, degree=3)


# u_expr = Expression(("sin(x[0] + x[1])", "-sin(x[0] + x[1])"), degree=3)
# p_expr = Expression("cos(x[0] + x[1])", degree=3)
# f_expr = Expression(("(2*nu + alpha - 1)*sin(x[0] + x[1])", "-(2*nu + alpha + 1)*sin(x[0] + x[1])"),
#                     nu=nu, alpha=alpha, degree=3)

u_expr = Expression(("x[0]*t", "-x[1]*t"), t=0.0, degree=3)
p_expr = Expression("x[1]", t=0.0, degree=3)
f_expr = Expression(("x[0]*(t*t + alpha*t + 1)",
                     "x[1]*t*t - alpha*x[1]*t - x[1] + 1"),
                    nu=nu, alpha=alpha, t=0.0, degree=3)

# f_expr = Expression(("0", "0"), t=0.0, degree=2)


err_values = []
err_J = []
h = []
h2 = []


for N in [20, 40, 80]:

    t = 0.0

    print N
    h.append(1./N)
    h2.append(1./(N*N))

    mesh = RectangleMesh(Point(0, 0), Point(1, 1), N, N)
    n = FacetNormal(mesh)

    # Define function spaces (P2-P1)
    V = VectorFunctionSpace(mesh, "CG", 2)
    Q = FunctionSpace(mesh, "CG", 1)

    # Define trial and test functions
    u = TrialFunction(V)
    p = TrialFunction(Q)
    v = TestFunction(V)
    q = TestFunction(Q)


    # Initial velocity and pressure
    u_expr.t = 0.0
    p_expr.t = 0.0
    f_expr.t = 0.0
    u0 = interpolate(u_expr, V)
    p0 = interpolate(p_expr, Q)
    f = interpolate(f_expr, V)

    # Boundary conditions
    bcu = DirichletBC(V, u_expr, "on_boundary")
    bcp = DirichletBC(Q, p_expr, "on_boundary")

    # Functions
    u1 = Function(V)
    p1 = Function(Q)
    k = Constant(dt)

    # IPCS
    # # Tentative velocity step01
    # U = 0.5 * (u0 + u)
    # F1 = (1./k)*inner(u - u0, v)*dx + inner(grad(u0)*u0, v)*dx + \
    #     nu*inner(grad(u), grad(v))*dx + inner(grad(p0), v)*dx + \
    #     alpha*inner(u, v)*dx - nu*inner(grad(U).T*n, v)*ds - inner(f, v)*dx
    # a1 = lhs(F1)
    # L1 = rhs(F1)
    #
    # # Pressure update
    # a2 = inner(grad(p), grad(q))*dx
    # L2 = inner(grad(p0), grad(q))*dx - ((1./k) + alpha)*div(u1)*q*dx
    #
    # # Velocity update
    # a3 = (1./k + alpha)*inner(u, v)*dx
    # L3 = (1./k + alpha)*inner(u1, v)*dx - inner(grad(p1-p0), v)*dx

    # CHORIN
    # Tentative velocity
    F1 = (1./k)*inner(u - u0, v)*dx + inner(grad(u0)*u0, v)*dx \
        + nu*inner(grad(u), grad(v))*dx + alpha*inner(u, v)*dx \
        - inner(f, v)*dx
    a1 = lhs(F1)
    L1 = rhs(F1)

    # Pressure update
    a2 = inner(grad(p), grad(q))*dx
    L2 = -(1./k)*div(u1)*q*dx - alpha*div(u1)*q*dx

    # Velocity update
    a3 = (1./k)*inner(u, v)*dx + alpha*inner(u, v)*dx
    L3 = (1./k)*inner(u1, v)*dx - inner(grad(p1), v)*dx + alpha*inner(u1, v)*dx

    # Assemble system
    A1 = assemble(a1)
    A2 = assemble(a2)
    A3 = assemble(a3)

    b1 = None
    b2 = None
    b3 = None

    t = dt

    while t < T:
        u_expr.t = t
        p_expr.t = t
        f_expr.t = t
        f = interpolate(f_expr, V)

        # Tentative velocity
        b1 = assemble(L1)
        bcu.apply(A1, b1)
        solve(A1, u1.vector(), b1, "minres", "sor")

        # Pressure correction
        b2 = assemble(L2)
        bcp.apply(A2, b2)
        bcp.apply(p1.vector())
        # normalize(b2)
        solve(A2, p1.vector(), b2, "gmres", "hypre_amg")
        # normalize(p1.vector())

        # Velocity update
        b3 = assemble(L3)
        bcu.apply(A3, b3)
        solve(A3, u1.vector(), b3, "minres", "sor")

        u0.assign(u1)
        p0.assign(p1)
        t += dt


    # plot(p0, title="Pressure", key="p")
    plot(u0, title="Velocity", key="u")
    # Exact solutions :
    u_exact = interpolate(u_expr, V)
    p_exact = interpolate(p_expr, Q)

    plot(u_exact, title="Exact u", key="u_e")

    u_err = Function(V)
    u_err.vector()[:] = u0.vector().array() - u_exact.vector().array()
    plot(u_err)

    p_err = Function(Q)
    p_err.vector()[:] = p0.vector().array() - p_exact.vector().array()

    err = sqrt(assemble(inner(u_err, u_err) * dx))
    err_values.append(err)



print err_values
plt.loglog(h, err_values, 'r.-', h, h, 'b', h, h2, 'g')
plt.title("L2-norm u")
plt.show()
interactive()




