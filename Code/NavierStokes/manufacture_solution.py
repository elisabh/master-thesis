from sympy import *

# Manufactured solution for the Navier-Stokes-Darcy equation

alpha = 0
nu = 0.1

x, y, t, pi, alpha, nu = symbols('x, y, t, pi, alpha, nu')

# u = 2*sin(8*t)*x**2*(1-x)**2*y*(1-y)*(2*y-1)
# v = 2*sin(8*t)*y**2*(1-y)**2*x*(1-x)*(1-2*x)
# p = sin(8*t)*x*(1-x)*y*(1-y)

# u = -cos(pi*x)*sin(pi*y)*exp(-t)
# v = sin(pi*x)*cos(pi*y)*exp(-t)
# p = x + y

# u = x**2*y*exp(-t)
# v = -y**2*x*exp(-t)

# p = sin(pi*x)*cos(pi*y)

# u = x**2*y*exp(-t)
# v = -y**2*x*exp(-t)
# p = x + y

u = x
v = -y
p = 0.0
#

u_t = u.diff(t, 1)
u_x = u.diff(x, 1)
u_y = u.diff(y, 1)
u_xx = u.diff(x, 2)
u_yy = u.diff(y, 2)

v_t = v.diff(t, 1)
v_x = v.diff(x, 1)
v_y = v.diff(y, 1)
v_xx = v.diff(x, 2)
v_yy = v.diff(y, 2)

# p_x = p.diff(x, 1)
# p_y = p.diff(y, 1)

fx = u_t + u*u_x + v*u_y - nu*(u_xx + u_yy) + alpha*u # p_x
fy = v_t + u*v_x + v*v_y - nu*(v_xx + v_yy) + alpha*v # + p_y

print "div u = " + str(u_x + v_y)
print ""
print "fx = " + str((fx))
print "fy = " + str((fy))


