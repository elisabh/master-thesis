# Navier-Stokes bypass topology optimization
# Splitting method (Chorin)
# Implemented with mixed function space

from dolfin import *
from dolfin_adjoint import *
import numpy
import time
import math


# All boundary for noslip, overloaded by inlet and outlet
class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


# Boundaries for Bypass
class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0, 0.2)) and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0, 0.2)) and near(x[0], 2.0)


class Vein(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[1], (0.0, 0.2))


# Velocity for inlet and outlet (Poiseuille flow)
class Poiseuille(Expression):
    def __init__(self, l, center, u_x, u_y):
        self.l = l
        self.dt = center
        self.u_x = u_x
        self.u_y = u_y

    def eval(self, value, x):
        t_y = x[1] - self.dt
        t_x = x[0] - self.dt
        value[0] = self.u_x * (1 - (2 * t_y / self.l) * (2 * t_y / self.l))
        value[1] = self.u_y * (1 - (2 * t_x / self.l) * (2 * t_x / self.l))

    def value_shape(self):
        return (2,)


# Expressions:

def diffusion(u, v, mu):
    return mu * inner(grad(u), grad(v))


def friction(u, v, alpha):
    return alpha * inner(u, v)


def incompressibility(u, q):
    return - div(u) * q


def a_stokes(u, p, v, q, mu, alpha):
    return diffusion(u, v, mu) + friction(u, v, alpha) \
           + incompressibility(u, q) + incompressibility(v, p)

def a_adjoint(u_, u, p, v, q, mu, alpha):
    return a_stokes(u, p, v, q, mu, alpha) \
           + inner(dot(grad(u_).T, u), v) - inner(dot(grad(u), u_), v)


def conv_comb(psi, psi_ref, g, k1, k2):
    psi.vector().zero()
    psi.vector().axpy(k1, psi_ref.vector())
    psi.vector().axpy(k2, g.vector())

def update_psi(psi, g, k1, k2):
    psi_ref = Function(V0)
    psi_ref.assign(psi)
    psi.vector().zero()
    psi.vector().axpy(k1, psi_ref.vector())
    psi.vector().axpy(k2, g.vector())

def alpha(psi):
    return ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi, 0), 0, 1)

def fluid(psi):
    return conditional(gt(psi, 0), 0, 1)

mesh_name = "../mesh/bypass4.xml"

# Global variables (constants)
f = Constant((0.0, 0.0))
MU = Constant(0.01)
ALPHA_U = 2.5 * MU / (0.01 * 0.01)
ALPHA_L = 2.5 * MU / (100 * 100)
BETA = 3.0
PSI = "-1"
tol_theta = 0.05
tol_kappa = 0.4
tol_F = 0.00001

# Mesh -------------------------------------
mesh = Mesh(mesh_name)
mesh = refine(mesh)
# mesh = refine(mesh)

# Function Spaces
P2 = VectorElement('P', 'triangle', 2)
P1 = FiniteElement('P', 'triangle', 1)
TH = P2 * P1
W = FunctionSpace(mesh, TH)

(V, Q) = W.split()
V0 = FunctionSpace(mesh, "DG", 0)

(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)



# Boundaries -------------------------------
boundaries = MeshFunction("size_t", mesh, 1)
boundaries.set_all(3)



noslip = Noslip()
noslip.mark(boundaries, 0)
inlet = Inlet()
inlet.mark(boundaries, 1)
outlet = Outlet()
outlet.mark(boundaries, 2)

# Subdomain ---------------------------------
domains = CellFunction("size_t", mesh)
domains.set_all(1)

vein = Vein()
vein.mark(domains, 0)
dx = Measure("dx")(subdomain_data=domains)

u_noslip = Constant((0.0, 0.0))
l = 0.2
center = 0.1
U_in = 1.0

u_in = Poiseuille(l, center, U_in, 0)

bcu = [DirichletBC(W.sub(0), u_noslip, boundaries, 0),
       DirichletBC(W.sub(0), u_in, boundaries, 1)]

bcp = [DirichletBC(W.sub(1), 0, boundaries, 2)]





# ---------------------------------------------------------------------

psi = project(Expression(PSI, degree=1), V0)
fluid_domain = conditional(gt(psi, 0), 0, 1)


# # Solve system

# Function value: default zero
w0 = Function(W, name='w0', annotate=False)
(u0, p0) = split(w0)

w1 = Function(W, name='w1', annotate=False)
(u1, p1) = split(w1)

w2 = Function(W, name='w2', annotate=False)
(u2, p2) = split(w2)

w3 = Function(W, name='w3')
(u3, p3) = split(w3)

dt = Constant(0.01)


# Tentative velocity step 0
a1 = inner(u, v)*dx('everywhere') + inner(p, q)*dx('everywhere')
L1 = dt*(inner(f, v) - inner(grad(u0)*u0, v) -
         MU*inner(grad(u0), grad(v)) -
         alpha(psi)*inner(u0, v))*dx('everywhere') \
    + inner(u0, v)*dx('everywhere')


problem1 = LinearVariationalProblem(a1, L1, w1, bcu)
solver1 = LinearVariationalSolver(problem1)
solver1.solve(annotate=False)

# print type(w1.sub(0))

# Pressure update 1
a2 = inner(grad(p), grad(q))*dx('everywhere') + inner(u, v)*dx('everywhere')
L2 = -(1/dt)*div(u1)*q*dx('everywhere')

problem2 = LinearVariationalProblem(a2, L2, w2, bcp)
solver2 = LinearVariationalSolver(problem2)
solver2.solve(annotate=False)

# plot(w2.sub(1))
# plot(p2, title='p2')
# plot(u2)
# plot(w1.sub(0))

# Velocity update
a3 = inner(u, v)*dx('everywhere') + inner(p, q)*dx('everywhere')
L3 = inner(u1, v)*dx('everywhere') \
     - dt*inner(grad(p2), v)*dx('everywhere')

problem3 = LinearVariationalProblem(a3, L3, w3, bcu)
solver3 = LinearVariationalSolver(problem3)
solver3.solve()


# Solution: u3, p2



J = Functional(0.5 * inner(alpha(psi) * u3, u3) * dx('everywhere')
               + MU * inner(grad(u3), grad(u3)) * dx('everywhere')) \
               + (BETA*fluid(psi))*dx(1)
adj_html("forward.html", "forward")

for (adjoint, var) in compute_adjoint(J, forget=False):
    print var
    # from IPython import embed; embed()
    break

plot(adjoint, title='adjoint from dolfin')




#
w_a = Function(W)
(u_a, p_a) = split(w_a)
adj_bcs = DirichletBC(W.sub(0), (0.0, 0.0), "on_boundary")
#
# # Adjoint:
a_3 = a_adjoint(u3, u, p, v, q, MU, alpha(psi)) * dx('everywhere')
L_3 = 2 * (alpha(psi)*inner(u3, v) + MU*inner(grad(u3), grad(v))) * dx('everywhere')
problem_3 = LinearVariationalProblem(a_3, L_3, w_a, adj_bcs)
solver_3 = LinearVariationalSolver(problem_3)
solver_3.solve()

plot(u_a, title='u_adjoint analytical')

# dolfin-adjoint



# Gradient-based method ############################

kappa = 1.0
theta = 10.0

F_ex = 0.5*(MU*inner(grad(u1), grad(u1))+alpha(psi)*inner(u1, u1))*dx('everywhere') + \
        (BETA*fluid(psi))*dx(1)
F = assemble(F_ex)
n = 0
n_max = 40


while theta > tol_theta and n < n_max:

    n = n+1
    print "\nIteration #" + str(n)

    kappa = min(1.0, kappa * 1.5)

    # Calculate theta - evaluate on Omega_1:
    k = - (ALPHA_U - ALPHA_L) * (inner(u3, u3) - inner(u3, adjoint)) + BETA
    g = project(k, V0)
    dot_prod = assemble(dot(g, psi) * dx(1))
    nrm_g = sqrt(assemble(dot(g, g) * dx(1)))
    nrm_psi = sqrt(assemble(dot(psi, psi) * dx(1)))

    theta_inv = min(1.0, dot_prod / (nrm_g * nrm_psi))
    if theta_inv is 1.0:
        theta = 0.0
        print "Theta = " + str(theta)
        plot(sign(psi), key='psi')
        break

    theta = math.acos(theta_inv)
    print "Theta = " + str(theta)


    # Update psi:
    k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
    k2 = sin(kappa * theta) / (sin(theta) * nrm_g)
    update_psi(psi, g, k1, k2)

    # Reset the vein part to be only fluid:
    psi = project(Expression("psi*(x[1]>=0.2) - 1*(x[1] < 0.2)", psi=psi, degree=1), V0)
    plot(alpha(psi), key='alpha')
    print "F = " + str(F)

    # # Solve system
    # # Stokes to make initial guess:
    # a_1 = a_stokes(u, p, v, q, MU, alpha(psi)) * dx('everywhere')
    # L_1 = inner(f, v) * dx('everywhere')
    # problem_1 = LinearVariationalProblem(a_1, L_1, w, bcs)
    # solver_1 = LinearVariationalSolver(problem_1)
    # solver_1.solve()
    #
    # # Newton:
    # J_N = a_stokes(u, p, v, q, MU, alpha(psi)) * dx('everywhere') \
    #     + inner(dot(grad(u), u_), v) * dx('everywhere') \
    #     + inner(dot(grad(u_), u), v) * dx('everywhere')
    #
    # F_N = a_stokes(u_, p_, v, q, MU, alpha(psi)) * dx('everywhere') \
    #     + inner(dot(grad(u_), u_), v) * dx('everywhere') \
    #     - inner(f, v) * dx('everywhere')
    #
    # problem_N = NonlinearVariationalProblem(F_N, w, bcs, J_N)
    # solver_N = NonlinearVariationalSolver(problem_N)
    #
    # prm = solver_N.parameters
    # prm['newton_solver']['absolute_tolerance'] = 1E-8
    # prm['newton_solver']['maximum_iterations'] = 25
    # prm['newton_solver']['relaxation_parameter'] = 1.0
    # solver_N.solve()
    #
    # # Adjoint:
    # a_3 = a_adjoint(u_, u, p, v, q, MU, alpha(psi)) * dx('everywhere')
    # L_3 = 2 * (alpha(psi) * inner(u_, v) + MU * inner(grad(u_), grad(v))) * dx('everywhere')
    # problem_3 = LinearVariationalProblem(a_3, L_3, w_a, adj_bcs)
    # solver_3 = LinearVariationalSolver(problem_3)
    # solver_3.solve()


    # F_ex = 0.5 * (MU * inner(grad(u_), grad(u_)) + alpha(psi) * inner(u_, u_)) * dx('everywhere') \
    #     + (BETA * fluid(psi)) * dx(1)
    # F_new = assemble(F_ex)

    if F_new >= F:
        print "Did not find descent direction, exiting."
        break

    F = F_new



J_ex = 0.5 * (MU * inner(grad(u3), grad(u3)) + alpha(psi) * inner(u3, u3)) * dx('everywhere')
print "J = " + str(assemble(J_ex))

interactive()
