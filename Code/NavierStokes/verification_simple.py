from dolfin import *
import numpy
import matplotlib.pyplot as plt

# Verification of solution for Navier-Stokes-Darcy equation in 2D

# Constants
dt = 0.01
T = 0.1
nu = Constant(0.1)
alpha = Constant(0.0)


u_expr = Expression(("-cos(pi*x[0])*sin(pi*x[1])*exp(-t)",
                     "sin(pi*x[0])*cos(pi*x[1])*exp(-t)"),
                    pi=DOLFIN_PI, t=0.0, degree=3)

p_expr = Expression("sin(pi*x[0])*cos(pi*x[1])", pi=DOLFIN_PI, degree=3)

f = Expression(("-alpha*exp(-t)*sin(pi*x[1])*cos(pi*x[0]) - 2*nu*pi*pi*exp(-t)*sin(pi*x[1])*cos(pi*x[0]) + \
                pi*cos(pi*x[0])*cos(pi*x[1]) - pi*exp(-2*t)*sin(pi*x[0])*sin(pi*x[1])*sin(pi*x[1])*cos(pi*x[0]) \
                - pi*exp(-2*t)*sin(pi*x[0])*cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[1]) + exp(-t)*sin(pi*x[1])*cos(pi*x[0])",
                "alpha*exp(-t)*sin(pi*x[0])*cos(pi*x[1]) + 2*nu*pi*pi*exp(-t)*sin(pi*x[0])*cos(pi*x[1]) \
                - pi*sin(pi*x[0])*sin(pi*x[1]) - pi*exp(-2*t)*sin(pi*x[0])*sin(pi*x[0])*sin(pi*x[1])*cos(pi*x[1])  \
                - pi*exp(-2*t)*sin(pi*x[1])*cos(pi*x[0])*cos(pi*x[0])*cos(pi*x[1]) - exp(-t)*sin(pi*x[0])*cos(pi*x[1])"),
               alpha=alpha, nu=nu, pi=DOLFIN_PI, t=0.0, degree=3)

# x = [1, 2]
# t = 0.0
# hh = -alpha*exp(-t)*sin(pi*x[1])*cos(pi*x[0]) - 2*nu*pi*pi*exp(-t)*sin(pi*x[1])*cos(pi*x[0]) + \
#     pi*cos(pi*x[0])*cos(pi*x[1]) - pi*exp(-2*t)*sin(pi*x[0])*sin(pi*x[1])*sin(pi*x[1])*cos(pi*x[0]) \
#     - pi*exp(-2*t)*sin(pi*x[0])*cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[1]) + exp(-t)*sin(pi*x[1])*cos(pi*x[0])
#
# hr = alpha*exp(-t)*sin(pi*x[0])*cos(pi*x[1]) + 2*nu*pi*pi*exp(-t)*sin(pi*x[0])*cos(pi*x[1]) \
#     - pi*sin(pi*x[0])*sin(pi*x[1]) - pi*exp(-2*t)*sin(pi*x[0])*sin(pi*x[0])*sin(pi*x[1])*cos(pi*x[1]) \
#     - pi*exp(-2*t)*sin(pi*x[1])*cos(pi*x[0])*cos(pi*x[0])*cos(pi*x[1]) - exp(-t)*sin(pi*x[0])*cos(pi*x[1])




err_values = []
err_J = []
h = []
h2 = []


for N in [20]:

    t = 0.0

    print N
    h.append(1./N)
    h2.append(1./(N*N))

    mesh = RectangleMesh(Point(0, 0), Point(1, 1), N, N)
    n = FacetNormal(mesh)

    # Define function spaces (P2-P1)
    V = VectorFunctionSpace(mesh, "CG", 2)
    Q = FunctionSpace(mesh, "CG", 1)

    # Define trial and test functions
    u = TrialFunction(V)
    p = TrialFunction(Q)
    v = TestFunction(V)
    q = TestFunction(Q)

    # Initial velocity and pressure
    u_initial = Expression(("-cos(pi*x[0])*sin(pi*x[1])", "sin(pi*x[0])*cos(pi*x[1])"), pi=DOLFIN_PI, degree=3)
    p_initial = Expression("sin(pi*x[0])*cos(pi*x[1])", pi=DOLFIN_PI, degree=3)
    u0 = interpolate(u_initial, V)
    p0 = interpolate(p_initial, Q)

    # Boundaray conditions
    bcu = [DirichletBC(V, u_expr, "on_boundary")]
    bcp = [DirichletBC(Q, p_expr, "on_boundary")]

    # Functions
    u_err = Function(V)
    u1 = Function(V)
    p1 = Function(Q)
    k = Constant(dt)

    # IPCS
    # # Tentative velocity step01
    # U = 0.5 * (u0 + u)
    # F1 = (1./k)*inner(u - u0, v)*dx + inner(grad(u0)*u0, v)*dx + \
    #     nu*inner(grad(u), grad(v))*dx + inner(grad(p0), v)*dx + \
    #     alpha*inner(u, v)*dx - inner(f, v)*dx \
    #     - nu * inner(grad(U).T * n, v) * ds
    # a1 = lhs(F1)
    # L1 = rhs(F1)
    #
    # # Pressure update
    # a2 = inner(grad(p), grad(q))*dx
    # L2 = inner(grad(p0), grad(q))*dx - ((1./k) + alpha)*div(u1)*q*dx
    #
    # # Velocity update
    # a3 = (1./k + alpha)*inner(u, v)*dx
    # L3 = (1./k + alpha)*inner(u1, v)*dx - inner(grad(p1-p0), v)*dx


    # # CHORIN
    # Tentative velocity
    F1 = (1./k)*inner(u - u0, v)*dx + inner(dot(u0, grad(u0)), v)*dx \
        + nu*inner(grad(u), grad(v))*dx + alpha*inner(u, v)*dx \
        - inner(f, v)*dx
    a1 = lhs(F1)
    L1 = rhs(F1)

    # Pressure update
    a2 = inner(grad(p), grad(q))*dx
    L2 = -(1./k)*div(u1)*q*dx - alpha*div(u1)*q*dx

    # Velocity update
    a3 = (1./k)*inner(u, v)*dx + alpha*inner(u, v)*dx
    L3 = (1./k)*inner(u1, v)*dx - inner(grad(p1), v)*dx + alpha*inner(u1, v)*dx

    # Assemble system
    A1 = assemble(a1)
    A2 = assemble(a2)
    A3 = assemble(a3)

    b1 = None
    b2 = None
    b3 = None

    t = dt

    while t < T:

        # Update expressions wrt t
        f.t = t
        u_expr.t = t
        # bcu = [DirichletBC(V, u_expr, "on_boundary")]

        # Tentative velocity
        b1 = assemble(L1, tensor=b1)
        [bc.apply(A1, b1) for bc in bcu]
        solve(A1, u1.vector(), b1, "minres", "sor")

        # Pressure correction
        b2 = assemble(L2, tensor=b2)
        [bc.apply(A2, b2) for bc in bcp]
        # if not bcp: normalize(b2)
        solve(A2, p1.vector(), b2, "gmres", "hypre_amg")
        # if not bcp: normalize(p1.vector())

        # Velocity update
        b3 = assemble(L3, tensor=b3)
        [bc.apply(A3, b3) for bc in bcu]
        solve(A3, u1.vector(), b3, "minres", "sor")

        u0.assign(u1)
        p0.assign(p1)

        # Compare to exact solution
        # u_exact = interpolate(u_expr, V)
        # u_err.vector()[:] = u0.vector().array() - u_exact.vector().array()
        # plot(u_exact, title="u_exact", key="ue")
        # print errornorm(u0, u_exact, degree_rise=2)

        t += dt

    t -= dt

    # Exact solutions :
    u_expr.t = t
    u_exact = interpolate(u_expr, V)
    p_exact = interpolate(p_expr, Q)

    plot(u_exact, title="Exact u", key="u_e")
    plot(u0, title="Numerical u", key="u")
    # plot(p_exact, title="Exact p")
    # plot(p0, title="Pressure", key="p")



    u_err.vector()[:] = u0.vector().array() - u_exact.vector().array()
    plot(u_err, title="Error u", key="eu")
    # err_values.append(sqrt(assemble(inner(u_err, u_err) * dx)))
    err_values.append(errornorm(u0, u_exact, degree_rise=3))


print err_values
# plt.loglog(h, err_values, 'r.-', h, h, 'b', h, h2, 'g')
# plt.title("L2-norm u")
# plt.show()
interactive()




