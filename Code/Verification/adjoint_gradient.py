from dolfin import *
from dolfin_adjoint import *
import numpy as np
import matplotlib.pyplot as plt


class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 47./42 + 5./42) and between(x[1], (0.3333, 0.6667))


class Design(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[0], (5./42, 47./42))


def alpha(psi, name="alpha", q=0.1):
    return ALPHA_L + (ALPHA_U - ALPHA_L) * (1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q)))

def fluid_domain(psi, q=0.1):
    return 1.0 - 1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q))

# Constants
nu = Constant(1.0, name="nu")
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)
timestep = Constant(0.0001)
T = 0.0002
PSI = "-1 + 2*(x[0]>1.0 + 5./42)*((x[1]>0.6667) + (x[1]<0.3333))"
nx = 52
ny = 42
beta = 8000.0

set_log_level(40)

# Mesh
# mesh = UnitSquareMesh(nx, ny)
mesh = RectangleMesh(Point(0, 0), Point(47./42 + 5./42, 1), nx, ny, "right/left")


# Design parameter
V0 = FunctionSpace(mesh, "CG", 1)
psi = project(Expression(PSI, degree=2, t=0.0), V0, name="psi")
# plot(psi)

# Function spaces
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)


# Mark boundaries
noslip = Noslip()
inlet = Inlet()
outlet = Outlet()

boundaries = MeshFunction("size_t", mesh, 1)
boundaries.set_all(4)
noslip.mark(boundaries, 0)
inlet.mark(boundaries, 1)
outlet.mark(boundaries, 2)
ds = Measure("ds")(subdomain_data=boundaries)
n = FacetNormal(mesh)

# Mark subdomain
design = Design()
domains = CellFunction("size_t", mesh)
domains.set_all(0)
design.mark(domains, 1)


# Boundary conditions
u_1 = Expression(("U * (1 - (2*(x[1]-0.5)) * (2*(x[1]-0.5)) )", "0"),
                 U=1.0, degree=3)
bcu = [DirichletBC(V, (0.0, 0.0), boundaries, 0),
       DirichletBC(V, u_1, boundaries, 1)]
bcp = [DirichletBC(Q, 0.0, boundaries, 2)]


# Initial velocity and pressure
u_ic = project(Expression(("0.0", "0.0"), degree=3),  V, name="u_ic")
p_ic = project(Expression("0.0", degree=1), Q, name="p_ic")
u0 = u_ic.copy(deepcopy=True)
p0 = p_ic.copy(deepcopy=True)

# Declare velocity functions
u = Function(V, name="u", annotate=False)
v = TestFunction(V)

# Weak form for tentative velocity
F1 = (1./timestep) * inner(u - u0, v) * dx \
    + inner(grad(u) * u0, v) * dx \
    + nu * inner(grad(u), grad(v)) * dx \
    + alpha(psi) * inner(u, v) * dx \
    + inner(grad(p0), v) * dx \
    - nu * inner(dot(n, nabla_grad(u)), v) * ds(2) \
    + inner(p0 * n, v) * ds(2)


# Declare pressure functions (after tentative velocity weak form!!)
p = Function(Q, name="p", annotate=False)
q = TestFunction(Q)

# Pressure correction weak form
F2 = inner(grad(p - p0), grad(q)) * dx \
     + (1. / timestep) * div(u0) * q * dx


# Velocity update weak form
F3 = (1./timestep)*inner(u - u0, v) * dx \
     + inner(grad(p - p0), v) * dx


# Start adjoint time step
t = 0.0
adj_time = False
annotate = False
u_list = []

psi_tmp = psi.copy(deepcopy=True)
count = 0
while t < T:
    print count
    count += 1

    adj_html("forward.html", "forward")
    adj_html("adjoint.html", "adjoint")

    if t > 0.0 and not adj_time:
        adj_time = True
        annotate = True
        adj_start_timestep()

    # u and p work as trial functions
    # u0 and p0 work as newest value

    solve(F1 == 0, u, bcu)
    u0.assign(u, annotate=annotate)
    solve(F2 == 0, p, bcp)
    solve(F3 == 0, u, bcu)

    psi.assign(psi_tmp, annotate=annotate)
    u0.assign(u, annotate=annotate)
    p0.assign(p, annotate=annotate)

    u_list.append(u0)

    # plot(u0, key="u0")

    t += float(timestep)
    if adj_time:
        adj_inc_timestep(t, t > T)

# plot(u0, title="Velocity at endtime")



dx = Measure("dx")(subdomain_data=domains)

# Calculate dolfin-adjoint gradient
J = Functional((alpha(psi)*inner(u0, u0) + nu*inner(grad(u0), grad(u0)))*dx*dt[FINISH_TIME])
                # + 0.01*fluid_domain(psi)*dx*dt)

m = Control(psi)
Jm = ReducedFunctional(J, m)

d = Jm.derivative(forget=False)[0]
plot(-d, title="derivative")
plot(-sign(d), title="sign(d)")


# import pdb
# pdb.set_trace()
print "Compute gradient"
g_1 = compute_gradient(J, m)

g_diff = Function(V0)
g_diff.assign(g_1 - d)

g_diff_sign = Function(V0)
# g_diff_sign.assign(sign(g_1) - sign(d))

plot(g_diff, title="g_diff")
plot(g_diff_sign, title="g_diff_sign")

nrm_g = sqrt(assemble(dot(g_1, g_1) * dx(1)))
print "norm(g_1) = " + str(nrm_g)
g = Function(V0)
g.assign(-1.0*g_1/nrm_g)

psi_da = project(Expression("psi*(x[0]>=5./42)*(x[0]<47./42) - 1*(x[0] < 5./42) + 1*(x[0] > 47./42) - 2*(x[0] > 47./42)*(x[1]<0.67)*(x[1]>0.33)", psi=g, degree=1), V0)

# plot(psi_da, title="Dolfin-adjoint gradient")
plot(sign(psi_da), title="Sign(g_1)")

print "Jm = " + str(Jm(psi))

print "Calculating functional"

u0 = u_list[-1]
J_ = assemble((alpha(psi)*inner(u0, u0) + nu*inner(grad(u0), grad(u0)))*dx)
print "J_ = " + str(J_)


# Calculate topological gradient
annotate = False

g_2 = Function(V0)

# Adjoint
W = V*Q
(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)
w = Function(W, annotate=annotate)
(ua, pa) = split(w)

assigner = FunctionAssigner(V, W.sub(0)) # to assign w.sub(0) to u_a

# Boundary conditions
adjoint_boundaries = MeshFunction("size_t", mesh, 1)
adjoint_boundaries.set_all(4)
noslip.mark(adjoint_boundaries, 0)
outlet.mark(adjoint_boundaries, 3)
bcu_a = [DirichletBC(V, (0.0, 0.0), adjoint_boundaries, 0)]

t = T
# First element in u_list will not be evaluated
N = 1 # len(u_list) - 1

# Reverse list to compute adjoint
u_list = u_list[::-1]

for it in range(N):
    print it + 1
    u0 = u_list[it]

    F_a = (1. / timestep) * inner(u - ua, v) * dx \
        - inner(dot(grad(u), u0), v) * dx \
        - inner(dot(u0, grad(u)), v) * dx \
        + nu * inner(grad(u), grad(v)) * dx \
        + alpha(psi) * inner(u, v) * dx \
        - div(u) * q * dx - div(v) * p * dx \
        - 2 * nu * inner(grad(u0), grad(v)) * dx \
        - 2 * alpha(psi) * inner(u0, v) * dx \
        - inner(p * n, v) * ds(3) \
        + nu * inner(dot(n, grad(u)), v) * ds(3) \
        + inner(dot(u0, u) * n, v) * ds(3) \
        + inner(dot(u0, n) * u, v) * ds(3)
    a = lhs(F_a)
    L = rhs(F_a)

    problem = LinearVariationalProblem(a, L, w, bcu_a)
    solver = LinearVariationalSolver(problem)
    solver.solve(annotate=annotate)
    (ua, pa) = split(w)

    u_a = Function(V, annotate=annotate)  # to solve the adjoints
    assigner.assign(u_a, w.sub(0))

    direction = - (ALPHA_U - ALPHA_L) * inner(u0, u0 - u_a)
    g_temp = project(direction + beta, V0)
    g_2.assign(g_2 + 1./N*g_temp)



nrm_g = sqrt(assemble(dot(g_2, g_2) * dx(1)))
print "norm(g_2) = " + str(nrm_g)
g.assign(g_2/nrm_g)

psi_td = project(Expression("psi*(x[0]>=5./42)*(x[0]<47./42) - 1*(x[0] < 5./42) + 1*(x[0] > 47./42) - 2*(x[0] > 47./42)*(x[1]<0.67)*(x[1]>0.33)", psi=g, degree=1), V0)


print "Calculation functional"
J_2 = 0.0
for it in range(N):
    u0 = u_list[it]
    J_2 += assemble((alpha(psi)*inner(u0, u0) + nu*inner(grad(u0), grad(u0)))*dx)
print "J_2 = " + str(J_2)

plot(sign(psi_td), title="Sign(g_2)")

print "Fluid(psi_da): " + str(assemble(conditional(psi_da > 0, 0, 1)*dx(1)))
print "Fluid(psi_td): " + str(assemble(conditional(psi_td > 0, 0, 1)*dx(1)))

diff = Function(V0)
diff.assign(psi_da - psi_td)
# plot(diff, title="Difference in gradients")
print "norm(psi_da - psi_td) = " + str(norm(diff))


a_1 = ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi_td, 0), 0, 1)
a_2 = ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi_da, 0), 0, 1)
# a = Function(V0)
# a.assign(a_2 - a_1)
plot(a_2 - a_1, title="Difference in alpha")

interactive()





# Plot alpha(x) for x in [-1, 1]:

# N = 30
# x1 = np.linspace(-1, 1, N, endpoint=True)
# a = []
# for x in x1:
#     a.append(fluid_domain(x))
#
# plt.plot(x1, a)
# plt.show()