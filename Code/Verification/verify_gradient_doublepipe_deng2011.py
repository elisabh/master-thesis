
from dolfin import *
# import dolfin_adjoint as da
from dolfin_adjoint import *
import time
import numpy as np
import matplotlib.pyplot as plt


class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet1(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0) and between(x[1], (0.6667, 0.8333))


class Inlet2(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0) and between(x[1], (0.1667, 0.3333))


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.0) and \
               (between(x[1], (0.6667, 0.8333)) or between(x[1], (0.1667, 0.3333)))


def alpha(psi, q=0.1):
    # return ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi, 0), 0, 1)
    return ALPHA_L + (ALPHA_U - ALPHA_L)*(1.0/(1.0 + 2.718281828459045**((-psi)/q)))


def fluid_domain(psi, q=0.01):
    # return conditional(psi > 0, 0, 1)
    return 1.0 - 1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q))

class Problem:

    def __init__(self):
        self.N = 40
        self.T = 0.003 #2*3.1415/100
        self.dt = 0.001 #0.00002 #self.T/self.N_t
        self.N_t = self.T/self.dt # Time steps
        self.mesh = None
        self.boundaries = None
        self.adjoint_boundaries = None

        self.make_mesh()
        self.mark_boundaries()
        self.mark_adjoint_boundaries()

    def make_mesh(self):
        self.mesh = RectangleMesh(Point(0, 0), Point(1, 1), self.N, self.N, "right/left")

        # plot(self.mesh, title="Double pipe mesh", interactive=True)

    def mark_boundaries(self):

        # Mark boundaries
        noslip = Noslip()
        inlet_1 = Inlet1()
        inlet_2 = Inlet2()
        outlet = Outlet()

        self.boundaries = MeshFunction("size_t", self.mesh, 1)
        self.boundaries.set_all(4)
        noslip.mark(self.boundaries, 0)
        inlet_1.mark(self.boundaries, 1)
        inlet_2.mark(self.boundaries, 2)
        outlet.mark(self.boundaries, 3)


    def boundary_conditions(self, V, Q, t):
        u_1 = Expression(("-U * (x[1]-4./6.)*(x[1]-5./6.) * (x[1]>0.6667)*(x[1]<0.8333) * cos(t)",
                          "0"), U=144.0, t=t, degree=3)
        u_2 = Expression(("-U * (x[1]-1./6.)*(x[1]-2./6.) * (x[1]>0.1667)*(x[1]<0.3333) * sin(t)",
                          "0"), U=144.0, t=t, degree=3)

        bcu = [DirichletBC(V, (0.0, 0.0), self.boundaries, 0),
               DirichletBC(V, u_1, self.boundaries, 1),
               DirichletBC(V, u_2, self.boundaries, 2)]
        bcp = [DirichletBC(Q, 0.0, self.boundaries, 3)]

        return bcu, bcp

    def mark_adjoint_boundaries(self):

        noslip = Noslip()
        outlet = Outlet()

        self.adjoint_boundaries = MeshFunction("size_t", self.mesh, 1)
        self.adjoint_boundaries.set_all(4)
        noslip.mark(self.adjoint_boundaries, 0)
        outlet.mark(self.adjoint_boundaries, 3)

    def adjoint_boundary_conditions(self, V):
        return [DirichletBC(V, (0.0, 0.0), self.adjoint_boundaries, 0)]


class NSDSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries
        self.T = problem.T
        self.dt = problem.dt

    def solve_IPCS(self, psi, beta):

        # Reset dolfin-adjoint to re-run time scheme
        adj_reset()
        annotate = False

        timestep = Constant(self.dt)

        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        # Initial velocity and pressure
        u_ic = project(Expression(("0.0", "0.0"), degree=2), V, name="u_ic")
        p_ic = project(Expression("0.0", degree=1), Q, name="p_ic")
        u0 = u_ic.copy(deepcopy=True)
        p0 = p_ic.copy(deepcopy=True)

        # Declare velocity functions
        u = Function(V, name="u", annotate=annotate)
        v = TestFunction(V)

        # Weak form for tentative velocity
        F1 = (1. / timestep) * inner(u - u0, v) * dx \
            + inner(grad(u) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + alpha(psi) * inner(u, v) * dx \
            + inner(grad(p0), v) * dx \
            - nu * inner(dot(n, nabla_grad(u)), v) * ds(3) \
            + inner(p0 * n, v) * ds(3)

        # Declare pressure functions (after tentative velocity weak form!!)
        p = Function(Q, name="p", annotate=annotate)
        q = TestFunction(Q)

        # Pressure correction weak form
        F2 = inner(grad(p - p0), grad(q)) * dx \
            + (1. / timestep) * div(u0) * q * dx

        # Velocity update weak form
        F3 = (1. / timestep) * inner(u - u0, v) * dx \
            + inner(grad(p - p0), v) * dx

        t = 0.0
        adj_time = False

        psi_tmp = psi.copy(deepcopy=True)
        # in case we are missing one psi:
        # psi.assign(psi_tmp, annotate=True)

        while t < self.T:

            adj_html("forward.html", "forward")
            adj_html("adjoint.html", "adjoint")

            # one initial run without annotation for initial condition
            if t > 0.0 and not adj_time:
                adj_time = True
                annotate = True
                adj_start_timestep()

            bcu, bcp = problem.boundary_conditions(V, Q, t)

            # u and p work as trial functions
            # u0 and p0 work as newest value

            solve(F1 == 0, u, bcu)
            u0.assign(u, annotate=annotate)
            solve(F2 == 0, p, bcp)
            solve(F3 == 0, u, bcu)

            psi.assign(psi_tmp, annotate=annotate)
            u0.assign(u, annotate=annotate)
            p0.assign(p, annotate=annotate)

            # plot(u0, title="u0", key="u0")

            t += float(timestep)
            if adj_time:
                adj_inc_timestep(t, t > self.T)

        J = Functional((alpha(psi)*inner(u0, u0) + nu*inner(grad(u0), grad(u0)))*dx*dt
                       + beta*fluid_domain(psi)*dx*dt[FINISH_TIME])




        m = Control(psi)
        Jm = ReducedFunctional(J, m)

        g = compute_gradient(J, m)

        # ascending gradient, so need a minus sign
        return -(ALPHA_U - ALPHA_L)*g, Jm(psi)

# class AdjointSolver:
#
#     def __init__(self, problem):
#         self.mesh = problem.mesh
#         self.boundaries = problem.boundaries
#         self.T = problem.T
#         self.dt = problem.dt
#
#     def solve_coupled(self, psi, u_list, annotate=False):
#
#         # Adjoint coupled:
#         V = VectorFunctionSpace(self.mesh, "CG", 2)
#         Q = FunctionSpace(self.mesh, "CG", 1)
#         W = V*Q
#
#         ds = Measure("ds")(subdomain_data=self.boundaries)
#         n = FacetNormal(self.mesh)
#
#         (u, p) = TrialFunctions(W, annotate=annotate)
#         (v, q) = TestFunctions(W, annotate=annotate)
#         w = Function(W, annotate=annotate)
#         (ua, pa) = split(w, annotate=annotate)
#
#         assigner = FunctionAssigner(V, W.sub(0)) # to assign w.sub(0) to u_a
#         ua_list = []
#
#         bcu = problem.adjoint_boundary_conditions(V)
#
#         k = Constant(self.dt)
#         t = self.T
#
#         for u0 in reversed(u_list):
#             t -= self.dt
#
#             # Not sure if this explicitly is necessary
#             F_a = (1. / k) * inner(u - ua, v) * dx \
#                   - inner(dot(grad(u), u0), v) * dx \
#                   - inner(dot(u0, grad(u)), v) * dx \
#                   + nu * inner(grad(u), grad(v)) * dx \
#                   + alpha(psi) * inner(u, v) * dx \
#                   - div(u) * q * dx - div(v) * p * dx \
#                   - 2 * nu * inner(grad(u0), grad(v)) * dx \
#                   - 2 * alpha(psi) * inner(u0, v) * dx \
#                   - inner(p * n, v) * ds(3) \
#                   + nu * inner(dot(n, grad(u)), v) * ds(3) \
#                   + inner(dot(u0, u) * n, v) * ds(3) \
#                   + inner(dot(u0, n) * u, v) * ds(3)
#             a = lhs(F_a)
#             L = rhs(F_a)
#
#             adj_problem = LinearVariationalProblem(a, L, w, bcu)
#             solver = LinearVariationalSolver(adj_problem)
#             solver.solve()
#             (ua, pa) = split(w, annotate=annotate)
#             # plot(ua, title="Adjoint", key="a")
#
#             u_a = Function(V)  # to solve the adjoints
#             assigner.assign(u_a, w.sub(0))
#             ua_list.append(u_a)
#
#         # return reversed ua_list, such that it has the same ordering as u_list
#         return ua_list[::-1]


class GradientMethod:

    def __init__(self, problem):
        self.problem = problem
        self.V0 = FunctionSpace(problem.mesh, "CG", 1)
        self.PSI = "-1"
        # self.beta = 3.16
        self.beta = 0.01
        self.n_max = 12
        self.tol_theta = 0.01
        self.tol_kappa = 0.1

    def angle(self, g, psi):
        dot_prod = assemble(dot(g, psi) * dx)
        nrm_g = sqrt(assemble(dot(g, g) * dx))
        nrm_psi = sqrt(assemble(dot(psi, psi) * dx))
        if nrm_g*nrm_psi == 0:
            return 1.0
        return acos(dot_prod / (nrm_g * nrm_psi))

    def new_psi(self, kappa, theta, psi_ref, g):

        nrm_g = sqrt(assemble(dot(g, g) * dx))
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
        k2 = sin(kappa * theta) / (sin(theta) * nrm_g)
        psi = project(k1*psi_ref + k2*g, self.V0, annotate=False)
        # psi.vector().zero()
        # psi.assign(k1*psi_ref + k2*g)
        # plot(psi, title="PSI", interactiove=True)
        return psi

    def run_gradient_method(self):


        kappa = 0.5
        # Design parameter
        psi_ref = project(Expression(self.PSI, degree=1), self.V0)
        nrm_psi = sqrt(assemble(dot(psi_ref, psi_ref) * dx))
        psi = project(Expression(self.PSI) / nrm_psi, self.V0, annotate=False)

        # Navier-Stokes-Darcy solver
        NSD = NSDSolver(self.problem)

        J_list = []

        for n in range(self.n_max):
            print "Iteration #: " + str(n+1)

            # Compute gradient using dolfin-adjoint:
            g, J = NSD.solve_IPCS(psi, self.beta)
            J_list.append(J)

            # Calculate and store fluid domain and theta
            fluid = assemble(fluid_domain(psi)*dx)
            print "Fluid = " + str(fluid)
            theta = self.angle(g, psi)
            print "Theta = " + str(theta)

            if theta < self.tol_theta:
                print "Theta smaller that tolerance!"
                break

            # Update step length
            kappa = min(1.0, kappa*1.5)

            # Store old psi and calculate new
            psi_ref.assign(psi)
            psi = self.new_psi(kappa, theta, psi_ref, g)

            # if plot_psi:
            #     plot(g, title="gradient", key="g")
            #     plot(psi, title="Sign(psi)", key="psi")
            #     plot(alpha(psi), title="alpha", key="alpha")

        print J_list
        plot(alpha(psi), title="alpha", key="alpha")

        plt.plot(J_list)
        plt.show()


nu = Constant(0.01)
ALPHA_U = 1.0 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)

plot_u = False
plot_psi = True
write_u = False
write_psi = False

set_log_level(40)

# make problem
problem = Problem()
method = GradientMethod(problem)
# run gradient_method
method.run_gradient_method()


if plot_u or plot_psi:
    interactive()
