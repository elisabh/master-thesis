# Plot alpha(x) for x in [-1, 1]:

import numpy as np
import matplotlib.pyplot as plt

ALPHA_U = 2.5/ (0.01 * 0.01)
ALPHA_L = 2.5 / (100 * 100)

def alpha(psi, q):
    return ALPHA_L + (ALPHA_U - ALPHA_L) * (1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q)))

N = 100
x1 = np.linspace(-2, 2, N, endpoint=True)
plt.plot(x1, alpha(x1, 0.0), "b.", label="q=0.0", linewidth=2)

plt.plot(x1, alpha(x1, 0.1), "r--", label="q=0.1", linewidth=2)
plt.plot(x1, alpha(x1, 1.0), "g-.", label="q=1.0", linewidth=2)

# plt.plot(x1, a3, label="q=0.01")

plt.legend(loc=2)
plt.axis([-2, 2, -1000, 26000])
plt.show()