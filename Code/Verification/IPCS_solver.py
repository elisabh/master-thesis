from dolfin import *
import numpy

set_log_active(False)

class PeriodicDomain(SubDomain):
    def inside(self, x, on_boundary):
        # return True if on left or bottom boundary AND NOT on one of the two corners (0, 2) and (2, 0)
        return bool((near(x[0], 0) or near(x[1], 0)) and
                    (not ((near(x[0], 0) and near(x[1], 2)) or
                          (near(x[0], 2) and near(x[1], 0)))) and on_boundary)

    def map(self, x, y):
        if near(x[0], 2) and near(x[1], 2):
            y[0] = x[0] - 2.0
            y[1] = x[1] - 2.0
        elif near(x[0], 2):
            y[0] = x[0] - 2.0
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1] - 2.0


N = 40
mesh = RectangleMesh(Point(0, 0), Point(2, 2), N, N)
u_initial = Expression(("-sin(pi*x[1])*cos(pi*x[0])", "sin(pi*x[0])*cos(pi*x[1])"), degree=3)
p_initial = Expression("-(cos(2*pi*x[0])+cos(2*pi*x[1]))/4.", degree=3)

T = 0.5
dt = 0.001
nu = 0.01

# IPCS solver:

# Define function spaces (P2-P1)
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)

# Define trial and test functions
u = TrialFunction(V)
p = TrialFunction(Q)
v = TestFunction(V)
q = TestFunction(Q)
n = FacetNormal(mesh)

# Initial velocity and pressure
u0 = interpolate(u_initial, V)
p0 = interpolate(p_initial, Q)

# Functions
u1 = Function(V)
p1 = Function(Q)
k = Constant(dt)

# Tentative velocity step
F1 = 1./k*inner(u - u0, v)*dx + inner(grad(u0) * u0, v)*dx + \
     nu*inner(grad(u), grad(v))*dx + inner(grad(p0), v)*dx
a1 = lhs(F1)
L1 = rhs(F1)

# Pressure update
a2 = inner(grad(p), grad(q))*dx
L2 = inner(grad(p0), grad(q))*dx - (1./k)*div(u1)*q*dx

# Velocity update
a3 = inner(u, v)*dx
L3 = inner(u1, v)*dx - k*inner(grad(p1 - p0), v)*dx

# Assemble system
A1 = assemble(a1)
A2 = assemble(a2)
A3 = assemble(a3)

b1 = None
b2 = None
b3 = None

t = 0

while t < T:
    t += dt

    # Tentative velocity
    b1 = assemble(L1)
    solve(A1, u1.vector(), b1, "minres", "sor")

    # Pressure correction
    b2 = assemble(L2)
    normalize(b2)
    solve(A2, p1.vector(), b2, "gmres", "hypre_amg")
    normalize(p1.vector())

    # Velocity update
    b3 = assemble(L3)
    solve(A3, u1.vector(), b3, "minres", "sor")

    u0.assign(u1)
    p0.assign(p1)

plot(p0, title="p0")
plot(u0, title="u0")
interactive()
