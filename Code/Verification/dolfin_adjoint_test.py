from dolfin import *
from dolfin_adjoint import *
import numpy as np
import matplotlib.pyplot as plt

ALPHA_U = 10000.0
ALPHA_L = 0.00001
q = 0.01

n = 30
mesh = UnitSquareMesh(n, n)


def alpha(psi, q=0.1):
    # return ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi, 0), 0, 1)
    return ALPHA_L + (ALPHA_U - ALPHA_L)*(1.0/(1.0 + 2.718281828459045**((-psi)/q)))


# Control function
V0 = FunctionSpace(mesh, "DG", 0)
psi = project(Expression("-1 + 2*(x[1]>0.5)", degree=2), V0, name="psi")

# Constants
timestep = Constant(0.001)
nu = Constant(0.0001)

# Function spaces
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)

# Boundary conditions
bcu = DirichletBC(V, (0.0, 0.0), "on_boundary")
bcp = DirichletBC(Q, 0.0, "on_boundary")

# Initial velocity and pressure
ic = project(Expression(("sin(2*pi*x[0])", "cos(2*pi*x[1])"), degree=2),  V)
u0 = ic.copy(deepcopy=True)
ip = project(Expression("0.0", degree=1), Q)
p0 = ip.copy(deepcopy=True)

# Declare velocity functions
u = Function(V)
v = TestFunction(V)

# Weak form for tentative velocity
F1 = (1./timestep) * inner(u - u0, v) * dx \
    + inner(grad(u) * u0, v) * dx \
    + nu * inner(grad(u), grad(v)) * dx \
    + alpha(psi) * inner(u, v) * dx \
    + inner(grad(p0), v) * dx

# Declare pressure functions (after tentative velocity weak form!!)
p = Function(Q)
q = TestFunction(Q)

# Pressure correction weak form
F2 = inner(grad(p - p0), grad(q)) * dx \
     + (1. / timestep) * div(u0) * q * dx

# Velocity update weak form
F3 = (1./timestep)*inner(u - u0, v) * dx \
     + inner(grad(p - p0), v) * dx

# Start and end criteria
t = 0.0
end = 0.003

# Start adjoint time step
psi_tmp = psi.copy(deepcopy=True)
adj_start_timestep()

while (t < end):

    # u and p work as trial functions
    # u0 and p0 work as newest value

    solve(F1 == 0, u, bcu)
    u0.assign(u)
    solve(F2 == 0, p, bcp)
    solve(F3 == 0, u, bcu)

    psi.assign(psi_tmp, annotate=True)
    u0.assign(u)
    p0.assign(p)

    t += float(timestep)
    adj_inc_timestep(t, t > end)

plot(u0, title="Velocity")
plot(p0, title="Pressure")

adj_html("forward.html", "forward")
adj_html("adjoint.html", "adjoint")



J = Functional(alpha(psi)*inner(u0, u0)*dx*dt[FINISH_TIME])
# J = Functional(inner(u0, u0)*dx*dt)
# J = Functional(p*ds*dt)
# dJdic = compute_gradient(J, Control(u0), forget=False)

dJdpsi = compute_gradient(J, Control(psi))
plot(dJdpsi, title="dJ/dpsi")

interactive()



# N = 30
# x1 = np.linspace(-1, 1, N, endpoint=True)
# a = []
# for x in x1:
#     a.append(alpha(x))
#
# plt.plot(x1, a)
# plt.show()