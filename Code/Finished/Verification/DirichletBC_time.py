from dolfin import *
import numpy
import matplotlib.pyplot as plt

# Verification of solution for Navier-Stokes-Darcy equation in 2D



nu = Constant(1.0)
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)
alpha = Constant(ALPHA_L)
t = 0.0

u_expr = Expression(("-cos(pi*x[0])*sin(pi*x[1])*exp(-t)",
                     "sin(pi*x[0])*cos(pi*x[1])*exp(-t)"),
                    pi=DOLFIN_PI, t=0.0, degree=3)

p_expr = Expression("sin(pi*x[0])*cos(pi*x[1])", pi=DOLFIN_PI, degree=3)

f = Expression(("-alpha*exp(-t)*sin(pi*x[1])*cos(pi*x[0]) - 2*nu*pi*pi*exp(-t)*sin(pi*x[1])*cos(pi*x[0]) + \
                pi*cos(pi*x[0])*cos(pi*x[1]) - pi*exp(-2*t)*sin(pi*x[0])*sin(pi*x[1])*sin(pi*x[1])*cos(pi*x[0]) \
                - pi*exp(-2*t)*sin(pi*x[0])*cos(pi*x[0])*cos(pi*x[1])*cos(pi*x[1]) + exp(-t)*sin(pi*x[1])*cos(pi*x[0])",
                "alpha*exp(-t)*sin(pi*x[0])*cos(pi*x[1]) + 2*nu*pi*pi*exp(-t)*sin(pi*x[0])*cos(pi*x[1]) \
                - pi*sin(pi*x[0])*sin(pi*x[1]) - pi*exp(-2*t)*sin(pi*x[0])*sin(pi*x[0])*sin(pi*x[1])*cos(pi*x[1])  \
                - pi*exp(-2*t)*sin(pi*x[1])*cos(pi*x[0])*cos(pi*x[0])*cos(pi*x[1]) - exp(-t)*sin(pi*x[0])*cos(pi*x[1])"),
               alpha=alpha, nu=nu, pi=DOLFIN_PI, t=0.0, degree=3)


err_values = []
h1 = []
N = 200
T = 0.000004
for dt in [0.000002, 0.000001, 0.0000005, 0.00000025]:
    t = 0.0

    print dt
    h1.append(dt)

    mesh = RectangleMesh(Point(0, 0), Point(1, 1), N, N)
    n = FacetNormal(mesh)

    # Define function spaces (P2-P1)
    V = VectorFunctionSpace(mesh, "CG", 2)
    Q = FunctionSpace(mesh, "CG", 1)

    # Define trial and test functions
    u = TrialFunction(V)
    p = TrialFunction(Q)
    v = TestFunction(V)
    q = TestFunction(Q)
    u_err = Function(V)

    # Initial velocity and pressure
    u_expr.t = 0.0
    f.t = 0.0
    u0 = project(u_expr, V)
    p0 = project(p_expr, Q)

    bcu = [DirichletBC(V, u_expr, "on_boundary")]
    bcp = [DirichletBC(Q, p_expr, "on_boundary")]


    # Functions
    u1 = Function(V)
    p1 = Function(Q)
    k = Constant(dt)

    # IPCS
    # # Tentative velocity step01
    # U = 0.5 * (u0 + u)
    # F1 = (1./k)*inner(u - u0, v)*dx + inner(grad(u0)*u0, v)*dx + \
    #     nu*inner(grad(u), grad(v))*dx + inner(grad(p0), v)*dx + \
    #     alpha*inner(u, v)*dx - inner(f, v)*dx - nu * inner(grad(U).T * n, v) * ds
    # a1 = lhs(F1)
    # L1 = rhs(F1)
    #
    # # Pressure update
    # a2 = inner(grad(p), grad(q))*dx
    # L2 = inner(grad(p0), grad(q))*dx - ((1./k) + alpha)*div(u1)*q*dx
    #
    # # Velocity update
    # a3 = (1./k + alpha)*inner(u, v)*dx
    # L3 = (1./k + alpha)*inner(u1, v)*dx - inner(grad(p1-p0), v)*dx


    # # CHORIN
    # Tentative velocity
    F1 = (1./k)*inner(u - u0, v)*dx + inner(dot(u0, nabla_grad(u0)), v)*dx \
        + nu*inner(grad(u), grad(v))*dx + alpha*inner(u, v)*dx \
        - inner(f, v)*dx
    a1 = lhs(F1)
    L1 = rhs(F1)

    # Pressure update

    a2 = inner(grad(p), grad(q))*dx
    L2 = -(1./k)*div(u1)*q*dx - alpha*div(u1)*q*dx

    # Velocity update
    a3 = (1./k)*inner(u, v)*dx + alpha*inner(u, v)*dx
    L3 = (1./k)*inner(u1, v)*dx - inner(grad(p1), v)*dx + alpha*inner(u1, v)*dx

    # Assemble system
    A1 = assemble(a1)
    A2 = assemble(a2)
    A3 = assemble(a3)

    b1 = None
    b2 = None
    b3 = None

    t = dt

    while t < T:

        # Tentative velocity
        f.t = t
        b1 = assemble(L1, tensor=b1)
        u_expr.t = t
        [bc.apply(A1, b1) for bc in bcu]
        solve(A1, u1.vector(), b1)

        # Pressure correction
        b2 = assemble(L2, tensor=b2)
        normalize(b2)
        solve(A2, p1.vector(), b2, "gmres", "hypre_amg")
        normalize(p1.vector())

        # Velocity update
        b3 = assemble(L3, tensor=b3)
        [bc.apply(A3, b3) for bc in bcu]
        solve(A3, u1.vector(), b3)

        u0.assign(u1)
        p0.assign(p1)

        t += dt

    t -= dt

    # # Exact solutions :
    u_expr.t = t
    u_exact = interpolate(u_expr, V)

    # u_err.vector()[:] = u0.vector().array() - u_exact.vector().array()
    err = errornorm(u0, u_exact, degree_rise=2)
    err_values.append(err)


print h1
print err_values

text_file = open("Output.txt", "w")

text_file.write(str(err_values) + "\n")
text_file.write(str(h1))

text_file.close()


# plt.loglog(h1, err_values, "b", marker='o', label='Error')
# plt.loglog(h1, h1, "r--", label="dt")
# # plt.loglog(h1, h2, "g--", label='dt^2')
# plt.legend()
# plt.title("Log-log plot of error in time. Flow with Dirichlet bcs, high alpha")
# plt.ylabel('Error')
# plt.xlabel('dt')
# plt.show()


