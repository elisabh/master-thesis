# Verify the value of the reduced functional by comparing it to the exact value, using assemble at time=T

from dolfin import *
from dolfin_adjoint import *


class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.0) and between(x[1], (0.3333, 0.6667))


def alpha(psi, name="alpha", q=0.1):
    return ALPHA_L + (ALPHA_U - ALPHA_L) * (1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q)))


def fluid_domain(psi, q=0.1):
    # return conditional(psi > 0, 0, 1)
    return 1.0 - 1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q))


set_log_level(40)

nu = Constant(0.01, name="nu")
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)
T = 0.01
PSI = "-1"
timestep = Constant(0.0001)
beta = 10.0

# Mesh
mesh = UnitSquareMesh(100, 100)

# Control space and function
V0 = FunctionSpace(mesh, "CG", 1)
psi = project(Expression(PSI, degree=2, t=0.0), V0, name="psi")

# Function spaces
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)

# Mark boundaries
noslip = Noslip()
inlet = Inlet()
outlet = Outlet()

boundaries = MeshFunction("size_t", mesh, 1)
boundaries.set_all(4)
noslip.mark(boundaries, 0)
inlet.mark(boundaries, 1)
outlet.mark(boundaries, 2)

ds = Measure("ds")(subdomain_data=boundaries)
n = FacetNormal(mesh)

# Boundary conditions, Pousille flow at inlet
t = 0.0
U = sin(3.1415/(2*T)*t)
u_1 = Expression(("U * (1 - (2*(x[1]-0.5)) * (2*(x[1]-0.5)) )", "0"),
                 U=U, degree=3)
bcu = [DirichletBC(V, (0.0, 0.0), boundaries, 0),
       DirichletBC(V, u_1, boundaries, 1)]
bcp = [DirichletBC(Q, 0.0, boundaries, 2)]


# Initial velocity and pressure
u_ic = project(Expression(("0.0", "0.0"), degree=3),  V, name="u_ic")
p_ic = project(Expression("0.0", degree=1), Q, name="p_ic")
u0 = u_ic.copy(deepcopy=True)
p0 = p_ic.copy(deepcopy=True)


# Declare velocity functions
u = Function(V, name="u", annotate=True)
v = TestFunction(V)

# Weak form for tentative velocity
F1 = (1./timestep) * inner(u - u0, v) * dx \
    + inner(grad(u) * u0, v) * dx \
    + nu * inner(grad(u), grad(v)) * dx \
    + alpha(psi) * inner(u, v) * dx \
    + inner(grad(p0), v) * dx \
    - nu * inner(dot(n, nabla_grad(u)), v) * ds(2) \
    + inner(p0 * n, v) * ds(2)


# Declare pressure functions (after tentative velocity weak form!!)
p = Function(Q, name="p", annotate=True)
q = TestFunction(Q)

# Pressure correction weak form
F2 = inner(grad(p - p0), grad(q)) * dx \
     + (1. / timestep) * div(u0) * q * dx


# Velocity update weak form
F3 = (1./timestep)*inner(u - u0, v) * dx \
     + inner(grad(p - p0), v) * dx


# solve(F1 == 0, u, bcu)
# u0.assign(u, annotate=True)
# solve(F2 == 0, p, bcp)
# solve(F3 == 0, u, bcu)
# u0.assign(u, annotate=True)

t = 0.0
psi_tmp = psi.copy(deepcopy=True)

u_list = []
adj_time = True
annotate = True
adj_start_timestep()

print "Solving Navier-Stokes-Darcy"
while t < T:

    adj_html("forward.html", "forward")
    adj_html("adjoint.html", "adjoint")

    # u and p work as trial functions
    # u0 and p0 work as newest value

    U = sin(3.1415 / (2 * T) * t)
    u_1 = Expression(("U * (1 - (2*(x[1]-0.5)) * (2*(x[1]-0.5)) )", "0"),
                     U=U, degree=3)
    bcu = [DirichletBC(V, (0.0, 0.0), boundaries, 0),
           DirichletBC(V, u_1, boundaries, 1)]

    solve(F1 == 0, u, bcu)
    u0.assign(u, annotate=annotate)
    solve(F2 == 0, p, bcp)
    solve(F3 == 0, u, bcu)

    psi.assign(psi_tmp, annotate=annotate)
    u0.assign(u, annotate=annotate)
    p0.assign(p, annotate=annotate)

    # plot(u0, interactive=True)

    u1 = Function(V, annotate=False)
    u1 = u0.copy(deepcopy=True)
    u_list.append(u1)

    t += float(timestep)
    if adj_time:
        adj_inc_timestep(t, t > T)

plot(u0, title="Velocity at end time")



# Dolfin-adjoint functional evaluated at end time
# J = Functional((alpha(psi)*inner(u0, u0) + nu*inner(grad(u0), grad(u0)))*dx*dt[FINISH_TIME]
#                + beta*fluid_domain(psi)*dx*dt[FINISH_TIME])
J = Functional((alpha(psi)*inner(u0, u0) + nu*inner(grad(u0), grad(u0)))*dx*dt
               + beta*fluid_domain(psi)*dx*dt)
m = Control(psi)

# Dolfin-adjoint gradient, check that it gets an expected value
g = compute_gradient(J, m, forget=False)
plot(-g, title="gradient")
plot(-sign(g), title="sign(gradient)")

# Reduced functional
Jm = ReducedFunctional(J, m)


# Exact functional evaluated at en time
# J_exact = assemble((alpha(psi)*inner(u0, u0) + nu*inner(grad(u0), grad(u0)))*dx
#                    + beta*fluid_domain(psi)*dx) # + beta*fluid_domain(psi)*dx)
J_exact = 0.0

for u0 in u_list:
    J_exact += float(timestep)*assemble((alpha(psi)*inner(u0, u0) + nu*inner(grad(u0), grad(u0)))*dx
                                        + beta * fluid_domain(psi) * dx)

print "J_dolfin(psi) = " + str(Jm(psi))
print "J_exact(psi)  = " + str(J_exact)

interactive()
