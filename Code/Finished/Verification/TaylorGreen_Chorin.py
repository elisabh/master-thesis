from dolfin import *
import numpy as np
import matplotlib.pyplot as plt


class PeriodicDomain(SubDomain):
    def inside(self, x, on_boundary):
        # return True if on left or bottom boundary AND NOT on one of the two corners (0, 2) and (2, 0)
        return bool((near(x[0], 0) or near(x[1], 0)) and
                    (not ((near(x[0], 0) and near(x[1], 2)) or
                          (near(x[0], 2) and near(x[1], 0)))) and on_boundary)

    def map(self, x, y):
        if near(x[0], 2) and near(x[1], 2):
            y[0] = x[0] - 2.0
            y[1] = x[1] - 2.0
        elif near(x[0], 2):
            y[0] = x[0] - 2.0
            y[1] = x[1]
        else:
            y[0] = x[0]
            y[1] = x[1] - 2.0

# Constants
dt = 0.001
T = 0.5
nu = 0.01
err_values = []
err_J = []
h = []
h2 = []

for N in [20, 40, 80, 160]:
    #, 40, 80, 160]:
    print N
    h.append(1./N)
    h2.append(1./N**2)

    mesh = RectangleMesh(Point(0, 0), Point(2, 2), N, N)
    n = FacetNormal(mesh)

    # Define function spaces (P2-P1)
    V = VectorFunctionSpace(mesh, "CG", 2, constrained_domain=PeriodicDomain())
    Q = FunctionSpace(mesh, "CG", 1, constrained_domain=PeriodicDomain())

    # Define trial and test functions
    u = TrialFunction(V)
    p = TrialFunction(Q)
    v = TestFunction(V)
    q = TestFunction(Q)

    # Initial velocity and pressure
    u_initial = Expression(("-sin(pi*x[1])*cos(pi*x[0])", "sin(pi*x[0])*cos(pi*x[1])"), degree=3)
    p_initial = Expression("-(cos(2*pi*x[0])+cos(2*pi*x[1]))/4.", degree=3)
    u0 = interpolate(u_initial, V)
    p0 = interpolate(p_initial, Q)

    # Functions
    u1 = Function(V)
    p1 = Function(Q)

    k = Constant(dt)

    # Tentative velocity step
    F1 = (1./k)*inner(u - u0, v)*dx + inner(grad(u0)*u0, v)*dx \
        + nu * inner(grad(u), grad(v)) * dx
    a1 = lhs(F1)
    L1 = rhs(F1)

    # Pressure update
    a2 = inner(grad(p), grad(q))*dx
    L2 = -(1./k)*div(u1)*q*dx

    # Velocity update
    a3 = inner(u, v)*dx
    L3 = inner(u1, v)*dx - k*inner(grad(p1), v)*dx


    # Assemble system
    A1 = assemble(a1)
    A2 = assemble(a2)
    A3 = assemble(a3)
    b1 = None; b2 = None; b3 = None

    t = dt


    while t < T:

        # Tentative velocity
        # A1 = assemble(a1) + A1_1 + A1_2

        b1 = assemble(L1)
        solve(A1, u1.vector(), b1, "minres", "sor")

        # Pressure correction
        b2 = assemble(L2)
        normalize(b2)
        solve(A2, p1.vector(), b2, "gmres", "hypre_amg")
        normalize(p1.vector())

        # Velocity update
        b3 = assemble(L3)
        solve(A3, u1.vector(), b3, "minres", "sor")
        # plot(u1, title="u1")

        u0.assign(u1)
        p0.assign(p1)
        t += dt

    t -= dt


    # Exact solutions :
    u_expr = Expression(("-sin(pi*x[1])*cos(pi*x[0])*exp(-2.*pi*pi*nu*t)",
                          "sin(pi*x[0])*cos(pi*x[1])*exp(-2.*pi*pi*nu*t)"), nu=nu, t=t, degree=3)
    u_exact = interpolate(u_expr, V)

    p_expr = Expression("-(cos(2*pi*x[0])+cos(2*pi*x[1]))/4.*exp(-4.*pi*pi*nu*t)", nu=nu, t=t, degree=3)
    p_exact = interpolate(p_expr, Q)


    # J_exact = 0.5 * norm(u_exact, mesh=mesh) ** 2
    # J = 0.5 * norm(u0, mesh=mesh) ** 2
    # err_J.append(abs(J_exact-J))

    u_err = Function(V)
    u_err.vector()[:] = u0.vector().array() - u_exact.vector().array()

    p_err = Function(Q)
    p_err.vector()[:] = p0.vector().array() - p_exact.vector().array()

    err = sqrt(assemble(inner(p_err, p_err)*dx))
    err_values.append(err)


    # plot(uu, title="error")
    # plot(u1, title="u1")
    # plot(u_err, title="error")
    # plot(u_exact, title="u_exact")

    # maxdiff = numpy.abs(u0.vector().array() - u_exact.vector().array()).max()
    # maxdiff = numpy.abs(u_err.vector().array().max())
    plot(p0, title="Pressure")



print err_values
plt.loglog(h, err_values, h, h, h, h2)
plt.title("L2-norm p")
plt.show()
# plt.loglog(h, err_J, h, h, h, h2)
# plt.title("J error")
# plt.show()
interactive()




