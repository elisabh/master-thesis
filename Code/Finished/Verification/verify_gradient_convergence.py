# Diffuser problem, try to make smooth gradient with object oriented coding

from dolfin import *
from dolfin_adjoint import *
import numpy as np
from matplotlib import pyplot as plt


class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 1.0) and between(x[1], (0.3333, 0.6667))


def alpha(psi, name="alpha", q=0.1):
    return ALPHA_L + (ALPHA_U - ALPHA_L) * (1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q)))


def fluid_domain(psi, q=0.1):
    return 1.0 - 1.0 / (1.0 + 2.718281828459045 ** ((-psi) / q))


set_log_level(40)

nu = Constant(0.01, name="nu")
ALPHA_U = 2.5 * nu / (0.01 * 0.01)
ALPHA_L = 2.5 * nu / (100 * 100)
beta = 90.0

class Problem:
    def __init__(self):
        self.mesh = None
        self.boundaries = None
        self.adjoint_boundaries = None

        self.make_mesh()
        self.mark_boundaries()
        self.mark_adjoint_boundaries()
        self.T = 0.15
        self.timestep = Constant(0.01)

    def make_mesh(self):
        self.mesh = mesh_unit

    def mark_boundaries(self):
        noslip = Noslip()
        inlet = Inlet()
        outlet = Outlet()

        self.boundaries = MeshFunction("size_t", self.mesh, 1)
        self.boundaries.set_all(4)
        noslip.mark(self.boundaries, 0)
        inlet.mark(self.boundaries, 1)
        outlet.mark(self.boundaries, 2)

    def mark_adjoint_boundaries(self):

        noslip = Noslip()
        outlet = Outlet()

        self.adjoint_boundaries = MeshFunction("size_t", self.mesh, 1)
        self.adjoint_boundaries.set_all(4)
        noslip.mark(self.adjoint_boundaries, 0)
        outlet.mark(self.adjoint_boundaries, 3)

    def boundary_conditions(self, V, Q, t):
        U = sin(3.1415 / (2 * self.T) * t)
        u_1 = Expression(("U * (1 - (2*(x[1]-0.5)) * (2*(x[1]-0.5)) )", "0"),
                         U=U, degree=3)

        bcu = [DirichletBC(V, (0.0, 0.0), self.boundaries, 0),
               DirichletBC(V, u_1, self.boundaries, 1)]
        bcp = [DirichletBC(Q, 0.0, self.boundaries, 2)]

        return bcu, bcp

    def adjoint_boundary_conditions(self, V):
        return [DirichletBC(V, (0.0, 0.0), self.adjoint_boundaries, 0)]

    def initial_values(self):
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        u_ic = project(Expression(("0.0", "0.0"), degree=3),  V, name="u_ic")
        p_ic = project(Expression("0.0", degree=1), Q, name="p_ic")
        u0 = u_ic.copy(deepcopy=True)
        p0 = p_ic.copy(deepcopy=True)
        return u0, p0


class NSDsolver:

    def __init__(self, problem):
        self.problem = problem
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries

    def solve(self, psi):

        print "Solving Navier-Stokes-Darcy"

        adj_reset()
        timestep = problem.timestep

        # Function spaces
        V = VectorFunctionSpace(self.mesh, "CG", 2)
        Q = FunctionSpace(self.mesh, "CG", 1)

        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)

        # Initial velocity and pressure
        u0, p0 = problem.initial_values()

        # Declare velocity functions
        u = Function(V, name="u", annotate=True)
        v = TestFunction(V)

        # Weak form for tentative velocity
        F1 = (1./timestep) * inner(u - u0, v) * dx \
            + inner(grad(u) * u0, v) * dx \
            + nu * inner(grad(u), grad(v)) * dx \
            + alpha(psi) * inner(u, v) * dx \
            + inner(grad(p0), v) * dx \
            - nu * inner(dot(n, nabla_grad(u)), v) * ds(2) \
            + inner(p0 * n, v) * ds(2)


        # Declare pressure functions (after tentative velocity weak form!!)
        p = Function(Q, name="p", annotate=True)
        q = TestFunction(Q)

        # Pressure correction weak form
        F2 = inner(grad(p - p0), grad(q)) * dx \
             + (1. / timestep) * div(u0) * q * dx


        # Velocity update weak form
        F3 = (1./timestep)*inner(u - u0, v) * dx \
             + inner(grad(p - p0), v) * dx

        t = 0.0
        psi_tmp = psi.copy(deepcopy=True)
        u_list = []

        adj_time = True
        annotate = True
        adj_start_timestep()

        while t < problem.T:

            # u and p work as trial functions
            # u0 and p0 work as newest value

            # Boundary conditions, Pousille flow at inlet
            bcu, bcp = problem.boundary_conditions(V, Q, t)

            solve(F1 == 0, u, bcu)
            u0.assign(u, annotate=annotate)
            solve(F2 == 0, p, bcp)
            solve(F3 == 0, u, bcu)

            psi.assign(psi_tmp, annotate=annotate)
            u0.assign(u, annotate=annotate)
            p0.assign(p, annotate=annotate)

            u1 = Function(V, annotate=False)
            u1 = u0.copy(deepcopy=True)
            u_list.append(u1)

            t += float(timestep)
            if adj_time:
                adj_inc_timestep(t, t > problem.T)

        return u0, u_list


class AdjointSolver:

    def __init__(self, problem):
        self.mesh = problem.mesh
        self.boundaries = problem.adjoint_boundaries
        self.T = problem.T

    def solve(self, psi, u_list):

        timestep = problem.timestep
        # Adjoint coupled:
        V_ = VectorFunctionSpace(self.mesh, "CG", 2)

        P2 = VectorElement('P', 'triangle', 2)
        P1 = FiniteElement('P', 'triangle', 1)
        TH = P2 * P1
        W = FunctionSpace(self.mesh, TH)

        # U_h = VectorElement("CG", self.mesh.ufl_cell(), 2)
        # P_h = FiniteElement("CG", self.mesh.ufl_cell(), 1)
        # W = FunctionSpace(self.mesh, U_h * P_h)


        ds = Measure("ds")(subdomain_data=self.boundaries)
        n = FacetNormal(self.mesh)
        # u0 = Function(V_)

        (u, p) = TrialFunctions(W)
        (v, q) = TestFunctions(W)
        w = Function(W)
        (ua, pa) = split(w)

        assigner = FunctionAssigner(V_, W.sub(0)) # to assign w.sub(0) to u_a
        ua_list = []

        bcu = problem.adjoint_boundary_conditions(W.sub(0))

        t = self.T

        for u0 in reversed(u_list):
            t -= float(timestep)

            # Not sure if this explicitly is necessary
            F_a = (1. / timestep) * inner(u - ua, v) * dx \
                  - inner(dot(grad(u), u0), v) * dx \
                  - inner(dot(u0, grad(u)), v) * dx \
                  + nu * inner(grad(u), grad(v)) * dx \
                  + alpha(psi) * inner(u, v) * dx \
                  - div(u) * q * dx - div(v) * p * dx \
                  - 2 * nu * inner(grad(u0), grad(v)) * dx \
                  - 2 * alpha(psi) * inner(u0, v) * dx \
                  - inner(p * n, v) * ds(3) \
                  + nu * inner(dot(n, grad(u)), v) * ds(3) \
                  + inner(dot(u0, u) * n, v) * ds(3) \
                  + inner(dot(u0, n) * u, v) * ds(3)

            a = lhs(F_a)
            L = rhs(F_a)

            adj_problem = LinearVariationalProblem(a, L, w, bcu)
            solver = LinearVariationalSolver(adj_problem)
            solver.solve()
            (ua, pa) = split(w)

            u_a = Function(V_)  # to solve the adjoints
            assigner.assign(u_a, w.sub(0))
            ua_list.append(u_a)

        # return reversed ua_list, such that it has the same ordering as u_list
        return ua_list[::-1]


class GradientMethod:
    def __init__(self, problem):
        self.problem = problem
        self.mesh = problem.mesh
        self.boundaries = problem.boundaries
        self.V0 = FunctionSpace(self.mesh, "CG", 1)
        self.PSI = "-1"


    def run_gradient_method(self):

        ds = Measure("ds")(subdomain_data=self.boundaries)
        timestep = problem.timestep

        # Design parameter
        psi = project(Expression(self.PSI, degree=2), self.V0, annotate=True)

        # g_file = File("g1.pvd")

        # Initialize problem and Navier-Stokes-Darcy solver
        nsd = NSDsolver(self.problem)
        u0, u_list = nsd.solve(psi)

        # Dolfin-adjoint functional
        J = Functional((alpha(psi)*inner(u0, u0) + nu*inner(grad(u0), grad(u0)))*dx*dt
                       + beta * fluid_domain(psi) * dx * dt)

        print "Computing dolfin-adjoint gradient"
        g1 = compute_gradient(J, Control(psi), forget=False)
        nrm_g1 = sqrt(assemble(dot(g1, g1) * dx))
        g1.assign(-1.0/nrm_g1*g1)
        # plot(g1, title="Dolfin-adjoint gradient")
        # g_file << g1


        print "Solving adjoint system"
        adj = AdjointSolver(self.problem)
        ua_list = adj.solve(psi, u_list)
        g2 = Function(self.V0)

        print "Computing topological gradient"
        for i in range(len(u_list)):
            u = u_list[i]
            ua = ua_list[i]
            g_temp = project(- (ALPHA_U-ALPHA_L)*inner(u, u - ua) + beta, self.V0)
            g2.assign(g2 + float(timestep)*g_temp)

        nrm_g2 = sqrt(assemble(dot(g2, g2) * dx))
        g2.assign(1.0/nrm_g2*g2)

        # plot(g2, title="Topological gradient")

        # g_file << g2

        nrm_diff = errornorm(g1, g2)
        print nrm_diff

        g_diff = project(1.0*g2 - 1.0*g1, self.V0)
        # g_file << g_diff

        # plot(g2-g1, title="Difference between gradients")
        return nrm_diff



diff_list = []
h = []
h_sqrt = []
n_list = [16, 32, 64, 128]
for nx in n_list:
    print "n = " + str(nx)
    h.append(1./nx)
    h_sqrt.append(1./np.sqrt(nx))
    mesh_unit = UnitSquareMesh(nx, nx)

    problem = Problem()
    method = GradientMethod(problem)
    diff_list.append(method.run_gradient_method())


print diff_list
plt.loglog(h, diff_list, "b", marker='o', label='Difference')
plt.loglog(h, h, "r", label='order 1')
plt.loglog(h, h_sqrt, "g", label='order 1/2')
plt.legend()
plt.show()

# plt.plot(n_list, diff_list, "b", marker='o', label='Difference')
# plt.legend()
# plt.show()
