# Stokes bypass topology optimization, not object oriented

from dolfin import *
from dolfin_adjoint import *
import numpy
import time
import math


# All boundary for noslip, overloaded by inlet and outlet
class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary


# Boundaries for Bypass
class Inlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0, 0.2)) and near(x[0], 0)


class Outlet(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and between(x[1], (0, 0.2)) and near(x[0], 2.0)


class Vein(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[1], (0.0, 0.2))


# Velocity for inlet and outlet (Poiseuille flow)
class Poiseuille(Expression):
    def __init__(self, l, center, u_x, u_y):
        self.l = l
        self.dt = center
        self.u_x = u_x
        self.u_y = u_y

    def eval(self, value, x):
        t_y = x[1] - self.dt
        t_x = x[0] - self.dt
        value[0] = self.u_x * (1 - (2 * t_y / self.l) * (2 * t_y / self.l))
        value[1] = self.u_y * (1 - (2 * t_x / self.l) * (2 * t_x / self.l))

    def value_shape(self):
        return (2,)


# Expressions:

def diffusion(u, v, mu):
    return mu * inner(grad(u), grad(v))


def friction(u, v, alpha):
    return alpha * inner(u, v)


def incompressibility(u, q):
    return - div(u) * q


def a_stokes(u, p, v, q, mu, alpha):
    return diffusion(u, v, mu) + friction(u, v, alpha) + \
           + incompressibility(u, q) + incompressibility(v, p)


def conv_comb(psi, g, k1, k2):
    psi_ref = Function(V0)
    psi_ref.assign(psi)
    psi.vector().zero()
    psi.vector().axpy(k1, psi_ref.vector())
    psi.vector().axpy(k2, g.vector())


def alpha(psi):
    return ALPHA_U - (ALPHA_U - ALPHA_L) * conditional(gt(psi, 0), 0, 1)


def fluid(psi):
    return conditional(gt(psi, 0), 0, 1)


mesh_name = "../mesh/bypass4.xml"

# Global variables (constants)
f = Constant((0.0, 0.0))
MU = Constant(0.001)
ALPHA_U = 2.5 * MU / (0.01 * 0.01)
ALPHA_L = 2.5 * MU / (100 * 100)
BETA = 0.3
# PSI = "1 - 2*(x[1] < 0.5)"
PSI = "-1"
tol_theta = 0.05
tol_kappa = 0.4
tol_F = 0.00001

# Mesh -------------------------------------
mesh = Mesh(mesh_name)
mesh = refine(mesh)
# mesh = refine(mesh)

# Function Spaces
P2 = VectorElement('P', 'triangle', 2)
P1 = FiniteElement('P', 'triangle', 1)
TH = P2 * P1
W = FunctionSpace(mesh, TH)

V0 = FunctionSpace(mesh, "DG", 0)

(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)

w = Function(W, name="w")
(u_, p_) = split(w)

# Boundaries -------------------------------
boundaries = MeshFunction("size_t", mesh, 1)
boundaries.set_all(3)

noslip = Noslip()
noslip.mark(boundaries, 0)
inlet = Inlet()
inlet.mark(boundaries, 1)
outlet = Outlet()
outlet.mark(boundaries, 2)

u_noslip = Constant((0.0, 0.0))
l = 0.2
center = 0.1
U = 1.0

u0 = Poiseuille(l, center, U, 0)
p0 = Constant(0.0)

bcs = [DirichletBC(W.sub(0), u_noslip, boundaries, 0),
       DirichletBC(W.sub(0), u0, boundaries, 1),
       DirichletBC(W.sub(1), p0, boundaries, 2)]

# Subdomain ---------------------------------
domains = CellFunction("size_t", mesh)
domains.set_all(1)

vein = Vein()
vein.mark(domains, 0)
dx = Measure("dx")(subdomain_data=domains)

# ---------------------------------------------------------------------

# Level-set function on whole domain
# Norm on Omega_1 is 1, so it shouldn't matter what happens in vein-part
#

# psi = project(Expression(PSI, degree=1), V0)
psi = interpolate(Constant(-1.0), V0, name="psi")
fluid_domain = conditional(gt(psi, 0), 0, 1)

# Variational form and solver
a = a_stokes(u, p, v, q, MU, alpha(psi)) * dx('everywhere')
L = inner(f, v) * dx('everywhere')
problem = LinearVariationalProblem(a, L, w, bcs)
solver = LinearVariationalSolver(problem)

# Solve once to record with dolfin-adjoint
solver.solve()

#
J = Functional(0.5 * inner(alpha(psi) * u_, u_) * dx('everywhere')
               + MU * inner(grad(u_), grad(u_)) * dx('everywhere'))

#m = Control(psi)
#Jhat = ReducedFunctional(J, m)
#Jhat.derivative(forget=False)
adj_html("forward.html", "forward")

for (adjoint, var) in compute_adjoint(J, forget=False):
    print var
    from IPython import embed; embed()
    exit()


# lmbd = compute_adjoint(J)
# dJdm = compute_gradient(J, m)
# H = compute_hessian(J, m)
# rf = ReducedFunctional()
# rf.derivative()
# minimize(rf)


a = a_stokes(u, p, v, q, MU, alpha(psi)) * dx('everywhere')
L = inner(f, v) * dx('everywhere')
problem = LinearVariationalProblem(a, L, w, bcs)
solver = LinearVariationalSolver(problem)
solver.solve()
plot(u_, title="Velocity")

interactive()

# Gradient-based method ############################

# kappa = 1.0
# theta = 10
#
# # Solve Stokes to obtain u:
#
# F_ex = 0.5 * (MU * inner(grad(u_), grad(u_)) + alpha(psi) * inner(u_, u_)) * dx('everywhere') \
#        + (BETA * fluid_domain) * dx(1)
# F = assemble(F_ex)
#
# n = 0
# n_max = 40
#
# while theta > tol_theta and n < n_max:
#
#     n = n + 1
#     print "\nIteration #" + str(n)
#
#     kappa = min(1.0, kappa * 1.5)
#
#     # Calculate theta - evaluate on Omega_1:
#     k = - (ALPHA_U - ALPHA_L) * inner(u_, u_) + BETA
#     g = project(k, V0)
#     dot_prod = assemble(dot(g, psi) * dx(1))
#     nrm_g = sqrt(assemble(dot(g, g) * dx(1)))
#     nrm_psi = sqrt(assemble(dot(psi, psi) * dx(1)))
#
#     theta_inv = min(1.0, dot_prod / (nrm_g * nrm_psi))
#     if theta_inv is 1.0:
#         theta = 0.0
#         print "Theta = " + str(theta)
#         plot(sign(psi), key='psi')
#         break
#
#     theta = math.acos(theta_inv)
#     print "Theta = " + str(theta)
#
#     # Update psi:
#     k1 = sin((1 - kappa) * theta) / (sin(theta) * nrm_psi)
#     k2 = sin(kappa * theta) / (sin(theta) * nrm_g)
#     conv_comb(psi, g, k1, k2)
#
#     # Reset the vein part to be only fluid:
#     psi = project(Expression("psi*(x[1]>=0.2) - 1*(x[1] < 0.2)", psi=psi, degree=1), V0)
#
#     # Solve Stokes for new psi:
#     print "F = " + str(F)
#
#     fluid_domain = conditional(psi > 0, 0, 1)
#     a = a_stokes(u, p, v, q, MU, alpha(psi)) * dx('everywhere')
#     problem = LinearVariationalProblem(a, L, w, bcs)
#     solver = LinearVariationalSolver(problem)
#     solver.solve()
#
#     F_ex = 0.5 * (MU * inner(grad(u_), grad(u_)) + alpha(psi) * inner(u_, u_)) * dx('everywhere') + \
#            (BETA * fluid_domain) * dx(1)
#     F_new = assemble(F_ex)
#
#     if F_new >= F:
#         print "Did not find descent direction, aborting."
#         break
#
#     F = F_new
#     plot(sign(psi), key='psi')
#     plot(psi, key='psi2')
#
# J_ex = 0.5 * (MU * inner(grad(u_), grad(u_)) + alpha(psi) * inner(u_, u_)) * dx('everywhere')
# print "J = " + str(assemble(J_ex))
# plot(u_)
# interactive()
