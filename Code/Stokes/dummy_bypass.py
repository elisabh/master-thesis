from dolfin import *

class Vein(SubDomain):
    def inside(self, x, on_boundary):
        return between(x[1], (0.0, 0.2))

mesh = UnitSquareMesh(16, 16)
V0 = FunctionSpace(mesh, "DG", 0)
    
vein = Vein()

sub_domains = CellFunction("size_t", mesh)
sub_domains.set_all(0)
vein.mark(sub_domains, 1)
#plot(sub_domains)

dg = Measure("dx")(subdomain_data=sub_domains)

submesh = SubMesh(mesh, sub_domains, 0)
V1 = FunctionSpace(submesh, "DG", 0)

psi = project(Expression("1", degree=2), V0)

psi1 = project(psi, V1)
#plot(psi1)

g = inner(psi, psi)*dg('everywhere')
G = assemble(g)

print G



# Function Spaces
V = VectorFunctionSpace(mesh, "CG", 2)
Q = FunctionSpace(mesh, "CG", 1)
print "mixed"
W = FunctionSpace(mesh, )
print "functionspace"
V0 = FunctionSpace(mesh, "DG", 0)


(u, p) = TrialFunctions(W)
(v, q) = TestFunctions(W)

w = Function(W)
(u_, p_) = w.split()

#psi = project(Expression("psi*(x[1]>0.2) - 1*(x[1] < 0.2)", psi = psi), V0)


#plot(psi)
#interactive()

